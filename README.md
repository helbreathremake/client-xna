# Visual studio 2019 support
https://flatredball.com/visual-studio-2019-xna-setup/

When you run into missing Microsoft.Build.Framework.dll issue, do following:
* Run VS as admin
* Open terminal (View > Terminal)
* Type: cd "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin" or wherever you have VS installed
* Type: gacutil /i Microsoft.Build.Framework.dll

# Installing MagicMedieval font
* Right click on MagicMedieval.ttf and select Install for all users
* Restart Visual Studio