﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.Effects
{
    public interface IGameEffect
    {
        Location Pivot { get; set; }
        bool Draw(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        bool DrawLights(SpriteBatch spriteBatch, GameDisplay display, int x, int y, int pivotX, int pivotY);
        void Update();
        bool IsComplete { get; }
    }
}
