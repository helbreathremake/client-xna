﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Game.Assets
{
    public class Guild
    {
        private string name;

        public Guild(string name)
        {
            this.name = name;
        }

        public string Name { get { return name; } }
    }
}
