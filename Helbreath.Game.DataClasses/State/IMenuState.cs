﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Game;
using Helbreath.Game.Assets.UI;
using Helbreath.Game.Assets.Effects;

namespace Helbreath.Game.Assets.State
{
    public interface IMenuState
    {
        Dictionary<MenuDialogBoxType, IMenuDialogBox> DialogBoxes { get; }
        LinkedList<MenuDialogBoxType> DialogBoxDrawOrder { get; set; }
        void BringToFront(MenuDialogBoxType type);
        void HideMessageBox(MenuDialogBoxType type);
        MenuDialogBoxType ClickedDialogBox { get; set; }
        void ShowMessageBox(MenuMessageBox box);
    }
}
