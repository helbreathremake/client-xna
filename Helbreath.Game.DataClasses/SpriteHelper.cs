﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Helbreath;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;
using System.Diagnostics;
using log4net;

namespace Helbreath.Game.Assets
{
    public static class SpriteHelper
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static object LoadingLock = new object();
        public static Dictionary<SpriteEffectType, Microsoft.Xna.Framework.Graphics.Effect> SpriteEffects = new Dictionary<SpriteEffectType, Microsoft.Xna.Framework.Graphics.Effect>();
        public static Dictionary<int, Dictionary<int, int>> ItemSpriteAssociations = new Dictionary<int, Dictionary<int, int>>();
        private static List<string> FailedSpriteLoads = new List<string>();

        public static Item ItemFromSprite(int sprite, int spriteFrame)
        {
            if (ItemSpriteAssociations.ContainsKey(sprite))
            {
                if (ItemSpriteAssociations[sprite].ContainsKey(spriteFrame))
                    return Cache.ItemConfiguration[ItemSpriteAssociations[sprite][spriteFrame]];
            }

            // not found, find then add to cache
            foreach (Item i in Cache.ItemConfiguration.Values)
                if (i.Sprite == sprite && i.SpriteFrame == spriteFrame)
                {
                    if (!ItemSpriteAssociations.ContainsKey(sprite))
                        ItemSpriteAssociations.Add(sprite, new Dictionary<int, int>());

                    if (!ItemSpriteAssociations[sprite].ContainsKey(spriteFrame))
                        ItemSpriteAssociations[sprite].Add(spriteFrame, i.ItemId);

                    return Cache.ItemConfiguration[i.ItemId];
                }

            return null;
        }

        public static void DrawBodyShadow(SpriteBatch spriteBatch, int type, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, Color colour, float fade, out bool isSelected)
        {
            isSelected = false;

            AnimationFrame frame = null;

            if (type <= 6)
            {
                if (SpriteExists(Cache.HumanAnimations, Animation, AnimationFrame))
                    frame = Cache.HumanAnimations[Animation].Frames[AnimationFrame];
            }
            else
            {
                if (SpriteExists(Cache.MonsterAnimations, Animation, AnimationFrame))
                    frame = Cache.MonsterAnimations[Animation].Frames[AnimationFrame];
            }

            if (frame != null)
            {
                Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);

                if (type <= 6)
                    spriteBatch.Draw(Cache.HumanAnimations[Animation].Texture, new Vector2(pos.X + (frame.Width / 2), pos.Y + frame.Height), frame.GetRectangle(), colour * 0.7f, -0.8f, new Vector2(frame.Width / 2, frame.Height), 0.6f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 0.5f);
                else
                    spriteBatch.Draw(Cache.MonsterAnimations[Animation].Texture, new Vector2(pos.X + (frame.Width / 2), pos.Y + frame.Height), frame.GetRectangle(), colour * 0.7f, -0.8f, new Vector2(frame.Width / 2, frame.Height), 0.6f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 0.5f);

                // doesnt use the SpriteHelper.IsSelected transparency version, as it would be harder to highlight players
                //isSelected = Utility.IsSelected((int)Cache.GameState.Display.Mouse.X, (int)Cache.GameState.Display.Mouse.Y, x + frame.PivotX + objectOffsetX, y + frame.PivotY + objectOffsetY, x + frame.PivotX + objectOffsetX + frame.Width, y + frame.PivotY + objectOffsetY + frame.Height);
            }
            else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
        }

        public static void DrawBody(SpriteBatch spriteBatch, int type, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, Color colour, float fade, out bool isSelected)
        {
            isSelected = false;

            AnimationFrame frame = null;

            if (type <= 6)
            {
                if (SpriteExists(Cache.HumanAnimations, Animation, AnimationFrame))
                    frame = Cache.HumanAnimations[Animation].Frames[AnimationFrame];
            }
            else
            {
                if (SpriteExists(Cache.MonsterAnimations, Animation, AnimationFrame))
                    frame = Cache.MonsterAnimations[Animation].Frames[AnimationFrame];
            }

            if (frame != null)
            {
                Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);

                if (type <= 6)
                    spriteBatch.Draw(Cache.HumanAnimations[Animation].Texture, pos, frame.GetRectangle(), colour * fade);
                else
                    spriteBatch.Draw(Cache.MonsterAnimations[Animation].Texture, pos, frame.GetRectangle(), colour * fade);

                // doesnt use the SpriteHelper.IsSelected transparency version, as it would be harder to highlight players
                isSelected = Utility.IsSelected((int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY, x + frame.PivotX + objectOffsetX + offsetX + frame.Width, y + frame.PivotY + objectOffsetY + offsetY + frame.Height, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);

            }
            else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
        }

        public static void DrawMonster(SpriteBatch spriteBatch, int npcId, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, Color colour, float fade, out bool isSelected)
        {
            isSelected = false;

            if (!Cache.NpcConfiguration.ContainsKey(npcId)) return;

            if (SpriteExists(Cache.MonsterAnimations, Animation, AnimationFrame))
            {
                if (Cache.MonsterAnimations[Animation].LoadState == AnimationLoadState.Loaded)
                {
                    AnimationFrame frame = Cache.MonsterAnimations[Animation].Frames[AnimationFrame];
                    Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);
                    spriteBatch.Draw(Cache.MonsterAnimations[Animation].Texture, pos, frame.GetRectangle(), colour * fade);

                    // doesnt use the SpriteHelper.IsSelected transparency version, as it would be harder to highlight players
                    isSelected = Utility.IsSelected((int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY, x + frame.PivotX + objectOffsetX + offsetX + frame.Width, y + frame.PivotY + objectOffsetY + offsetY + frame.Height, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
                } // new code - load on-deman
            }
            else
            {
                QueueSpriteLoad(new SpriteLoad(SpriteType.Monster, "Sprites\\" + Cache.NpcConfiguration[npcId].Sprite, (npcId * 100), 56, 0));
            }
        }

        public static void DrawMonsterShadow(SpriteBatch spriteBatch, int npcId, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, Color colour, float fade, out bool isSelected)
        {
            isSelected = false;

            if (!Cache.NpcConfiguration.ContainsKey(npcId)) return;

            if (SpriteExists(Cache.MonsterAnimations, Animation, AnimationFrame))
            {
                if (Cache.MonsterAnimations[Animation].LoadState == AnimationLoadState.Loaded)
                {
                    AnimationFrame frame = Cache.MonsterAnimations[Animation].Frames[AnimationFrame];
                    Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);

                    float shadowRotation = -.8f;
                    switch (Cache.NpcConfiguration[npcId].Type)
                    {
                        case 1: // slime
                        case 55: // rabit
                        case 56: // cat
                        case 16: // ant
                        case 22: // snek
                        case 17: // scorp
                            shadowRotation = 0f; // short mobs look bad with rotated shadows
                            break;
                    }


                    spriteBatch.Draw(Cache.MonsterAnimations[Animation].Texture, new Vector2(pos.X + (frame.Width / 2), pos.Y + frame.Height), frame.GetRectangle(), colour * 0.7f, shadowRotation, new Vector2(frame.Width / 2, frame.Height), 0.6f, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 0.5f);
                }
            }
            else QueueSpriteLoad(new SpriteLoad(SpriteType.Monster, "Sprites\\" + Cache.NpcConfiguration[npcId].Sprite, (npcId * 100), 56, 0));
        }

        public static bool SpriteExists(Dictionary<int, Animation> spriteSet, int animation, int animationFrame)
        {
            if (spriteSet.ContainsKey(animation) && animationFrame >= 0 &&
                spriteSet[animation].Frames.Count > animationFrame &&
                spriteSet[animation].Frames[animationFrame] != null)
                return true;
            else return false;
        }

        public static void DrawEffect(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int offsetX, int offsetY, Color colour)
        {
            AnimationFrame frame = null;

            if (SpriteExists(Cache.Effects, Animation, AnimationFrame))
                frame = Cache.Effects[Animation].Frames[AnimationFrame];

            if (frame != null)
            {
                Vector2 pos = new Vector2(x + frame.PivotX + offsetX, y + frame.PivotY + offsetY);
                spriteBatch.Draw(Cache.Effects[Animation].Texture, pos, frame.GetRectangle(), colour);
            }
            //else DrawRedBox(spriteBatch, x, y, offsetX, offsetY);
            // dont redbox missing effects. looks shit
        }

        public static void DrawDynamicObject(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int offsetX, int offsetY, Color colour, out bool isSelected)
        {
            AnimationFrame frame = null;

            if (SpriteExists(Cache.Effects, Animation, AnimationFrame))
                frame = Cache.Effects[Animation].Frames[AnimationFrame];

            if (frame != null)
            {
                Vector2 pos = new Vector2(x + frame.PivotX + offsetX, y + frame.PivotY + offsetY);
                spriteBatch.Draw(Cache.Effects[Animation].Texture, pos, frame.GetRectangle(), colour);

                isSelected = IsSelected(Cache.Effects[Animation], Cache.Effects[Animation].Frames[AnimationFrame],
                                        (int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + frame.PivotX + offsetX,
                                        y + frame.PivotY + offsetY, x + frame.PivotX + offsetX + frame.Width,
                                        y + frame.PivotY + offsetY + frame.Height, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
            }
            else DrawRedBox(spriteBatch, x, y, offsetX, offsetY, out isSelected, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
        }

        public static void DrawAngel(SpriteBatch spriteBatch, MotionType motion, MotionDirection direction, int type, int currentFrame, int x, int y, int offsetX, int offsetY, out bool isSelected, string spriteFile = "")
        {
            AnimationFrame frame = null;
            int Animation = (int)SpriteId.Angels + (50 * type);
            int AnimationFrame = 0;

            switch (motion)
            {
                case MotionType.Attack:
                case MotionType.BowAttack:
                case MotionType.Bow:
                case MotionType.Dash:
                case MotionType.AttackStationary:
                    Animation += ((int)direction) - 1;
                    AnimationFrame = currentFrame % 8;
                    break;
                case MotionType.TakeDamage:
                case MotionType.Fly:
                    Animation += 16 + (((int)direction) - 1);
                    AnimationFrame = currentFrame % 4;
                    break;
                case MotionType.Die:
                    Animation += 24 + (((int)direction) - 1);
                    AnimationFrame = currentFrame;
                    break;
                case MotionType.Magic:
                    Animation += 32 + (((int)direction) - 1);
                    AnimationFrame = currentFrame;
                    break;
                case MotionType.PickUp:
                case MotionType.Move:
                case MotionType.Idle:
                case MotionType.Run:
                    Animation += 40 + (((int)direction) - 1);
                    AnimationFrame = currentFrame % 4;
                    break;
            }

            if (SpriteExists(Cache.Equipment, Animation, AnimationFrame))
            {
                frame = Cache.Equipment[Animation].Frames[AnimationFrame];

                if (frame != null)
                {
                    DrawEquipment(spriteBatch, Animation, AnimationFrame, x + 20, y - 20, offsetX, offsetY, 0, 0, Cache.Colors[GameColor.Normal], 1.0f);

                    isSelected = IsSelected(Cache.Equipment[Animation], Cache.Equipment[Animation].Frames[AnimationFrame],
                                            (int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + 20 + frame.PivotX + offsetX,
                                            y - 20 + frame.PivotY + offsetY, x + 20 + frame.PivotX + offsetX + frame.Width,
                                            y - 20 + frame.PivotY + offsetY + frame.Height, Cache.DefaultState.Display.GameBoardShiftX, Cache.DefaultState.Display.GameBoardShiftY);
                }
                else DrawRedBox(spriteBatch, x, y, offsetX, offsetY, out isSelected);
            }
            else
            {
                if (!string.IsNullOrEmpty(spriteFile) && Cache.SpriteConfiguration.ContainsKey(spriteFile))
                {
                    SpriteConfig config = Cache.SpriteConfiguration[spriteFile];
                    QueueSpriteLoad(new SpriteLoad(config.Type, "Sprites\\" + config.Name, (int)config.Id + (config.Range * config.Appearance), config.Count, config.StartIndex));
                }

                isSelected = false;
            }
        }

        /// <summary>
        /// This version of IsSelected checks the same as Utility.IsSelected, but also ignores transparent areas of a sprite
        /// </summary>
        /// <param name="animation">The animation the frame belongs to</param>
        /// <param name="frame">The frame to check for transparency</param>
        /// <param name="mouseX">Current Mouse X</param>
        /// <param name="mouseY">Current Mouse Y</param>
        /// <param name="left">Left X Coordinate of the frame</param>
        /// <param name="top">Top Y Coordinate of the frame</param>
        /// <param name="right">Right X Coordinate of the frame</param>
        /// <param name="bottom">Bottom Y Coordinate of the frame</param>
        /// <returns>True or False if the mouse is within the bounds of the Frame and is not hovering a transparent pixel</returns>
        public static bool IsSelected(Animation animation, AnimationFrame frame, int mouseX, int mouseY, int left, int top, int right, int bottom)
        {
            // check if mouse within bounds of the frame
            if (mouseX > left && mouseX < right && mouseY > top && mouseY < bottom)
            {
                Color[] currentColour = new Color[1];
                animation.Texture.GetData<Color>(0, new Rectangle(frame.X + (mouseX - left), frame.Y + (mouseY - top), 1, 1), currentColour, 0, 1);

                if (currentColour[0] != animation.Mask)
                    return true;
            }
            return false;
        }

        public static bool IsSelected(Animation animation, AnimationFrame frame, int mouseX, int mouseY, int left, int top, int right, int bottom, int shiftX, int shiftY)
        {
            // check if mouse within bounds of the frame
            if (mouseX + shiftX > left && mouseX + shiftX < right && mouseY + shiftY > top && mouseY + shiftY < bottom)
            {
                Color[] currentColour = new Color[1];
                animation.Texture.GetData<Color>(0, new Rectangle(frame.X + (mouseX + shiftX - left), frame.Y + (mouseY + shiftY - top), 1, 1), currentColour, 0, 1);

                if (currentColour[0] != animation.Mask)
                    return true;
            }
            return false;
        }

        //public static void DrawEquipment(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, int colour, float fade, out bool isSelected)
        //{
        //    AnimationFrame frame = null;

        //    if (SpriteExists(Cache.Equipment, Animation, AnimationFrame))
        //        frame = Cache.Equipment[Animation].Frames[AnimationFrame];

        //    if (frame != null)
        //    {
        //        DrawEquipment(spriteBatch, Animation, AnimationFrame, x, y, objectOffsetX, objectOffsetY, offsetX, offsetY, colour, fade);

        //        isSelected = IsSelected(Cache.Equipment[Animation], Cache.Equipment[Animation].Frames[AnimationFrame],
        //                                (int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + frame.PivotX + objectOffsetX,
        //                                y + frame.PivotY + objectOffsetY, x + frame.PivotX + objectOffsetX + frame.Width,
        //                                y + frame.PivotY + objectOffsetY + frame.Height);
        //    }
        //    else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected);
        //}

        //public static void DrawEquipment(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, int colour, float fade, int shiftX, int shiftY, out bool isSelected)
        //{
        //    AnimationFrame frame = null;

        //    if (SpriteExists(Cache.Equipment, Animation, AnimationFrame))
        //        frame = Cache.Equipment[Animation].Frames[AnimationFrame];

        //    if (frame != null)
        //    {
        //        DrawEquipment(spriteBatch, Animation, AnimationFrame, x, y, objectOffsetX, objectOffsetY, offsetX, offsetY, colour, fade);

        //        isSelected = IsSelected(Cache.Equipment[Animation], Cache.Equipment[Animation].Frames[AnimationFrame],
        //                                (int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, x + frame.PivotX + objectOffsetX,
        //                                y + frame.PivotY + objectOffsetY, x + frame.PivotX + objectOffsetX + frame.Width,
        //                                y + frame.PivotY + objectOffsetY + frame.Height, shiftX, shiftY);
        //    }
        //    else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected, shiftX, shiftY);
        //}

        public static void DrawEquipment(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, Color colour, float fade, string spriteFile = "")
        {
            AnimationFrame frame = null;

            if (SpriteExists(Cache.Equipment, Animation, AnimationFrame))
            {

                frame = Cache.Equipment[Animation].Frames[AnimationFrame];

                if (frame != null)
                {
                    Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);
                    spriteBatch.Draw(Cache.Equipment[Animation].Texture, pos, frame.GetRectangle(), colour * fade);
                }
                else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY);
            }
            else
            {
                if (!string.IsNullOrEmpty(spriteFile) && Cache.SpriteConfiguration.ContainsKey(spriteFile))
                {
                    SpriteConfig config = Cache.SpriteConfiguration[spriteFile];
                    QueueSpriteLoad(new SpriteLoad(config.Type, "Sprites\\" + config.Name, (int)config.Id + (config.Range * config.Appearance), config.Count, config.StartIndex));
                }
            }
        }

        //public static void DrawEquipment(SpriteBatch spriteBatch, int Animation, int AnimationFrame, int x, int y, int objectOffsetX, int objectOffsetY, int offsetX, int offsetY, int colour, float fade, int shiftX, int shiftY, string spriteFile = "")
        //{
        //    AnimationFrame frame = null;

        //    if (SpriteExists(Cache.Equipment, Animation, AnimationFrame))
        //    {

        //        frame = Cache.Equipment[Animation].Frames[AnimationFrame];

        //        if (frame != null)
        //        {
        //            Vector2 pos = new Vector2(x + frame.PivotX + objectOffsetX + offsetX, y + frame.PivotY + objectOffsetY + offsetY);
        //            spriteBatch.Draw(Cache.Equipment[Animation].Texture, pos, frame.GetRectangle(), SpriteHelper.ColourFromArgb(colour) * fade);
        //        }
        //        else DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, shiftX, shiftY);
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(spriteFile) && Cache.SpriteConfiguration.ContainsKey(spriteFile))
        //        {
        //            SpriteConfig config = Cache.SpriteConfiguration[spriteFile];
        //            QueueSpriteLoad(new SpriteLoad(config.Type, "Sprites\\" + config.Name, (int)config.Id + (config.Range * config.Appearance), config.Count, config.StartIndex));
        //        }
        //    }
        //}

        public static void DrawTextCenteredWithShadow(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, int width, Color foreColour)
        {
            if (string.IsNullOrEmpty(text)) return;

            int offset = 1;
            int textSize = (int)Cache.Fonts[font].MeasureString(text).X;
            Vector2 location = new Vector2(position.X + (width - textSize) / 2, position.Y);
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(location.X + offset, location.Y + offset), Color.Black);
            spriteBatch.DrawString(Cache.Fonts[font], text, location, foreColour);
        }

        public static void DrawTextRightWithShadow(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, int width, Color foreColour) { SpriteHelper.DrawTextRightWithShadow(spriteBatch, text, font, position, width, foreColour, Color.Black); }
        public static void DrawTextRightWithShadow(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, int width, Color foreColour, Color shadowColour)
        {
            if (string.IsNullOrEmpty(text)) return;

            int offset = 1;
            int textSize = (int)Cache.Fonts[font].MeasureString(text).X;
            Vector2 location = new Vector2(position.X + (width - textSize), position.Y);
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(location.X + offset, location.Y + offset), shadowColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, location, foreColour);
        }

        public static void DrawTextWithShadow(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, Color foreColour) { DrawTextWithShadow(spriteBatch, text, font, position, foreColour, Color.Black); }
        public static void DrawTextWithShadow(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, Color foreColour, Color shadowColour)
        {
            if (string.IsNullOrEmpty(text)) return;

            int offset = 1;
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(position.X + offset, position.Y + offset), shadowColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, position, foreColour);
        }

        public static void DrawTextWithOutline(SpriteBatch spriteBatch, string text, FontType font, Vector2 position, Color foreColour, Color outLineColour)
        {
            if (string.IsNullOrEmpty(text)) return;

            int offset = 1;
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(position.X + offset, position.Y + offset), outLineColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(position.X - offset, position.Y - offset), outLineColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(position.X - offset, position.Y + offset), outLineColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, new Vector2(position.X + offset, position.Y - offset), outLineColour);
            spriteBatch.DrawString(Cache.Fonts[font], text, position, foreColour);
        }

        public static void DrawRedBox(SpriteBatch spriteBatch, int x, int y, int objectOffsetX, int objectOffsetY)
        { bool isSelected; DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected); }
        public static void DrawRedBox(SpriteBatch spriteBatch, int x, int y, int objectOffsetX, int objectOffsetY, out bool isSelected)
        {
            Rectangle r = new Rectangle(x + objectOffsetX, y + objectOffsetY, 32, 32);
            Color c = Color.Red;

            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, 1, r.Height), c); // Left
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Right, r.Top + 1, 1, r.Height), c); // Right
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, r.Width, 1), c); // Top
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Bottom, r.Width, 1), c); // Bottom

            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "X", new Vector2((int)(r.Left + 12), (int)(r.Top + 8)), Color.Red);

            isSelected = Utility.IsSelected((int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, r.Left, r.Top, r.Left + r.Width, r.Top + r.Height);
        }

        public static void DrawRedBox(SpriteBatch spriteBatch, int x, int y, int objectOffsetX, int objectOffsetY, int shiftX, int shiftY)
        { bool isSelected; DrawRedBox(spriteBatch, x, y, objectOffsetX, objectOffsetY, out isSelected, shiftX, shiftY); }
        public static void DrawRedBox(SpriteBatch spriteBatch, int x, int y, int objectOffsetX, int objectOffsetY, out bool isSelected, int shiftX, int shiftY)
        {
            Rectangle r = new Rectangle(x + objectOffsetX, y + objectOffsetY, 32, 32);
            Color c = Color.Red;

            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, 1, r.Height), c); // Left
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Right, r.Top + 1, 1, r.Height), c); // Right
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Top + 1, r.Width, 1), c); // Top
            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(r.Left + 1, r.Bottom, r.Width, 1), c); // Bottom

            spriteBatch.DrawString(Cache.Fonts[FontType.GeneralSize10], "X", new Vector2((int)(r.Left + 12), (int)(r.Top + 8)), Color.Red);

            isSelected = Utility.IsSelected((int)Cache.DefaultState.Display.Mouse.X, (int)Cache.DefaultState.Display.Mouse.Y, r.Left, r.Top, r.Left + r.Width, r.Top + r.Height, shiftX, shiftY);
        }

        public static void DrawLine(SpriteBatch spriteBatch, int sourceX, int sourceY, int destinationX, int destinationY, Color color)
        {
            Vector2 edge = new Vector2(destinationX - sourceX, destinationY - sourceY);
            float angle = (float)Math.Atan2(edge.Y, edge.X);

            Rectangle r = new Rectangle(sourceX, sourceY, (int)edge.Length(), 1);

            spriteBatch.Draw(Cache.BackgroundTexture, r, null, color, angle, new Vector2(0, 0), Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 0);
        }

        public static void DrawSlider(SpriteBatch spriteBatch, int x, int y, int position)
        {
            float transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);

            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x + position, y), Cache.Interface[(int)SpriteId.Interface2].Frames[8].GetRectangle(), Color.White * transparency);
        }

        public static void DrawSliderMainMenu(SpriteBatch spriteBatch, int x, int y, int position)
        {
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x + position, y), Cache.Interface[(int)SpriteId.Interface2].Frames[8].GetRectangle(), Color.White);
        }

        public static void DrawSliderMainMenu(SpriteBatch spriteBatch, int x, int y, int position, Color color)
        {
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x + position, y), Cache.Interface[(int)SpriteId.Interface2].Frames[8].GetRectangle(), color);
        }

        public static void DrawScrollBar(SpriteBatch spriteBatch, int x, int y, int position)
        {
            float transparency = (Cache.GameSettings.TransparentDialogs ? 0.5F : 1F);

            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x, y + position), Cache.Interface[(int)SpriteId.Interface2].Frames[7].GetRectangle(), Color.White * transparency);
        }

        public static void DrawScrollBarMainMenu(SpriteBatch spriteBatch, int x, int y, int position)
        {
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x, y + position), Cache.Interface[(int)SpriteId.Interface2].Frames[7].GetRectangle(), Color.White);
        }

        public static void DrawScrollBarMainMenu(SpriteBatch spriteBatch, int x, int y, int position, Color color)
        {
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface2].Texture, new Vector2(x, y + position), Cache.Interface[(int)SpriteId.Interface2].Frames[7].GetRectangle(), color);
        }

        public static Color ColourFromArgb(int argb)
        {
            System.Drawing.Color c = System.Drawing.Color.FromArgb(argb);

            return new Color(c.R, c.G, c.B, 255);
        }

        public static int ArgbFromColour(System.Drawing.Color color)
        {
            int c = color.ToArgb();

            return c;
        }

        public static Color GetItemColour(Item item, int colour, int glowFrame = 0)
        {
            if (item == null) return Color.White;

            switch (item.SpecialAbilityType)
            {
                case ItemSpecialAbilityType.IceWeapon:
                    return new Color(80, 100, 255, glowFrame);
                case ItemSpecialAbilityType.XelimaWeapon:
                    return new Color(255, 0, 0, glowFrame);
                case ItemSpecialAbilityType.MedusaWeapon:
                case ItemSpecialAbilityType.MerienArmour:
                case ItemSpecialAbilityType.MerienShield:
                    return new Color(0, 255, 0, glowFrame);
                default: return GetItemColour(item.EffectType, colour);
            }
        }

        public static Color GetItemColour(ItemEffectType type, int colour)
        {
            switch (type)
            {
                case ItemEffectType.Attack:
                case ItemEffectType.AttackActivation:
                case ItemEffectType.AttackBow:
                case ItemEffectType.AttackDefence:
                case ItemEffectType.AttackManaSave:
                case ItemEffectType.AttackMaxHPDown:
                    switch (colour)
                    {
                        case 1: return Lighten(70, 70, 80);
                        case 2: return Lighten(70, 70, 80);
                        case 3: return Lighten(70, 70, 80);
                        case 4: return Lighten(70, 100, 70); // poison
                        case 5: return Lighten(130, 90, 10); // critical
                        case 6: return Lighten(42, 53, 111); // sharp
                        //case 7: return Lighten(145, 145, 145); // righteous original
                        case 7: return Lighten(198, 199, 200); // righteous
                        //case 8: return Lighten(120, 100, 120); // ancient original
                        case 8: return Lighten(119, 60, 119); // ancient
                        case 9: return new Color(75, 10, 10); // blood
                        case 10: return Lighten(135, 105, 30); // gold
                        default: return Color.White;
                    }
                case ItemEffectType.Defence:
                case ItemEffectType.DefenceActivation:
                case ItemEffectType.DefenceAntiMine:
                    switch (colour)
                    {
                        //case 0: return new Color(100, 100, 100);
                        case 1: return Lighten(40, 40, 90);
                        case 2: return Lighten(79, 79, 62);
                        case 3: return Lighten(135, 104, 30);
                        case 4: return Lighten(128, 18, 0);
                        case 5: return Lighten(10, 60, 10);
                        case 6: return Lighten(40, 40, 40);
                        case 7: return Lighten(47, 79, 80);
                        case 8: return Lighten(128, 52, 90);
                        case 9: return Lighten(90, 60, 90);
                        case 10: return Lighten(0, 35, 60);
                        case 11: return Lighten(105, 90, 70);
                        case 12: return Lighten(94, 91, 53);
                        case 13: return Lighten(85, 85, 8);
                        case 14: return Lighten(75, 10, 10);
                        case 15: return Lighten(48, 48, 48);
                        default: return Color.White;
                    }
                default: return Color.White;
            }
        }

        public static Color Lighten(int r, int g, int b)
        {
            System.Drawing.Color c = System.Windows.Forms.ControlPaint.Light(System.Drawing.Color.FromArgb(r, g, b), 0.8f);

            return new Color(c.R, c.G, c.B, c.A);
        }

        public static int Glow(int colour, int glowFrame)
        {
            return colour - (int)(((double)colour / 100.0F) * glowFrame);
        }

        //Used for switching color of object from red to white to blue and back again.
        public static Color Fade(Color color, ref bool fadeDirection, ref bool fadeStage) //Red = {R:255 G:0 B:0 A:255}, White = {R:255 G:255 B:255 A:255} Blue = R:0 G:0 B:255 A:255
        {
            if (fadeDirection)
            {
                if (fadeStage) //Reaches Red
                {
                    if (color.R == 255 && color.G == 0 && color.B == 0) //Reached Red, Switch Direction
                    {
                        fadeDirection = false;
                        fadeStage = false;
                    }
                    else
                    {
                        if (color.R < 255) color.R++;
                        if (color.G > 0) color.G--;
                        if (color.B > 0) color.B--;
                    }
                }

                if (!fadeStage) //Reaches White
                {
                    if (color.R == 255 && color.G == 255 && color.B == 255) //Reached White, Switch Stage
                    {
                        fadeStage = true;
                    }
                    else
                    {
                        if (color.R < 255) color.R++;
                        if (color.G < 255) color.G++;
                        if (color.B < 255) color.B++;
                    }
                }
            }
            else
            {
                if (fadeStage)
                {
                    if (color.R == 0 && color.G == 0 && color.B == 255) //Fade to Blue
                    {
                        fadeDirection = true;
                        fadeStage = false;
                    }
                    else
                    {
                        if (color.R > 0) color.R--;
                        if (color.G > 0) color.G--;
                        if (color.B < 255) color.B++;
                    }
                }

                if (!fadeStage)
                {
                    if (color.R == 255 && color.G == 255 && color.B == 255) //Reached White, Switch Stage
                    {
                        fadeStage = true;
                    }
                    else
                    {
                        if (color.R < 255) color.R++;
                        if (color.G < 255) color.G++;
                        if (color.B < 255) color.B++;
                    }
                }
            }
            return color;
        }

        //public static Color Fade(Color fadeFrom, Color fadeTo)
        //{
        //    Color result = fadeFrom 
        //    return 
        //}

        public static void IncreaseHueBy(ref Color color, float value, out float hue) //TEST THIS OUT
        {
            float h, s, v;

            RgbToHsv(color.R, color.G, color.B, out h, out s, out v);
            h += value;

            float r, g, b;

            HsvToRgb(h, s, v, out r, out g, out b);


            color.R = (byte)(r);
            color.G = (byte)(g);
            color.B = (byte)(b);

            hue = h;
        }

        private static void RgbToHsv(float r, float g, float b, out float h, out float s, out float v)
        {
            float min, max, delta;
            min = System.Math.Min(System.Math.Min(r, g), b);
            max = System.Math.Max(System.Math.Max(r, g), b);
            v = max;               // v
            delta = max - min;
            if (max != 0)
            {
                s = delta / max;       // s

                if (r == max)
                    h = (g - b) / delta;       // between yellow & magenta
                else if (g == max)
                    h = 2 + (b - r) / delta;   // between cyan & yellow
                else
                    h = 4 + (r - g) / delta;   // between magenta & cyan
                h *= 60;               // degrees
                if (h < 0)
                    h += 360;
            }
            else
            {
                // r = g = b = 0       // s = 0, v is undefined
                s = 0;
                h = -1;
            }

        }

        public static Texture2D PreMultiply(this Texture2D value)
        {
            Color[] data = new Color[value.Width * value.Height];

            value.GetData<Color>(data);
            for (int i = 0; i < data.Length; i++)
            {
                data[i].R = (byte)(data[i].R * (data[i].A / 255.0f));
                data[i].G = (byte)(data[i].G * (data[i].A / 255.0f));
                data[i].B = (byte)(data[i].B * (data[i].A / 255.0f));
            }
            value.SetData<Color>(data);
            return value;
        }

        private static void HsvToRgb(float h, float s, float v, out float r, out float g, out float b)
        {
            // Keeps h from going over 360
            h = h - ((int)(h / 360) * 360);

            int i;
            float f, p, q, t;
            if (s == 0)
            {
                // achromatic (grey)
                r = g = b = v;
                return;
            }
            h /= 60;           // sector 0 to 5

            i = (int)h;
            f = h - i;         // factorial part of h
            p = v * (1 - s);
            q = v * (1 - s * f);
            t = v * (1 - s * (1 - f));
            switch (i)
            {
                case 0:
                    r = v;
                    g = t;
                    b = p;
                    break;
                case 1:
                    r = q;
                    g = v;
                    b = p;
                    break;
                case 2:
                    r = p;
                    g = v;
                    b = t;
                    break;
                case 3:
                    r = p;
                    g = q;
                    b = v;
                    break;
                case 4:
                    r = t;
                    g = p;
                    b = v;
                    break;
                default:       // case 5:
                    r = v;
                    g = p;
                    b = q;
                    break;
            }
        }

        public static Dictionary<string, SpriteConfig> LoadSpriteConfiguration(string configFilePath)
        {

            Dictionary<string, SpriteConfig> sprites = new Dictionary<string, SpriteConfig>(StringComparer.OrdinalIgnoreCase);

            XmlTextReader reader = new XmlTextReader(configFilePath);

            while (reader.Read())
                if (reader.IsStartElement())
                    if (reader.Name.Equals("SpriteFile"))
                        if (!sprites.ContainsKey(reader["Name"].ToString()))
                        {
                            SpriteConfig config = SpriteConfig.Parse(reader);
                            sprites.Add(config.Name, config);
                        }
            reader.Close();

            return sprites;
        }

        public static void HandleSpriteQueue(object o)
        {
            SpriteLoad nextSprite = null;

            lock (Cache.SpriteQueue)
            {
                if (Cache.SpriteQueue.Count > 0)
                    nextSprite = Cache.SpriteQueue.Dequeue();
            }

            if (nextSprite != null)
            {
                LoadSprite(nextSprite.Type, nextSprite.Name, nextSprite.Id, nextSprite.Count, nextSprite.StartIndex);
            }

        }

        private static void QueueSpriteLoad(SpriteLoad load)
        {
            lock (Cache.SpriteQueue)
            {
                if (FailedSpriteLoads.Contains(load.Name))
                {
                    return;
                }

                foreach (SpriteLoad l in Cache.SpriteQueue)
                    if (l.Name.Equals(load.Name)) return;

                Cache.SpriteQueue.Enqueue(load);
            }
        }

        public static void LoadSprite(SpriteType type, string fileName, int start, int end, int startIndex = 0)
        {
            if (Cache.DefaultState == null || Cache.DefaultState.Display == null || Cache.DefaultState.Display.Device == null) return;
            if (!fileName.EndsWith(".spr")) return;

            try
            {
                SpriteFile spriteFile;

                if (type == SpriteType.Monster)
                    spriteFile = new SpriteFile(fileName, false);
                else spriteFile = new SpriteFile(fileName);

                Sprite sprite;
                Stream stream;
                Animation anim;
                Tile tile;

                DateTime timeStart = DateTime.Now;
                end += startIndex;

                int indexModifier = 0, index = 0;
                switch (type)
                {
                    case SpriteType.EquipmentPack:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                // TODO - oh my god this is shit
                                if (fileName.Contains("item-equipM"))
                                    switch (i)
                                    {
                                        case 6: index = 7; break;
                                        case 7: index = 8; break;
                                        case 8: index = 9; break;
                                        case 9: index = 18; break;
                                        case 10: index = 19; break;
                                        case 11: index = 15; break;
                                        case 13: index = 20; break;
                                        case 14: index = 21; break;
                                        default: index = i; break;
                                    }
                                else if (fileName.Contains("item-equipW"))
                                    switch (i)
                                    {
                                        case 4: index = 5; break;
                                        case 5: index = 10; break;
                                        case 6: index = 11; break;
                                        case 7: index = 12; break;
                                        case 8: index = 13; break;
                                        case 11: index = 15; break;
                                        case 12: index = 17; break;
                                        case 9: index = 18; break;
                                        case 10: index = 19; break;
                                        case 13: index = 20; break;
                                        case 14: index = 21; break;
                                        default: index = i; break;
                                    }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 15))) break;

                                lock (LoadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                    }

                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index, anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.HairAndUndies: // does something funky with undies and hair sprite indexes
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 12 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 12 animations, split by 15 indexes instead of 12
                                }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 15))) break;

                                lock (LoadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                    }

                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 15), anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.Bows:
                    case SpriteType.Weapons:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 56 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 56 animations, split by 64 indexes instead of 56
                                }

                                sprite = spriteFile[i + startIndex];

                                lock (LoadingLock)
                                {
                                    if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 64))) break;
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                    }
                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 64), anim);
                                }

                                index++;
                            }
                        break;
                    case SpriteType.Shields:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                if (i > 0 && i % 7 == 0)
                                {
                                    index = 0;
                                    indexModifier++; // every 7 animations, split by 8 indexes instead of 7
                                }

                                sprite = spriteFile[i + startIndex];

                                if (Cache.Equipment.ContainsKey(start + index + (indexModifier * 8))) break;

                                lock (LoadingLock)
                                {
                                    using (stream = new MemoryStream(sprite.ImageData))
                                    {
                                        anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                    }
                                    foreach (SpriteFrame frame in sprite.Frames)
                                        anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                    Cache.Equipment.Add(start + index + (indexModifier * 8), anim);
                                }

                                index++;
                            }
                        break;
                    default:
                        for (int i = 0; i < end; i++)
                            if (spriteFile.Sprites.Count > i)
                            {
                                sprite = spriteFile[i + startIndex];

                                switch (type)
                                {
                                    case SpriteType.Human:
                                        if (Cache.HumanAnimations.ContainsKey(start + i)) break;
                                        lock (LoadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.HumanAnimations.Add(start + i, anim);
                                        }
                                        break;
                                    case SpriteType.Monster:
                                        if (Cache.MonsterAnimations.ContainsKey(start + i)) break;
                                        lock (LoadingLock)
                                        {
                                            spriteFile.LoadSprite(i);

                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                // TODO FIX
                                                if (sprite.Height > 2048 || sprite.Width > 2048)
                                                {
                                                    Texture2D tex = new Texture2D(Cache.DefaultState.Display.Device, sprite.Width, sprite.Height);
                                                    tex.SetData(sprite.ImageData);
                                                    anim = new Animation(tex);
                                                }
                                                else anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                            }

                                            anim.LoadState = AnimationLoadState.Loading;
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.MonsterAnimations.Add(start + i, anim);
                                            anim.LoadState = AnimationLoadState.Loaded;
                                        }
                                        break;
                                    case SpriteType.Armour:
                                        lock (LoadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                            }

                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));

                                            if (!Cache.Equipment.ContainsKey(start + i))
                                                Cache.Equipment.Add(start + i, anim);

                                            // todo - more shit so dodgy..... 
                                            if (fileName.Contains("item-pack") || fileName.Contains("item-ground"))
                                            {
                                                if (i == 17 || i == 18 || i == 19)
                                                    Cache.Equipment.Add((start - 1) + i + 3, anim);

                                                if (fileName.Contains("item-pack"))
                                                {
                                                    if (i == 15)
                                                    {
                                                        Cache.Equipment.Add((int)SpriteId.EquipmentPack + 16, anim);
                                                        Cache.Equipment.Add((int)SpriteId.EquipmentPack + 56, anim);
                                                    }
                                                    if (i == 19)
                                                    {
                                                        Cache.Equipment.Add((int)SpriteId.EquipmentPack + 22, anim);
                                                        Cache.Equipment.Add((int)SpriteId.EquipmentPack + 62, anim);
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case SpriteType.Tiles:
                                        if (Cache.Tiles.ContainsKey(start + i)) break;
                                        lock (LoadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                tile = new Tile(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream));
                                                tile.MiniMapImage = MapHelper.ResizeImage((System.Drawing.Bitmap)System.Drawing.Image.FromStream(stream), tile.Texture.Width / MapHelper.MiniMapTempScaleFactor, tile.Texture.Height / MapHelper.MiniMapTempScaleFactor);
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                tile.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Tiles.Add(start + i, tile);
                                        }
                                        break;
                                    case SpriteType.Effect:
                                        if (Cache.Effects.ContainsKey(start + i)) break;
                                        lock (LoadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream).PreMultiply());
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Effects.Add(start + i, anim);
                                        }
                                        break;
                                    case SpriteType.Interface:
                                        if (Cache.Interface.ContainsKey(start + i)) break;
                                        lock (LoadingLock)
                                        {
                                            using (stream = new MemoryStream(sprite.ImageData))
                                            {
                                                anim = new Animation(Texture2D.FromStream(Cache.DefaultState.Display.Device, stream).PreMultiply());
                                            }
                                            foreach (SpriteFrame frame in sprite.Frames)
                                                anim.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));
                                            Cache.Interface.Add(start + i, anim);
                                        }
                                        break;
                                }

                            }
                        break;
                }
                // For testing delayed lazy loading
                //if (Cache.DefaultState is MainGame)
                //{
                //    Thread.Sleep(100);
                //}
                spriteFile.Dispose();
                TimeSpan timeTaken = DateTime.Now - timeStart;
                Console.WriteLine("Load {0}: {1} seconds", fileName, timeTaken.TotalSeconds);
            }
            catch (Exception e)
            {
                FailedSpriteLoads.Add(fileName);
                logger.Error(String.Format("Failed to load sprite - Type: {0} - File Name: {1} - Start: {2} - End: {3} - Start Index: {4}", type, fileName, start, end, startIndex), e);
            }
        }
    }
}
