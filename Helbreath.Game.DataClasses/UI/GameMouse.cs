using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Helbreath.Game;

namespace Helbreath.Game.Assets
{
    public class GameMouse
    {
        private GameMouseState state;
        private Texture2D texture;
        private List<AnimationFrame> frames;
        private Vector2 position;
        private GameMouseState animationState;
        private DateTime animationTime;
        private GameDisplay display;
        private int selectedObjectId;

        private MouseState currentMouseState;
        private MouseState lastMouseState;

        private bool leftClick;
        private bool leftHeld;
        private bool leftReleased;

        private bool leftDoubleClick;
        private DateTime leftClickTime;

        private bool rightClick;
        private bool rightHeld;
        private bool rightReleased;

        //LeftHeld //TODO - Possible change implementation to use previous state?
        private bool dragging; //If drag happens while button is held, turns true until released
        private bool drag; //true only during a drag movement
        private int dragX; //Amount of pixels to move in X
        private int dragY; //Amount of pixels to move in Y
        private int clickX; //previous states X location, used to know when to activate dragging
        private int clickY; //previous states X location, used to know when to activate dragging
        private int previousX;
        private int previousY;
        private int dragTolerance = 3; //TODO add this to game settings

        private int previousScrollValue;
        private int scrollDirection;

        public GameMouse(Texture2D texture, GameDisplay display)
        {
            state = animationState = GameMouseState.Normal;
            this.texture = texture;
            this.display = display;
            position = Vector2.One;
            frames = new List<AnimationFrame>();
            selectedObjectId = -1;
            clickX = clickY = -1;
        }


        public void Update(MouseState state) //TODO go over this code and clean it up
        {
            // Get the mouse state relevant for this frame
            currentMouseState = Mouse.GetState();

            // Recognize a single click of the left mouse button
            if (lastMouseState.LeftButton == ButtonState.Released && currentMouseState.LeftButton == ButtonState.Pressed)
            { leftClick = true; }
            else { leftClick = false; }

            //Recognize a single click of the right mouse button
            if (lastMouseState.RightButton == ButtonState.Released && currentMouseState.RightButton == ButtonState.Pressed)
            { rightClick = true; }
            else { rightClick = false; }

            // Recognize a single release of the left mouse button
            if (lastMouseState.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
            { leftReleased = true; }
            else { leftReleased = false; }

            //Recognize a single release of the right mouse button
            if (lastMouseState.RightButton == ButtonState.Pressed && currentMouseState.RightButton == ButtonState.Released)
            { rightReleased = true; }
            else { rightReleased = false; }

            X = currentMouseState.X;
            Y = currentMouseState.Y;

            if (currentMouseState.LeftButton == ButtonState.Pressed)
            {
                if (clickX != -1 && clickY != -1) //Wait for second pass to check
                {
                    if ((int)X > clickX + dragTolerance || (int)X < clickX - dragTolerance || (int)Y > clickY + dragTolerance || (int)Y < clickY - dragTolerance)
                        dragging = true;

                    if ((int)X == previousX && (int)Y == previousY) { drag = false; }
                    else { drag = true; }

                    dragX = (int)X - previousX;
                    dragY = (int)Y - previousY;
                }
                else //Set up held state
                {
                    //update clickX & Y
                    clickX = (int)X;
                    clickY = (int)Y;
                    leftHeld = true;
                }

                previousX = (int)X;
                previousY = (int)Y;
            }
            else if (currentMouseState.LeftButton == ButtonState.Released)
            {
                leftHeld = false;
                drag = false;
                dragging = false;
                clickX = clickY = previousX = previousY = -1;
                dragX = dragY = 0;
            }

            if (currentMouseState.RightButton == ButtonState.Pressed) rightHeld = true;
            else if (currentMouseState.RightButton == ButtonState.Released) rightHeld = false;

            if (state.ScrollWheelValue != 0)
            {
                // gets direction based on previous value
                if (state.ScrollWheelValue > previousScrollValue) scrollDirection = 1;
                else if (state.ScrollWheelValue < previousScrollValue) scrollDirection = -1;
                else scrollDirection = 0;

                previousScrollValue = state.ScrollWheelValue;
            }

            this.selectedObjectId = -1; //what is this?

            // The active state from the last frame is now old
            lastMouseState = currentMouseState;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 drawPosition;

            // mouse animations
            switch (this.state)
            {
                case GameMouseState.Pickup:
                    if ((DateTime.Now - animationTime).Milliseconds > 250)
                    {
                        if (animationState == GameMouseState.Pickup1)
                            animationState = GameMouseState.Pickup2;
                        else animationState = GameMouseState.Pickup1;
                        animationTime = DateTime.Now;
                    }

                    drawPosition = new Vector2(Position.X + Frames[(int)animationState].PivotX, Position.Y + Frames[(int)animationState].PivotY);
                    spriteBatch.Draw(Texture, drawPosition, Frames[(int)animationState].GetRectangle(), Color.White);
                    break;
                case GameMouseState.Selection:
                    if ((DateTime.Now - animationTime).Milliseconds > 100)
                    {
                        if (animationState == GameMouseState.Selection1)
                            animationState = GameMouseState.Selection2;
                        else if (animationState == GameMouseState.Selection2)
                            animationState = GameMouseState.Selection3;
                        else animationState = GameMouseState.Selection1;
                        animationTime = DateTime.Now;
                    }

                    drawPosition = new Vector2(Position.X + Frames[(int)animationState].PivotX, Position.Y + Frames[(int)animationState].PivotY);
                    spriteBatch.Draw(Texture, drawPosition, Frames[(int)animationState].GetRectangle(), Color.White);
                    break;
                default:
                    drawPosition = new Vector2(Position.X + Frames[(int)State].PivotX, Position.Y + Frames[(int)State].PivotY);
                    spriteBatch.Draw(Texture, drawPosition, Frames[(int)State].GetRectangle(), Color.White);
                    break;
            }
        }
        public GameMouseState State { get { return state; } set { state = value; } }
        public Texture2D Texture { get { return texture; } }
        public List<AnimationFrame> Frames { get { return frames; } }
        public Vector2 Position { get { return position; } }
        public GameDisplay Display { get { return display; } }
        public int SelectedObjectId { get { return selectedObjectId; } set { selectedObjectId = value; } }

        public bool LeftClick { get { return leftClick; } }
        public bool LeftHeld { get { return leftHeld; } }
        public bool LeftReleased { get { return leftReleased; } }

        public bool RightClick { get { return rightClick; } }
        public bool RightHeld { get { return rightHeld; } }
        public bool RightReleased { get { return rightReleased; } }

        public DateTime LeftClickTime { get { return leftClickTime; } set { leftClickTime = value; } }

        //LeftHeld - Left mouse only
        public bool IsDragging { get { return dragging; } } //if mouse button held down and mouse moved X or Y
        public int DragX
        {
            get
            {
                if (Cache.GameSettings.DisplayMode == DisplayMode.WindowedFullscreen)
                {
                    return (int)(dragX / display.ResolutionWidthScale); // TODO: return float, so imprecisions won't happen
                }
                return dragX;
            }
        }
        public int DragY
        {
            get
            {
                if (Cache.GameSettings.DisplayMode == DisplayMode.WindowedFullscreen)
                {
                    return (int)(dragY / display.ResolutionHeightScale); // TODO: return float, so imprecisions won't happen
                }
                return dragY;
            }
        }
        public bool Drag { get { return drag; } }
        public int DragTolerance { get { return dragTolerance; } set { dragTolerance = value; } }

        public int Scroll { get { return scrollDirection; } }



        public float X { get { return position.X; } set { position.X = value; } }
        public float Y { get { return position.Y; } set { position.Y = value; } }
        public Point Point { get { return new Point((int)position.X, (int)position.Y); } }
        public int CellX { get { return (((int)position.X + Cache.DefaultState.Display.GameBoardShiftX) / Globals.CellWidth); } }
        public int CellY { get { return (((int)position.Y + Cache.DefaultState.Display.GameBoardShiftY) / Globals.CellHeight); } }
        //private GameMouseState animationState;
        //private DateTime animationTime;
        //private MouseState currentMouseState;
        //private MouseState lastMouseState;
        //private bool leftDoubleClick;
        //private bool leftDoubleClick;
        //private int previousScrollValue;
    }
}
