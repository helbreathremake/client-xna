﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HelbreathWorld.Game.Assets.UI
{
    public class WinForms
    {
        public static void ShowAlertMessage(string message)
        {
            Cursor.Show();
            MessageBox.Show(message);
            Cursor.Hide();
        }
    }
}
