﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    public interface IMenuDialogBox
    {
        MenuDialogBoxType Type { get; }

        int X { get; set; }
        int Y { get; set; }
        int Page { get; set; }

        bool IsScrollable { get; set; }
        bool Moveable { get; set; }
        bool Visible { get; }
        bool Hidden { get; set; }
        bool AlwaysVisible { get; set; }

        int SelectedItemIndex { get; set; }
        int ClickedItemIndex { get; set; }
        int HighlightedItemIndex { get; set; }
        int SelectedItemIndexOffsetX { get; set; }
        int SelectedItemIndexOffsetY { get; set; }

        bool Draw(SpriteBatch spriteBatch, GameTime gameTime);
        void Update(GameTime gameTime);
        void Click();
        bool Drag();
        void Scroll(int direction);
        void OffsetLocation(int x, int y);
        void Show();
        void Hide();
        void Toggle(DialogBoxState state = DialogBoxState.Normal);
        void SetData(byte[] data);
    }
}
