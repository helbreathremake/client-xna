﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    public class MenuMessageBox : IMenuDialogBox
    {
        public event MenuMessageBoxHandler ResponseSelected;

        // IDIALOG
        private MenuDialogBoxType type;
        private bool isScrollable;
        private bool moveable;
        private int maxPages;
        private int page;
        private int clickedIndex; // clicked item
        private int selectedIndex; // mouse over item
        private Vector2 selectedLocation;
        private int selectedItemIndexOffsetX;
        private int selectedItemIndexOffsetY;
        private int highlightedIndex; // chosen item (non volatile)

        private GameDialogBoxConfiguration config;

        public int ClickedItemIndex { get { return clickedIndex; } set { clickedIndex = value; } }
        public int SelectedItemIndex { get { return selectedIndex; } set { selectedIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedIndex; } set { highlightedIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public int Page { get { return page; } set { page = value; } }
        public bool IsScrollable { get { return isScrollable; } set { isScrollable = value; } }

        public bool Moveable { get { return moveable; } set { moveable = value; } }
        public MenuDialogBoxType Type { get { return type; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }

        public DialogBoxState State { get { return config.State; } set { config.State = value; } }
        public bool Hidden { get { return config.Hidden; } set { config.Hidden = value; } }
        public bool Visible { get { return ((config.AlwaysVisible && Cache.GameSettings.LockedDialogs) || !config.Hidden); } }
        public bool AlwaysVisible { get { return config.AlwaysVisible; } set { config.AlwaysVisible = value; } }
        public int X { get { return config.X; } set { config.X = value; } }
        public int Y { get { return config.Y; } set { config.Y = value; } }

        // MESSAGE BOX
        private string text;
        private List<string> textLines;
        private string button1;
        private string button2;

        public MenuMessageBox(int x, int y, MenuDialogBoxType type, string text, string button1, string button2)
        {
            this.type = type;
            State = DialogBoxState.Normal;
            X = x;
            Y = y;
            this.moveable = true;
            this.highlightedIndex = selectedIndex = clickedIndex = -1;
            this.page = 1;
            this.button1 = button1;
            this.button2 = button2;

            this.textLines = new List<string>();
            this.text = text;

            // split the text in to separate lines so it doesn't overlfow the dialog sprite
            string[] textWords = text.Split(' ');
            StringBuilder lineBuilder = new StringBuilder();
            for (int j = 0; j < textWords.Length; j++)
            {
                int lineLength = (int)Cache.Fonts[FontType.GeneralSize10].MeasureString(lineBuilder.ToString() + textWords[j]).X;
                if (lineLength > 120)
                {
                    lineBuilder.Append(textWords[j]);
                    textLines.Add(lineBuilder.ToString().Trim());
                    lineBuilder.Clear();
                }
                else
                {
                    lineBuilder.Append(textWords[j]);
                    lineBuilder.Append(" ");
                }
            }
            if (lineBuilder.Length > 0) textLines.Add(lineBuilder.ToString().Trim());
            if (textLines.Count <= 0) textLines.Add(text);



            Hide();
        }

        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (Hidden)
                if (!AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            int x = X;
            int y = Y;
            int sprite = (int)SpriteId.DialogsV2 + 2;
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || Hidden) ? 0.5F : 1F);
            Player player = ((MainGame)Cache.DefaultState).Player;
            highlightedIndex = selectedIndex = -1;

            AnimationFrame frame = Cache.Interface[sprite].Frames[10];
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);

            switch (State)
            {
                case DialogBoxState.Normal:

                    // text
                    for (int line = 0; line < textLines.Count; line++)
                    {
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, textLines[line], FontType.GeneralSize10, new Vector2(x + 13, y + 20 + (line * 15)), 155, Cache.Colors[GameColor.Orange]);
                    }

                    // button 1
                    if (!string.IsNullOrEmpty(button1))
                    {
                        if (Utility.IsSelected(mouseX, mouseY, x + 13, y + 62, x + 13 + 65, y + 62 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button1, FontType.GeneralSize10, new Vector2(x + 13, y + 65), 65, Color.White * transparency);
                            selectedIndex = highlightedIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button1, FontType.GeneralSize10, new Vector2(x + 13, y + 65), 65, Color.White * transparency);
                        }
                    }

                    // button 2
                    if (!string.IsNullOrEmpty(button2))
                    {
                        if (Utility.IsSelected(mouseX, mouseY, x + 97, y + 62, x + 97 + 65, y + 62 + 33))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 97, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button2, FontType.GeneralSize10, new Vector2(x + 97, y + 65), 65, Color.White * transparency);
                            selectedIndex = highlightedIndex;
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 97, y + 62), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, button2, FontType.GeneralSize10, new Vector2(x + 97, y + 65), 65, Color.White * transparency);
                        }
                    }
                    break;
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Click()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;

            // button 1
            if (!string.IsNullOrEmpty(button1))
            {
                if (Utility.IsSelected(mouseX, mouseY, X + 13, Y + 62, X + 13 + 65, Y + 62 + 33))
                    if (ResponseSelected != null) ResponseSelected(type, MessageBoxResponse.Yes);
                    else ((IMenuState)Cache.DefaultState).HideMessageBox(type);
            }

            // button 2
            if (!string.IsNullOrEmpty(button2))
            {
                if (Utility.IsSelected(mouseX, mouseY, X + 97, Y + 62, X + 97 + 65, Y + 62 + 33))
                    if (ResponseSelected != null) ResponseSelected(type, MessageBoxResponse.No);
                    else ((IMenuState)Cache.DefaultState).HideMessageBox(type);
            }
        }

        public bool Drag()
        {
            return false;
        }

        public void Scroll(int direction)
        {
            if (page - direction > maxPages) page = 1;
            else if (page - direction < 1) page = maxPages;
            else page -= direction;

            highlightedIndex = -1; // clear popup
        }

        public void OffsetLocation(int x, int y)
        {
            X += x;
            Y += y;
        }

        public void SetLocation(GameDialogBoxConfiguration location)
        {
            // dont load state or visibility from XML
        }

        public void DefaultLocation(Resolution resolution)
        {

        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {
            if (Visible && state == State) Hide();
            else
            {
                State = state;
                SetPagination();
                Show();
            }
        }

        public void Show()
        {
            if (!AlwaysVisible && Cache.DefaultState != null)
                ((IMenuState)Cache.DefaultState).BringToFront(Type);
            highlightedIndex = -1;
            Hidden = false;
        }

        public void Hide()
        {
            if (!AlwaysVisible && Cache.DefaultState != null)
            {
                ((IMenuState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IMenuState)Cache.DefaultState).ClickedDialogBox = MenuDialogBoxType.None;
            }
            Hidden = true;
        }

        private void SetPagination()
        {

        }

        public void SetData(byte[] data)
        {

        }
    }
}
