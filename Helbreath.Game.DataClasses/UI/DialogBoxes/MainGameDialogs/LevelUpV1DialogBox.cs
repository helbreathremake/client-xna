﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    public class LevelUpV1DialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;
        //Base graphic info
        int sprite = (int)SpriteId.Interface2; //Main texture
        int spriteFrame = 0; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height 

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.LevelUpV1; } }

        public LevelUpV1DialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            //Assign height and width if has sprite
            if (sprite != -1 && spriteFrame != -1)
            {
                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            }
        }


        public LevelUpV1DialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            //Assign height and width if has sprite
            if (sprite != -1 && spriteFrame != -1)
            {
                width = Cache.Interface[sprite].Frames[spriteFrame].Width;
                height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            }
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            float transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;
            Player player = ((MainGame)Cache.DefaultState).Player;

            //Draws base frame if not null
            AnimationFrame frame; // holds frame in case we need to check bounds later
            if (sprite != -1 && spriteFrame != -1) // some dialog boxes dont have a box sprite such as minimap
            {
                if (spriteFrameHover != -1 &&
                    (mouseX > x) && (mouseX < x + width) && (mouseY > y) && (mouseY <= y + height)) //Hover
                    frame = Cache.Interface[sprite].Frames[spriteFrameHover];
                else frame = Cache.Interface[sprite].Frames[spriteFrame]; //Normal 
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture
            }
            else frame = null;

            //draw extra 
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceText].Texture, new Vector2(x, y), Cache.Interface[(int)SpriteId.InterfaceText].Frames[2].GetRectangle(), Color.White * transparency);
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 16, y + 100), Cache.Interface[(int)SpriteId.Interface4].Frames[4].GetRectangle(), Color.White * transparency);

            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "* Points Left:", new Vector2((int)(x + 20), (int)(y + 85)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.RemainderLevelUpPoints.ToString(), new Vector2((int)(x + 73), (int)(y + 102)), Color.Black * 0.7f);

            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Strength", new Vector2((int)(x + 24), (int)(y + 125)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Vitality", new Vector2((int)(x + 24), (int)(y + 144)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Dexterity", new Vector2((int)(x + 24), (int)(y + 163)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Intelligence", new Vector2((int)(x + 24), (int)(y + 182)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Magic", new Vector2((int)(x + 24), (int)(y + 201)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "Agility", new Vector2((int)(x + 24), (int)(y + 220)), Color.Black * 0.7f);

            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Strength.ToString(), new Vector2((int)(x + 109), (int)(y + 125)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Vitality.ToString(), new Vector2((int)(x + 109), (int)(y + 144)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Dexterity.ToString(), new Vector2((int)(x + 109), (int)(y + 163)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Intelligence.ToString(), new Vector2((int)(x + 109), (int)(y + 182)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Magic.ToString(), new Vector2((int)(x + 109), (int)(y + 201)), Color.Black * 0.7f);
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.Agility.ToString(), new Vector2((int)(x + 109), (int)(y + 220)), Color.Black * 0.7f);

            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpStrength.ToString(), new Vector2((int)(x + 162), (int)(y + 125)), (player.LevelUpStrength > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpVitality.ToString(), new Vector2((int)(x + 162), (int)(y + 144)), (player.LevelUpVitality > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpDexterity.ToString(), new Vector2((int)(x + 162), (int)(y + 163)), (player.LevelUpDexterity > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpIntelligence.ToString(), new Vector2((int)(x + 162), (int)(y + 182)), (player.LevelUpIntelligence > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpMagic.ToString(), new Vector2((int)(x + 162), (int)(y + 201)), (player.LevelUpMagic > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));
            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], player.LevelUpAgility.ToString(), new Vector2((int)(x + 162), (int)(y + 220)), (player.LevelUpAgility > 0 ? Color.Red * 0.7f : Color.Black * 0.7f));

            // strength
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 127) && (mouseY <= y + 133) && (player.Strength < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 127), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 127) && (mouseY <= y + 133) && (player.LevelUpStrength > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 127), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // vitality
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 146) && (mouseY <= y + 152) && (player.Vitality < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 146), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 146) && (mouseY <= y + 152) && (player.LevelUpVitality > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 146), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // dexterity
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 165) && (mouseY <= y + 171) && (player.Dexterity < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 165), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 165) && (mouseY <= y + 171) && (player.LevelUpDexterity > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 165), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // intelligence
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 184) && (mouseY <= y + 190) && (player.Intelligence < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 184), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 184) && (mouseY <= y + 190) && (player.LevelUpIntelligence > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 184), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // magic
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 203) && (mouseY <= y + 209) && (player.Magic < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 203), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 203) && (mouseY <= y + 209) && (player.LevelUpMagic > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 203), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // agility
            if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 222) && (mouseY <= y + 228) && (player.Agility < Globals.MaximumStat))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 195, y + 222), Cache.Interface[(int)SpriteId.Interface4].Frames[5].GetRectangle(), Color.White * transparency);
            if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 222) && (mouseY <= y + 228) && (player.LevelUpAgility > 0))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.Interface4].Texture, new Vector2(x + 210, y + 222), Cache.Interface[(int)SpriteId.Interface4].Frames[6].GetRectangle(), Color.White * transparency);

            // ok
            if (player.RemainderLevelUpPoints == player.LevelUpPoints)
            {
                if ((mouseX > x + 30) && (mouseX < x + 30 + 74) && (mouseY > y + 292) && (mouseY <= y + 292 + 20))
                    spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 30, y + 292), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[1].GetRectangle(), Color.White * transparency);
                else spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 30, y + 292), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[0].GetRectangle(), Color.White * transparency);
            }

            // change
            if ((mouseX > x + 154) && (mouseX < x + 154 + 74) && (mouseY > y + 292) && (mouseY <= y + 292 + 20))
                spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 154, y + 292), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[21].GetRectangle(), Color.White * transparency);
            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.InterfaceButton].Texture, new Vector2(x + 154, y + 292), Cache.Interface[(int)SpriteId.InterfaceButton].Frames[20].GetRectangle(), Color.White * transparency);


            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {

        }

        public void LeftClicked()
        {
            int mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            int mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            int x = config.X;
            int y = config.Y;
            Player player = ((MainGame)Cache.DefaultState).Player;

            if (player.RemainderLevelUpPoints > 0)
            {
                int increase = Math.Min(player.RemainderLevelUpPoints, (Cache.DefaultState.Display.Keyboard.LeftCtrl ? 10 : 1));
                int decrease = Math.Max(0, (Cache.DefaultState.Display.Keyboard.LeftCtrl ? 10 : 1));

                // strength
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 127) && (mouseY <= y + 133) && (player.Strength + player.LevelUpStrength + increase <= Globals.MaximumStat))
                { player.LevelUpStrength += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 127) && (mouseY <= y + 133) && (player.LevelUpStrength - decrease >= 0))
                { player.LevelUpStrength -= decrease; player.RemainderLevelUpPoints += decrease; }

                // vitality
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 146) && (mouseY <= y + 152) && (player.Vitality + player.LevelUpVitality + increase <= Globals.MaximumStat))
                { player.LevelUpVitality += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 146) && (mouseY <= y + 152) && (player.LevelUpVitality - decrease >= 0))
                { player.LevelUpVitality -= decrease; player.RemainderLevelUpPoints += decrease; }

                // dexterity
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 165) && (mouseY <= y + 171) && (player.Dexterity + player.LevelUpDexterity + increase <= Globals.MaximumStat))
                { player.LevelUpDexterity += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 165) && (mouseY <= y + 171) && (player.LevelUpDexterity - decrease >= 0))
                { player.LevelUpDexterity -= decrease; player.RemainderLevelUpPoints += decrease; }

                // intelligence
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 184) && (mouseY <= y + 190) && (player.Intelligence + player.LevelUpIntelligence + increase <= Globals.MaximumStat))
                { player.LevelUpIntelligence += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 184) && (mouseY <= y + 190) && (player.LevelUpIntelligence - decrease >= 0))
                { player.LevelUpIntelligence -= decrease; player.RemainderLevelUpPoints += decrease; }

                // magic
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 203) && (mouseY <= y + 209) && (player.Magic + player.LevelUpMagic + increase <= Globals.MaximumStat))
                { player.LevelUpMagic += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 203) && (mouseY <= y + 209) && (player.LevelUpMagic - decrease >= 0))
                { player.LevelUpMagic -= decrease; player.RemainderLevelUpPoints += decrease; }

                // agility
                if ((mouseX >= x + 195) && (mouseX <= x + 205) && (mouseY >= y + 222) && (mouseY <= y + 228) && (player.Agility + player.LevelUpAgility + increase < Globals.MaximumStat))
                { player.LevelUpAgility += increase; player.RemainderLevelUpPoints -= increase; }
                if ((mouseX >= x + 210) && (mouseX <= x + 220) && (mouseY >= y + 222) && (mouseY <= y + 228) && (player.LevelUpAgility - decrease >= 0))
                { player.LevelUpAgility -= decrease; player.RemainderLevelUpPoints += decrease; }
            }
            // ok

            if (player.RemainderLevelUpPoints == player.LevelUpPoints)
            {
                if ((mouseX > x + 30) && (mouseX < x + 30 + 74) && (mouseY > y + 292) && (mouseY <= y + 292 + 20))
                    Hide();
            }
            else
            {
                // change  
                if ((mouseX > x + 154) && (mouseX < x + 154 + 74) && (mouseY > y + 292) && (mouseY <= y + 292 + 20))
                {
                    ((MainGame)Cache.DefaultState).ChangeStats();
                    Hide();
                }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
