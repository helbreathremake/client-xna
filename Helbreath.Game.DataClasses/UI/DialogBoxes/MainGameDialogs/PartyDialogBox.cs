﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    class PartyDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 17; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Party; } }

        int mouseX;
        int mouseY;
        int x;
        int y;
        float transparency;
        Player player;
        int width;
        int height;

        public PartyDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }


        public PartyDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 48, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party", FontType.DisplayNameSize13Spacing1, new Vector2(x + 58, y - 9), 140, Cache.Colors[GameColor.Orange]);

            int lineIndex = 0;
            Vector2 loc = new Vector2(x, y + 30);
            if (player.HasParty)
            {
                Party party = player.Party;

                //Mode
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Mode: {0}", party.Mode.ToString()), FontType.DialogsSmallSize8, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                lineIndex++;

                //Party Member Count
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Party Members: {0}/{1}", party.MemberCount, Globals.MaximumPartyMembers), FontType.DialogsSmallSize8, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                lineIndex++;

                //Party Leader
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Leader: " + party.MemberInfo[party.LeaderId].Name, FontType.DialogsSmallSize8, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                lineIndex++;

                //Party Name
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Party Name: " + party.Name, FontType.DialogsSmallSize8, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                lineIndex++;

                foreach (KeyValuePair<int, PartyMember> memberInfo in player.Party.MemberInfo)
                {
                    PartyMember member = memberInfo.Value;

                    //Name
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, member.Name, FontType.DialogsSmallSize8, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                    lineIndex++;

                    //Location
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Location: {0} Cords: ({1}),({2})", member.MapName, member.X, member.Y), FontType.DialogsSmallerSize7, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                    lineIndex++;

                    //Health & Mana
                    SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, string.Format("Health: {0}% Mana: {1}%", ((float)member.HP / (float)member.MaxHP) * 100, ((float)member.MP / (float)member.MaxMP) * 100), FontType.DialogsSmallerSize7, new Vector2(loc.X, loc.Y + (lineIndex * 15)), width, Cache.Colors[GameColor.Orange]);
                    lineIndex += 2;
                }
            }
            else
            {
                //Mode
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Not in a party", FontType.DialogsSmallSize8, new Vector2(x, y - 9), width, Cache.Colors[GameColor.Orange]);
            }

            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftClicked()
        {

        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
