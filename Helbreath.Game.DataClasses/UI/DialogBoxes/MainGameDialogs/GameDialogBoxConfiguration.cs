﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    public class GameDialogBoxConfiguration
    {
        private int x;
        private int y;
        private int page;
        private int maxPages;

        private bool isScrollable;
        private bool moveable;
        private bool hidden;
        private bool alwaysVisible;

        private DialogBoxState state;

        //For creating default dialogboxesConfigurations 
        public GameDialogBoxConfiguration(GameDialogBoxType type, Resolution resolution)
        {
            DefaultDialogValues(type, resolution);
        }

        //Dialog Box Default Locations and values
        //Values include: X, Y, hidden, alwaysVisiable, Page, isScrollable, state, moveable
        public void DefaultDialogValues(GameDialogBoxType type, Resolution resolution)
        {
            //default settings
            hidden = true;
            x = 32;
            y = 32;
            page = 1;
            alwaysVisible = false;
            state = DialogBoxState.Normal;
            maxPages = 1; // should be 1?
            isScrollable = false;
            moveable = true;

            //Change if needed to based on type
            switch (type)
            {
                case GameDialogBoxType.HPIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        case Resolution.Classic: x = 6; y = 535; break;
                        case Resolution.Standard: x = 8; y = 655; break;
                        case Resolution.Large: x = 8; y = 735; break;
                    }
                    break;
                case GameDialogBoxType.MPIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        case Resolution.Classic: x = 6; y = 555; break;
                        case Resolution.Standard: x = 8; y = 675; break;
                        case Resolution.Large: x = 8; y = 755; break;
                    }
                    break;
                case GameDialogBoxType.HungerIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 3; y = 455; break;
                        case Resolution.Classic: x = 6; y = 575; break;
                        case Resolution.Standard: x = 8; y = 695; break;
                        case Resolution.Large: x = 8; y = 775; break;
                    }
                    break;
                case GameDialogBoxType.SPIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 127; y = 423; break;
                        case Resolution.Classic: x = 220; y = 545; break;
                        case Resolution.Standard: x = 453; y = 665; break;
                        case Resolution.Large: x = 453; y = 745; break;
                    }
                    break;
                case GameDialogBoxType.LocationIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 127; y = 439; break;
                        case Resolution.Classic: x = 220; y = 560; break;
                        case Resolution.Standard: x = 453; y = 680; break;
                        case Resolution.Large: x = 453; y = 760; break;
                    }
                    break;
                case GameDialogBoxType.WeaponIconPanel:
                    moveable = false;
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 323; y = 430; break;
                        case Resolution.Classic: x = 475; y = 550; break;
                        case Resolution.Standard: x = 937; y = 670; break;
                        case Resolution.Large: x = 937; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.CharacterIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 372; y = 430; break;
                        case Resolution.Classic: x = 525; y = 550; break;
                        case Resolution.Standard: x = 986; y = 670; break;
                        case Resolution.Large: x = 986; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.InventoryIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 412; y = 430; break;
                        case Resolution.Classic: x = 565; y = 550; break;
                        case Resolution.Standard: x = 1026; y = 670; break;
                        case Resolution.Large: x = 1026; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.SpellBookIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 452; y = 430; break;
                        case Resolution.Classic: x = 605; y = 550; break;
                        case Resolution.Standard: x = 1066; y = 670; break;
                        case Resolution.Large: x = 1066; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.SkillsIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 492; y = 430; break;
                        case Resolution.Classic: x = 645; y = 550; break;
                        case Resolution.Standard: x = 1106; y = 670; break;
                        case Resolution.Large: x = 1106; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.ChatIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 532; y = 430; break;
                        case Resolution.Classic: x = 685; y = 550; break;
                        case Resolution.Standard: x = 1146; y = 670; break;
                        case Resolution.Large: x = 1146; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.LevelUpIconPanel:
                    hidden = true;
                    alwaysVisible = false;
                    moveable = false;
                    switch (resolution)
                    {
                        case Resolution.Classic: x = 100; y = 100; break;
                        case Resolution.Standard: x = 100; y = 100; break;
                        case Resolution.Large: x = 100; y = 100; break;
                    }
                    break;
                case GameDialogBoxType.SystemIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 572; y = 430; break;
                        case Resolution.Classic: x = 725; y = 550; break;
                        case Resolution.Standard: x = 1186; y = 670; break;
                        case Resolution.Large: x = 1186; y = 750; break;
                    }
                    break;
                case GameDialogBoxType.LockIconPanel:
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 615; y = 450; break;
                        case Resolution.Classic: x = 775; y = 570; break;
                        case Resolution.Standard: x = 1236; y = 680; break;
                        case Resolution.Large: x = 1236; y = 760; break;
                    }
                    break;
                case GameDialogBoxType.HotBar:
                    state = DialogBoxState.Vertical;
                    hidden = false;
                    alwaysVisible = true;
                    moveable = false;
                    switch (resolution)
                    {
                        case Resolution.Classic: x = 2; y = 2; break;
                        case Resolution.Standard: x = 2; y = 2; break;
                        case Resolution.Large: x = 2; y = 2; break;
                    }
                    break;
                case GameDialogBoxType.PartyBar:
                    alwaysVisible = true;
                    switch (resolution)
                    {
                        //case Resolution.Classic: x = 430; y = 40; break;
                        case Resolution.Classic: x = 590; y = 40; break;
                        case Resolution.Standard: x = 1180; y = 40; break;
                        case Resolution.Large: x = 1180; y = 40; break;
                    }
                    break;
                case GameDialogBoxType.MiniMap:
                    hidden = false;
                    x = Cache.DefaultState.Display.ResolutionWidth;
                    y = 0;
                    break;
                case GameDialogBoxType.SpellBook:
                    maxPages = 10;
                    isScrollable = true;
                    break;
                case GameDialogBoxType.Skill:
                    isScrollable = true;
                    break;
                case GameDialogBoxType.Chat:
                    isScrollable = true;
                    break;
                case GameDialogBoxType.System:
                    maxPages = 4;
                    isScrollable = true;
                    break;
                case GameDialogBoxType.CharacterV2:
                    isScrollable = true;
                    break;
                case GameDialogBoxType.Merchant:
                    state = DialogBoxState.Buy;
                    isScrollable = true;
                    break;
                //case GameDialogBoxType.SpellBookV2:                                       
                //    isScrollable = true;
                //    break;
                case GameDialogBoxType.MagicShop:
                    maxPages = 10;
                    isScrollable = true;
                    break;
                case GameDialogBoxType.Stats:
                    {
                        maxPages = 5;
                        break;
                    }
                case GameDialogBoxType.TradeV2Confirmation:
                case GameDialogBoxType.PartyRequest:
                case GameDialogBoxType.Informational:
                case GameDialogBoxType.HUDResetConfirmation:
                    {
                        x = 100;
                        y = 100;
                        hidden = false;
                        break;
                    }
                case GameDialogBoxType.InventoryV1:
                case GameDialogBoxType.LevelUpV1:
                case GameDialogBoxType.Quest:
                case GameDialogBoxType.Party:
                case GameDialogBoxType.Manufacture:
                case GameDialogBoxType.TradeV2:
                case GameDialogBoxType.UpgradeV2:
                case GameDialogBoxType.WarehouseV2:
                case GameDialogBoxType.Alchemy:
                case GameDialogBoxType.Slate:
                case GameDialogBoxType.Craft:
                case GameDialogBoxType.Information:
                case GameDialogBoxType.HotBarMagic:
                default: break;
            }
        }

        //For Creating DialogBoxes to populate the game with Configuration info from this object
        public IGameDialogBox DialogBoxFactory(GameDialogBoxType type)
        {
            IGameDialogBox box = null;
            switch (type)
            {
                case GameDialogBoxType.Alchemy: { box = new AlchemyDialogBox(this); break; }
                case GameDialogBoxType.CharacterIconPanel: { box = new CharacterIconPanelDialogBox(this); break; }
                case GameDialogBoxType.CharacterV2: { box = new CharacterV2DialogBox(this); break; }
                case GameDialogBoxType.Chat: { box = new ChatDialogBox(this); break; }
                case GameDialogBoxType.ChatIconPanel: { box = new ChatIconPanelDialogBox(this); break; }
                case GameDialogBoxType.HotBar: { box = new HotBarDialogBox(this); break; }
                case GameDialogBoxType.HPIconPanel: { box = new HPIconPanelDialogBox(this); break; }
                case GameDialogBoxType.HungerIconPanel: { box = new HungerIconPanelDialogBox(this); break; }
                case GameDialogBoxType.InventoryIconPanel: { box = new InventoryIconPanelDialogBox(this); break; }
                case GameDialogBoxType.InventoryV1: { box = new InventoryV1DialogBox(this); break; }
                case GameDialogBoxType.LevelUpV1: { box = new LevelUpV1DialogBox(this); break; }
                case GameDialogBoxType.LocationIconPanel: { box = new LocationIconPanelDialogBox(this); break; }
                case GameDialogBoxType.LockIconPanel: { box = new LockIconPanelDialogBox(this); break; }
                case GameDialogBoxType.MagicShop: { box = new MagicShopDialogBox(this); break; }
                case GameDialogBoxType.Manufacture: { box = new ManufactureDialogBox(this); break; }
                case GameDialogBoxType.Merchant: { box = new MerchantDialogBox(this); break; }
                case GameDialogBoxType.MiniMap: { box = new MiniMapDialogBox(this); break; }
                case GameDialogBoxType.MPIconPanel: { box = new MPIconPanelDialogBox(this); break; }
                case GameDialogBoxType.Party: { box = new PartyDialogBox(this); break; }
                case GameDialogBoxType.PartyBar: { box = new PartyBarDialogBox(this); break; }
                case GameDialogBoxType.Quest: { box = new QuestDialogBox(this); break; }
                case GameDialogBoxType.Stats: { box = new StatsDialogBox(this); break; }
                case GameDialogBoxType.Skill: { box = new SkillDialogBox(this); break; }
                case GameDialogBoxType.SpellBook: { box = new SpellBookDialogBox(this); break; }
                case GameDialogBoxType.SpellBookIconPanel: { box = new SpellBookIconPanelDialogBox(this); break; }
                case GameDialogBoxType.SPIconPanel: { box = new SPIconPanelDialogBox(this); break; }
                case GameDialogBoxType.SkillsIconPanel: { box = new SkillsIconPanelDialogBox(this); break; }
                case GameDialogBoxType.System: { box = new SystemDialogBox(this); break; }
                case GameDialogBoxType.SystemIconPanel: { box = new SystemIconPanelDialogBox(this); break; }
                case GameDialogBoxType.TradeV2: { box = new TradeV2DialogBox(this); break; }
                case GameDialogBoxType.UpgradeV2: { box = new UpgradeV2DialogBox(this); break; }
                case GameDialogBoxType.WarehouseV2: { box = new WarehouseV2DialogBox(this); break; }
                case GameDialogBoxType.WeaponIconPanel: { box = new WeaponIconPanelDialogBox(this); break; }
                case GameDialogBoxType.CreateSet: { box = new CreateSetDialogBox(this); break; }
                case GameDialogBoxType.ClearSet: { box = new ClearSetDialogBox(this); break; }
                case GameDialogBoxType.Craft: { box = new CraftDialogBox(this); break; }
                case GameDialogBoxType.Slate: { box = new SlateDialogBox(this); break; }
                case GameDialogBoxType.Information: { box = new InformationDialogBox(this); break; }
                case GameDialogBoxType.LevelUpIconPanel: { box = new LevelUpIconPanelDialogBox(this); break; }
                case GameDialogBoxType.HotBarMagic: { box = new HotBarMagicDialogBox(this); break; }
                default: { break; }
            }
            return box;
        }

        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }
        public int Page { get { return page; } set { page = value; } }
        public int MaxPages { get { return maxPages; } set { maxPages = value; } }

        public bool IsScrollable { get { return isScrollable; } set { isScrollable = value; } }
        public bool Moveable { get { return moveable; } set { moveable = value; } }
        public bool Hidden { get { return hidden; } set { hidden = value; } }
        public bool AlwaysVisible { get { return alwaysVisible; } set { alwaysVisible = value; } }
        public bool Visible { get { return ((alwaysVisible && Cache.GameSettings.LockedDialogs) || !hidden); } }

        public DialogBoxState State { get { return state; } set { state = value; } }
    }
}
