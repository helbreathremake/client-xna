﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;
using System.IO;
using System.Reflection;

namespace Helbreath.Game.Assets.UI
{
    public class SystemDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 6; //Main texture
        int spriteFrame = 1; //rectangle in texutre
        AnimationFrame frame;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.System; } }

        int mouseX;
        int mouseY;
        int x;
        int y;
        float transparency;
        Player player;
        bool settingsChanged;

        public SystemDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }


        public SystemDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            switch (config.Page)
            {
                case 1:
                    {
                        //screen mode

                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Screen Mode", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 35)), Color.White);
                        switch (Cache.GameSettings.DisplayMode)
                        {
                            case DisplayMode.Fullscreen:
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                if ((mouseY > y + 35) && (mouseY < y + 47) && (mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 35)), Cache.Colors[GameColor.Orange]);
                                }
                                if ((mouseY > y + 55) && (mouseY < y + 67) && (mouseX > x + 118) && (mouseX < x + 116 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed Fullscreen").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 55)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 55)), Cache.Colors[GameColor.Orange]);
                                }
                                break;
                            case DisplayMode.Windowed:
                                if ((mouseY > y + 35) && (mouseY < y + 47) && (mouseX > x + 118) && (mouseX < x + 116 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Fullscreen").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 35)), Cache.Colors[GameColor.Orange]);
                                }
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                if ((mouseY > y + 55) && (mouseY < y + 67) && (mouseX > x + 118) && (mouseX < x + 116 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed Fullscreen").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 55)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 55)), Cache.Colors[GameColor.Orange]);
                                }
                                break;
                            case DisplayMode.WindowedFullscreen:
                                if ((mouseY > y + 35) && (mouseY < y + 47) && (mouseX > x + 118) && (mouseX < x + 116 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Fullscreen").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 35)), Cache.Colors[GameColor.Orange]);
                                }
                                if ((mouseY > y + 35) && (mouseY < y + 47) && (mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed").X))
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 35)), Cache.Colors[GameColor.Friendly]);
                                }
                                else
                                {
                                    SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 35)), Cache.Colors[GameColor.Orange]);
                                }
                                SpriteHelper.DrawTextWithShadow(spriteBatch, "Windowed Fullscreen", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 55)), Cache.Colors[GameColor.Friendly]);
                                break;
                        }

                        //detail
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Detail Level", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 75)), Color.White);

                        if (Cache.GameSettings.DetailMode == GraphicsDetail.Low || ((mouseY > y + 75) && (mouseY < y + 87) && (mouseX > x + 118) && (mouseX < x + 121 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Low").X)))
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Low", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 75)), Cache.Colors[GameColor.Friendly]);
                        else SpriteHelper.DrawTextWithShadow(spriteBatch, "Low", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 75)), Cache.Colors[GameColor.Orange]);

                        if (Cache.GameSettings.DetailMode == GraphicsDetail.Medium || ((mouseY > y + 75) && (mouseY < y + 87) && (mouseX > x + 153) && (mouseX < x + 153 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Medium").X)))
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Medium", FontType.DialogSize8Bold, new Vector2((int)(x + 153 + 1), (int)(y + 75)), Cache.Colors[GameColor.Friendly]);
                        else SpriteHelper.DrawTextWithShadow(spriteBatch, "Medium", FontType.DialogSize8Bold, new Vector2((int)(x + 153 + 1), (int)(y + 75)), Cache.Colors[GameColor.Orange]);

                        if (Cache.GameSettings.DetailMode == GraphicsDetail.High || ((mouseY > y + 75) && (mouseY < y + 87) && (mouseX > x + 205) && (mouseX < x + 205 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("High").X)))
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "High", FontType.DialogSize8Bold, new Vector2((int)(x + 205 + 1), (int)(y + 75)), Cache.Colors[GameColor.Friendly]);
                        else SpriteHelper.DrawTextWithShadow(spriteBatch, "High", FontType.DialogSize8Bold, new Vector2((int)(x + 205 + 1), (int)(y + 75)), Cache.Colors[GameColor.Orange]);

                        //sounds
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Sound", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 95)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.SoundsOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 84 + 1), (int)(y + 95)), Cache.GameSettings.SoundsOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        //music
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Music", FontType.DialogSize8Bold, new Vector2((int)(x + 123 + 1), (int)(y + 95)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.SoundsOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 180 + 1), (int)(y + 95)), Cache.GameSettings.MusicOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        //soundvolume
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Sound Volume", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 115)), Color.White);
                        SpriteHelper.DrawSlider(spriteBatch, x + 130, y + 115, (int)Cache.GameSettings.SoundVolume);
                        //musicvolume
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Music Volume", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 135)), Color.White);
                        SpriteHelper.DrawSlider(spriteBatch, x + 130, y + 135, (int)Cache.GameSettings.MusicVolume);
                        //transparency
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Transparency", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 155)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.TransparentDialogs ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 155)), Cache.GameSettings.TransparentDialogs ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        //minimap
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Mini Map", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 175)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MiniMap].Config.Visible ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 175)), ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MiniMap].Config.Visible ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        //resolution
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Resolution", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 195)), Color.White);
                        if (Cache.GameSettings.Resolution == Resolution.Classic || ((mouseY > y + 195) && (mouseY < y + 207) && (mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("800x600").X)))
                            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "800x600", new Vector2((int)(x + 118), (int)(y + 195)), Cache.Colors[GameColor.Friendly]);
                        else spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "800x600", new Vector2((int)(x + 118), (int)(y + 195)), Cache.Colors[GameColor.Orange]);
                        if (Cache.GameSettings.Resolution == Resolution.Standard || ((mouseY > y + 195) && (mouseY < y + 207) && (mouseX > x + 185) && (mouseX < x + 185 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("1280x720").X)))
                            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "1280x720", new Vector2((int)(x + 185 + 1), (int)(y + 195)), Cache.Colors[GameColor.Friendly]);
                        else spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "1280x720", new Vector2((int)(x + 185 + 1), (int)(y + 195)), Cache.Colors[GameColor.Orange]);
                        if (Cache.GameSettings.Resolution == Resolution.Large || ((mouseY > y + 215) && (mouseY < y + 227) && (mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("1280x800").X)))
                            spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "1280x800", new Vector2((int)(x + 118 + 1), (int)(y + 215)), Cache.Colors[GameColor.Friendly]);
                        else spriteBatch.DrawString(Cache.Fonts[FontType.DialogSize8Bold], "1280x800", new Vector2((int)(x + 118 + 1), (int)(y + 215)), Cache.Colors[GameColor.Orange]);

                        //inventory dialog
                        //SpriteHelper.DrawTextWithShadow(spriteBatch, "Inventory", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 215)), Color.White);
                        //if (!Cache.GameSettings.InventoryV2)
                        //{
                        //    SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 215)), Cache.Colors[GameColor.Friendly]);//
                        //    if ((mouseY > y + 215) && (mouseY < y + 227) && (mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Version2").X))
                        //        SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 215)), Cache.Colors[GameColor.Friendly]);
                        //    else SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 215)), Cache.Colors[GameColor.Orange]);
                        //}
                        //else
                        //{
                        //    if ((mouseY > y + 215) && (mouseY < y + 227) && (mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Classic").X))
                        //        SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 215)), Cache.Colors[GameColor.Friendly]);//
                        //    else SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.DialogSize8Bold, new Vector2((int)(x + 118), (int)(y + 215)), Cache.Colors[GameColor.Orange]);//
                        //    SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.DialogSize8Bold, new Vector2((int)(x + 178 + 1), (int)(y + 215)), Cache.Colors[GameColor.Friendly]);
                        //}
                        ////spell dialog
                        //SpriteHelper.DrawTextWithShadow(spriteBatch, "Spell Book", FontType.Dialog, new Vector2((int)(x + 31), (int)(y + 235)), Color.White);
                        //if (!Cache.GameSettings.SpellBookV2)
                        //{
                        //    SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.Dialog, new Vector2((int)(x + 118), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.Friendly]);//
                        //    if ((mouseY > y + 235) && (mouseY < y + 247) && (mouseX > x + 178) && (mouseX < x + 178 + Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString("Version2").X))
                        //        SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.Dialog, new Vector2((int)(x + 178 + 1), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.Friendly]);
                        //    else SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.Dialog, new Vector2((int)(x + 178 + 1), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);
                        //}
                        //else
                        //{
                        //    if ((mouseY > y + 235) && (mouseY < y + 247) && (mouseX > x + 118) && (mouseX < x + 118 + Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString("Classic").X))
                        //        SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.Dialog, new Vector2((int)(x + 118), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.Friendly]);//
                        //    else SpriteHelper.DrawTextWithShadow(spriteBatch, "Classic", FontType.Dialog, new Vector2((int)(x + 118), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.DialogsV2Orange]);//
                        //    SpriteHelper.DrawTextWithShadow(spriteBatch, "Version2", FontType.Dialog, new Vector2((int)(x + 178 + 1), (int)(y + 235)), Cache.DefaultState.Display.FontColours[FontType.Friendly]);
                        //}

                        //logout
                        if (!Cache.GameSettings.IsLoggingOut)
                        {
                            if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Logout", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Continue", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }

                        //restart
                        if (!Cache.GameSettings.IsRestarting && player.IsDead)
                        {
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Restart", FontType.GeneralSize10, new Vector2(x + 188, y + 283), 65, Color.White * transparency);
                        }

                        //time
                        SpriteHelper.DrawTextWithShadow(spriteBatch, DateTime.Now.ToString("MMM dd hh:mm:ss tt"), FontType.DialogSize8Bold, new Vector2((int)(x + 78), (int)(y + 255)), Color.White);


                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 55, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Settings", FontType.DisplayNameSize13Spacing1, new Vector2(x + 65, y - 9), 140, Cache.Colors[GameColor.Orange]);

                        /* PAGE # */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 232, y + 315), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(x + 204, y + 314), 25, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                case 2:
                    {
                        //LOW HEALTH INDICATOR
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Low Health Indicator:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 35)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.LowHealthIndicator ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 155), (int)(y + 35)), Cache.GameSettings.LowHealthIndicator ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                        if (Cache.GameSettings.LowHealthIndicator)
                        {

                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Health Limit", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 55)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "1", FontType.DialogSize8Bold, new Vector2((int)(x + 105), (int)(y + 55)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "99", FontType.DialogSize8Bold, new Vector2((int)(x + 233), (int)(y + 55)), Color.White);
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(x + 122, y + 62, 100, 2), Color.White);
                            SpriteHelper.DrawSlider(spriteBatch, x + 112, y + 55, (int)Cache.GameSettings.LowHealthFlashValue);
                        }
                        else
                        {
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Health Limit", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 55)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "1", FontType.DialogSize8Bold, new Vector2((int)(x + 105), (int)(y + 55)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "99", FontType.DialogSize8Bold, new Vector2((int)(x + 233), (int)(y + 55)), Color.Gray);
                            spriteBatch.Draw(Cache.BackgroundTexture, new Rectangle(x + 122, y + 62, 100, 2), Color.Gray);
                            SpriteHelper.DrawSlider(spriteBatch, x + 112, y + 55, (int)Cache.GameSettings.LowHealthFlashValue);
                        }

                        //REGEN CIRCLES             
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Regen Rings:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 75)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.RegenRings ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 108), (int)(y + 75)), Cache.GameSettings.RegenRings ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                        if (Cache.GameSettings.RegenRings)
                        {
                            //HP CIRCLE              
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "HP:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 95)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.HpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 51), (int)(y + 95)), Cache.GameSettings.HpRing ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                            //MP CIRCLE               
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "MP:", FontType.DialogSize8Bold, new Vector2((int)(x + 81), (int)(y + 95)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.MpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 103), (int)(y + 95)), Cache.GameSettings.MpRing ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                            //SP CIRCLE
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "SP:", FontType.DialogSize8Bold, new Vector2((int)(x + 131), (int)(y + 95)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.SpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 151), (int)(y + 95)), Cache.GameSettings.SpRing ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                            //EXP CIRCLE              
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "EXP:", FontType.DialogSize8Bold, new Vector2((int)(x + 181), (int)(y + 95)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.ExpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 206), (int)(y + 95)), Cache.GameSettings.ExpRing ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        }
                        else
                        {
                            //HP CIRCLE
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "HP:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 95)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.HpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 51), (int)(y + 95)), Color.Gray);

                            //MP CIRCLE
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "MP:", FontType.DialogSize8Bold, new Vector2((int)(x + 81), (int)(y + 95)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.MpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 103), (int)(y + 95)), Color.Gray);

                            //SP CIRCLE
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "SP:", FontType.DialogSize8Bold, new Vector2((int)(x + 131), (int)(y + 95)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.SpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 151), (int)(y + 95)), Color.Gray);

                            //EXP CIRCLE
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "EXP:", FontType.DialogSize8Bold, new Vector2((int)(x + 181), (int)(y + 95)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.ExpRing ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 206), (int)(y + 95)), Color.Gray);
                        }


                        //MAX CHAT HISTORY
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Max Text Above Characters:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 115)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.MaxChatHistory.ToString(), FontType.DialogSize8Bold, new Vector2((int)(x + 195), (int)(y + 115)), Cache.GameSettings.MaxChatHistory == 0 ? Cache.Colors[GameColor.Orange] : Cache.Colors[GameColor.Friendly]);

                        //TEAM COLOR MODE
                        SpriteHelper.DrawTextWithShadow(spriteBatch, "Team Color Mode:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 135)), Color.White);
                        SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.TeamColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 135), (int)(y + 135)), Cache.GameSettings.TeamColorOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                        if (Cache.GameSettings.TeamColorOn)
                        {
                            //TOWN
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Town:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 155)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.TownColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 68), (int)(y + 155)), Cache.GameSettings.TownColorOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                            //GUILD
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Guild:", FontType.DialogSize8Bold, new Vector2((int)(x + 101), (int)(y + 155)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.GuildColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 137), (int)(y + 155)), Cache.GameSettings.GuildColorOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);

                            //PARTY
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Party:", FontType.DialogSize8Bold, new Vector2((int)(x + 171), (int)(y + 155)), Color.White);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.PartyColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 209), (int)(y + 155)), Cache.GameSettings.PartyColorOn ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Orange]);
                        }
                        else
                        {
                            //TOWN
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Town:", FontType.DialogSize8Bold, new Vector2((int)(x + 31), (int)(y + 155)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.TownColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 68), (int)(y + 155)), Color.Gray);

                            //GUILD
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Guild:", FontType.DialogSize8Bold, new Vector2((int)(x + 101), (int)(y + 155)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.GuildColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 137), (int)(y + 155)), Color.Gray);

                            //PARTY
                            SpriteHelper.DrawTextWithShadow(spriteBatch, "Party:", FontType.DialogSize8Bold, new Vector2((int)(x + 171), (int)(y + 155)), Color.Gray);
                            SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.GameSettings.PartyColorOn ? "On" : "Off", FontType.DialogSize8Bold, new Vector2((int)(x + 209), (int)(y + 155)), Color.Gray);
                        }

                        //logout
                        if (!Cache.GameSettings.IsLoggingOut)
                        {
                            if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Logout", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Continue", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }

                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }

                        //restart
                        if (!Cache.GameSettings.IsRestarting && player.IsDead)
                        {
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Restart", FontType.GeneralSize10, new Vector2(x + 188, y + 283), 65, Color.White * transparency);
                        }

                        //time
                        SpriteHelper.DrawTextWithShadow(spriteBatch, DateTime.Now.ToString("MMM dd hh:mm:ss tt"), FontType.DialogSize8Bold, new Vector2((int)(x + 78), (int)(y + 255)), Color.White);

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 55, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Settings", FontType.DisplayNameSize13Spacing1, new Vector2(x + 65, y - 9), 140, Cache.Colors[GameColor.Orange]);

                        /* PAGE # */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 232, y + 315), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (Config.Page) + "/" + (Config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(x + 204, y + 314), 25, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                case 3:
                    {
                        //logout
                        if (!Cache.GameSettings.IsLoggingOut)
                        {
                            if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Logout", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Continue", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }

                        //restart
                        if (!Cache.GameSettings.IsRestarting && player.IsDead)
                        {
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Restart", FontType.GeneralSize10, new Vector2(x + 188, y + 283), 65, Color.White * transparency);
                        }

                        //time
                        SpriteHelper.DrawTextWithShadow(spriteBatch, DateTime.Now.ToString("MMM dd hh:mm:ss tt"), FontType.DialogSize8Bold, new Vector2((int)(x + 78), (int)(y + 255)), Color.White);

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 55, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Settings", FontType.DisplayNameSize13Spacing1, new Vector2(x + 65, y - 9), 140, Cache.Colors[GameColor.Orange]);

                        /* PAGE # */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 232, y + 315), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(x + 204, y + 314), 25, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                case 4:
                    {
                        //logout
                        if (!Cache.GameSettings.IsLoggingOut)
                        {
                            if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Logout", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 13, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Continue", FontType.GeneralSize10, new Vector2(x + 13, y + 283), 65, Color.White * transparency);
                        }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }
                        else
                        {
                            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 100, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Save", FontType.GeneralSize10, new Vector2(x + 103, y + 283), 65, Color.White * transparency);
                        }

                        //restart
                        if (!Cache.GameSettings.IsRestarting && player.IsDead)
                        {
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[16].GetRectangle(), Color.White * transparency);
                            else spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 5].Texture, new Vector2(x + 188, y + 280), Cache.Interface[(int)SpriteId.DialogsV2 + 5].Frames[15].GetRectangle(), Color.White * transparency);
                            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Restart", FontType.GeneralSize10, new Vector2(x + 188, y + 283), 65, Color.White * transparency);
                        }

                        //time
                        SpriteHelper.DrawTextWithShadow(spriteBatch, DateTime.Now.ToString("MMM dd hh:mm:ss tt"), FontType.DialogSize8Bold, new Vector2((int)(x + 78), (int)(y + 255)), Color.White);

                        /* TITLE */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 55, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Settings", FontType.DisplayNameSize13Spacing1, new Vector2(x + 65, y - 9), 140, Cache.Colors[GameColor.Orange]);

                        /* PAGE # */
                        spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2 + 2].Texture, new Vector2(x + 232, y + 315), Cache.Interface[(int)SpriteId.DialogsV2 + 2].Frames[9].GetRectangle(), Color.White * transparency);
                        SpriteHelper.DrawTextRightWithShadow(spriteBatch, (config.Page) + "/" + (config.MaxPages), FontType.DialogsSmallerSize7, new Vector2(x + 204, y + 314), 25, Cache.Colors[GameColor.Orange]);
                        break;
                    }
                default: break;
            }

            //figure out what this is used for                  
            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            player = ((MainGame)Cache.DefaultState).Player;
        }

        public void LeftClicked() //If clicked, save info
        {
            bool settingsChanged = false;

            switch (config.Page)
            {
                case 1:
                    {
                        //screen mode
                        if ((mouseY > y + 35) && (mouseY < y + 47))
                        {
                            if ((mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Fullscreen").X))
                            {
                                Cache.GameSettings.DisplayMode = DisplayMode.Fullscreen;
                                Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                settingsChanged = true;
                            }
                            if ((mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed").X))
                            {
                                Cache.GameSettings.DisplayMode = DisplayMode.Windowed;
                                Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                settingsChanged = true;
                            }
                        }
                        if ((mouseY > y + 55) && (mouseY < y + 67))
                        {
                            if ((mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Windowed Fullscreen").X))
                            {
                                Cache.GameSettings.DisplayMode = DisplayMode.WindowedFullscreen;
                                Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                settingsChanged = true;
                            }
                        }

                        //detail
                        if ((mouseY > y + 75) && (mouseY < y + 87))
                        {
                            if ((mouseX > x + 118) && (mouseX < x + 150))
                            {
                                ((MainGame)Cache.DefaultState).ChangeDetailMode(GraphicsDetail.Low);
                                ((MainGame)Cache.DefaultState).AddEvent("Detail Level : Low");
                                settingsChanged = true;
                            }
                            if ((mouseX > x + 151) && (mouseX < x + 200))
                            {
                                ((MainGame)Cache.DefaultState).ChangeDetailMode(GraphicsDetail.Medium);
                                ((MainGame)Cache.DefaultState).AddEvent("Detail Level : Medium");
                                settingsChanged = true;
                            }
                            if ((mouseX > x + 200) && (mouseX < x + 234))
                            {
                                ((MainGame)Cache.DefaultState).ChangeDetailMode(GraphicsDetail.High);
                                ((MainGame)Cache.DefaultState).AddEvent("Detail Level : High");
                                settingsChanged = true;
                            }
                        }

                        // sounds and music
                        if ((mouseY > y + 95) && (mouseY < y + 107))
                        {
                            if ((mouseX > x + 24) && (mouseX < x + 115))
                            {
                                if (Cache.GameSettings.SoundsOn)
                                {
                                    Cache.GameSettings.SoundsOn = false;
                                    ((MainGame)Cache.DefaultState).AddEvent("Sounds OFF");
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.SoundsOn = true;
                                    ((MainGame)Cache.DefaultState).AddEvent("Sounds ON");
                                    settingsChanged = true;
                                }
                            }

                            if ((mouseX > x + 116) && (mouseX < x + 202))
                            {
                                if (Cache.GameSettings.MusicOn)
                                {
                                    AudioHelper.StopMusic();
                                    Cache.GameSettings.MusicOn = false;
                                    ((MainGame)Cache.DefaultState).AddEvent("Music OFF");
                                    settingsChanged = true;
                                }
                                else
                                {
                                    AudioHelper.PlayMusic(player.Map.MusicName);
                                    Cache.GameSettings.MusicOn = true;
                                    ((MainGame)Cache.DefaultState).AddEvent("Music ON");
                                    settingsChanged = true;
                                }
                            }
                        }
                        // sound volume
                        /*if ((mouseX > x + 127) && (mouseX < x + 238)
                            && (mouseY > y + 122) && (mouseY < y + 137))
                        {
                            Cache.GameSettings.SoundVolume = MathHelper.Clamp(mouseX - (x + 127), 0, 100);
                        }
                        // music volume
                        if ((mouseX > x + 127) && (mouseX < x + 238)
                            && (mouseY > y + 139) && (mouseY < y + 155))
                        {
                            Cache.GameSettings.MusicVolume = MathHelper.Clamp(mouseX - (x + 127), 0, 100);
                            Cache.BackgroundMusic.Volume = Cache.GameSettings.MusicVolume / 100.0f; //Object was disposed off exception (turned off music and turned it back on)
                        }*/

                        //minimap
                        if ((mouseX > x + 28) && (mouseX < x + 235)
                             && (mouseY > y + 175) && (mouseY < y + 187))
                        {
                            if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MiniMap].Config.Visible)
                                ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MiniMap].Hide();
                            else
                            {
                                ((IGameState)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MiniMap].Show();
                                ((IGameState)Cache.DefaultState).BringToFront(GameDialogBoxType.MiniMap);
                            }
                            settingsChanged = true;
                        }

                        // dialog transparency
                        if ((mouseX > x + 28) && (mouseX < x + 235)
                             && (mouseY > y + 155) && (mouseY < y + 167))
                        {
                            Cache.GameSettings.TransparentDialogs = !Cache.GameSettings.TransparentDialogs;
                            settingsChanged = true;
                        }

                        //resolution
                        if ((mouseY > y + 195) && (mouseY < y + 207))
                        {
                            if ((mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("800x600").X))
                            {
                                if (Cache.GameSettings.Resolution != Resolution.Classic)
                                {
                                    Cache.GameSettings.Resolution = Resolution.Classic;
                                    Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                    settingsChanged = true;
                                }
                            }
                            if ((mouseX > x + 185) && (mouseX < x + 185 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("1280x720").X))
                            {
                                if (Cache.GameSettings.Resolution != Resolution.Standard)
                                {
                                    Cache.GameSettings.Resolution = Resolution.Standard;
                                    Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                    settingsChanged = true;
                                }
                            }
                        }

                        if ((mouseY > y + 215) && (mouseY < y + 227))
                        {
                            if ((mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("1280x800").X))
                            {
                                if (Cache.GameSettings.Resolution != Resolution.Large)
                                {
                                    Cache.GameSettings.Resolution = Resolution.Large;
                                    Cache.DefaultState.DisplayModeOrResolutionChange = true;
                                    settingsChanged = true;
                                }
                            }
                        }
                        //inventory mode
                        //if ((mouseY > y + 215) && (mouseY < y + 227))
                        //{
                        //    if ((mouseX > x + 118) && (mouseX < x + 118 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Classic").X))
                        //    {
                        //        Cache.GameSettings.InventoryV2 = false;
                        //        settingsChanged = true;
                        //    }
                        //    if ((mouseX > x + 178) && (mouseX < x + 178 + Cache.Fonts[FontType.DialogSize8Bold].MeasureString("Version2").X))
                        //    {
                        //        if (Cache.DefaultState.Display.Resolution == Resolution.Classic)
                        //            ((IGameState)Cache.DefaultState).ShowMessageBox(new GameMessageBox(GameDialogBoxType.Informational, "Classic resolution cannot use v2 Inventory", "Ok", ""));
                        //        else Cache.GameSettings.InventoryV2 = true;
                        //        settingsChanged = true;
                        //    }
                        //}
                        ////spellbook mode
                        //if ((mouseY > y + 235) && (mouseY < y + 247))
                        //{
                        //    if ((mouseX > x + 118) && (mouseX < x + 118 + Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString("Classic").X))
                        //    {
                        //        Cache.GameSettings.SpellBookV2 = false;
                        //        settingsChanged = true;
                        //    }
                        //    if ((mouseX > x + 178) && (mouseX < x + 178 + Cache.DefaultState.Display.Fonts[FontType.Dialog].MeasureString("Version2").X))
                        //    {
                        //        if (Cache.DefaultState.Display.Resolution == Resolution.Classic)
                        //            ((IGameState)Cache.DefaultState).ShowMessageBox(new GameMessageBox(GameDialogBoxType.Informational, "Classic resolution cannot use v2 Spell Book", "Ok", ""));
                        //        else Cache.GameSettings.SpellBookV2 = true;
                        //        settingsChanged = true;
                        //    }
                        //}

                        // logout
                        if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                            if (!Cache.GameSettings.IsLoggingOut)
                            {
                                Cache.GameSettings.LogOutCount = 11;
                                Cache.GameSettings.IsLoggingOut = true;
                                settingsChanged = true;
                            }
                            else
                            {
                                ((MainGame)Cache.DefaultState).AddEvent("Logout count stopped.");
                                Cache.GameSettings.IsLoggingOut = false;
                                settingsChanged = true;
                            }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            //((MainGame)Cache.DefaultState).SaveGameSettings();
                        }


                        // restart
                        if (player.IsDead)
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                if (!Cache.GameSettings.IsRestarting)
                                {
                                    Cache.GameSettings.RestartCount = 6;
                                    Cache.GameSettings.IsRestarting = true;
                                    Hide();
                                    settingsChanged = true;
                                }

                        // save changes to settings
                        //((MainGame)Cache.DefaultState).SaveGameSettings();
                        break;
                    }
                case 2:
                    {
                        //LOW HEALTH INDICATOR
                        if ((mouseY > y + 33) && (mouseY < y + 47))
                        {
                            if ((mouseX > x + 152) && (mouseX < x + 170))
                            {
                                if (Cache.GameSettings.LowHealthIndicator)
                                {
                                    Cache.GameSettings.LowHealthIndicator = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.LowHealthIndicator = true;
                                    settingsChanged = true;
                                }
                            }
                        }

                        //LOW HEALTH FLASH VALUE
                        if ((mouseX > x + 130) && (mouseX < x + 230)
                            && (mouseY > y + 52) && (mouseY < y + 69))
                        {
                            Cache.GameSettings.LowHealthFlashValue = MathHelper.Clamp(mouseX - (x + 130), 1, 99);
                            settingsChanged = true;
                        }

                        //REGEN CIRCLES
                        if ((mouseY > y + 72) && (mouseY < y + 89))
                        {
                            if ((mouseX > x + 103) && (mouseX < x + 125))
                            {
                                if (Cache.GameSettings.RegenRings)
                                {
                                    Cache.GameSettings.RegenRings = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.RegenRings = true;
                                    settingsChanged = true;
                                }
                            }
                        }

                        //HP/MP/SP/EXP CIRCLE ON/OFF
                        if ((mouseY > y + 93) && (mouseY < y + 109))
                        {
                            //HP
                            if ((mouseX > x + 46) && (mouseX < x + 68))
                            {
                                if (Cache.GameSettings.HpRing)
                                {
                                    Cache.GameSettings.HpRing = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.HpRing = true;
                                    settingsChanged = true;
                                }
                            }

                            //MP
                            if ((mouseX > x + 98) && (mouseX < x + 120))
                            {
                                if (Cache.GameSettings.MpRing)
                                {
                                    Cache.GameSettings.MpRing = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.MpRing = true;
                                    settingsChanged = true;
                                }
                            }

                            //SP
                            if ((mouseX > x + 146) && (mouseX < x + 168))
                            {
                                if (Cache.GameSettings.SpRing)
                                {
                                    Cache.GameSettings.SpRing = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.SpRing = true;
                                    settingsChanged = true;
                                }
                            }

                            //EXP
                            if ((mouseX > x + 201) && (mouseX < x + 223))
                            {
                                if (Cache.GameSettings.ExpRing)
                                {
                                    Cache.GameSettings.ExpRing = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.ExpRing = true;
                                    settingsChanged = true;
                                }
                            }
                        }

                        //MAX CHAT HISTORY
                        if ((mouseY > y + 113) && (mouseY < y + 128))
                        {
                            if ((mouseX > x + 190) && (mouseX < x + 204))
                            {
                                if (Cache.GameSettings.MaxChatHistory < 10)
                                {
                                    Cache.GameSettings.MaxChatHistory += 1;
                                    settingsChanged = true;
                                }
                                else if (Cache.GameSettings.MaxChatHistory >= 10)
                                {
                                    Cache.GameSettings.MaxChatHistory = 0;
                                    settingsChanged = true;
                                }
                            }
                        }

                        //TEAM COLOR MODE
                        if ((mouseY > y + 133) && (mouseY < y + 148))
                        {
                            if ((mouseX > x + 132) && (mouseX < x + 152))
                            {
                                if (Cache.GameSettings.TeamColorOn)
                                {
                                    Cache.GameSettings.TeamColorOn = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.TeamColorOn = true;
                                    settingsChanged = true;
                                }
                            }
                        }


                        //TOWN/GUILD/PARTY TEAM COLOR ON/OFF
                        if ((mouseY > y + 153) && (mouseY < y + 168))
                        {
                            //TOWN
                            if ((mouseX > x + 65) && (mouseX < x + 85))
                            {
                                if (Cache.GameSettings.TownColorOn)
                                {
                                    Cache.GameSettings.TownColorOn = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.TownColorOn = true;
                                    settingsChanged = true;
                                }
                            }

                            //GUILD
                            if ((mouseX > x + 132) && (mouseX < x + 152))
                            {
                                if (Cache.GameSettings.GuildColorOn)
                                {
                                    Cache.GameSettings.GuildColorOn = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.GuildColorOn = true;
                                    settingsChanged = true;
                                }
                            }

                            //PARTY
                            if ((mouseX > x + 204) && (mouseX < x + 224))
                            {
                                if (Cache.GameSettings.PartyColorOn)
                                {
                                    Cache.GameSettings.PartyColorOn = false;
                                    settingsChanged = true;
                                }
                                else
                                {
                                    Cache.GameSettings.PartyColorOn = true;
                                    settingsChanged = true;
                                }
                            }
                        }

                        // logout
                        if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                            if (!Cache.GameSettings.IsLoggingOut)
                            {
                                Cache.GameSettings.LogOutCount = 11;
                                Cache.GameSettings.IsLoggingOut = true;
                            }
                            else
                            {
                                ((MainGame)Cache.DefaultState).AddEvent("Logout count stopped.");
                                Cache.GameSettings.IsLoggingOut = false;
                            }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            //((MainGame)Cache.DefaultState).SaveGameSettings();
                        }


                        // restart
                        if (player.IsDead)
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                if (!Cache.GameSettings.IsRestarting)
                                {
                                    Cache.GameSettings.RestartCount = 6;
                                    Cache.GameSettings.IsRestarting = true;
                                    Hide();
                                }

                        //((MainGame)Cache.DefaultState).SaveGameSettings();
                        break;
                    }
                case 3:
                    {
                        // logout
                        if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                            if (!Cache.GameSettings.IsLoggingOut)
                            {
                                Cache.GameSettings.LogOutCount = 11;
                                Cache.GameSettings.IsLoggingOut = true;
                            }
                            else
                            {
                                ((MainGame)Cache.DefaultState).AddEvent("Logout count stopped.");
                                Cache.GameSettings.IsLoggingOut = false;
                            }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            //((MainGame)Cache.DefaultState).SaveGameSettings();
                        }


                        // restart
                        if (player.IsDead)
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                if (!Cache.GameSettings.IsRestarting)
                                {
                                    Cache.GameSettings.RestartCount = 6;
                                    Cache.GameSettings.IsRestarting = true;
                                    Hide();
                                }

                        //((MainGame)Cache.DefaultState).SaveGameSettings();
                        break;
                    }
                case 4:
                    {
                        // logout
                        if ((mouseX > x + 13) && (mouseX < x + 13 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                            if (!Cache.GameSettings.IsLoggingOut)
                            {
                                Cache.GameSettings.LogOutCount = 11;
                                Cache.GameSettings.IsLoggingOut = true;
                            }
                            else
                            {
                                ((MainGame)Cache.DefaultState).AddEvent("Logout count stopped.");
                                Cache.GameSettings.IsLoggingOut = false;
                            }

                        //save
                        if ((mouseX > x + 100) && (mouseX < x + 100 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                        {
                            //((MainGame)Cache.DefaultState).SaveGameSettings();
                        }


                        // restart
                        if (player.IsDead)
                            if ((mouseX > x + 188) && (mouseX < x + 188 + 65) && (mouseY > y + 280) && (mouseY <= y + 280 + 20))
                                if (!Cache.GameSettings.IsRestarting)
                                {
                                    Cache.GameSettings.RestartCount = 6;
                                    Cache.GameSettings.IsRestarting = true;
                                    Hide();
                                }

                        //((MainGame)Cache.DefaultState).SaveGameSettings();
                        break;
                    }
            }

            if (settingsChanged) { ((MainGame)Cache.DefaultState).SaveGameSettings(); }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {

        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {

        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {

        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
            ((MainGame)Cache.DefaultState).SaveGameSettings();
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
            ((MainGame)Cache.DefaultState).SaveGameSettings();
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
