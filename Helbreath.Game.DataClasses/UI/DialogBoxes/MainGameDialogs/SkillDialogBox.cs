﻿using System;
using Helbreath.Game.Assets.State;
using HelbreathWorld.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Helbreath.Game.Assets.UI
{
    public class SkillDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 6; //Main texture
        int spriteFrame = 1; //rectangle in texutre
        AnimationFrame frame;
        private Player player;
        private int mouseX;
        private int mouseY;
        private float transparency;

        private Vector2 alchemySkillLocation;
        private Vector2 manufacturingSkillLocation;
        private Vector2 craftingSkillLocation;
        private Vector2 pretendCorpseSkillLocation;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.Skill; } }

        public SkillDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];

        }


        public SkillDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = clickedItemIndex = selectedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            //draw extra 
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 55, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Skills", FontType.DisplayNameSize13Spacing1, new Vector2(X + 65, Y - 9), 140, Cache.Colors[GameColor.Orange]);

            //draw skills data
            int yLoc = Y;
            yLoc += 45;

            foreach (int skilltype in Enum.GetValues(typeof(SkillType)))
            {
                if (skilltype == -1)
                    break;

                switch ((SkillType)skilltype)
                {
                    case SkillType.Alchemy:
                        alchemySkillLocation = new Vector2(X + 60, yLoc);
                        break;
                    case SkillType.Manufacturing:
                        manufacturingSkillLocation = new Vector2(X + 60, yLoc);
                        break;
                    case SkillType.Crafting:
                        craftingSkillLocation = new Vector2(X + 60, yLoc);
                        break;
                    case SkillType.PretendCorpse:
                        pretendCorpseSkillLocation = new Vector2(X + 60, yLoc);
                        break;
                }

                if (skilltype == (int)SkillType.Alchemy ||
                    skilltype == (int)SkillType.Manufacturing ||
                    skilltype == (int)SkillType.Crafting ||
                    skilltype == (int)SkillType.PretendCorpse)
                {
                    SpriteHelper.DrawTextWithShadow(spriteBatch, ((SkillType)skilltype).GetDescription(), FontType.DialogsSmallSize8, new Vector2(X + 60, yLoc), Color.Blue * transparency, Color.Black * transparency);
                }
                else
                    SpriteHelper.DrawTextWithShadow(spriteBatch, ((SkillType)skilltype).GetDescription(), FontType.DialogsSmallSize8, new Vector2(X + 60, yLoc), Color.White * transparency, Color.Black * transparency);

                SpriteHelper.DrawTextRightWithShadow(spriteBatch, player.Skills[skilltype].ToString(), FontType.DialogsSmallSize8, new Vector2(X + 60, yLoc), 140, Color.White * transparency, Color.Black * transparency);
                yLoc += 12;
            }

            //figure out what this is used for
            if (frame != null && Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = Cache.GameSettings.TransparentDialogs || config.Hidden ? 0.5F : 1F;
        }

        public int Y { get; set; }

        public int X { get; set; }

        public void LeftClicked()
        {
            if (pretendCorpseSkillLocation.X + 1 < mouseX && pretendCorpseSkillLocation.X + 140 + 1 > mouseX && pretendCorpseSkillLocation.Y + 1 < mouseY && pretendCorpseSkillLocation.Y + 1 + 12 > mouseY)
            {
                //TODO: update with proper pretend corpse functionality
                player.TakeDamage(9999, DamageType.Environment, 1, true);
            }

            if (alchemySkillLocation.X + 1 < mouseX && alchemySkillLocation.X + 140 + 1 > mouseX && alchemySkillLocation.Y + 1 < mouseY && alchemySkillLocation.Y + 1 + 12 > mouseY)
            {
                ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Alchemy].Toggle();
            }

            if (craftingSkillLocation.X + 1 < mouseX && craftingSkillLocation.X + 140 + 1 > mouseX && craftingSkillLocation.Y + 1 < mouseY && craftingSkillLocation.Y + 1 + 12 > mouseY)
            {
                ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Craft].Toggle();
            }

            if (manufacturingSkillLocation.X + 1 < mouseX && manufacturingSkillLocation.X + 140 + 1 > mouseX && manufacturingSkillLocation.Y + 1 < mouseY && manufacturingSkillLocation.Y + 1 + 12 > mouseY)
            {
                ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Manufacture].Toggle();
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {
        }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {
        }
    }
}