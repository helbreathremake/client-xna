﻿using Helbreath.Game.Assets.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Game.Assets.UI
{
    public class SpellBookDialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 2; //Main texture
        int spriteFrame = 8; //rectangle in texutre
        int width = -1; //frame width
        int height = -1; //frame height 
        AnimationFrame frame;
        Player player;

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.SpellBook; } }

        int mouseX;
        int mouseY;
        int X;
        int Y;
        float transparency;
        int knownSpells;


        public SpellBookDialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            knownSpells = 0;
            for (int i = (config.Page * 10) - 10; i < (config.Page * 10); i++)
                if (player.MagicLearned[i] && Cache.MagicConfiguration.ContainsKey(i))
                    knownSpells++;

        }


        public SpellBookDialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            frame = Cache.Interface[sprite].Frames[spriteFrame];
            width = frame.Width;
            height = frame.Height;
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            knownSpells = 0;
            for (int i = (config.Page * 10) - 10; i < (config.Page * 10); i++)
                if (player.MagicLearned[i] && Cache.MagicConfiguration.ContainsKey(i))
                    knownSpells++;

        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            highlightedItemIndex = -1;

            //Base //TODO base texture isn't used/right anymore because of varying dialogbox size
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y), Cache.Interface[sprite].Frames[33].GetRectangle(), Color.White * transparency);
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30))), Cache.Interface[sprite].Frames[43].GetRectangle(), Color.White * transparency);
            for (int i = 1; i <= knownSpells; i++)
            {
                switch (i)
                {
                    case 2: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[34].GetRectangle(), Color.White * transparency); break;
                    case 3: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[35].GetRectangle(), Color.White * transparency); break;
                    case 4: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[36].GetRectangle(), Color.White * transparency); break;
                    case 5: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[37].GetRectangle(), Color.White * transparency); break;
                    case 6: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[38].GetRectangle(), Color.White * transparency); break;
                    case 7: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[39].GetRectangle(), Color.White * transparency); break;
                    case 8: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[40].GetRectangle(), Color.White * transparency); break;
                    case 9: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[41].GetRectangle(), Color.White * transparency); break;
                    case 10: spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X, Y + 87 + ((i - 2) * 30)), Cache.Interface[sprite].Frames[42].GetRectangle(), Color.White * transparency); break;
                    default: break;
                }
            }

            if (knownSpells == 0) SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "No known spells", FontType.DialogsSmallSize8, new Vector2(X, Y + 58), width, Cache.Colors[GameColor.Orange]);

            //Show casting probability
            int probability = Math.Min(100, Utility.GetCastingProbability(player.Skills[(int)SkillType.Magic], config.Page, player.Level, player.Intelligence, (((MainGame)Cache.DefaultState).Weather != null ? ((MainGame)Cache.DefaultState).Weather.Type : WeatherType.Clear), player.Inventory.CastProbabilityBonus, player.SP));
            string castprob = "Casting Chance: " + probability + "%";
            SpriteHelper.DrawTextWithShadow(spriteBatch, castprob, FontType.DialogsSmallerSize7, new Vector2(X + 16, Y + 86 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30))), Cache.Colors[GameColor.Orange]);

            /* PAGINATION */
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 155, Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30))), Cache.Interface[sprite].Frames[9].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextRightWithShadow(spriteBatch, config.Page + "/" + config.MaxPages, FontType.DialogsSmallerSize7, new Vector2(X + 128, Y + 86 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30))), 25, Cache.Colors[GameColor.Orange]);

            /* PAGINATION SCROLL HIGHLIGHT*/
            if ((Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) < mouseY) && (Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) + 7 > mouseY))
            {
                if ((X + 155 < mouseX) && (X + 155 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 155 - 3, Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) - 3), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
                if ((X + 155 + 11 < mouseX) && (X + 155 + 11 + 11 > mouseX)) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 155 - 3 + 11, Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) - 3), Cache.Interface[sprite].Frames[25].GetRectangle(), Color.White * transparency); }
            }

            //Display Circle # in text at top of dialog box
            int pagenumber = config.Page;
            string circlenumber = "Circle: " + pagenumber.ToString();
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, circlenumber, FontType.DialogSize8Bold, new Vector2(X, Y + 30), 185, Color.White);

            //Cast/Drag mode
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 17, Y + 28), Cache.Interface[sprite].Frames[30].GetRectangle(), Color.White * transparency);
            if ((X + 17 + 1 < mouseX) && (X + 17 + 1 + 23 > mouseX) && (Y + 28 + 1 < mouseY) && (Y + 28 + 1 + 12 > mouseY))
            {
                if (Cache.GameSettings.SpellBookCasting) { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 17 + 1 - 3, Y + 28 + 1 - 3), Cache.Interface[sprite].Frames[32].GetRectangle(), Color.White * transparency); }
                else { spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 17 + 1 - 3, Y + 28 + 1 - 3), Cache.Interface[sprite].Frames[31].GetRectangle(), Color.White * transparency); }
            }

            if (Cache.GameSettings.SpellBookCasting) { SpriteHelper.DrawTextWithShadow(spriteBatch, "Cast", FontType.DialogsSmallerSize7, new Vector2(X + 20, Y + 29), Cache.Colors[GameColor.Friendly]); }
            else { SpriteHelper.DrawTextWithShadow(spriteBatch, "Drag", FontType.DialogsSmallerSize7, new Vector2(X + 20, Y + 29), Cache.Colors[GameColor.Orange]); }

            //Display SpellBook name + Cost
            int spellIndex = 0;
            for (int i = (config.Page * 10) - 10; i < (config.Page * 10); i++)
                if (player.MagicLearned[i] && Cache.MagicConfiguration.ContainsKey(i))
                {
                    bool cantCast = player.IsCasting || !player.CanCast(i) || !player.HandsFree;
                    bool hover = Utility.IsSelected(mouseX, mouseY, X + 15, Y + 50 + (spellIndex * 30), X + 15 + 30 + 125, Y + 50 + (spellIndex * 30) + 30);
                    spriteBatch.Draw(Cache.Interface[sprite + 3].Texture, new Vector2(X + 15 + 30, Y + 50 + (spellIndex * 30)), Cache.Interface[sprite + 3].Frames[24].GetRectangle(), Color.White);
                    spriteBatch.Draw(Cache.Interface[sprite + 2].Texture, new Vector2(X + 15, Y + 50 + (spellIndex * 30)), Cache.Interface[sprite + 2].Frames[(hover ? Cache.MagicConfiguration[i].SmallIconSpriteFrame + 1 : Cache.MagicConfiguration[i].SmallIconSpriteFrame)].GetRectangle(), (!cantCast ? Color.White : Color.Gray) * transparency);
                    SpriteHelper.DrawTextWithShadow(spriteBatch, Cache.MagicConfiguration[i].Name, FontType.DialogsSmallSize8, new Vector2(X + 15 + 34, Y + 50 + (spellIndex * 30) + 3), (hover ? (!cantCast ? Cache.Colors[GameColor.Friendly] : Cache.Colors[GameColor.Enemy]) : !cantCast ? Cache.Colors[GameColor.Orange] : Color.DimGray));
                    SpriteHelper.DrawTextWithShadow(spriteBatch, string.Format("Mana: {0}", player.RequiredMana(i)), FontType.DialogsSmallerSize7, new Vector2(X + 15 + 34, Y + 50 + (spellIndex * 30) + 15), Color.White);

                    if (hover) highlightedItemIndex = i; // selected spell for cast

                    spellIndex++;
                }

            /* MAXIMIZE BUTTON */
            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 158, Y + 24), Cache.Interface[sprite].Frames[28].GetRectangle(), Color.White * transparency);
            if ((X + 158 + 1 < mouseX) && (X + 158 + 1 + 12 > mouseX) && (Y + 24 + 1 < mouseY) && (Y + 24 + 1 + 10 > mouseY))
            {
                spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(X + 158 + 1 - 3, Y + 24 + 1 - 3), Cache.Interface[sprite].Frames[26].GetRectangle(), Color.White * transparency);
            }

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(X + 18, Y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Magic", FontType.DisplayNameSize13Spacing1, new Vector2(X, Y - 9), 185, Cache.Colors[GameColor.Orange]);

            if (clickedItemIndex == -1) // if no spell dragging
                if (highlightedItemIndex != -1) // and no spell hovered
                    selectedItemIndex = highlightedItemIndex; // set hover index
                else selectedItemIndex = -1; // cancel hovered spell when not dragging

            if (Utility.IsSelected(mouseX, mouseY, X, Y, X + frame.Width, Y + 87 + 14 + ((knownSpells <= 1) ? 0 : (knownSpells - 1) * 30)))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            X = config.X;
            Y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            knownSpells = 0;
            for (int i = (config.Page * 10) - 10; i < (config.Page * 10); i++)
                if (player.MagicLearned[i] && Cache.MagicConfiguration.ContainsKey(i))
                    knownSpells++;
        }

        public void LeftClicked()
        {
            /* PAGINATION SCROLL HIGHLIGHT*/
            if ((Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) < mouseY) && (Y + 88 + ((knownSpells <= 1) ? 0 : ((knownSpells - 1) * 30)) + 7 > mouseY))
            {
                if ((X + 155 < mouseX) && (X + 155 + 11 > mouseX)) { Scroll(1); }
                if ((X + 155 + 11 < mouseX) && (X + 155 + 11 + 11 > mouseX)) { Scroll(-1); }
            }

            /* MAXIMIZE BUTTON */
            if ((X + 158 + 1 < mouseX) && (X + 158 + 1 + 12 > mouseX) && (Y + 24 + 1 < mouseY) && (Y + 24 + 1 + 10 > mouseY))
            {
                ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.MagicShop].Toggle();
            }

            /* CASTING MODE BUTTON */
            if ((X + 17 + 1 < mouseX) && (X + 17 + 1 + 23 > mouseX) && (Y + 28 + 1 < mouseY) && (Y + 28 + 1 + 12 > mouseY))
            {
                Cache.GameSettings.SpellBookCasting = !Cache.GameSettings.SpellBookCasting;
                if (Cache.GameSettings.SpellBookCasting) { ((MainGame)Cache.DefaultState).AddEvent("Casting mode activated"); }
                else { ((MainGame)Cache.DefaultState).AddEvent("Dragging mode activated"); ; }
            }

            /* CLICK A SPELL */
            if (selectedItemIndex != -1 && clickedItemIndex != -1)
            {
                if (Cache.GameSettings.SpellBookCasting) //Cast spell
                {
                    if (player.HandsFree)
                    {
                        if (player.CanCast(selectedItemIndex))
                        {
                            if (!player.MoveReady) player.SpellRequest = selectedItemIndex;
                            else if (!player.IsCasting)
                            {
                                player.Idle(player.Direction, true);
                                ((MainGame)Cache.DefaultState).Idle(player.Direction);

                                player.Cast(selectedItemIndex);
                                ((MainGame)Cache.DefaultState).Cast(selectedItemIndex);
                                Hide();
                            }
                            else Cache.DefaultState.AddEvent("Already casting a spell");
                        }
                        else Cache.DefaultState.AddEvent("Not enough mana or intelligence to cast.");
                    }
                    else Cache.DefaultState.AddEvent("Your hands must be free to cast magic.");
                }
                else //Drag Spell Icons
                {
                    ((MainGame)Cache.DefaultState).SetDraggedItem(DraggedType.MagicIcon, clickedItemIndex);
                }
            }
        }


        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId) { }

        public void LeftHeld() { }

        public void LeftDragged() { }

        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            if (Cache.GameSettings.SpellBookCasting) //Casting
            {
                switch (highlightedDialogBox)
                {
                    case GameDialogBoxType.SpellBook: //Dont hide when self drag
                        if (clickedItemIndex != -1) { Hide(); }
                        break;
                    default: Hide(); break;
                }
            }
            else //Dragging spell icons
            {
                switch (highlightedDialogBox)
                {
                    case GameDialogBoxType.HotBar:
                        // if hotbaricon at destination, remove
                        if (Cache.HotBarConfiguration.ContainsKey(highlightedDialogBoxItemIndex))
                            Cache.HotBarConfiguration.Remove(highlightedDialogBoxItemIndex);

                        //set hotbar icon at destination
                        HotBarIcon icon = new HotBarIcon(clickedItemIndex);
                        icon.Type = HotBarType.Spell;
                        Cache.HotBarConfiguration.Add(highlightedDialogBoxItemIndex, icon);
                        ((MainGame)Cache.DefaultState).SaveHotKeyConfiguration();
                        break;
                    default: break;
                }
            }
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            highlightedItemIndex = selectedItemIndex = clickedItemIndex = -1;
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
