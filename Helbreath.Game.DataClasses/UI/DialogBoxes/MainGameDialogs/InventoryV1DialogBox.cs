﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Game.Assets.State;

namespace Helbreath.Game.Assets.UI
{
    public class InventoryV1DialogBox : IGameDialogBox
    {
        int selectedItemIndex = -1;
        int clickedItemIndex = -1;
        int highlightedItemIndex = -1;
        int selectedItemIndexOffsetX; //Dragging items
        int selectedItemIndexOffsetY; //Dragging items

        GameDialogBoxConfiguration config;

        //Base graphic info
        int sprite = (int)SpriteId.DialogsV2 + 6; //Main texture
        int spriteFrame = 0; //rectangle in texutre
        int spriteFrameHover = -1; //frame to display when hovering mouse over
        int width = -1; //frame width
        int height = -1; //frame height 

        public int SelectedItemIndex { get { return selectedItemIndex; } set { selectedItemIndex = value; } }
        public int ClickedItemIndex { get { return clickedItemIndex; } set { clickedItemIndex = value; } }
        public int HighlightedItemIndex { get { return highlightedItemIndex; } set { highlightedItemIndex = value; } }
        public int SelectedItemIndexOffsetX { get { return selectedItemIndexOffsetX; } set { selectedItemIndexOffsetX = value; } }
        public int SelectedItemIndexOffsetY { get { return selectedItemIndexOffsetY; } set { selectedItemIndexOffsetY = value; } }
        public GameDialogBoxConfiguration Config { get { return config; } set { config = value; } }
        public GameDialogBoxType Type { get { return GameDialogBoxType.InventoryV1; } }

        Player player;
        int mouseX;
        int mouseY;
        int x;
        int y;
        float transparency;
        AnimationFrame frame;
        Item item;
        int itemIndex;
        int itemFrame;
        bool selected;
        int colour;

        public InventoryV1DialogBox()
        {
            config = new GameDialogBoxConfiguration(Type, Cache.GameSettings.Resolution);
            width = Cache.Interface[sprite].Frames[spriteFrame].Width;
            height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public InventoryV1DialogBox(GameDialogBoxConfiguration config)
        {
            this.config = config;
            width = Cache.Interface[sprite].Frames[spriteFrame].Width;
            height = Cache.Interface[sprite].Frames[spriteFrame].Height;
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
            frame = Cache.Interface[sprite].Frames[spriteFrame];
        }


        public bool Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (config.Hidden)
                if (!config.AlwaysVisible) return false; // dont draw dialogs if not a HUD item
                else if (Cache.GameSettings.LockedDialogs) return false;  // show HUD items when HUD is unlocked (transparent)

            if (((MainGame)Cache.DefaultState).Player.IsCasting && Cache.GameSettings.HideDialogDuringCasting) return false;

            selectedItemIndex = -1;

            spriteBatch.Draw(Cache.Interface[sprite].Texture, new Vector2(x, y), frame.GetRectangle(), Color.White * transparency);  //draw the base texture

            //draw extra 
            /* UPGRADE BUTTON */
            if ((x + 32 < mouseX) && (x + 86 > mouseX) && (y + 207 < mouseY) && (y + 225 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "UPGR", FontType.DialogsSmallSize8, new Vector2(x + 34, y + 209), 58, Cache.Colors[GameColor.Orange]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "UPGR", FontType.DialogsSmallSize8, new Vector2(x + 34, y + 209), 58, Color.White);

            /* MANU BUTTON */
            if ((x + 150 < mouseX) && (x + 209 > mouseX) && (y + 207 < mouseY) && (y + 225 > mouseY))
                SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "MANU", FontType.DialogsSmallSize8, new Vector2(x + 152, y + 209), 58, Cache.Colors[GameColor.Orange]);
            else SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "MANU", FontType.DialogsSmallSize8, new Vector2(x + 152, y + 209), 58, Color.White);

            /* TITLE */
            spriteBatch.Draw(Cache.Interface[(int)SpriteId.DialogsV2].Texture, new Vector2(x + 48, y - 16), Cache.Interface[(int)SpriteId.DialogsV2].Frames[28].GetRectangle(), Color.White * transparency);
            SpriteHelper.DrawTextCenteredWithShadow(spriteBatch, "Inventory", FontType.DisplayNameSize13Spacing1, new Vector2(x + 58, y - 9), 140, Cache.Colors[GameColor.Orange]);

            foreach (int i in player.InventoryDrawOrder) //TODO check this out, possibly change it? //Add dragged item option to this?
                if (i != -1 && player.Inventory[i] != null && !player.Inventory[i].IsEquipped)
                {
                    item = player.Inventory[i];
                    itemIndex = (int)SpriteId.ItemBag + item.Sprite;
                    itemFrame = item.SpriteFrame;
                    selected = false;

                    colour = (item.Category == ItemCategory.Dyes ? item.SpecialEffect2 : item.Colour);

                    SpriteHelper.DrawEquipment(spriteBatch, itemIndex, itemFrame, x, y, item.BagX + 32, item.BagY + 44,
                                                ((selectedItemIndex == i) ? selectedItemIndexOffsetX : 0), ((selectedItemIndex == i) ? selectedItemIndexOffsetY : 0),
                                                (item.ColorType == GameColor.Custom) ? SpriteHelper.ColourFromArgb(colour) : Cache.Colors[item.ColorType], 1.0f);

                    if (selected) selectedItemIndex = i;

                    if (item.Count > 1)
                        SpriteHelper.DrawTextWithShadow(spriteBatch, item.Count.ToString(), FontType.DialogsSmallSize8, new Vector2((int)(x + 29 + item.BagX + 10), (int)(y + 41 + item.BagY)), Cache.Colors[GameColor.Orange]);
                }

            // if clicked, show popup
            if (clickedItemIndex != -1 && player.Inventory[clickedItemIndex] != null)
                ((MainGame)Cache.DefaultState).SetItemPopup(Type, player.Inventory[clickedItemIndex], new Vector2(mouseX + 10, mouseY - 10), 0, 0);

            if (frame != null && Utility.IsSelected(mouseX, mouseY, x, y, x + frame.Width, y + frame.Height))
                return true;
            else return false;
        }

        public void Update(GameTime gameTime)
        {
            player = ((MainGame)Cache.DefaultState).Player;
            mouseX = (int)Cache.DefaultState.Display.Mouse.X;
            mouseY = (int)Cache.DefaultState.Display.Mouse.Y;
            x = config.X;
            y = config.Y;
            transparency = ((Cache.GameSettings.TransparentDialogs || config.Hidden) ? 0.5F : 1F);
        }

        public void LeftClicked()
        {
            if ((y + 207 < mouseY) && (y + 225 > mouseY))
            {
                if ((x + 32 < mouseX) && (x + 86 > mouseX))
                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Config.Visible)
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Hide();
                    else
                    {
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Show();
                        ((IGameState)Cache.DefaultState).BringToFront(GameDialogBoxType.UpgradeV2);
                    }
                if ((x + 150 < mouseX) && (x + 209 > mouseX))
                    if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Manufacture].Config.Visible)
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Manufacture].Hide();
                    else
                    {
                        ((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Manufacture].Show();
                        ((MainGame)Cache.DefaultState).BringToFront(GameDialogBoxType.Manufacture);
                    }
            }
        }

        public void LeftDoubleClicked(ref SelectionMode selectionMode, ref int selectionModeId)
        {
            if (selectionMode == SelectionMode.ApplyItem)
            {
                if (SelectedItemIndex != -1)
                    ((MainGame)Cache.DefaultState).ApplyItem(selectionModeId, selectedItemIndex);

                //clear selection mode for apply item
                selectionMode = SelectionMode.None;
                selectionModeId = -1;
            }
            // special case - add sell item if merchant window open + in sell state
            else if (SelectedItemIndex != -1)
            {
                if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.Visible)
                    switch (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.State)
                    {
                        case DialogBoxState.Sell: ((MainGame)Cache.DefaultState).AddSellItem(SelectedItemIndex); break;
                        case DialogBoxState.Repair: ((MainGame)Cache.DefaultState).AddRepairItem(SelectedItemIndex); break;
                    }
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.TradeV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).AddTradeItem(SelectedItemIndex);
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.WarehouseV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).ItemToWarehouse(SelectedItemIndex, -1);
                else if (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.UpgradeV2].Config.Visible)
                    ((MainGame)Cache.DefaultState).ChangeUpgradeItem(SelectedItemIndex);
                else ((MainGame)Cache.DefaultState).UseItem(SelectedItemIndex);
            }
        }

        public void LeftHeld()
        {
        }

        public void LeftDragged()
        {

        }


        public void LeftReleased(GameDialogBoxType highlightedDialogBox, int highlightedDialogBoxItemIndex)
        {
            switch (highlightedDialogBox)
            {
                case GameDialogBoxType.None: // no destination dialog box
                    // check for player highlight (not self)
                    int selectedObjectId = ((MainGame)Cache.DefaultState).SelectedObjectId;
                    if (Cache.OwnerCache.ContainsKey((UInt16)selectedObjectId) && !Cache.OwnerCache[(UInt16)selectedObjectId].Location.Equals(player.Location))
                    {
                        IOwner owner = Cache.OwnerCache[(UInt16)selectedObjectId];
                        if (owner.OwnerType == OwnerType.Player)
                        {
                            ((MainGame)Cache.DefaultState).AddEvent(string.Format("Requesting trade with {0}...", owner.Name));
                            ((MainGame)Cache.DefaultState).RequestTrade(owner.ObjectId, clickedItemIndex);
                        }
                    }
                    else ((MainGame)Cache.DefaultState).DropItem(clickedItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.CharacterV2: // moving items around the dialog box //TODO - create a general equiplocation?
                    if (clickedItemIndex != highlightedDialogBoxItemIndex)
                        ((MainGame)Cache.DefaultState).SwapItem(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.Merchant:
                    if (clickedItemIndex != -1)
                        switch (((MainGame)Cache.DefaultState).DialogBoxes[GameDialogBoxType.Merchant].Config.State)
                        {
                            case DialogBoxState.Sell: ((MainGame)Cache.DefaultState).AddSellItem(clickedItemIndex); break;
                            case DialogBoxState.Repair: ((MainGame)Cache.DefaultState).AddRepairItem(clickedItemIndex); break;
                        }
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.TradeV2:
                    if (clickedItemIndex != -1) ((MainGame)Cache.DefaultState).AddTradeItem(clickedItemIndex);
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.UpgradeV2:
                    if (clickedItemIndex != -1)
                    {
                        ((MainGame)Cache.DefaultState).ChangeUpgradeItem(clickedItemIndex);
                        //dialogBoxes[clickedDialogBox].ClickedItemIndex = -1;
                    }
                    highlightedItemIndex = -1;
                    break;
                case GameDialogBoxType.WarehouseV2:
                    if (clickedItemIndex != -1)
                        ((MainGame)Cache.DefaultState).ItemToWarehouse(clickedItemIndex, highlightedDialogBoxItemIndex);
                    highlightedItemIndex = -1;
                    break;

            }

            if (highlightedDialogBox == GameDialogBoxType.InventoryV1)
            {
                // HOLD SHIFT TO SET LOCATION OF ALL RELATED ITEMS
                if (((MainGame)Cache.DefaultState).Display.Keyboard.LeftShift)
                {
                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
                        if (((MainGame)Cache.DefaultState).Player.Inventory[i] != null && ((MainGame)Cache.DefaultState).Player.Inventory[i].ItemId == ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex].ItemId)
                        {
                            ((MainGame)Cache.DefaultState).Player.Inventory[i].BagX = ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex].BagX;
                            ((MainGame)Cache.DefaultState).Player.Inventory[i].BagY = ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex].BagY;
                            ((MainGame)Cache.DefaultState).SetBagLocation(i); // update server
                        }
                }
                else ((MainGame)Cache.DefaultState).SetBagLocation(clickedItemIndex); // update server
            }
            else if (highlightedDialogBox != GameDialogBoxType.None)
            {
                Item movedItem = ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex];
                movedItem.BagX = ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex].BagOldX;
                movedItem.BagY = ((MainGame)Cache.DefaultState).Player.Inventory[clickedItemIndex].BagOldY;
            }
        }

        public void RightClicked() { }

        public void RightHeld() { }

        public void RightReleased() { }

        public void Scroll(int direction)
        {
            if (config.Page - direction > config.MaxPages) config.Page = 1;
            else if (config.Page - direction < 1) config.Page = config.MaxPages;
            else config.Page -= direction;
        }

        public void OffsetLocation(int x, int y)
        {
            config.X += x;
            config.Y += y;
        }

        public void Show()
        {

            if (!config.AlwaysVisible && Cache.DefaultState != null)
                ((IGameState)Cache.DefaultState).BringToFront(Type);
            highlightedItemIndex = -1;
            config.Hidden = false;
        }

        public void Hide()
        {
            if (!config.AlwaysVisible && Cache.DefaultState != null)
            {
                ((IGameState)Cache.DefaultState).DialogBoxDrawOrder.Remove(Type);
                ((IGameState)Cache.DefaultState).ClickedDialogBox = GameDialogBoxType.None;
            }
            config.Hidden = true;
        }

        public void Toggle(DialogBoxState state = DialogBoxState.Normal)
        {

            if (config.Visible && state == config.State) Hide();
            else
            {
                config.State = state;
                Show();
            }
        }

        public void SetData(byte[] data)
        {

        }
    }
}
