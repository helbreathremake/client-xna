﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath;

using Microsoft.Xna.Framework.Input;

namespace Helbreath.Game.Assets
{
    public class HotBarIcon
    {
        private int index;
        private HotBarType type;

        public static HotBarIcon ParseXml(XmlReader r)
        {
            HotBarIcon hotbar = new HotBarIcon(Int32.Parse(r["Index"]));
            HotBarType type;
            if (Enum.TryParse<HotBarType>(r["Type"], out type))
                hotbar.Type = type;

            return hotbar;
        }

        public HotBarIcon(int index)
        {
            this.index = index;
        }

        public int Index { get { return index; } set { index = value; } }
        public HotBarType Type { get { return type; } set { type = value; } }
    }
}
