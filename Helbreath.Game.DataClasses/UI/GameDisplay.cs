﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Helbreath.Game.Assets
{
    public class GameDisplay
    {
        private GraphicsDetail detailmode;
        private bool debugMoge;

        private GraphicsDevice device;
        private GraphicsDeviceManager deviceManager;
        private GameMouse mouse;
        private GameKeyboard keyboard;
        private Rectangle zoom;
        private GameWindow window;

        private const int SM_CXSCREEN = 0;
        private const int SM_CYSCREEN = 1;
        private IntPtr HWND_TOP = IntPtr.Zero;
        private const int SWP_SHOWWINDOW = 64;

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndIntertAfter, int X, int Y, int cx, int cy, int uFlags);

        [DllImport("user32.dll")]
        static extern int GetSystemMetrics(int Which);

        public GameDisplay(Resolution resolution, GameWindow window)
        {
            //this.Resolution = resolution;
            this.window = window;
            ResolutionWidth = Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.Width];
            ResolutionHeight = Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.Height];
            zoom = new Rectangle(GameBoardShiftX, GameBoardShiftY, ResolutionWidth, ResolutionHeight);
        }

        public void ApplyDisplayModeAndResolution()
        {
            ResolutionWidth = Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.Width];
            ResolutionHeight = Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.Height];
            var nativeResolutionWidth = GetSystemMetrics(SM_CXSCREEN);
            var nativeResolutionHeight = GetSystemMetrics(SM_CYSCREEN);

            switch (Cache.GameSettings.DisplayMode)
            {
                case DisplayMode.Windowed:
                    deviceManager.IsFullScreen = false;
                    deviceManager.PreferredBackBufferWidth = ResolutionWidth;
                    deviceManager.PreferredBackBufferHeight = ResolutionHeight;
                    DisableWindowedFullScreen(ResolutionWidth, ResolutionHeight, nativeResolutionWidth, nativeResolutionHeight);
                    break;
                case DisplayMode.Fullscreen:
                    deviceManager.IsFullScreen = true;
                    deviceManager.PreferredBackBufferWidth = ResolutionWidth;
                    deviceManager.PreferredBackBufferHeight = ResolutionHeight;
                    DisableWindowedFullScreen(ResolutionWidth, ResolutionHeight, nativeResolutionWidth, nativeResolutionHeight);
                    break;
                case DisplayMode.WindowedFullscreen:
                    if (deviceManager.IsFullScreen)
                    {
                        deviceManager.IsFullScreen = false;
                        deviceManager.ApplyChanges();
                        nativeResolutionWidth = GetSystemMetrics(SM_CXSCREEN);
                        nativeResolutionHeight = GetSystemMetrics(SM_CYSCREEN);
                    }
                    ResolutionWidthScale = (float)nativeResolutionWidth / (float)ResolutionWidth;
                    ResolutionHeightScale = (float)nativeResolutionHeight / (float)ResolutionHeight;
                    var scalingFactor = new Vector3(ResolutionWidthScale, ResolutionHeightScale, 1);
                    WindowedFullscreenScale = Matrix.CreateScale(scalingFactor);
                    deviceManager.PreferredBackBufferWidth = nativeResolutionWidth;
                    deviceManager.PreferredBackBufferHeight = nativeResolutionHeight;
                    EnableWindowedFullScreen(nativeResolutionWidth, nativeResolutionHeight);
                    break;
            }

            deviceManager.ApplyChanges();
        }

        private void EnableWindowedFullScreen(int nativeDisplayWidth, int nativeDisplayHeight)
        {
            var form = Form.FromHandle(window.Handle).FindForm();
            form.WindowState = FormWindowState.Maximized;
            form.FormBorderStyle = FormBorderStyle.None;
            //form.TopMost = true;
            SetWindowPos(window.Handle, HWND_TOP, 0, 0, nativeDisplayWidth, nativeDisplayHeight, SWP_SHOWWINDOW);
        }

        private void DisableWindowedFullScreen(int resolutionWidth, int resolutionHeight, int nativeDisplayWidth, int nativeDisplayHeight)
        {
            var form = Form.FromHandle(window.Handle).FindForm();
            form.WindowState = FormWindowState.Normal;
            form.FormBorderStyle = FormBorderStyle.FixedSingle;
            //form.TopMost = false;
            SetWindowPos(window.Handle, HWND_TOP, nativeDisplayWidth / 2 - resolutionWidth / 2, nativeDisplayHeight / 2 - resolutionHeight / 2, resolutionWidth, resolutionHeight, SWP_SHOWWINDOW);
        }

        public int GameBoardWidth { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.GameBoardWidth]; } }
        public int GameBoardHeight { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.GameBoardHeight]; } }
        public int GameBoardShiftX { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.GameBoardShiftX]; } }
        public int GameBoardShiftY { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.GameBoardShiftY]; } }
        public int PlayerCenterX { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.CenterX]; } }
        public int PlayerCenterY { get { return Globals.Resolutions[(int)Cache.GameSettings.Resolution, (int)ResolutionSetting.CenterY]; } }
        public bool DebugMode { get { return debugMoge; } set { debugMoge = value; } }
        public GraphicsDevice Device { get { return device; } set { device = value; } }
        public GraphicsDeviceManager DeviceManager { get { return deviceManager; } set { deviceManager = value; } }
        public GameMouse Mouse { get { return mouse; } set { mouse = value; } }
        public GameKeyboard Keyboard { get { return keyboard; } set { keyboard = value; } }
        public Rectangle Zoom { get { return zoom; } set { zoom = value; } }

        public Matrix WindowedFullscreenScale { get; set; }

        public int ResolutionWidth { get; set; }

        public int ResolutionHeight { get; set; }

        public float ResolutionWidthScale { get; set; }

        public float ResolutionHeightScale { get; set; }

        public int ViewportWidth
        {
            get
            {
                if (Cache.GameSettings.DisplayMode == DisplayMode.WindowedFullscreen)
                {
                    return ResolutionWidth;
                }
                else
                {
                    return device.Viewport.Width;
                }
            }
        }

        public int ViewportHeight
        {
            get
            {
                if (Cache.GameSettings.DisplayMode == DisplayMode.WindowedFullscreen)
                {
                    return ResolutionHeight;
                }
                else
                {
                    return device.Viewport.Height;
                }
            }
        }
    }
}
