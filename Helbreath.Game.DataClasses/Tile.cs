﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Helbreath.Game;


namespace Helbreath.Game.Assets
{
    public class Tile
    {
        private Texture2D texture;
        //private List<Rectangle> frames;
        private List<AnimationFrame> frames;

        public Tile(Texture2D texture)
        {
            this.texture = texture;
            //this.frames = new List<Rectangle>();
            this.frames = new List<AnimationFrame>();
        }

        public Texture2D Texture { get { return texture; } }
        //public List<Rectangle> Frames { get { return frames; } set { frames = value; } }
        public List<AnimationFrame> Frames { get { return frames; } set { frames = value; } }
        public System.Drawing.Image MiniMapImage;
    }
}
