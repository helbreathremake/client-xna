﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Helbreath.Common.Assets;

namespace Helbreath.Game.Assets
{
    public static class GameUtility
    {
        public static int[] GetPoint(int sourceX, int sourceY, int targetX, int targetY, int step = 1)
        {
            int[] point = new int[2];

            int dx, dy, x_inc, y_inc, error, index;
            int iResultX, iResultY, iCnt = 0;

            if ((sourceX == targetX) && (sourceY == targetY))
            {
                point[0] = sourceX;
                point[1] = sourceY;
                return point;
            }
            error = 0;
            iResultX = sourceX;
            iResultY = sourceY;
            dx = targetX - sourceX;
            dy = targetY - sourceY;
            if (dx >= 0) x_inc = 1;
            else
            {
                x_inc = -1;
                dx = -dx;
            }
            if (dy >= 0) y_inc = 1;
            else
            {
                y_inc = -1;
                dy = -dy;
            }
            if (dx > dy)
            {
                for (index = 0; index <= dx; index++)
                {
                    error += dy;
                    if (error > dx)
                    {
                        error -= dx;
                        iResultY += y_inc;
                    }
                    iResultX += x_inc;
                    iCnt++;
                    if (iCnt >= step) break;
                }
            }
            else
            {
                for (index = 0; index <= dy; index++)
                {
                    error += dx;
                    if (error > dy)
                    {
                        error -= dy;
                        iResultX += x_inc;
                    }
                    iResultY += y_inc;
                    iCnt++;
                    if (iCnt >= step) break;
                }
            }
            point[0] = iResultX;
            point[1] = iResultY;

            return point;
        }

        /// <summary>
        /// Gets the next coordinate between source and destination
        /// </summary>
        /// <param name="sourceX">Source X coordinate</param>
        /// <param name="sourceY">Source Y coordinate</param>
        /// <param name="destinationX">Destination X coordinate</param>
        /// <param name="destinationY">Destination Y coordinate</param>
        /// <param name="step">Maximum of steps to take towards the destination</param>
        /// <returns></returns>
        public static int[] GetNextPoint(int sourceX, int sourceY, int destinationX, int destinationY, int step = 1)
        {
            int[] point = new int[2];

            // take steps towards to the X coordinate. Ensure it stays within bounds
            if (destinationX > sourceX) point[0] = Math.Min(sourceX + step, destinationX);
            else if (destinationX < sourceX) point[0] = Math.Max(sourceX - step, destinationX);
            else point[0] = sourceX;

            // take steps towards the Y coordinate. Ensure it stays within bounds
            if (destinationY > sourceY) point[1] = Math.Min(sourceY + step, destinationY);
            else if (destinationY < sourceY) point[1] = Math.Max(sourceY - step, destinationY);
            else point[1] = sourceY;

            return point;
        }

        public static bool WithinRange(int x, int y, int destinationX, int destinationY, int range)
        {
            int distanceX, distanceY;

            distanceX = Math.Abs(x - destinationX);
            distanceY = Math.Abs(y - destinationY);

            if (distanceX <= range && distanceY <= range) return true;
            return false;
        }

        public static void DrawRectangle(Texture2D texture, SpriteBatch spriteBatch, int x, int y, int width, int height)
        {
            spriteBatch.Draw(texture, new Rectangle(x, y, 1, height), Color.Red); // Left
            spriteBatch.Draw(texture, new Rectangle(x + width, y, 1, height), Color.Red); // Right
            spriteBatch.Draw(texture, new Rectangle(x, y, width, 1), Color.Red); // Top
            spriteBatch.Draw(texture, new Rectangle(x, y + height, width, 1), Color.Red); // Bottom
        }

        public static Color GetRarityColour(Item item)
        {
            switch (item.Rarity)
            {
                default:
                case ItemRarity.VeryCommon:
                case ItemRarity.Common: return Color.WhiteSmoke;
                case ItemRarity.Uncommon: return Color.WhiteSmoke;
                case ItemRarity.Rare:
                    if (item.SpecialAbilityType != ItemSpecialAbilityType.None)
                        return new Color(0, 255, 50);
                    else if (item.Stats.Count > 0)
                        return new Color(0, 255, 50);
                    else return Color.WhiteSmoke;
                case ItemRarity.VeryRare: return Color.WhiteSmoke;
                case ItemRarity.UltraRare:
                    return new Color(255, 208, 60);

            }
        }

        public static int GetHeroType(int type, int appearance3)
        {
            int armor, legs, arms, helm;
            armor = (appearance3 & 0xF000) >> 12;
            legs = (appearance3 & 0x0F00) >> 8;
            helm = (appearance3 & 0x00F0) >> 4;
            arms = appearance3 & 0x000F;
            switch (type)
            {
                case 1:
                case 2:
                case 3:
                    if ((armor == 8) && (legs == 5) && (helm == 9) && (arms == 3)) return (1); // Warr elv M
                    if ((armor == 9) && (legs == 6) && (helm == 10) && (arms == 4)) return (1); // Warr ares M
                    if ((armor == 10) && (legs == 5) && (helm == 11) && (arms == 3)) return (2); // Mage elv M
                    if ((armor == 11) && (legs == 6) && (helm == 12) && (arms == 4)) return (2); // Mage ares M
                    break;
                case 4:
                case 5:
                case 6: // fixed
                    if ((armor == 9) && (legs == 6) && (helm == 9) && (arms == 4)) return (1); //warr elv W
                    if ((armor == 10) && (legs == 7) && (helm == 10) && (arms == 5)) return (1); //warr ares W
                    if ((armor == 11) && (legs == 6) && (helm == 11) && (arms == 4)) return (2); //mage elv W
                    if ((armor == 12) && (legs == 7) && (helm == 12) && (arms == 5)) return (2); //mage ares W
                    break;
            }
            return 0;
        }

        public static string FormatStatistic(int value)
        {
            double v;
            if (value > 1000000000)
            {
                v = ((double)value / 1000000000);
                return string.Format("{0}", ((int)v)) + "b";
            }
            else if (value > 1000000)
            {
                v = ((double)value / 1000000);
                return string.Format("{0}", ((int)v)) + "m";
            }
            else if (value > 10000)
            {
                v = ((double)value / 1000);
                return string.Format("{0}", ((int)v)) + "k";
            }
            else return string.Format("{0}", ((int)value));
        }
    }
}
