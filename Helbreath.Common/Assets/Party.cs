﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class Party
    {
        private List<int> members;
        private int leaderID;

        private Guid id;

        public Party(int leaderID)
        {
            members = new List<int>();
            for (int i = 0; i < Globals.MaximumPartyMembers; i++)
                members.Add(-1);

            AddMember(leaderID);

            this.leaderID = leaderID;
            this.id = new Guid();
            this.Mode = PartyMode.Hunting;
            this.MemberInfo = new Dictionary<int, PartyMember>();

            this.Name = "Party";
            this.IsGlobal = false;
        }

        public bool AddMember(int playerID)
        {
            if (MemberCount < 12)
            {
                for (int i = 0; i < Globals.MaximumPartyMembers; i++)
                    if (members[i] == -1)
                    {
                        members[i] = playerID;
                        break;
                    }

                return true;
            }
            else return false;
        }

        public bool RemoveMember(int playerID)
        {
            for (int i = 0; i < members.Count; i++)
                if (members[i] == playerID)
                    members[i] = -1;

            // reallocate a leader
            if (playerID == leaderID)
                for (int i = 0; i < members.Count; i++)
                    if (members[i] != -1)
                        leaderID = members[i];

            return true;
        }

        public List<int> Members { get { return members; } set { members = value; } }
        public int MemberCount { get { int count = 0; for (int i = 0; i < Globals.MaximumPartyMembers; i++) if (members[i] != -1) count++; return count; } }
        public int LeaderId { get { return leaderID; } }
        public Guid ID { get { return id; } }
        public PartyMode Mode;
        public Dictionary<int, PartyMember> MemberInfo;

        // for party advertising
        public string Name;
        public bool IsGlobal;
    }

    public class PartyMember
    {
        public PartyMember(string name)
        {
            Name = name;
        }

        public string Name;
        public string MapName;
        public int X;
        public int Y;
        public int HP;
        public int MP;
        public int MaxHP;
        public int MaxMP;
        public int SP;
        public bool Poison;
    }
}
