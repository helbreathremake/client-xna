﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Helbreath;

namespace Helbreath.Common.Assets
{
    public class Magic
    {
        private int index; // in configs
        private int level;
        private Dice npcDamage;
        private Dice playerDamage;
        private MagicType type;
        private MagicAttribute attribute;
        private MagicCategory category;
        private TimeSpan delayTime;
        private TimeSpan lastTime;
        private int goldCost;
        private int manaCost;
        private int requiredIntelligence;
        private string name;
        private string description;
        private int rangeY;
        private int rangeX;
        private int smallIconFrame;
        private int largeIconFrame;

        private int effect1;
        private int effect2;
        private int effect3;
        private int effect4;
        private int effect5;
        private int effect6;
        private int effect7;
        private int effect8;
        private int effect9;

        private bool ignorePFM;

        private DrawEffectType drawEffect;

        /// <summary>
        /// Creates a quick magic object for use in magic effects.
        /// </summary>
        /// <param name="type">Type of the magic effect.</param>
        /// <param name="lastTime">Time this effect lasts in seconds.</param>
        public Magic(MagicType type, int lastTime)
        {
            this.type = type;
            this.lastTime = new TimeSpan(0, 0, lastTime);
        }

        /// <summary>
        /// Creates a quick magic object for use in magic effects.
        /// </summary>
        /// <param name="type">Type of the magic effect.</param>
        /// <param name="lastTime">Time this effect lasts in seconds.</param>
        /// <param name="effect">Effect value associated with this magic effect.</param>
        public Magic(MagicType type, int lastTime, int effect)
        {
            this.type = type;
            this.lastTime = new TimeSpan(0, 0, lastTime);
            this.effect1 = this.effect2 = effect; // some use 1, some use 2
        }

        public static Magic ParseXml(XmlReader r)
        {
            Magic magic = new Magic(Int32.Parse(r["Index"]));
            magic.Name = r["Name"];
            magic.Level = (r["Level"] != null ? Int32.Parse(r["Level"]) : 1);
            MagicType magicType;
            if (Enum.TryParse<MagicType>(r["Type"], out magicType))
                magic.Type = magicType;
            magic.DelayTime = TimeSpan.Parse(r["DelayTime"]);
            magic.LastTime = TimeSpan.Parse(r["LastTime"]);
            magic.RangeY = Int32.Parse(r["RangeY"]);
            magic.RangeX = Int32.Parse(r["RangeX"]);
            magic.Effect1 = Int32.Parse(r["Effect1"]);
            magic.Effect2 = Int32.Parse(r["Effect2"]);
            magic.Effect3 = Int32.Parse(r["Effect3"]);
            magic.Effect4 = Int32.Parse(r["Effect4"]);
            magic.Effect5 = Int32.Parse(r["Effect5"]);
            magic.Effect6 = Int32.Parse(r["Effect6"]);
            magic.Effect7 = Int32.Parse(r["Effect7"]);
            magic.Effect8 = Int32.Parse(r["Effect8"]);
            magic.Effect9 = Int32.Parse(r["Effect9"]);
            magic.RequiredIntelligence = Int32.Parse(r["RequiredIntelligence"]);
            magic.GoldCost = Int32.Parse(r["GoldCost"]);
            magic.ManaCost = Int32.Parse(r["ManaCost"]);
            MagicCategory magicCategory;
            if (Enum.TryParse<MagicCategory>(r["Category"], out magicCategory))
                magic.Category = magicCategory;
            MagicAttribute magicAttribute;
            if (Enum.TryParse<MagicAttribute>(r["Attribute"], out magicAttribute))
                magic.Attribute = magicAttribute;
            if (r["IgnorePFM"] != null)
                magic.IgnorePFM = Boolean.Parse(r["IgnorePFM"]);
            else magic.IgnorePFM = false;

            DrawEffectType drawEffect;
            if (r["DrawEffect"] != null && Enum.TryParse<DrawEffectType>(r["DrawEffect"], out drawEffect))
                magic.DrawEffect = drawEffect;
            else magic.DrawEffect = DrawEffectType.None;

            if (r["SmallIconSpriteFrame"] != null) magic.SmallIconSpriteFrame = Int32.Parse(r["SmallIconSpriteFrame"]);
            else magic.SmallIconSpriteFrame = 6;
            if (r["LargeIconSpriteFrame"] != null) magic.LargeIconSpriteFrame = Int32.Parse(r["LargeIconSpriteFrame"]);
            else magic.LargeIconSpriteFrame = 6;

            return magic;
        }

        public Magic(int index)
        {
            this.index = index;
        }

        public int Level { get { return level; } set { level = value; } }

        public int Index
        {
            get { return index; }
        }

        public Dice NpcDamage
        {
            get { return npcDamage; }
        }

        public Dice PlayerDamage
        {
            get { return playerDamage; }
        }

        public MagicType Type
        {
            get { return type; }
            set { type = value; }
        }

        public MagicAttribute Attribute
        {
            get { return attribute; }
            set { attribute = value; }

        }

        public MagicCategory Category
        {
            get { return category; }
            set { category = value; }
        }

        public TimeSpan DelayTime
        {
            get { return delayTime; }
            set { delayTime = value; }
        }

        public TimeSpan LastTime
        {
            get { return lastTime; }
            set { lastTime = value; }
        }

        public Int32 GoldCost
        {
            get { return goldCost; }
            set { goldCost = value; }
        }

        public Int32 ManaCost
        {
            get { return manaCost; }
            set { manaCost = value; }
        }

        public Int32 RequiredIntelligence
        {
            get { return requiredIntelligence; }
            set { requiredIntelligence = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public int RangeY
        {
            get { return rangeY; }
            set { rangeY = value; }
        }

        public int RangeX
        {
            get { return rangeX; }
            set { rangeX = value; }
        }

        public int Effect1
        {
            get { return effect1; }
            set { effect1 = value; }
        }

        public int Effect2
        {
            get { return effect2; }
            set { effect2 = value; }
        }
        public int Effect3
        {
            get { return effect3; }
            set { effect3 = value; }
        }
        public int Effect4
        {
            get { return effect4; }
            set { effect4 = value; }
        }
        public int Effect5
        {
            get { return effect5; }
            set { effect5 = value; }
        }
        public int Effect6
        {
            get { return effect6; }
            set { effect6 = value; }
        }
        public int Effect7
        {
            get { return effect7; }
            set { effect7 = value; }
        }
        public int Effect8
        {
            get { return effect8; }
            set { effect8 = value; }
        }
        public int Effect9
        {
            get { return effect9; }
            set { effect9 = value; }
        }
        public bool IgnorePFM
        {
            get { return ignorePFM; }
            set { ignorePFM = value; }
        }

        public DrawEffectType DrawEffect
        {
            get { return drawEffect; }
            set { drawEffect = value; }
        }

        public int SmallIconSpriteFrame { get { return smallIconFrame; } set { smallIconFrame = value; } }
        public int LargeIconSpriteFrame { get { return largeIconFrame; } set { largeIconFrame = value; } }
    }
}
