﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class LootTable
    {
        private string npcName;

        private Dictionary<ItemRarity, List<int>> tables; // list of items per rarity
        private Dictionary<ItemRarity, List<int>> cache; // list cache based on maximum rarity

        public LootTable(string npcName)
        {
            this.npcName = npcName;

            tables = new Dictionary<ItemRarity, List<int>>();
            tables.Add(ItemRarity.VeryCommon, new List<int>());
            tables.Add(ItemRarity.Common, new List<int>());
            tables.Add(ItemRarity.Uncommon, new List<int>());
            tables.Add(ItemRarity.Rare, new List<int>());
            tables.Add(ItemRarity.VeryRare, new List<int>());
            tables.Add(ItemRarity.UltraRare, new List<int>());

            cache = new Dictionary<ItemRarity, List<int>>();
        }

        /// <summary>
        /// Generates a list of loot based on maximum rarity. For example if maximum rarity is Rare, it will also
        /// return Uncommon, Common and VeryCommon items. Caches lists on first call.
        /// </summary>
        /// <param name="maximumRarity">The maximum rarity of the items allowed in the list.</param>
        /// <returns>List of item names.</returns>
        public List<int> GetLoot(ItemRarity maximumRarity)
        {
            if (!cache.ContainsKey(maximumRarity))
            {
                List<int> itemsCache = new List<int>();
                switch (maximumRarity)
                {
                    case ItemRarity.VeryCommon:
                        foreach (KeyValuePair<ItemRarity, List<int>> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare &&
                                list.Key != ItemRarity.Uncommon &&
                                list.Key != ItemRarity.Common)
                                foreach (int item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Common:
                        foreach (KeyValuePair<ItemRarity, List<int>> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare &&
                                list.Key != ItemRarity.Uncommon)
                                foreach (int item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Uncommon:
                        foreach (KeyValuePair<ItemRarity, List<int>> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare &&
                                list.Key != ItemRarity.Rare)
                                foreach (int item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.Rare:
                        foreach (KeyValuePair<ItemRarity, List<int>> list in tables)
                            if (list.Key != ItemRarity.UltraRare &&
                                list.Key != ItemRarity.VeryRare)
                                foreach (int item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.VeryRare:
                        foreach (KeyValuePair<ItemRarity, List<int>> list in tables)
                            if (list.Key != ItemRarity.UltraRare)
                                foreach (int item in list.Value)
                                    itemsCache.Add(item);
                        break;
                    case ItemRarity.UltraRare:
                        foreach (List<int> list in tables.Values)
                            foreach (int item in list)
                                itemsCache.Add(item);
                        break;
                }
                cache.Add(maximumRarity, itemsCache);
            }

            return cache[maximumRarity];
        }

        public string NpcName { get { return npcName; } }
        public List<int> this[ItemRarity rarity]
        {
            set { tables[rarity] = value; }
            get { return tables[rarity]; }
        }
    }
}
