﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Helbreath.Common.Assets
{
    public class CraftingItem
    {
        private int itemId;
        private string name;
        private int difficulty;
        private int skillLimit;
        private Dictionary<int, int> ingredients;

        public CraftingItem()
        {
            ingredients = new Dictionary<int, int>();
        }

        public static CraftingItem ParseXml(XmlReader r)
        {
            CraftingItem craftItem = new CraftingItem();
            craftItem.ItemId = Int32.Parse(r["ItemId"]);
            craftItem.Difficulty = Int32.Parse(r["Difficulty"]);
            craftItem.SkillLimit = Int32.Parse(r["SkillLevel"]);
            XmlReader ingrediantReader = r.ReadSubtree();
            while (ingrediantReader.Read())
                if (ingrediantReader.IsStartElement() && ingrediantReader.Name.Equals("Ingrediant") && !craftItem.Ingredients.ContainsKey(Int32.Parse(r["ItemId"])))
                    craftItem.Ingredients.Add(Int32.Parse(r["ItemId"]), Int32.Parse(r["Count"]));
            ingrediantReader.Close();
            return craftItem;
        }

        public int ItemId { get { return itemId; } set { itemId = value; } }
        public string Name { get { return name; } set { name = value; } }
        public int Difficulty { get { return difficulty; } set { difficulty = value; } }
        public int SkillLimit { get { return skillLimit; } set { skillLimit = value; } }
        public Dictionary<int, int> Ingredients { get { return ingredients; } set { ingredients = value; } }
    }
}
