﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public struct MagicEffect
    {
        private Magic magic;
        private DateTime startTime;

        public MagicEffect(Magic magic, DateTime startTime)
        {
            this.magic = magic;
            this.startTime = startTime;
        }

        public Magic Magic
        {
            get { return magic; }
        }

        public DateTime StartTime
        {
            get { return startTime; }
        }
    }
}
