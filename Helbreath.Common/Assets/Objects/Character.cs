﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using Helbreath;
using Helbreath.Common.Events;
using Helbreath.Common.Assets.Objects.Dynamic;
using Helbreath.Common.Assets.Quests;

namespace Helbreath.Common.Assets.Objects
{
    public class Character : IOwner, IDeadOwner
    {
        public event GuildHandler GuildCreated;
        public event GuildHandler GuildDisbanded;
        public event ItemPurchaseHandler ItemPurchased;
        public event ItemSoldHandler ItemSold;

        public event OwnerHandler StatusChanged;
        public event MotionHandler MotionChanged;
        public event DamageHandler DamageTaken;
        public event VitalsChangedHandler VitalsChanged;
        public event DeathHandler Killed;
        public event ItemHandler ItemDropped;
        public event ItemHandler ItemPickedUp;
        public event LogHandler MessageLogged;

        private Dictionary<MagicType, MagicEffect> magicEffects;
        private Dictionary<ConditionGroup, MagicEffect> conditions;
        //private List<Item> inventory;       // bag items
        private Inventory inventory;
        private List<Item> warehouse;       // wh items
        private bool[] spellStatus;         // determines if a spell has been learned or not
        private int[] equipment;            // item indexes of items equipped in each slot (head, arms, body etc) //TODO add to inventory?

        private List<Quest> activeQuests;
        private List<int> summons;
        private Dictionary<TitleType, int> titles;
        private Dictionary<SkillType, Skill> skills;
        private SkillType skillInUse;
        private DateTime skillTime;
        private int skillTargetX;
        private int skillTargetY;
        private Fish targetFish;
        private int targetFishChance;

        private int id;
        private PlayType playType;
        private string accountName;
        private ClientInfo clientInfo;
        private string databaseID; // database id
        private string name;
        private string profile;
        private OwnerSide town;
        private int adminLevel;
        private bool isCombatMode;
        private bool isSafeMode;
        private bool isCivilian;
        private bool isReady;
        private bool isGod;
        private TimeSpan muteTime;
        private int whisperID;

        private int enemyKills;
        private int criminalCount;
        private int gold; // EconomyType.Virtual --//TODO add to inventory?
        private int rewardGold; // from quests etc when EconomyType.Classic //TODO - add to inventory?

        private Guild guild;
        private int guildRank;
        private string loadedGuildName;

        private Party party;
        private int partyRequestID;

        private int tradePartner;

        private Dictionary<int, ComboDamageCounter> comboDamageTargets;
        private Dictionary<int, DateTime> damagedByObjects;
        private Dictionary<int, DateTime> supportedByObjects;

        private int hunger;
        private int level;
        private int rebirthLevel;
        private int gladiatorPoints;
        private int strength;
        private int dexterity;
        private int vitality;
        private int magic;
        private int intelligence;
        private int agility;
        private long experience, experienceStored;
        private int contribution;
        private int reputation;
        private int hp, hpStock;
        private int mp;
        private int sp;
        private int criticals;
        private int criticalCharge;
        private int majestics;
        private int luck;              // percentage. used for chance of survival when at 0hp
        private int totalLogins;

        private int type;
        private GenderType gender;
        private SkinType skin;
        private int hairStyle;
        private int hairColour;
        private int underwearColour;

        private int appearance1;
        private int appearance2;
        private int appearance3;
        private int appearance4;
        private int appearanceColour;
        private MotionDirection direction;
        private bool isDead;
        private bool isRemoved;
        private bool isCorpseExploited;
        private bool canFly;
        private bool showDamageEnabled;
        private int lastDamage;
        private DamageType lastDamageType;
        private DateTime lastDamageTime;
        private bool notifyTickHP;
        private bool notifyTickMP;
        private bool notifyTickSP;
        private bool notifyTickEXP;

        private int status;
        private object statusLock = new object(); // thread sync

        private Resolution resolution; // keep track of client map view
        private int prevMapX;
        private int prevMapY;
        private int mapX;
        private int mapY;
        private Map map;
        private string loadedMapName; // loaded from database to find map reference

        private DateTime lastLogin;

        private DateTime statusTime; // magic effects etc
        private DateTime hungerTime; // hunger degen
        private DateTime hpUpTime;   // hp regen
        private DateTime mpUpTime;   // mp regen
        private DateTime spUpTime;   // sp regen
        private DateTime poisonTime; // poison time
        private DateTime experienceUpTime; // experience store purge
        private TimeSpan reputationTime; // time before can rep+ or rep- players


        private bool specialAbilityEnabled;
        private TimeSpan specialAbilityTime; // time before can activate
        private DateTime specialAbilityStartTime;

        //world events
        private CrusadeDuty crusadeDuty;
        private int crusadeConstructionPoints;
        private int crusadeWarContribution;

        public Character()
        {
            comboDamageTargets = new Dictionary<int, ComboDamageCounter>();
            damagedByObjects = new Dictionary<int, DateTime>();
            supportedByObjects = new Dictionary<int, DateTime>();
            inventory = new Inventory(Globals.MaximumTotalItems);
            warehouse = new List<Item>(Globals.MaximumWarehouseTabItems);
            magicEffects = new Dictionary<MagicType, MagicEffect>();
            conditions = new Dictionary<ConditionGroup, MagicEffect>();
            summons = new List<int>(Globals.MaximumSummons);
            skills = new Dictionary<SkillType, Skill>(Globals.MaximumSkills);
            activeQuests = new List<Quest>();
            TradeList = new List<int>();

            for (int i = 0; i < Globals.MaximumTotalItems; i++) inventory.Add(null);
            for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++) warehouse.Add(null);

            //equipment = new int[15] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
            spellStatus = new bool[Globals.MaximumSpells];

            this.direction = MotionDirection.South;
            this.isDead = false;
            this.isCorpseExploited = false;
            this.canFly = true;

            this.whisperID = -1;
            this.skillInUse = SkillType.None;
            this.partyRequestID = -1;
            this.tradePartner = -1;

            // initializes each title type
            titles = new Dictionary<TitleType, int>();
            foreach (TitleType type in (TitleType[])Enum.GetValues(typeof(TitleType)))
                titles.Add(type, 0);
        }

        /// <summary>
        /// Initilizes this Character on the map.
        /// </summary>
        public void Init() { Init(CurrentMap, mapX, mapY); }
        /// <summary>
        /// Initilizes this Character on the map.
        /// </summary>
        public bool Init(Map targetMap, int targetX, int targetY)
        {
            if (hp <= 0) // handle death before log in (due to disconnect or log out while dead). similar to Restart()
            {
                hp = MaxHP;
                mp = MaxMP;
                sp = MaxSP;
                hunger = 100;
                targetX = targetY = -1; // default locations set in revival zone configs

                switch (town)
                {
                    case OwnerSide.Neutral: targetMap = Cache.World.Maps[Globals.TravellerRevivalZone]; break;
                    case OwnerSide.Aresden: targetMap = Cache.World.Maps[Globals.AresdenRevivalZone]; break;
                    case OwnerSide.Elvine: targetMap = Cache.World.Maps[Globals.ElvineRevivalZone]; break;
                }
            }

            if (targetX == -1 || targetY == -1) // handle default locations -1,-1
            {
                if (targetMap.DefaultLocations.Count > 0)
                {
                    Location location;
                    if (targetMap.DefaultLocations.Count == 1) location = targetMap.DefaultLocations[0];
                    else location = targetMap.DefaultLocations[Math.Max(Dice.Roll(1, targetMap.DefaultLocations.Count) - 1, 0)];
                    targetX = location.X;
                    targetY = location.Y;
                }
                else
                {
                    // send to BI if location cannot be determined. hardcoded failsafe
                    targetMap = Cache.World.Maps[Globals.NeutralZoneName];
                    targetX = 144;
                    targetY = 117;
                }
            }

            int x, y;
            if (targetMap.GetEmptyTile(targetX, targetY, out x, out y))
            {
                this.map = targetMap;
                this.loadedMapName = targetMap.Name;
                this.mapX = x;
                this.mapY = y;
                targetMap[y][x].SetOwner(this);
                targetMap.Players.Add(id);
            }

            status = ((0x0FFFFFFF & status) | (GetPlayerRelationship(null) << 28));

            if (specialAbilityTime.TotalSeconds <= 0) Notify(CommandMessageType.NotifySpecialAbilityEnabled); //specialAbilityTime = new TimeSpan(0, 0, 10);
            if (Cache.World.IsCrusade) Notify(CommandMessageType.NotifyCrusade, (int)CrusadeMode.Start, 0, 0);
            if (Cache.World.IsHeldenianBattlefield)
            {
                if (Cache.World.Heldenian.IsStarted) Notify(CommandMessageType.NotifyHeldenianStarted);
                else Notify(CommandMessageType.NotifyHeldenian);
                if (map.Name.Equals(Globals.HeldenianBattleFieldName))
                    Notify(CommandMessageType.NotifyHeldenianStatistics, Cache.World.Heldenian.AresdenStructures, Cache.World.Heldenian.ElvineStructures, Cache.World.Heldenian.AresdenCasualties, Cache.World.Heldenian.ElvineCasualties);
            }

            return true; //TODO ?
        }

        public bool Load(DataRow data)
        {
            databaseID = data["ID"].ToString();
            playType = (PlayType)(int)data["Type"];
            name = data["Name"].ToString();
            profile = data["Profile"].ToString();
            gender = (GenderType)((int)data["Gender"]);
            skin = (SkinType)((int)data["Skin"]);
            hairStyle = (int)data["HairStyle"];
            hairColour = (int)data["HairColour"];
            underwearColour = (int)data["UnderwearColour"];
            strength = (int)data["Strength"];
            vitality = (int)data["Vitality"];
            dexterity = (int)data["Dexterity"];
            intelligence = (int)data["Intelligence"];
            magic = (int)data["Magic"];
            agility = (int)data["Charisma"];
            hp = (int)data["HP"];
            mp = (int)data["MP"];
            sp = (int)data["SP"];
            level = (int)data["Level"];
            rebirthLevel = (int)data["RebirthLevel"];
            gladiatorPoints = (int)data["GladiatorPoints"];
            experience = (int)data["Experience"];
            appearance1 = (int)data["Appearance1"];
            appearance2 = (int)data["Appearance2"];
            appearance3 = (int)data["Appearance3"];
            appearance4 = (int)data["Appearance4"];
            appearanceColour = (int)data["AppearanceColour"];
            loadedMapName = data["MapName"].ToString();
            mapX = (int)data["MapX"];
            mapY = (int)data["MapY"];
            lastLogin = (DateTime)data["LastLogin"];
            town = (OwnerSide)((int)data["Town"]);
            hunger = (int)data["Hunger"];
            criticals = (int)data["Criticals"];
            majestics = (int)data["Majestics"];
            specialAbilityTime = new TimeSpan(0, 0, (int)data["SpecialAbilityTime"]);
            muteTime = new TimeSpan(0, 0, (int)data["MuteTime"]);
            adminLevel = (int)data["AdminLevel"];
            loadedGuildName = data["Guild"].ToString();
            guildRank = (int)data["GuildRank"];
            totalLogins = (int)data["TotalLogins"];
            reputation = (int)data["Reputation"];
            contribution = (int)data["Contribution"];
            reputationTime = new TimeSpan(0, 0, (int)data["ReputationTime"]);
            enemyKills = (int)data["EnemyKills"];
            criminalCount = (int)data["CriminalCount"];
            gold = (int)data["Gold"];

            for (int i = 0; i < Globals.MaximumSpells; i++)
                if (data["SpellStatus"].ToString()[i] == '1')
                    spellStatus[i] = true;
                else spellStatus[i] = false;


            string[] skillList = data["SkillStatus"].ToString().Split();
            for (int i = 0; i < Globals.MaximumSkills; i++)
                if (Enum.IsDefined(typeof(SkillType), i)) // check this skill is defined
                    if (!skills.ContainsKey((SkillType)i)) // check not already added
                    {
                        Skill s = new Skill((SkillType)i, Int32.Parse(skillList[i]));
                        s.LevelUp += new SkillHandler(OnSkillLevelUp);
                        skills.Add((SkillType)i, s);
                    }

            switch (gender)
            {
                case GenderType.Male: type = 1; break;
                case GenderType.Female: type = 4; break;
            }
            type += ((int)skin) - 1;

            statusTime = hungerTime = hpUpTime = mpUpTime = spUpTime = DateTime.Now;

            return true;
        }

        public void Talk(string message)
        {
            // handle muted characters
            if (IsMuted)
            {
                Cache.World.SendClientMessage(this, "You are muted for " + MuteTime.Hours + "h " + MuteTime.Minutes + "m and " + MuteTime.Seconds + "s");
                return;
            }

            GameColor mode = GameColor.Local;
            if (IsWhispering) mode = GameColor.Whisper;

            switch (message[0])
            {
                case '#': mode = GameColor.Local; break; // overrides whisper
                case '$': mode = GameColor.Party; message = message.TrimStart('$'); break;
                case '@': mode = GameColor.Guild; message = message.TrimStart('@'); break;
                case '!': mode = (IsAdmin) ? GameColor.GameMaster : mode = GameColor.Global; message = message.TrimStart('!'); break;
                case '^': mode = GameColor.Guild; message = message.TrimStart('^'); break;
                case '~': mode = GameColor.Town; message = message.TrimStart('~'); break;
                case '/':
                    mode = GameColor.Command; message = message.TrimStart('/');

                    string[] tokens = message.Split(' ');

                    Character c;
                    switch (tokens[0].ToLower())
                    {
                        case "startapocalypse": if (IsAdmin) Cache.World.StartWorldEvent(WorldEventType.Apocalypse); break;
                        case "startcrusade": if (IsAdmin) Cache.World.StartWorldEvent(WorldEventType.Crusade); break;
                        case "startheldenian": if (IsAdmin) Cache.World.StartWorldEvent(WorldEventType.Heldenian); break;
                        case "startctf": if (IsAdmin) Cache.World.StartWorldEvent(WorldEventType.CaptureTheFlag); break;
                        case "endapocalypse":
                        case "endcrusade":
                        case "endheldenian":
                        case "endctf": if (IsAdmin) Cache.World.EndWorldEvent(OwnerSide.Neutral); break; // manual end - draw
                        case "setattackmode":
                            if (IsAdmin)
                                if (tokens.Length > 1)
                                {
                                    bool boolParser;
                                    if (Boolean.TryParse(tokens[1], out boolParser)) CurrentMap.IsAttackEnabled = boolParser;
                                    MessageLogged(Name + " - /setattackmode succeeded. " + CurrentMap.Name + " attack enabled: " + boolParser.ToString(), LogType.Admin);
                                }
                            break;
                        case "summon":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                {
                                    string npcName = tokens[1];
                                    if (!World.NpcConfiguration.ContainsKey(npcName))
                                    {
                                        Cache.World.SendClientMessage(this, npcName + " does not exist.");
                                        MessageLogged(Name + " - /summon command failed. " + npcName + " does not exist.", LogType.Admin);
                                        break;

                                    }

                                    int count = 1;
                                    int summoned = 0;
                                    if (tokens.Length > 2) Int32.TryParse(tokens[2], out count);
                                    if (count <= 0) count = 1;
                                    if (count > 768) count = 768;
                                    NpcPerk perk = NpcPerk.None;
                                    if (tokens.Length > 3) Enum.TryParse<NpcPerk>(tokens[3], out perk);

                                    //while (summoned <= count)
                                    //{
                                    for (int x = -16; x < 16; x++)
                                    {
                                        for (int y = -12; y < 12; y++)
                                        {
                                            if (summoned > count) break;

                                            if (!Cache.World.CreateNpc(npcName, CurrentMap, X + x, Y + y, perk))
                                            {
                                                Cache.World.SendClientMessage(this, npcName + " does not exist.");
                                                MessageLogged(Name + " - /summon command failed. " + npcName + " does not exist.", LogType.Admin);
                                            }
                                            else
                                            {
                                                summoned += 1;
                                            }

                                            if (summoned > count) break;
                                            //else  MessageLogged(owner.Name + " - /summon " + npcName + " succeeded.", LogType.Admin);
                                        }
                                    }
                                    //}
                                }
                            }
                            break;
                        case "createparty": CreateParty(); break;
                        case "leaveparty": LeaveParty(); break;
                        case "joinparty":
                            if (tokens.Length > 1 && Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                JoinParty(c);
                            break;
                        case "createitem":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                {
                                    int itemId;
                                    if (Int32.TryParse(tokens[1], out itemId))
                                    {
                                        uint attribute = 0;

                                        if (World.ItemConfiguration.ContainsKey(itemId))
                                        {
                                            int count = 1;
                                            if (tokens.Length > 2)
                                            {
                                                if (Globals.EconomyType == EconomyType.Virtual && World.ItemConfiguration[itemId].IsGold)
                                                {
                                                    Int32.TryParse(tokens[2], out count);
                                                    AddGold(count);
                                                }
                                                else
                                                {
                                                    if (World.ItemConfiguration[itemId].Type == ItemType.Arrow ||
                                                        World.ItemConfiguration[itemId].Type == ItemType.Consume)
                                                        Int32.TryParse(tokens[2], out count);
                                                    else
                                                    {
                                                        /*if (tokens.Length > 5)
                                                        {
                                                            ItemSpecialWeaponPrimaryType pType; ItemSpecialWeaponSecondaryType sType;
                                                            int pValue, sValue;
                                                            if (Enum.TryParse<ItemSpecialWeaponPrimaryType>(tokens[2], out pType) &&
                                                                Enum.TryParse<ItemSpecialWeaponSecondaryType>(tokens[4], out sType) &&
                                                                Int32.TryParse(tokens[3], out pValue) &&
                                                                Int32.TryParse(tokens[5], out sValue))
                                                                attribute = Item.CalculateAttribute(pType, pValue, sType, sValue);
                                                        }
                                                        else UInt32.TryParse(tokens[2], out attribute);*/
                                                    }
                                                    // if consumable or arrow, second variable is count else second variable is attribute

                                                    Item item = World.ItemConfiguration[itemId].Copy(count);
                                                    //item.Attribute = attribute;

                                                    AddInventoryItem(item);
                                                    MessageLogged(Name + " - /createitem " + itemId + " succeeded.", LogType.Admin);
                                                }
                                            }
                                            else
                                            {
                                                if (Globals.EconomyType == EconomyType.Virtual && World.ItemConfiguration[itemId].IsGold)
                                                {
                                                    gold++;
                                                    Notify(CommandMessageType.NotifyGold, gold);
                                                }
                                                else
                                                {
                                                    Item item = World.ItemConfiguration[itemId].Copy(count);
                                                    //item.Attribute = attribute;

                                                    AddInventoryItem(item);
                                                    MessageLogged(Name + " - /createitem " + itemId + " succeeded.", LogType.Admin);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Cache.World.SendClientMessage(this, itemId + " does not exist.");
                                            MessageLogged(Name + " - /item command failed. " + itemId + " does not exist.", LogType.Admin);
                                        }
                                    }
                                    else
                                    {
                                        Cache.World.SendClientMessage(this, tokens[1] + " is not a valid number.");
                                        MessageLogged(Name + " - /item command failed. " + tokens[1] + " is not a valid number.", LogType.Admin);
                                    }
                                }
                            }
                            break;
                        case "tp":
                        case "teleport":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                {
                                    if (tokens.Length > 3 && Cache.World.Maps.ContainsKey(tokens[1]))
                                    {
                                        Teleport(Cache.World.Maps[tokens[1]], Int32.Parse(tokens[2]), Int32.Parse(tokens[3]));
                                        MessageLogged(Name + " - /teleport " + tokens[1] + " (" + tokens[2] + "," + tokens[3] + ") succeeded.", LogType.Admin);
                                    }
                                    else if (Cache.World.Maps.ContainsKey(tokens[1]))
                                    {
                                        Teleport(Cache.World.Maps[tokens[1]], -1, -1);
                                        MessageLogged(Name + " - /teleport " + tokens[1] + " (default location (-1,-1)) succeeded.", LogType.Admin);
                                    }
                                }
                            }
                            else Cache.World.SendClientMessage(this, "Current Location: " + map.FriendlyName + " (" + mapX + ", " + mapY + ")");
                            break;
                        case "goto":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                    {
                                        Teleport(c.CurrentMap, c.X, c.Y, MotionDirection.South);
                                        MessageLogged(Name + " - /goto " + tokens[1] + " (" + c.CurrentMap.Name + ": " + c.X + "," + c.Y + ") succeeded.", LogType.Admin);
                                    }
                                    else
                                    {
                                        Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                                        MessageLogged(Name + " - /goto " + tokens[1] + " failed. Player not online.", LogType.Admin);
                                    }
                            }
                            break;
                        case "summonplayer":
                        case "sp:":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                    {
                                        c.Teleport(CurrentMap, X, Y, MotionDirection.South);
                                        MessageLogged(Name + " - /summonplayer " + tokens[1] + " (" + CurrentMap.Name + ": " + X + "," + Y + ") succeeded.", LogType.Admin);
                                    }
                                    else
                                    {
                                        Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                                        MessageLogged(Name + " - /summonplayer " + tokens[1] + " failed. Player not online.", LogType.Admin);
                                    }
                            }
                            break;
                        case "who":
                            int online = 0;
                            foreach (Map m in Cache.World.Maps.Values) online += m.Players.Count;

                            Cache.World.SendClientMessage(this, string.Format("Players Online: {0}", online));
                            break;
                        case "setberserk":
                            if (IsAdmin)
                            {
                                if (MagicEffects.ContainsKey(MagicType.Berserk))
                                    RemoveMagicEffect(MagicType.Berserk);
                                else SetMagicEffect(World.MagicConfiguration[50], true); // firm berserk
                            }
                            break;
                        case "setfrozen":
                            if (IsAdmin)
                            {
                                if (MagicEffects.ContainsKey(MagicType.Ice))
                                    RemoveMagicEffect(MagicType.Ice);
                                else SetMagicEffect(World.MagicConfiguration[45], true); // firm chillwind
                            }
                            break;
                        case "setinvisi":
                            if (IsAdmin)
                            {
                                if (MagicEffects.ContainsKey(MagicType.Invisibility))
                                    RemoveMagicEffect(MagicType.Invisibility);
                                else SetMagicEffect(World.MagicConfiguration[32], true); // firm invisibility
                            }
                            break;
                        case "rep+":
                            if (ReputationTime.Seconds > 0 || IsCriminal)
                                Notify(CommandMessageType.NotifyReputationFailed, ReputationTime.Seconds);
                            else if (Town == OwnerSide.Neutral)
                                Notify(CommandMessageType.NotifyReputationFailed, 0);
                            else if (tokens.Length > 1)
                                if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.Reputation = Math.Max(c.Reputation + 1, Globals.MaximumReputation);
                                    c.Notify(CommandMessageType.NotifyReputationSuccess, 1, c.Reputation, c.Name);
                                    ReputationTime = new TimeSpan(0, 0, 1200);
                                    Notify(CommandMessageType.NotifyReputationSuccess, 1, Reputation, c.Name);

                                    // for kloness
                                    Notify(CommandMessageType.NotifyDamageStatistics, GetMinMeleeDamage(false, false), GetMaxMeleeDamage(false, false), GetMinMeleeDamage(true, false), GetMaxMeleeDamage(true, false));
                                }
                                else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            break;
                        case "rep-":
                            if (ReputationTime.Seconds > 0 || IsCriminal)
                                Notify(CommandMessageType.NotifyReputationFailed, ReputationTime.Seconds);
                            else if (Town == OwnerSide.Neutral)
                                Notify(CommandMessageType.NotifyReputationFailed, 0);
                            else if (tokens.Length > 1)
                                if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    c.Reputation = Math.Min(c.Reputation - 1, Globals.MinimumReputation);
                                    c.Notify(CommandMessageType.NotifyReputationSuccess, 0, c.Reputation, c.Name);
                                    ReputationTime = new TimeSpan(0, 0, 1200);
                                    Notify(CommandMessageType.NotifyReputationSuccess, 0, Reputation, c.Name);

                                    // for kloness
                                    c.Notify(CommandMessageType.NotifyDamageStatistics, c.GetMinMeleeDamage(false, false), c.GetMaxMeleeDamage(false, false), c.GetMinMeleeDamage(true, false), c.GetMaxMeleeDamage(true, false));
                                }
                                else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            break;
                        case "getrep":
                        case "rep":
                            Cache.World.SendClientMessage(this, string.Format("Reputation: {0}", Reputation));
                            break;
                        case "setrep":
                            if (IsAdmin)
                            {
                                int rep;
                                if (tokens.Length > 1 && Int32.TryParse(tokens[1], out rep))
                                {
                                    Reputation = Math.Max(Math.Min(rep, Globals.MaximumReputation), Globals.MinimumReputation);

                                    Notify(CommandMessageType.NotifyReputationSuccess, 0, Reputation, Name);

                                    // for kloness
                                    Notify(CommandMessageType.NotifyDamageStatistics, GetMinMeleeDamage(false, false), GetMaxMeleeDamage(false, false), GetMinMeleeDamage(true, false), GetMaxMeleeDamage(true, false));
                                }

                                Cache.World.SendClientMessage(this, string.Format("Reputation: {0}", Reputation));
                            }
                            break;
                        case "showdmg":
                            ShowDamageEnabled = !ShowDamageEnabled;
                            Cache.World.SendClientMessage(this, string.Format("Show Damage {0}.", (showDamageEnabled) ? "ON" : "OFF"));
                            break;
                        case "tgt":
                            if (tokens.Length > 1 && Summons.Count > 0)
                                if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                {
                                    for (int s = 0; s < CurrentMap.Npcs.Count; s++)
                                        if (Cache.World.Npcs.ContainsKey(CurrentMap.Npcs[s]))
                                        {
                                            if (Cache.World.Npcs[CurrentMap.Npcs[s]].Summoner == id)
                                            {
                                                Cache.World.Npcs[CurrentMap.Npcs[s]].Target = c;
                                                Cache.World.Npcs[CurrentMap.Npcs[s]].CurrentAction = MotionType.Attack;
                                            }
                                        }
                                        else Cache.World.SendClientMessage(this, tokens[1] + " is not in this area.");
                                }
                                else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            break;
                        case "regen":
                            if (IsAdmin)
                            {
                                Revive(false);
                                ReplenishHP(MaxHP, false);
                                ReplenishMP(MaxMP, false);
                                ReplenishSP(MaxSP, false);
                                criticals = MaxCriticals;
                                ReplenishHunger(100, false);

                                Notify(CommandMessageType.NotifyHP, HP, MP);
                                Notify(CommandMessageType.NotifyMP, MP);
                                Notify(CommandMessageType.NotifySP, SP);
                                Notify(CommandMessageType.NotifyCriticals, criticals);
                            }
                            break;
                        case "revive":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                {
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c) && c.isDead)
                                        c.Revive();
                                    else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                                }
                                else Revive(); // revive self
                            }
                            break;
                        case "clearmobs":
                            if (IsAdmin)
                            {
                                bool dropLoot = false;
                                if (tokens.Length > 1 && tokens[1].Equals("1"))
                                    dropLoot = true;

                                for (int n = 0; n < CurrentMap.Npcs.Count; n++)
                                    if (Cache.World.Npcs.ContainsKey(CurrentMap.Npcs[n]))
                                        if (!Cache.World.Npcs[CurrentMap.Npcs[n]].IsFriendly) // dont kill friendly npcs like shopkeeper!
                                            Cache.World.Npcs[CurrentMap.Npcs[n]].Die(this, -1, (DamageType)0, 1, dropLoot);
                            }
                            break;
                        case "mute":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 2)
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                    {
                                        c.MuteTime = new TimeSpan(0, Int32.Parse(tokens[2]), 0);
                                        Cache.World.SendClientMessage(c, "You are muted for " + c.MuteTime.Hours + "h " + c.MuteTime.Minutes + "m and " + c.MuteTime.Seconds + "s");
                                    }
                                    else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            }
                            break;
                        case "unmute":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                        c.MuteTime = new TimeSpan(0, 0, 0);
                            }
                            break;
                        case "dc":
                        case "disconnect":
                            if (IsAdmin)
                            {
                                if (tokens.Length > 1)
                                    if (Cache.World.FindPlayer(tokens[1].Trim(), out c))
                                    {
                                        if (tokens.Length > 2) c.Disconnect(tokens[2]);
                                        else c.Disconnect("Not specified");
                                    }
                                    else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            }
                            break;
                        case "to":
                            if (tokens.Length > 1)
                            {
                                if (Cache.World.FindPlayer(tokens[1].Trim(), out c) && this != c)
                                {
                                    WhisperIndex = c.ObjectId;
                                    Notify(CommandMessageType.NotifyWhisperOn, c.Name);
                                }
                                else Cache.World.SendClientMessage(this, tokens[1] + " not found.");
                            }
                            else
                            {
                                WhisperIndex = -1;
                                Notify(CommandMessageType.NotifyWhisperOff, "");
                            }
                            break;
                        case "getskills":
                            if (IsAdmin)
                            {
                                for (int i = 0; i < Globals.MaximumSkills; i++)
                                    if (Skills.ContainsKey((SkillType)i))
                                    {
                                        Skills[(SkillType)i].MaxOut();
                                        Notify(CommandMessageType.NotifyStudySkillSuccess, i, Skills[(SkillType)i].Level);
                                    }
                            }
                            break;
                        case "getmagic":
                            if (IsAdmin)
                            {
                                for (int i = 0; i < Globals.MaximumSpells; i++)
                                    if (World.MagicConfiguration.ContainsKey(i) && !spellStatus[i])
                                    {
                                        spellStatus[i] = true;
                                        Notify(CommandMessageType.NotifyStudySpellSuccess, i, 0, World.MagicConfiguration[i].Name);
                                    }
                            }
                            break;
                        case "getcrits":
                            if (IsAdmin)
                            {
                                criticals = MaxCriticals;
                                Notify(CommandMessageType.NotifyCriticals, criticals);
                            }
                            break;
                        case "god":
                            if (IsAdmin)
                            {
                                isGod = !isGod;

                                if (isGod)
                                    Cache.World.SendClientMessage(this, "God Mode enabled");
                                else Cache.World.SendClientMessage(this, "God Mode disabled");
                            }
                            break;
                        case "setweather":
                            if (IsAdmin)
                            {
                                WeatherType type;
                                if (tokens.Length > 1 && Enum.TryParse<WeatherType>(tokens[1], out type))
                                    map.SetWeather(type);
                            }
                            break;
                        case "citizen":
                            if (IsAdmin)
                            {
                                OwnerSide side;
                                if (tokens.Length > 1 && Enum.TryParse<OwnerSide>(tokens[1], out side))
                                    switch (side)
                                    {
                                        case OwnerSide.Aresden:
                                        case OwnerSide.Elvine: BecomeCitizen(side); break;
                                        default: Cache.World.SendClientMessage(this, "You cannot become a " + side.ToString()); break;
                                    }
                            }
                            break;
                        case "spawnrock": if (IsAdmin) CurrentMap.CreateDynamicObject(DynamicObjectType.Rock, X, Y); break;
                        case "startpublicevent":
                            if (IsAdmin)
                            {
                                PublicEventDifficulty difficulty = PublicEventDifficulty.Random;
                                if (tokens.Length > 1)
                                    Enum.TryParse<PublicEventDifficulty>(tokens[1], out difficulty);

                                CurrentMap.CreatePublicEvent(PublicEventType.Defence, difficulty);
                            }
                            break;
                        case "endpublicevent": if (IsAdmin) CurrentMap.EndPublicEvent(); break;
                    }
                    return; // commands dont need to update players
            }

            if (mode != GameColor.Local && IsDead) return; // can only speak in local when dead

            // confuse language effect
            if (MagicEffects.ContainsKey(MagicType.Confuse) && MagicEffects[MagicType.Confuse].Magic.Effect1 == 1 && Dice.Roll(1, 3) != 2)
            {
                StringBuilder confusedText = new StringBuilder();
                for (int l = 0; l < message.Length; l++)
                    if (message[l] != ' ')
                        switch (Dice.Roll(1, 8))
                        {
                            case 1: confusedText.Append('!'); break;
                            case 2: confusedText.Append('#'); break;
                            case 3: confusedText.Append('%'); break;
                            case 4: confusedText.Append('$'); break;
                            case 5: confusedText.Append('^'); break;
                            default: confusedText.Append(message[l]); break;
                        }
                    else confusedText.Append(" ");
                message = confusedText.ToString();
            }

            // SP depletion
            switch (mode)
            {
                case GameColor.Global:
                case GameColor.Town:
                case GameColor.Guild:
                case GameColor.Party:
                    //if (SP < 3) mode = ChatType.Local;
                    //else DepleteSP(3);
                    break;
            }

            byte[] data = new byte[17 + message.Length];
            Buffer.BlockCopy(ClientID.GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(X.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(Y.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(Name.GetBytes(10), 0, data, 6, 10);
            data[16] = (byte)((int)mode);
            Buffer.BlockCopy(message.GetBytes(message.Length), 0, data, 17, message.Length);

            switch (mode)
            {
                case GameColor.Whisper:
                    if (Cache.World.Players.ContainsKey(WhisperIndex))
                    {
                        Cache.World.Send(this, CommandType.Chat, CommandMessageType.Confirm, data);
                        Cache.World.Send(Cache.World.Players[WhisperIndex], CommandType.Chat, CommandMessageType.Confirm, data);
                    }
                    else
                    {
                        WhisperIndex = -1;
                        Notify(CommandMessageType.NotifyWhisperOff, "");
                    }
                    break;
                case GameColor.Local: // nearby chat based on mapView including offscreencells
                    foreach (Map map in Cache.World.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (Cache.World.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = Cache.World.Players[map.Players[p]];
                                if (player.ClientID == ClientID || player.IsWithinView(this))
                                    Cache.World.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case GameColor.Guild: // guild chat
                    if (Guild == Guild.None) break;
                    foreach (Map map in Cache.World.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (Cache.World.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = Cache.World.Players[map.Players[p]];
                                if (player.ClientID == ClientID || player.Guild == Guild)
                                    Cache.World.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case GameColor.Global: // global chat
                case GameColor.GameMaster:
                    foreach (Map map in Cache.World.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (Cache.World.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = Cache.World.Players[map.Players[p]];
                                Cache.World.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case GameColor.Town: // town chat
                    foreach (Map map in Cache.World.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (Cache.World.Players.ContainsKey(map.Players[p]))
                            {
                                Character player = Cache.World.Players[map.Players[p]];
                                if (player.Town == Town)
                                    Cache.World.Send(player, CommandType.Chat, CommandMessageType.Confirm, data);
                            }
                    break;
                case GameColor.Party: // party chat
                    if (HasParty)
                        for (int p = 0; p < Party.MemberCount; p++)
                            if (Cache.World.Players.ContainsKey(Party.Members[p]))
                                Cache.World.Send(Cache.World.Players[Party.Members[p]], CommandType.Chat, CommandMessageType.Confirm, data);
                    break;
            }

            // logging
            switch (mode)
            {
                case GameColor.Whisper:
                    if (Cache.World.Players.ContainsKey(WhisperIndex))
                        MessageLogged(Name + " [" + mode.ToString() + " >> " + Cache.World.Players[WhisperIndex].Name + "]: " + message, LogType.Chat);
                    else MessageLogged(Name + " [" + mode.ToString() + " >> Unknown]: " + message, LogType.Chat);
                    break;
                case GameColor.Guild:
                    MessageLogged(Name + " [" + Guild.Name + " " + mode.ToString() + " Chat]: " + message, LogType.Chat);
                    break;
                default:
                    MessageLogged(Name + " [" + mode.ToString() + " Chat]: " + message, LogType.Chat);
                    break;
            }
        }

        public void AcceptQuest()
        {
            // TODO - talktonpc (cityhall guy) gives "requestedquestid". accept it heres
        }

        public void TalkToNpc(Npc npc)
        {
            switch (npc.NpcType)
            {

            }
        }

        public void SetItemSet(int setNumber)
        {
            ClearItemSet(setNumber); //Clear Set number

            Item setItem;
            Item inventoryItem;
            for (int i = 0; i < Globals.MaximumEquipment; i++)
            {
                setItem = inventory[i];
                if (setItem != null)
                {
                    for (int x = 0; x < Globals.MaximumTotalItems; x++)
                    {
                        inventoryItem = inventory[x];
                        if (inventoryItem != null && inventoryItem.GuidMatch(setItem))
                        {
                            inventory[x].SetNumber = setNumber;
                            Notify(CommandMessageType.NotifyItemSet, x, setNumber);
                        }
                    }
                }
            }
        }

        public void ClearItemSet(int setNumber)
        {
            for (int x = 0; x < Globals.MaximumTotalItems; x++) //Go through equipped items and inventory
            {
                if (inventory[x] != null && inventory[x].SetNumber == setNumber)
                {
                    inventory[x].SetNumber = 0;
                }
            }
        }

        public void UseItemSet(int setNumber)
        {
            for (int x = Globals.MaximumEquipment; x < Globals.MaximumTotalItems; x++)
            {
                if (inventory[x] != null && inventory[x].SetNumber == setNumber && !inventory[x].IsEquipped) { EquipItem(x); }
            }

            Notify(CommandMessageType.NotifyItemSetUsed, setNumber);
        }

        public void JoinGuild(Character guildMaster)
        {
            this.guild = guildMaster.Guild;
            guildRank = 10;

            Notify(CommandMessageType.JoinGuildApprove, guildRank, guild.Name);

            Cache.World.SendCommonEventToNearbyPlayers(this, CommandMessageType.ClearGuildName, 0, 0, 0, 0);

            // TODO send DEF_NOTIFY_NEWGUILDSMAN to all guild members
        }

        public void CreateParty()
        {
            if (!HasParty)
            {
                party = new Party(id);

                Cache.World.Parties.Add(party);

                // notify self of creation "player joined the party"
                Notify(CommandMessageType.NotifyParty, (int)PartyAction.Create);
            }
            partyRequestID = -1;
        }

        public void JoinParty(Character character)
        {
            if (character == null) return;
            if (HasParty) { Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.InParty); return; }
            if (IsCombatMode || character.isCombatMode) { Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.InCombatMode); return; }
            if (this.Side != character.Side) { Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.InWrongTown); return; }
            if (character.PartyRequestID != -1) { Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.RequestPending); return; }


            character.Notify(CommandMessageType.NotifyParty, (int)PartyAction.Request, name);
            character.PartyRequestID = id;
        }

        public void LeaveParty(bool fromDisconnect = false)
        {
            // remove character from party
            if (HasParty)
            {
                // notify all members of player leaving
                for (int p = 0; p < party.Members.Count; p++)
                    if (Cache.World.Players.ContainsKey(party.Members[p]) && party.Members[p] != id)
                        Cache.World.Players[party.Members[p]].Notify(CommandMessageType.NotifyParty, (int)PartyAction.Update, (int)PartyAction.Leave, id);

                party.RemoveMember(id);

                // notify self
                if (!fromDisconnect) Notify(CommandMessageType.NotifyParty, (int)PartyAction.Leave);

                // if last member in party, remove the party from the world
                if (party.MemberCount <= 0) Cache.World.Parties.Remove(party);
                party = null;
            }
            partyRequestID = -1;
        }

        private void UpdateParty()
        {
            if (HasParty)
                for (int p = 0; p < Party.MemberCount; p++)
                    if (Cache.World.Players.ContainsKey(Party.Members[p]) && Party.Members[p] != id)
                        Notify(CommandMessageType.NotifyParty, (int)PartyAction.Update, (int)PartyAction.Info, Party.Members[p]);
        }

        public void HandlePartyRequest(bool accept)
        {
            if (Cache.World.Players.ContainsKey(partyRequestID))
            {
                Character newMember = Cache.World.Players[partyRequestID];

                if (!accept)
                {
                    newMember.Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.Rejected);
                }
                else
                {
                    if (!HasParty) CreateParty();

                    // add them to party, notify if party is full
                    if (party.MemberCount < Globals.MaximumPartyMembers)
                    {
                        party.AddMember(partyRequestID);
                        newMember.Party = party;
                        newMember.Notify(CommandMessageType.NotifyParty, (int)PartyAction.Join, 0);
                    }
                    else
                    {
                        newMember.Notify(CommandMessageType.NotifyParty, (int)PartyAction.Reject, (int)PartyRejectReason.PartyFull);
                        partyRequestID = -1;
                        return;
                    }

                    // notify all members of new player joining
                    for (int p = 0; p < party.Members.Count; p++)
                        if (party.Members[p] != -1 && party.Members[p] != partyRequestID && Cache.World.Players.ContainsKey(party.Members[p]))
                            Cache.World.Players[party.Members[p]].Notify(CommandMessageType.NotifyParty, (int)PartyAction.Update, (int)PartyAction.Join, partyRequestID);
                }
            }
            // clear request id to allow new requests
            partyRequestID = -1;
        }

        public void CreateGuild(string guildName)
        {
            if (Cache.World.IsCrusade) return;
            if (string.IsNullOrEmpty(guildName)) return;
            if (guildName.Equals(Guild.None.Name)) return;

            if (!HasGuild && level >= Globals.CreateGuildLevel) // && agility >= Globals.CreateGuildCharisma) removed
            {
                switch (Globals.EconomyType)
                {
                    case EconomyType.Classic:
                        if (GoldItem == null || GoldItem.Count < Globals.CreateGuildCost)
                        {
                            Notify(CommandMessageType.NotifyNotEnoughGold, -1);
                            Cache.World.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
                            return;
                        }
                        break;
                    case EconomyType.Virtual:
                        if (gold < Globals.CreateGuildCost)
                        {
                            Notify(CommandMessageType.NotifyNotEnoughGold, -1);
                            Cache.World.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
                            return;
                        }
                        break;
                }

                Guild guild = new Guild(guildName, Guid.NewGuid().ToString());
                guild.FoundedBy = name;
                guild.FoundedOn = DateTime.Now;
                if (GuildCreated(guild))
                {
                    // take payment
                    AddGold(-Globals.CreateGuildCost);

                    // set player guild
                    this.guild = guild;
                    this.guildRank = 0;
                    Cache.World.Guilds.Add(guild.Name, guild);
                    RemoveMagicEffect(MagicType.CreateDynamic); // TODO replace this; dummy to update nearby players

                    Cache.World.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Confirm);
                }
                else Cache.World.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
            }
            else Cache.World.Send(this, CommandType.ResponseCreateGuild, CommandMessageType.Reject);
        }

        public void DisbandGuild(string guildName)
        {
            if (Cache.World.IsCrusade) return;
            if (string.IsNullOrEmpty(guildName)) return;
            if (guildName.Equals(Guild.None.Name)) return;

            if (HasGuild && guildRank == 0 && guild.Name.Equals(guildName))
            {
                if (Cache.World.Guilds.ContainsKey(guildName) && GuildDisbanded(Cache.World.Guilds[guildName]))
                {
                    foreach (Map map in Cache.World.Maps.Values)
                        for (int p = 0; p < map.Players.Count; p++)
                            if (Cache.World.Players.ContainsKey(map.Players[p]) && Cache.World.Players[map.Players[p]].Guild.ID == Cache.World.Guilds[guildName].ID)
                            {
                                if (map.Players[p] != ClientID) Cache.World.Players[map.Players[p]].Notify(CommandMessageType.NotifyGuildDisbanded, Cache.World.Players[map.Players[p]].Town.ToString().ToLower());

                                Cache.World.Players[map.Players[p]].Guild = Guild.None;
                                Cache.World.Players[map.Players[p]].GuildRank = -1;

                                Cache.World.SendCommonEventToNearbyPlayers(Cache.World.Players[map.Players[p]], CommandMessageType.ClearGuildName, 0, 0, 0, 0);
                            }

                    Cache.World.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Confirm);
                }
                else Cache.World.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Reject);
            }
            else Cache.World.Send(this, CommandType.ResponseDisbandGuild, CommandMessageType.Reject);
        }

        public void SetCrusadeDuty(int duty)
        {
            if (Enum.TryParse<CrusadeDuty>(duty.ToString(), out crusadeDuty))
                switch (crusadeDuty)
                {
                    case CrusadeDuty.Commander:
                        if (guildRank != 0) crusadeDuty = CrusadeDuty.None;
                        else crusadeConstructionPoints = 3000;
                        break;
                }

            Notify(CommandMessageType.NotifyCrusade, (int)CrusadeMode.Start, (int)crusadeDuty, 0);
        }

        public bool SetCondition(ConditionGroup group, MagicType type)
        {
            return false;
        }

        public bool SetMagicEffect(Magic magic, bool firm = false)
        {
            if (magicEffects.ContainsKey(magic.Type)) return false;
            magicEffects.Add(magic.Type, new MagicEffect(magic, (firm ? DateTime.Now.AddYears(1) : DateTime.Now.Add(magic.LastTime))));

            // flags and notify
            unchecked
            {
                switch (magic.Type)
                {
                    case MagicType.Berserk:
                        status = status | 0x00000020;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Invisibility:
                        status = status | 0x00000010;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.IceLine: // uses Ice effect type
                    case MagicType.Ice:
                        status = status | 0x00000040;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)MagicType.Ice, 1);
                        break;
                    case MagicType.Protect:
                        switch (magic.Effect1)
                        {
                            case 1: status = status | 0x08000000; break; // arrow
                            case 2:
                            case 5: status = status | 0x04000000; break; // magic
                            case 3:
                            case 4: status = status | 0x02000000; break; // physical
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Poison:
                        status = status | 0x00000080;
                        poisonTime = DateTime.Now;
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect2);
                        UpdateParty();
                        break;
                    case MagicType.Paralyze:
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.Confuse:
                        switch (magic.Effect1)
                        {
                            case 3: // illusion
                            case 4: status = status | 0x01000000; break; // illusion movement
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOn, (int)magic.Type, magic.Effect1);
                        break;
                    case MagicType.TurnUndead: break;
                }
            }

            if (magic.Type == MagicType.Berserk)
                Notify(CommandMessageType.NotifyDamageStatistics,
                            GetMinMeleeDamage(false, false),
                            GetMaxMeleeDamage(false, false),
                            GetMinMeleeDamage(true, false),
                            GetMaxMeleeDamage(true, false));

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public bool RemoveMagicEffect(MagicType type)
        {
            if (!magicEffects.ContainsKey(type)) return false;

            // flags and notify
            unchecked
            {
                switch (type)
                {
                    case MagicType.Berserk:
                        status = status & (int)0xFFFFFFDF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Invisibility:
                        status = status & (int)0xFFFFFFEF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Ice:
                        status = status & (int)0xFFFFFFBF;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        break;
                    case MagicType.Protect:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 1: status = status & (int)0xF7FFFFFF; break; // arrows
                            case 2:
                            case 5: status = status & (int)0xFBFFFFFF; break; // magic
                            case 3:
                            case 4: status = status & (int)0xFDFFFFFF; break; // physical
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type, magicEffects[type].Magic.Effect1);
                        break;
                    case MagicType.Poison:
                        status = status & (int)0xFFFFFF7F;
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type);
                        UpdateParty();
                        break;
                    case MagicType.Paralyze:
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type, magicEffects[type].Magic.Effect1);
                        break;
                    case MagicType.Confuse:
                        switch (magicEffects[type].Magic.Effect1)
                        {
                            case 3: // illusion
                            case 4: status = status & (int)0xFEFFFFFF; break; // illusion movement
                        }
                        Notify(CommandMessageType.NotifyMagicEffectOff, (int)type, magicEffects[type].Magic.Effect1);
                        break;
                }
            }

            magicEffects.Remove(type);

            if (type == MagicType.Berserk)
                Notify(CommandMessageType.NotifyDamageStatistics,
                        GetMinMeleeDamage(false, false),
                        GetMaxMeleeDamage(false, false),
                        GetMinMeleeDamage(true, false),
                        GetMaxMeleeDamage(true, false));

            if (StatusChanged != null) StatusChanged(this);
            return true;
        }

        public void UseSpecialAbility(int abilityType)
        {
            ItemSpecialAbilityType type = (ItemSpecialAbilityType)abilityType;
            if (specialAbilityTime.TotalSeconds > 0) return;
            if (specialAbilityEnabled) return;
            if (SpecialAbilitiesActive != null && SpecialAbilitiesActive.Count > 0 && SpecialAbilitiesActive.ContainsKey(type))
            {
                specialAbilityEnabled = true;
                specialAbilityStartTime = DateTime.Now;

                switch (type)
                {
                    case ItemSpecialAbilityType.None: break;
                    case ItemSpecialAbilityType.IceWeapon:
                    case ItemSpecialAbilityType.MedusaWeapon:
                    case ItemSpecialAbilityType.XelimaWeapon:
                        {
                            appearance4 = ((appearance4 & 0xFF0F) | 0x0010);
                            Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Activated, (int)type, inventory.ActivationTime);
                            break;
                        }
                    case ItemSpecialAbilityType.MerienArmour:
                    case ItemSpecialAbilityType.MerienShield:
                        {
                            appearance4 = ((appearance4 & 0xFF0F) | 0x0020);
                            Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Activated, (int)type, inventory.ActivationTime);
                            break;
                        }
                }
            }

            if (StatusChanged != null) StatusChanged(this);

            //specialAbilityEnabled = true; 
            //specialAbilityStartTime = DateTime.Now;

            //switch (inventory[specialAbilityItemIndex].SpecialAbilityType)
            //{
            //    case ItemSpecialAbilityType.IceWeapon:
            //    case ItemSpecialAbilityType.MedusaWeapon:
            //    case ItemSpecialAbilityType.XelimaWeapon:
            //        appearance4 = ((appearance4 & 0xFF0F) | 0x0010);
            //        break;
            //    case ItemSpecialAbilityType.MerienArmour:
            //    case ItemSpecialAbilityType.MerienShield:
            //    case ItemSpecialAbilityType.Unknown:
            //        appearance4 = ((appearance4 & 0xFF0F) | 0x0020);
            //        break;
            //}


            //Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Activated, (int)inventory[specialAbilityItemIndex].SpecialAbilityType, inventory[specialAbilityItemIndex].SpecialEffect1);

            //if (StatusChanged != null) StatusChanged(this);
        }

        /// <summary>
        /// Handles special abilities.
        /// </summary>
        public void ChargeSpecialAbility()
        {
            if (specialAbilityEnabled && SpecialAbilitiesActive != null && SpecialAbilitiesActive.Count > 0)
            {
                TimeSpan ts = DateTime.Now - specialAbilityStartTime;
                if (ts.TotalSeconds >= inventory.ActivationTime)
                {
                    Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Expired, 0, 0);
                    specialAbilityEnabled = false;
                    specialAbilityTime = new TimeSpan(0, 0, Globals.SpecialAbilityTime);

                    appearance4 = (appearance4 & 0xFF0F);

                    if (StatusChanged != null) StatusChanged(this);
                }
            }
            else
            {
                if (specialAbilityTime.TotalSeconds <= 0) return;

                specialAbilityTime = specialAbilityTime.Subtract(new TimeSpan(0, 0, 3)); // this happens on the timer every 3 seconds

                if (specialAbilityTime.TotalSeconds <= 0)
                {
                    specialAbilityTime = new TimeSpan(0, 0, 0);
                    Notify(CommandMessageType.NotifySpecialAbilityEnabled);
                }
            }
        }

        public void ChargeCritical()
        {
            //Critical Increase Bonus --//TODO adjust?
            if (inventory.CriticalIncreaseChanceBonus > 0)
            {
                int result = Dice.Roll(1, 100);
                if (result <= inventory.CriticalIncreaseChanceBonus) { criticalCharge++; }
            }

            criticalCharge++;

            if (criticalCharge > 14)
            {
                criticals++;
                if (criticals > MaxCriticals) criticals = MaxCriticals;
                Notify(CommandMessageType.NotifyCriticals, criticals);
                criticalCharge = 0;
            }
        }

        /// <summary>
        /// Handles setting of all stats. i.e after level up.
        /// </summary>
        /// <returns>True or False to indicate that stat change has succeeded or not.</returns>
        public bool ChangeStats(int strength, int vitality, int dexterity, int intelligence, int magic, int agility)
        {
            if (this.strength + this.vitality + this.dexterity + this.intelligence + this.magic + this.agility +
                strength + vitality + dexterity + intelligence + magic + agility > TotalStatPoints)
            {
                Notify(CommandMessageType.NotifyStatChangeLevelUpFailed);
                return false;
            }

            if (this.strength + strength > Globals.MaximumStat || this.dexterity + dexterity > Globals.MaximumStat || this.vitality + vitality > Globals.MaximumStat ||
                this.magic + magic > Globals.MaximumStat || this.agility + agility > Globals.MaximumStat || this.intelligence + intelligence > Globals.MaximumStat)
            {
                Notify(CommandMessageType.NotifyStatChangeLevelUpFailed);
                return false;
            }

            this.strength += strength;
            this.dexterity += dexterity;
            this.vitality += vitality;
            this.intelligence += intelligence;
            this.magic += magic;
            this.agility += agility;

            // if strength changes, check swing speed and update status
            if (strength > 0 && Weapon != null)
            {
                unchecked
                {
                    int temp;
                    int speed = Weapon.Speed - ((this.strength + this.StrengthBonus) / 13);
                    if (speed < 0) speed = 0;

                    temp = status;
                    temp = temp & (int)0xFFFFFFF0;
                    temp = temp | speed;
                    status = temp;
                    if (StatusChanged != null) StatusChanged(this);
                }
            }


            Notify(CommandMessageType.NotifyStatChangeLevelUpSuccess);

            return true;
        }

        /// <summary>
        /// Handles decrement of a particular stat by 1 using majestic points.
        /// </summary>
        /// <param name="stat">Enum type of the stat. These are hexidecimal values used by the client.</param>
        public void ChangeStat(Stat stat)
        {
            if (majestics <= 0)
            {
                Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
            }

            switch (stat)
            {
                case Stat.Strength:
                    if (strength - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    strength--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Dexterity:
                    if (dexterity - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    dexterity--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Intelligence:
                    if (intelligence - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    intelligence--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Magic:
                    if (magic - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    magic--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Vitality:
                    if (vitality - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    vitality--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                case Stat.Agility:
                    if (agility - 1 < 10)
                    {
                        Notify(CommandMessageType.NotifyStatChangeMajesticsFailed);
                        return;
                    }
                    agility--;
                    majestics--;
                    Notify(CommandMessageType.NotifyMajestics, majestics);
                    Notify(CommandMessageType.NotifyStatChangeMajesticsSuccess);
                    break;
                default: return;
            }

            // stat changes may cause the player to have too many points:
            if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
            if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
            if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }
        }

        /// <summary>
        /// Loads equipped items in to the correct slots (Arms, Body, Legs, Feet) etc.
        /// </summary>
        public void LoadEquipment()
        {
            for (int i = 0; i < Globals.MaximumEquipment; i++)
                if (inventory[i] != null && inventory[i].IsEquipped)
                    EquipItem(i);
        }

        public void Parse(byte[] data)
        {
            name = Encoding.ASCII.GetString(data, 6, 10).Trim('\0');
            gender = (GenderType)((int)data[66]);
            skin = (SkinType)((int)data[67]);
            hairStyle = (int)data[68];
            hairColour = (int)data[69];
            underwearColour = (int)data[70];
            strength = (int)data[71];
            vitality = (int)data[72];
            dexterity = (int)data[73];
            intelligence = (int)data[74];
            magic = (int)data[75];
            agility = (int)data[76];
            town = (OwnerSide)((int)data[77]);

            appearance1 = appearance2 = appearance3 = appearance4 = appearanceColour = 0;
            appearance1 = appearance1 | underwearColour;
            appearance1 = appearance1 | (hairStyle << 8);
            appearance1 = appearance1 | (hairColour << 4);
        }

        /// <summary>
        /// Updates Equipment and changes the Appearance ready to be sent to the client.
        /// </summary>
        /// <param name="itemIndex">Index of the item in the InventoryV1 list.</param>
        /// <returns>True or False to indicate successful Equip of the item.</returns>
        public bool EquipItem(int itemIndex)
        {

            if (inventory[itemIndex] == null) return false;
            if (itemIndex < Globals.MaximumEquipment) return false;

            Item item = inventory[itemIndex];
            if (item.Type != ItemType.Equip) return false;
            if (item.EquipType == EquipType.None) return false;
            if (item.IsEquipped) return false;
            if (item.IsBroken) return false;
            if (item.LevelLimit > level) return false;
            if (item.GenderLimit != GenderType.None && item.GenderLimit != gender) return false;
            if (item.MinimumStrength > strength + StrengthBonus) return false;
            if (item.MinimumDexterity > dexterity + DexterityBonus) return false;
            if (item.MinimumVitality > vitality + VitalityBonus) return false;
            if (item.MinimumIntelligence > intelligence + IntelligenceBonus) return false;
            if (item.MinimumMagic > magic + MagicBonus) return false;
            if (item.MinimumAgility > agility + AgilityBonus) return false;
            if (!(item.BoundID.Equals(Globals.UnboundItem)))
                if (!(item.BoundID.Equals(this.databaseID)))
                {
                    Cache.World.SendClientMessage(this, "This item is bound to " + item.BoundName);
                    return false;
                }

            lock (statusLock)
            {
                switch (item.EquipType)
                {
                    case EquipType.LeftHand:
                    case EquipType.RightHand: // shield or weapon should unequip existing dual hand
                        if (inventory[(int)EquipType.RightHand] != null && inventory[(int)EquipType.RightHand].EquipType == EquipType.DualHand) UnequipItem((int)EquipType.RightHand);
                        if (inventory[(int)item.EquipType] != null) UnequipItem((int)item.EquipType);
                        break;
                    case EquipType.DualHand: // dual hand should unequip existing left and right hands
                        if (inventory[(int)EquipType.LeftHand] != null) UnequipItem((int)EquipType.LeftHand); //Shield
                        if (inventory[(int)EquipType.RightHand] != null) UnequipItem((int)EquipType.RightHand); //Weapon
                        break;
                    case EquipType.FullBody: // full body should unequip any other equipment slots
                        if (inventory[(int)EquipType.Head] != null) UnequipItem((int)EquipType.Head);
                        if (inventory[(int)EquipType.Body] != null) UnequipItem((int)EquipType.Body);
                        if (inventory[(int)EquipType.Arms] != null) UnequipItem((int)EquipType.Arms);
                        if (inventory[(int)EquipType.Feet] != null) UnequipItem((int)EquipType.Feet);
                        if (inventory[(int)EquipType.Legs] != null) UnequipItem((int)EquipType.Legs);
                        if (inventory[(int)EquipType.Back] != null) UnequipItem((int)EquipType.Back);
                        break;
                    case EquipType.Head:
                    case EquipType.Body:
                    case EquipType.Arms:
                    case EquipType.Feet:
                    case EquipType.Legs:
                    case EquipType.Back: // armour and cape should unequip full body costumes
                        if (inventory[(int)EquipType.Body] != null && inventory[(int)EquipType.Body].EquipType == EquipType.FullBody) UnequipItem((int)EquipType.Body);
                        if (inventory[(int)item.EquipType] != null) UnequipItem((int)item.EquipType);
                        break;
                    default: if (inventory[(int)item.EquipType] != null) UnequipItem((int)item.EquipType); break;
                }

                //inventory[itemIndex] = null;
                inventory[itemIndex].IsEquipped = true;
                item.IsEquipped = true;

                // equip the item - handle special cases
                switch (item.EquipType)
                {
                    case EquipType.DualHand: inventory[(int)EquipType.RightHand] = item; break;
                    case EquipType.FullBody: inventory[(int)EquipType.Body] = item; break;
                    default: inventory[(int)item.EquipType] = item; break;
                }

                int temp;
                switch (item.EquipType)
                {
                    case EquipType.RightHand:
                        /*temp = appearance2;
                        temp = temp & 0xF00F;
                        temp = temp | (item.Appearance << 4);
                        appearance2 = temp;

                        temp = appearanceColour;
                        temp = temp & 0x0FFFFFFF;
                        temp = temp | (item.Colour << 28);
                        appearanceColour = temp;*/

                        unchecked
                        {
                            int speed = item.Speed - ((strength + StrengthBonus) / 13);
                            if (speed < 0) speed = 0;

                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | speed;
                            status = temp;
                        }
                        break;
                    case EquipType.LeftHand:
                        /*temp = appearance2;
                        temp = temp & 0xFFF0;
                        temp = temp | (item.Appearance);
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xF0FFFFFF;
                            temp = temp | (item.Colour << 24);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.DualHand:
                        /*temp = appearance2;
                        temp = temp & 0xF00F;
                        temp = temp | (item.Appearance << 4);
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0x0FFFFFFF;
                            temp = temp | (item.Colour << 28);
                            appearanceColour = temp;
                        }*/

                        unchecked
                        {
                            int speed = item.Speed - ((strength + StrengthBonus) / 13);
                            if (speed < 0) speed = 0;

                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | speed;
                            status = temp;
                        }
                        break;
                    case EquipType.Body:
                        /*if (item.Appearance < 100)
                        {
                            temp = appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            appearance3 = temp;
                        }
                        else
                        {
                            temp = appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | ((item.Appearance - 100) << 12);
                            appearance3 = temp;

                            temp = appearance4;
                            temp = temp | 0x080;
                            appearance4 = temp;
                        }

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFF0FFFFF;
                            temp = temp | (item.Colour << 20);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.Back:
                        /*temp = appearance4;
                        temp = temp & 0xF0FF;
                        temp = temp | (item.Appearance << 8);
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            temp = temp | (item.Colour << 16);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.Arms:
                        /*temp = appearance3;
                        temp = temp & 0xFFF0;
                        temp = temp | (item.Appearance);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFF0FFF;
                            temp = temp | (item.Colour << 12);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.Legs:
                        /*temp = appearance3;
                        temp = temp & 0xF0FF;
                        temp = temp | (item.Appearance << 8);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFF0FF;
                            temp = temp | (item.Colour << 8);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.Feet:
                        /*temp = appearance4;
                        temp = temp & 0x0FFF;
                        temp = temp | (item.Appearance << 12);
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFF0F;
                            temp = temp | (item.Colour << 4);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.Head:
                        /*temp = appearance3;
                        temp = temp & 0xFF0F;
                        temp = temp | (item.Appearance << 4);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | (item.Colour);
                            appearanceColour = temp;
                        }*/
                        break;
                    case EquipType.FullBody:
                        /*temp = appearance3;
                        temp = temp & 0x0FFF;
                        temp = temp | (item.Appearance << 12);
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }*/
                        break;
                }

                // weapon glows
                //if (item.EffectType == ItemEffectType.AttackActivation)
                //{
                //if (specialAbilityItemIndex != -1)
                //{
                //    Notify(CommandMessageType.NotifyItemUnequipped, (int)inventory[specialAbilityItemIndex].EquipType, specialAbilityItemIndex);
                //    UnequipItem(specialAbilityItemIndex);
                //}
                //appearance4 = appearance4 & 0xFFF3;
                //switch (item.SpecialAbilityType)
                //{
                //    case ItemSpecialAbilityType.XelimaWeapon:
                //        appearance4 = appearance4 | 0x0004;
                //        break;
                //    case ItemSpecialAbilityType.IceWeapon:
                //        appearance4 = appearance4 | 0x000C;
                //        break;
                //    case ItemSpecialAbilityType.MedusaWeapon:
                //        appearance4 = appearance4 | 0x0008;
                //        break;
                //}
                //specialAbilityItemIndex = itemIndex;
                //Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Equipped, (int)item.SpecialAbilityTypeActive, (int)specialAbilityTime.TotalSeconds);
                //}

                //// armour glows
                //if (item.EffectType == ItemEffectType.DefenceActivation)
                //{
                //if (specialAbilityItemIndex != -1)
                //{
                //    Notify(CommandMessageType.NotifyItemUnequipped, (int)inventory[specialAbilityItemIndex].EquipType, specialAbilityItemIndex);
                //    UnequipItem(specialAbilityItemIndex);
                //}
                //appearance4 = appearance4 & 0xFFFC;
                //switch (item.SpecialAbilityType)
                //{
                //    case ItemSpecialAbilityType.MerienArmour:
                //    case ItemSpecialAbilityType.MerienShield:
                //    case ItemSpecialAbilityType.Unknown:
                //        appearance4 = appearance4 | 0x0002;
                //        break;
                //}
                //specialAbilityItemIndex = itemIndex;
                //    Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Equipped, (int)item.SpecialAbilityType, (int)specialAbilityTime.TotalSeconds);
                //}

                // angels
                if (item.IsAngel)
                {
                    switch ((AngelType)item.Effect1)
                    {
                        case AngelType.Str: status = status | 0x00001000; break; //TODO FIX FOR VIT/AGI ANGEL's
                        case AngelType.Dex: status = status | 0x00002000; break;
                        case AngelType.Int: status = status | 0x00004000; break;
                        case AngelType.Mag: status = status | 0x00008000; break;
                    }
                    if (item.Level > 5)
                    {
                        int stars = (item.Level / 3) * (item.Level / 5);
                        status = status | (stars << 8);
                    }
                    Notify(CommandMessageType.NotifyAngelStats, StrengthBonus, IntelligenceBonus, DexterityBonus, MagicBonus);
                }

                Notify(CommandMessageType.NotifyItemEquipped, itemIndex);

                inventory.Update();

                if (IsReady)
                {
                    // stat changes may cause the player to have too many points:
                    if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
                    if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
                    if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }

                    Notify(CommandMessageType.NotifyDamageStatistics,
                            GetMinMeleeDamage(false, false),
                            GetMaxMeleeDamage(false, false),
                            GetMinMeleeDamage(true, false),
                            GetMaxMeleeDamage(true, false));
                }

                if (StatusChanged != null) StatusChanged(this);
                return true;
            }
        }

        public bool GiveItem(int itemIndex, int count, int destinationX, int destinationY, int ownerID, string itemName)
        {
            if (isDead) return false;
            if (ownerID == 0) return false;
            if (CurrentMap[destinationY][destinationX].Owner == null) return false;
            if (inventory[itemIndex] == null || count < 1 || count > inventory[itemIndex].Count) return false;

            Item item = inventory[itemIndex];
            IOwner target = CurrentMap[destinationY][destinationX].Owner;
            switch (target.OwnerType)
            {
                case OwnerType.Npc:
                    if (target.ObjectId != ownerID - 10000) return false;
                    //if (target.Name.Equals(Globals.WarehouseKeeperName)) // TODO add NpcType="WarehouseKeeper" either to map config or npc config instead of global strings
                    //SetWarehouseItem(itemIndex, count);

                    break;
                case OwnerType.Player:
                    Character character = (Character)target;
                    if (character.ObjectId != ownerID) return false;

                    if (item.EffectType == ItemEffectType.GuildAdmission)
                    {
                        if (!HasGuild && town != OwnerSide.Neutral)//Removed crusade restriction cause that was silly
                        {
                            inventory[itemIndex] = null;
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                            character.Notify(CommandMessageType.NotifyGuildAdmissionRequest, this.name);
                        }
                    }
                    else if (item.EffectType == ItemEffectType.GuildDismissal)
                    {
                        if (!Cache.World.IsCrusade && HasGuild && Guild == character.Guild && character.GuildRank == 0)
                        {
                            inventory[itemIndex] = null;
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                            character.Notify(CommandMessageType.NotifyGuildDismissalRequest, this.name);
                        }
                    }
                    else
                    {
                        inventory[itemIndex] = null;
                        inventory.Update();
                        if (!character.isDead && character.AddInventoryItem(item))
                            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, target.Name);
                        else DropItem(itemIndex, count, item.ItemId);
                    }
                    break;
            }

            return true;
        }

        /// <summary>
        /// Updates Equipment and changes the Appearance ready to be sent to the client.
        /// </summary>
        /// <param name="itemIndex">Index of the item in the InventoryV1 list.</param>
        /// <returns>True or False to indicate successful Un-Equip of the item.</returns>
        public bool UnequipItem(int equippedItemIndex, int destinationItemIndex = -1)
        {
            if (inventory[equippedItemIndex] == null) return false;

            Item equippedItem = inventory[equippedItemIndex];
            if (!equippedItem.IsEquipped) return false;
            if (equippedItem.Type != ItemType.Equip) return false;
            if (equippedItem.EquipType == EquipType.None) return false;

            int originalItemIndex = -1;
            for (int itemlocation = Globals.MaximumEquipment; itemlocation < Globals.MaximumTotalItems; itemlocation++)
            {
                if (inventory[itemlocation] != null)
                {
                    if (inventory[itemlocation].GuidMatch(inventory[equippedItemIndex])) { originalItemIndex = itemlocation; }
                }
            }

            if (originalItemIndex == -1) return false; //cant find original location
            if (inventory[originalItemIndex] == null) return false;

            Item originalItem = inventory[originalItemIndex];
            originalItem.IsEquipped = false;
            EquipType type = originalItem.EquipType;

            lock (statusLock)
            {
                if (equippedItemIndex == originalItemIndex)
                {
                    inventory[originalItemIndex] = originalItem; // set unequip
                }
                else
                {
                    //inventory[equippedItemIndex] = null; // remove the equipped item

                    if (destinationItemIndex != -1 && inventory[destinationItemIndex] == null) //Swap Original with destination if Destination
                    {
                        inventory[destinationItemIndex] = originalItem;
                        inventory[originalItemIndex] = null;
                    }
                    else
                    {
                        inventory[originalItemIndex] = originalItem;
                    }

                }

                //remove from equipement slot
                switch (type)
                {
                    case EquipType.DualHand: inventory[(int)EquipType.RightHand] = null; break;
                    case EquipType.FullBody: inventory[(int)EquipType.Body] = null; break;
                    default: inventory[(int)type] = null; break;
                }

                //else{ inventory[originalItemIndex].IsEquipped = false; } //Set original as unequipped 
                //else { int outIndex; AddInventoryItem(equippedItem, out outIndex, false); } // adds it back to next available inventory slot //equipped items take up inventory space now

                //inventory.Update();

                int temp;
                switch (equippedItem.EquipType)
                {
                    case EquipType.RightHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        appearance2 = temp;

                        temp = appearanceColour;
                        temp = temp & 0x0FFFFFFF;
                        appearanceColour = temp;

                        unchecked // speed 0 when using fists
                        {
                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | 0;
                            status = temp;
                        }
                        break;
                    case EquipType.LeftHand:
                        temp = appearance2;
                        temp = temp & 0xFFF0;
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xF0FFFFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.DualHand:
                        temp = appearance2;
                        temp = temp & 0xF00F;
                        appearance2 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0x0FFFFFFF;
                            appearanceColour = temp;
                        }

                        unchecked // speed 0 when using fists
                        {
                            temp = status;
                            temp = temp & (int)0xFFFFFFF0;
                            temp = temp | 0;
                            status = temp;
                        }
                        break;
                    case EquipType.Body:
                        temp = appearance3;
                        temp = temp & 0x0FFF;
                        appearance3 = temp;

                        temp = appearance4;
                        temp = temp & 0xFF7F;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFF0FFFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Back:
                        temp = appearance4;
                        temp = temp & 0xF0FF;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Arms:
                        temp = appearance3;
                        temp = temp & 0xFFF0;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFF0FFF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Legs:
                        temp = appearance3;
                        temp = temp & 0xF0FF;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFF0FF;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Feet:
                        temp = appearance4;
                        temp = temp & 0x0FFF;
                        appearance4 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFF0F;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.Head:
                        temp = appearance3;
                        temp = temp & 0xFF0F;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFFFFFF0;
                            appearanceColour = temp;
                        }
                        break;
                    case EquipType.FullBody:
                        temp = appearance3;
                        temp = temp & 0x0FFF;
                        appearance3 = temp;

                        unchecked
                        {
                            temp = appearanceColour;
                            temp = temp & (int)0xFFF0FFFF;
                            appearanceColour = temp;
                        }
                        break;
                }

                // end activation if the item is unequipped while in use
                //if ((item.EffectType == ItemEffectType.AttackActivation || item.EffectType == ItemEffectType.DefenceActivation) && (itemIndex == specialAbilityItemIndex) && specialAbilityEnabled)


                //if (specialAbilityEnabled && item.SpecialAbilityTypeActive) { specialAbilityTime

                //if ((item.EffectType == ItemEffectType.AttackActivation || item.EffectType == ItemEffectType.DefenceActivation) && specialAbilityEnabled)
                //{
                //    Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Expired, 0, 0);
                //    specialAbilityEnabled = false;
                //    specialAbilityTime = new TimeSpan(0, 0, Globals.SpecialAbilityTime);

                //    appearance4 = (appearance4 & 0xFF0F);
                //specialAbilityItemIndex = -1;
                //}
                //else if (item.EffectType == ItemEffectType.AttackActivation || item.EffectType == ItemEffectType.DefenceActivation)
                //{
                //    Notify(CommandMessageType.NotifySpecialAbilityStatus, (int)ItemSpecialAbilityStatus.Unequipped, 0, 0);
                //    specialAbilityEnabled = false;
                //    appearance4 = (appearance4 & 0xFF0F);
                //specialAbilityItemIndex = -1;
                //}

                // remove weapon glows //Move to draw?
                //if (item.EffectType == ItemEffectType.AttackActivation)
                //{
                //    appearance4 = appearance4 & 0xFFF3;
                //if (itemIndex == specialAbilityItemIndex) specialAbilityItemIndex = -1;
                //}
                // remove armour glows //Move to draw
                //if (item.EffectType == ItemEffectType.DefenceActivation)
                //{
                //    appearance4 = appearance4 & 0xFFFC;
                //if (itemIndex == specialAbilityItemIndex) specialAbilityItemIndex = -1;
                //}

                // angels
                if (equippedItem.IsAngel)
                {
                    unchecked { status = status & (int)0xFFFF00FF; }
                    Notify(CommandMessageType.NotifyAngelStats, StrengthBonus, IntelligenceBonus, DexterityBonus, MagicBonus);
                }

                // check that minimum stat requirements are still upheld (e.g after removal of strength/dexterity angels
                //TODO create equiped list?
                //for (int i = 0; i < 15; i++)
                //    if ((equipment[i] != -1) &&
                //        (inventory[equipment[i]].MinimumStrength > strength + StrengthBonus ||
                //        inventory[equipment[i]].MinimumDexterity > dexterity + DexterityBonus ||
                //        inventory[equipment[i]].MinimumVitality > vitality + VitalityBonus ||
                //        inventory[equipment[i]].MinimumIntelligence > intelligence + IntelligenceBonus ||
                //        inventory[equipment[i]].MinimumMagic > magic + MagicBonus ||
                //        inventory[equipment[i]].MinimumAgility > agility + AgilityBonus))
                //        UnequipItem(equipment[i]);

                Notify(CommandMessageType.NotifyItemUnequipped, equippedItemIndex, originalItemIndex, destinationItemIndex);

                inventory.Update();

                if (IsReady)
                {
                    // stat changes may cause the player to have too many points:
                    if (hp > MaxHP) { hp = MaxHP; Notify(CommandMessageType.NotifyHP, hp, mp); }
                    if (mp > MaxMP) { mp = MaxMP; Notify(CommandMessageType.NotifyMP, mp); }
                    if (sp > MaxSP) { sp = MaxSP; Notify(CommandMessageType.NotifySP, sp); }

                    Notify(CommandMessageType.NotifyDamageStatistics,
                            GetMinMeleeDamage(false, false),
                            GetMaxMeleeDamage(false, false),
                            GetMinMeleeDamage(true, false),
                            GetMaxMeleeDamage(true, false));
                }

                if (StatusChanged != null) StatusChanged(this);
                return true;
            }
        }

        public bool CastMagic(int sourceX, int sourceY, Magic magic) { throw new NotImplementedException(); }

        /// <summary>
        /// Character casts a spell at the destination specified. Checks requirements only. SpellBook effects are handled by GameServer.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="magic">SpellBook type being cast.</param>
        /// <returns>True or False to indicate if CastMagic has failed or not.</returns>
        public bool CastMagic(int sourceX, int sourceY, Magic magic, out bool notifyFailed)
        {
            notifyFailed = false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (Shield != null) return false; // no shield casting
            if (Weapon != null && Weapon.RelatedSkill != SkillType.Staff && Weapon.RelatedSkill != SkillType.BattleStaff) return false; // staff casting only
            if (magic == null) return false;
            if ((intelligence + IntelligenceBonus) < magic.RequiredIntelligence) { notifyFailed = true; return false; }
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start
            int manaCost = Utility.GetManaCost(magic.ManaCost, inventory.ManaSaveBonus);
            if (mp < manaCost) return false;

            if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap || CurrentMap[sourceY][sourceX].IsSafe)
                switch (magic.Category)
                {
                    case MagicCategory.Offensive: return false;
                    default: break; // utility, support and defensive spells can be cast
                }

            int probability = Utility.GetCastingProbability(skills[SkillType.Magic].Level, magic.Level, level, intelligence, map.Weather, inventory.CastProbabilityBonus, sp);
            if (probability < Dice.Roll(1, 100))
            {
                notifyFailed = true;
                return false;
            }

            if (!spellStatus[magic.Index]) // not learned (packet edit?)
            {
                notifyFailed = true;
                return false;
            }

            skills[SkillType.Magic].Experience++;
            return true;
        }

        /// <summary>
        /// CharacterV1 starts casting a spell.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <returns>True or False to indicate if PrepareMagic has failed or not.</returns>
        public bool PrepareMagic(int sourceX, int sourceY, MotionDirection direction)
        {
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (Shield != null) return false; // no shield casting
            if (Weapon != null && Weapon.RelatedSkill != SkillType.Staff) return false; // staff casting only
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction;

            return true;
        }

        public int GetMagicHitChance(Magic spell)
        {
            int magicCircle = (spell.Index / 10) + 1;
            int magicLevel = (level / 10);
            int hitChance = Math.Max(skills[SkillType.Magic].Level, 1);
            double temp1 = ((double)hitChance / 100.0f);
            double temp2 = ((double)(temp1 * (double)Globals.MagicCastingProbability[magicCircle]));
            hitChance = (int)temp2;

            if (intelligence + IntelligenceBonus > 50) hitChance += (intelligence + IntelligenceBonus) / 2;

            if (magicCircle != magicLevel)
                if (magicCircle > magicLevel)
                {
                    temp1 = (double)((magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle]);
                    temp2 = (double)((magicCircle - magicLevel) * 10);
                    double temp3 = ((double)level / temp2) * temp1;
                    hitChance -= Math.Abs(Math.Abs(magicCircle - magicLevel) * Globals.MagicCastingLevelPenalty[magicCircle] - (int)temp3);
                }
                else hitChance += 5 * Math.Abs(magicCircle - magicLevel);

            switch (CurrentMap.Weather)
            {
                case WeatherType.Rain: hitChance -= (hitChance / 24); break; // 4% reduction
                case WeatherType.RainMedium: hitChance -= (hitChance / 12); break; // 8% reduction
                case WeatherType.RainHeavy: hitChance -= (hitChance / 5); break; // 20% reduction
            }

            if (magic + MagicBonus > 50) hitChance += (magic + MagicBonus) - 50;

            hitChance += inventory.HitChanceBonus;

            int result = Dice.Roll(1, 100);
            if (hitChance < result) return -1;

            if (hitChance < 0) hitChance = 1;

            return hitChance;
        }

        //Used before all attacks to check for fails
        public bool PrepareAttack(IOwner target, ref bool takeDamage, ref bool destroyWeapon, ref int targetReputation)
        {
            if (target == null) return false;
            if (target.IsDead) return false;
            if (!CurrentMap.IsAttackEnabled) return false; //Maybe only against players?
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            if (target.OwnerType == OwnerType.Player)
            {
                Character character = (Character)target;
                if (character.isGod) return false;
                if (character.Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you                        
                if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false; //Can't attack players in safe maps
                if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false; // Can't attack players in safe zone //Change for enviromental?
                if (HasParty & character.HasParty && party.ID.Equals(character.Party.ID)) return false; //Party
                if (isSafeMode && character.Side == this.Side) return false; // safe mode on
                targetReputation = character.Reputation;
                if (character.specialAbilityEnabled && character.inventory.MerienArmour) destroyWeapon = true; //enviromental?
                if (character.specialAbilityEnabled && character.inventory.MerienShield) takeDamage = false;
            }

            return true;
        }


        public bool EnvironmentalAttack(IOwner target, Dice dice)
        {
            bool takeDamage = true;
            bool destroyWeapon = false;
            int targetReputation = 0;
            if (target == null) return false;
            if (target.IsDead) return false;
            if (!CurrentMap.IsAttackEnabled) return false; //Maybe only against players?
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            if (target.OwnerType == OwnerType.Player)
            {
                Character character = (Character)target;
                if (character.isGod) return false;
                if (character.Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you                        
                if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false; //Can't attack players in safe maps
                //if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false; // Can't attack players in safe zone //Don't do this for environmental
                if (HasParty & character.HasParty && party.ID.Equals(character.Party.ID)) return false; //Party
                if (isSafeMode && character.Side == this.Side) return false; // safe mode on
                targetReputation = character.Reputation;
                //if (character.specialAbilityEnabled && character.inventory.MerienArmour) destroyWeapon = true; //Don't do this for environmental
                if (character.specialAbilityEnabled && character.inventory.MerienShield) takeDamage = false;
            }

            int experience = 0;
            int damage = 0;

            // Weapon bonus for level / quality / stats
            if (Weapon != null)
            {
                dice = new Dice(dice.Die, dice.Faces + Weapon.MagicDamageFacesMod, dice.Bonus + Weapon.Level); //TODO add this to inventory
                damage = dice.Roll();
            }
            else if (Weapon == null)
            {
                damage = dice.Roll();
            }

            damage += CalculateEnvironmentalDamage(damage, targetReputation, target.OwnerType, target.ObjectId);

            // do damage absorb. return false if 0 damage remaining //Should this absorb? and how to calulate 
            //if (!target.AbsorbMagic(spell.Attribute, ref damage, inventory.PiercingBonus)) return false;

            //Apply Xelima after absorption
            if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

            // apply poison
            if (inventory.PoisonousBonus > 0)
                if (!target.EvadePoison()) { target.SetMagicEffect(new Magic(MagicType.Poison, 30, inventory.PoisonousBonus)); }

            //Apply Ice
            if (specialAbilityEnabled && inventory.IceWeapon) { target.SetMagicEffect(new Magic(MagicType.Ice, 30)); }

            // apply damage to target and add experience
            if (isGod) damage = target.HP;
            if (takeDamage)
            {
                //Add ComboCounter if doesn't exist
                if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                else
                {
                    ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                    if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits set to 1 hit if been to long
                    else { counter.HitCounter++; counter.DamageCounter += damage; }
                    counter.StartTime = DateTime.Now; //set new hit time
                }

                experience += target.TakeDamage(this, DamageType.Environment, damage, MotionDirection.None, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                if (ShowDamageEnabled)
                {
                    if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, target.Name)); }
                    else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2}.", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, target.Name)); }
                }
            }
            AddExp(experience + (int)(experience * (float)(inventory.ExperienceBonus / 100)));

            //Apply Medusa
            if (specialAbilityEnabled && inventory.MedusaWeapon) { target.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); }

            // deplete endurance of staff
            //TakeItemDamage(1, Weapon, (int)EquipType.RightHand);

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    {
                        if (target != this)// cant earn title points from self - cheat!
                        {
                            if (destroyWeapon)
                            {
                                if (Weapon != null)
                                {
                                    Weapon.Endurance = 0;
                                    Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                    UnequipItem((int)EquipType.RightHand);
                                }
                            }

                            titles[TitleType.PlayerDamageDealt] += damage;
                            Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDamageDealt, titles[TitleType.PlayerDamageDealt]);
                        }
                    }
                    break;
                case OwnerType.Npc:
                    {
                        titles[TitleType.MonsterDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDamageDealt, titles[TitleType.MonsterDamageDealt]);
                    }
                    break;
            }

            if (inventory.VampiricBonus > 0) ReplenishHP((int)((((double)damage) / 100.0f) * (double)inventory.VampiricBonus), true);
            if (inventory.ZealousBonus > 0) ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ZealousBonus), true);
            if (inventory.ExhaustiveBonus > 0) target.DepleteSP(inventory.ExhaustiveBonus, true);

            if (damage > titles[TitleType.HighestMagicDamage]) //Environmental title?
            {
                titles[TitleType.HighestMagicDamage] = damage;
                Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestMagicDamage, titles[TitleType.HighestMagicDamage]);
            }

            return true;
        }

        /// <summary>
        /// Character has casted offensive magic. Calculate damage and resistance of target.
        /// </summary>
        /// <param name="hitX">Actual hit location X.</param>
        /// <param name="hitY">Actual hit location Y.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetType">Target object's type.</param>
        /// <param name="spell">Magic type casted.</param>
        /// <returns>True or False to indicate damage has been dealt.</returns>
        public bool MagicAttack(int hitX, int hitY, IOwner target, Magic spell, MagicTarget magicTarget)
        {
            bool takeDamage = true;
            bool destroyWeapon = false;
            int targetReputation = 0;
            if (!PrepareAttack(target, ref takeDamage, ref destroyWeapon, ref targetReputation)) return false;
            int experience = 0;
            int damage = 0;

            // different effect values are used for the targeted object or for collatoral object in the "area of effect"
            // Weapon bonus for level / quality / stats
            if (Weapon != null && (Weapon.RelatedSkill == SkillType.Staff || Weapon.RelatedSkill == SkillType.BattleStaff)) //Add dice mod/level to magic damage if weapon in hand
            {
                if (magicTarget == MagicTarget.Single) damage = Dice.Roll(spell.Effect1, spell.Effect2 + Weapon.MagicDamageFacesMod, spell.Effect3 + Weapon.Level); //Todo - fix to use inventory
                else if (magicTarget == MagicTarget.Area) damage = Dice.Roll(spell.Effect4, spell.Effect5 + Weapon.MagicDamageFacesMod, spell.Effect6 + Weapon.Level);
            }
            else if (Weapon == null)
            {
                if (magicTarget == MagicTarget.Single) damage = Dice.Roll(spell.Effect1, spell.Effect2, spell.Effect3); //Todo - fix to use inventory
                else if (magicTarget == MagicTarget.Area) damage = Dice.Roll(spell.Effect4, spell.Effect5, spell.Effect6);
            }

            MotionDirection flyDirection = Utility.GetFlyDirection(hitX, hitY, target.X, target.Y);

            damage += CalculateMagicAttackDamage(damage, targetReputation, target.OwnerType, target.ObjectId);

            // do damage absorb. return false if 0 damage remaining
            if (!target.AbsorbMagic(spell.Attribute, ref damage, inventory.PiercingBonus)) return false;

            //Apply Xelima after absorption
            if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

            // apply poison
            if (inventory.PoisonousBonus > 0)
                if (!target.EvadePoison()) { target.SetMagicEffect(new Magic(MagicType.Poison, 30, inventory.PoisonousBonus)); }

            //Apply Ice
            if (specialAbilityEnabled && inventory.IceWeapon) { target.SetMagicEffect(new Magic(MagicType.Ice, 30)); }

            // apply damage to target and add experience
            if (isGod) damage = target.HP;
            if (takeDamage)
            {
                //Add ComboCounter if doesn't exist
                if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                else
                {
                    ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                    if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits set to 1 hit if been to long
                    else { counter.HitCounter++; counter.DamageCounter += damage; }
                    counter.StartTime = DateTime.Now; //set new hit time
                }

                experience += target.TakeDamage(this, DamageType.Magic, damage, flyDirection, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                if (ShowDamageEnabled)
                {
                    if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, target.Name)); }
                    else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2}.", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, target.Name)); }
                }
            }
            AddExp(experience + (int)(experience * (float)(inventory.ExperienceBonus / 100)));

            //Apply Medusa
            if (specialAbilityEnabled && inventory.MedusaWeapon) { target.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); }

            // deplete endurance of staff
            TakeItemDamage(1, Weapon, (int)EquipType.RightHand);

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    {
                        if (target != this)// cant earn title points from self - cheat!
                        {
                            if (destroyWeapon)
                            {
                                if (Weapon != null)
                                {
                                    Weapon.Endurance = 0;
                                    Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                    UnequipItem((int)EquipType.RightHand);
                                }
                            }

                            titles[TitleType.PlayerDamageDealt] += damage;
                            Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDamageDealt, titles[TitleType.PlayerDamageDealt]);
                        }
                    }
                    break;
                case OwnerType.Npc:
                    {
                        titles[TitleType.MonsterDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDamageDealt, titles[TitleType.MonsterDamageDealt]);
                    }
                    break;
            }

            if (inventory.VampiricBonus > 0) ReplenishHP((int)((((double)damage) / 100.0f) * (double)inventory.VampiricBonus), true);
            if (inventory.ZealousBonus > 0) ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ZealousBonus), true);
            if (inventory.ExhaustiveBonus > 0) target.DepleteSP(inventory.ExhaustiveBonus, true);

            if (damage > titles[TitleType.HighestMagicDamage])
            {
                titles[TitleType.HighestMagicDamage] = damage;
                Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestMagicDamage, titles[TitleType.HighestMagicDamage]);
            }

            return true;
        }

        private bool BattleStaffAttack(IOwner target, bool isCritical, bool isDash) //bool isCritical
        {
            if (Weapon == null) return false;
            if (Weapon.RelatedSkill != SkillType.BattleStaff) return false;
            bool takeDamage = true;
            bool destroyWeapon = false;
            int targetReputation = 0;
            if (!PrepareAttack(target, ref takeDamage, ref destroyWeapon, ref targetReputation)) return false;

            int experience = 0;
            int damage = Weapon.DamageSmall.Roll();

            MagicAttribute element; // check valid element number from effect4 - return false if not
            if (!Enum.TryParse<MagicAttribute>(Weapon.Effect4.ToString(), out element)) return false;

            damage += CalculateBattleStaffDamage(damage, isCritical, isDash, targetReputation, target.OwnerType, target.ObjectId);

            //Deplete Criticals
            if (isCritical) criticals--;

            // do damage absorb. return false if 0 damage remaining
            if (!target.AbsorbMagic(element, ref damage, inventory.PiercingBonus)) return false;

            //Apply Xelima after absorption
            if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

            // apply poison
            if (inventory.PoisonousBonus > 0)
                if (!target.EvadePoison()) { target.SetMagicEffect(new Magic(MagicType.Poison, 30, inventory.PoisonousBonus)); }

            //Apply Ice
            if (specialAbilityEnabled && inventory.IceWeapon) { target.SetMagicEffect(new Magic(MagicType.Ice, 30)); }

            // get fly direction
            MotionDirection flyDirection = Utility.GetFlyDirection(X, Y, target.X, target.Y);

            // apply damage to target and add experience
            if (isGod) damage = target.HP;

            if (takeDamage)
            {
                //Add ComboCounter if doesn't exist
                if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                else
                {
                    ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                    if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits
                    else { counter.HitCounter++; counter.DamageCounter += damage; }
                    counter.StartTime = DateTime.Now; //set new hit time
                }

                experience += target.TakeDamage(this, DamageType.Magic, damage, flyDirection, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                if (ShowDamageEnabled)
                {
                    if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("{0} damage to {1}.", damage, target.Name)); }
                    else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2}.", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, target.Name)); }
                }
            }

            AddExp(experience + (int)(experience * (float)(inventory.ExperienceBonus / 100)));

            //Apply Medusa
            if (specialAbilityEnabled && inventory.MedusaWeapon) { target.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); }

            // deplete endurance of battle staff
            TakeItemDamage(1, Weapon, (int)EquipType.RightHand);

            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    if (target != this) // cant earn title points from self- cheat!
                    {
                        if (destroyWeapon)
                        {
                            if (Weapon != null)
                            {
                                Weapon.Endurance = 0;
                                Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                UnequipItem((int)EquipType.RightHand);
                            }
                        }

                        titles[TitleType.PlayerDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDamageDealt, titles[TitleType.PlayerDamageDealt]);
                    }
                    break;
                case OwnerType.Npc:
                    {
                        titles[TitleType.MonsterDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDamageDealt, titles[TitleType.MonsterDamageDealt]);
                        break;
                    }
            }

            if (inventory.VampiricBonus > 0) ReplenishHP((int)((((double)damage) / 100.0f) * (double)inventory.VampiricBonus), true);
            if (inventory.ZealousBonus > 0) ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ZealousBonus), true);
            if (inventory.ExhaustiveBonus > 0) target.DepleteSP(inventory.ExhaustiveBonus, true);

            //TODO add TitleType.HighestBattleStaffDamage?
            if (damage > titles[TitleType.HighestMagicDamage])
            {
                titles[TitleType.HighestMagicDamage] = damage;
                Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestMagicDamage, titles[TitleType.HighestMagicDamage]);
            }

            return true;
        }

        private bool RangedAttack(IOwner target, bool isCritical)
        {
            if (Weapon == null) return false;
            if (Weapon.RelatedSkill != SkillType.Archery) return false;
            bool takeDamage = true;
            bool destroyWeapon = false;
            int targetReputation = 0;
            if (!PrepareAttack(target, ref takeDamage, ref destroyWeapon, ref targetReputation)) return false;
            int experience = 0;
            int damage = 0;
            int hitChance = 0;

            if (target.Size == OwnerSize.Large || target.Size == OwnerSize.Medium) damage = Weapon.DamageLarge.Roll();
            else damage = Weapon.DamageSmall.Roll();

            // deplete arrows
            Arrows.Count--; //TODO allow different type of arrows
            if (Arrows.Count <= 0) DepleteItem(ArrowsIndex);
            else Notify(CommandMessageType.NotifyItemCount, ArrowsIndex, Arrows.Count, 0);

            // try evade
            hitChance += CalculateMeleeHitProbability(isCritical, false, (direction == target.Direction));
            if (target.EvadeMelee(hitChance)) return false;

            // calculate damage        
            damage += CalculateRangedDamage(damage, isCritical, targetReputation, target.OwnerType, target.ObjectId);

            // if evade fails, handle absorption and experience gain separately depending on target type
            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    {
                        Character character = (Character)target;
                        // roll dice to get chance to hit particular item location
                        EquipType hitLocation;
                        int hitLocationChance = Dice.Roll(1, 10000);
                        if ((hitLocationChance >= 5000) && (hitLocationChance < 7500)) { hitLocation = EquipType.Legs; }// 25%
                        else if ((hitLocationChance >= 7500) && (hitLocationChance < 9000)) { hitLocation = EquipType.Arms; }// 15%
                        else if ((hitLocationChance >= 9000) && (hitLocationChance <= 10000)) { hitLocation = EquipType.Head; }// 10%
                        else { hitLocation = EquipType.Body; } // 50%

                        // endurance damage amount and hammer strip
                        int armourDamage = 1; bool stripAttempt = false;
                        if (Weapon != null) //TODO add stripAttempt to inventory
                        {
                            switch (Weapon.RelatedSkill)
                            {
                                case SkillType.Hammer: armourDamage = 20; stripAttempt = true; break;
                                case SkillType.Axe: armourDamage = 3; break;
                                default: armourDamage = 1; break;
                            }
                            armourDamage += Weapon.StripBonus;
                        }

                        // do damage absorb. return false if 0 damage remaining
                        if (!target.AbsorbMelee(ref damage, hitLocation, armourDamage, stripAttempt, inventory.PiercingBonus)) return false;

                        if (guild == character.Guild) damage /= 2; // same guild

                        //Apply Xelima after absorption
                        if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

                        // deal the damage and store any experience received
                        if (isGod) damage = target.HP;
                        if (damage <= 1) damage = 1;

                        if (takeDamage)
                        {
                            //Add ComboCounter if doesn't exist
                            if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                            else
                            {
                                ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                                if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits
                                else { counter.HitCounter++; counter.DamageCounter += damage; }
                                counter.StartTime = DateTime.Now; //set new hit time
                            }

                            experience += character.TakeDamage(this, DamageType.Ranged, damage, direction, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                            if (ShowDamageEnabled)
                            {
                                if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1} ({2}).", damage, character.Name, hitLocation.ToString())); }
                                else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2} ({3}).", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, character.Name, hitLocation.ToString())); }
                            }
                        }

                        if (destroyWeapon)
                        {
                            if (Weapon != null)
                            {
                                Weapon.Endurance = 0;
                                Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                UnequipItem((int)EquipType.RightHand);
                            }
                        }

                        titles[TitleType.PlayerDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDamageDealt, titles[TitleType.PlayerDamageDealt]);
                        break;
                    }
                case OwnerType.Npc:
                    {
                        Npc npc = (Npc)target;
                        //TODO add hitlocation/armourDamage and StripAttempt to NPC's
                        // do damage absorb. return false if 0 damage remaining
                        if (!target.AbsorbMelee(ref damage, EquipType.None, 0, false, inventory.PiercingBonus)) return false;

                        if (Cache.World.IsCrusade)
                        {
                            if ((Side == OwnerSide.Neutral) || (Side == npc.Side && (npc.NpcType == NpcType.CrusadeEnergyShield || npc.NpcType == NpcType.CrusadeGrandMagicGenerator)))
                                return false; // cant hit ES or GMG if traveller or own side
                            // TODO crusade mode - hitting grand magic generator (type = 41) reduces mana stock. 500 hit points before mana stock down
                        }

                        //Apply Xelima after absorption
                        if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

                        // deal the damage and store any experience received
                        if (isGod) damage = target.HP;
                        if (damage <= 1) damage = 1;
                        if (takeDamage)
                        {
                            //Add ComboCounter if doesn't exist
                            if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                            else
                            {
                                ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                                if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits
                                else { counter.HitCounter++; counter.DamageCounter += damage; }
                                counter.StartTime = DateTime.Now; //set new hit time
                            }

                            experience += npc.TakeDamage(this, DamageType.Ranged, damage, direction, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                            if (ShowDamageEnabled)
                            {
                                if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, npc.Name)); }
                                else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2}.", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, npc.Name)); }
                            }
                        }

                        titles[TitleType.MonsterDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDamageDealt, titles[TitleType.MonsterDamageDealt]);
                        break;
                    }
                default: return false;
            }
            // apply poison
            if (inventory.PoisonousBonus > 0)
                if (!target.EvadePoison()) { target.SetMagicEffect(new Magic(MagicType.Poison, 30, inventory.PoisonousBonus)); }

            //Apply Ice
            if (specialAbilityEnabled && inventory.IceWeapon) { target.SetMagicEffect(new Magic(MagicType.Ice, 30)); }


            //Apply Medusa
            if (specialAbilityEnabled && inventory.MedusaWeapon) { target.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); }

            if (Weapon != null)
            {
                skills[Weapon.RelatedSkill].Experience++;
            }

            AddExp(experience + (int)(experience * (float)(inventory.ExperienceBonus / 100)));
            //TODO - Archery Title
            //if (damage > titles[TitleType.HighestMeleeDamage])
            //{
            //    titles[TitleType.HighestMeleeDamage] = damage;
            //    Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestMeleeDamage, titles[TitleType.HighestMeleeDamage]);
            //}

            if (inventory.VampiricBonus > 0) ReplenishHP((int)((((double)damage) / 100.0f) * (double)inventory.VampiricBonus), true);
            if (inventory.ZealousBonus > 0) ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ZealousBonus), true);
            if (inventory.ExhaustiveBonus > 0) target.DepleteSP(inventory.ExhaustiveBonus, true);
            return true;
        }

        private bool MeleeAttack(IOwner target, bool isCritical, bool isDash)
        {
            bool takeDamage = true;
            bool destroyWeapon = false;
            int targetReputation = 0;
            if (!PrepareAttack(target, ref takeDamage, ref destroyWeapon, ref targetReputation)) return false;
            int experience = 0;
            int damage = 0;
            int hitChance = 0;

            // get base damage and check attack distances
            if (Weapon != null)
            {
                if (target.Size == OwnerSize.Large || target.Size == OwnerSize.Medium) damage = Weapon.DamageLarge.Roll();
                else damage = Weapon.DamageSmall.Roll();

                // deplete endurance if weapon used
                TakeItemDamage(1, Weapon, (int)EquipType.RightHand);
            }

            //Deplete Criticals
            if (isCritical) criticals--;

            // try evade
            hitChance += CalculateMeleeHitProbability(isCritical, isDash, (direction == target.Direction));
            if (target.EvadeMelee(hitChance)) return false;

            // calculate damage
            damage += CalculateMeleeDamage(damage, isCritical, isDash, targetReputation, target.OwnerType, target.ObjectId);

            // if evade fails, handle absorption and experience gain separately depending on target type
            switch (target.OwnerType)
            {
                case OwnerType.Player:
                    {
                        Character character = (Character)target;
                        // roll dice to get chance to hit particular item location
                        EquipType hitLocation;
                        int hitLocationChance = Dice.Roll(1, 10000);
                        if ((hitLocationChance >= 5000) && (hitLocationChance < 7500)) { hitLocation = EquipType.Legs; }// 25%
                        else if ((hitLocationChance >= 7500) && (hitLocationChance < 9000)) { hitLocation = EquipType.Arms; }// 15%
                        else if ((hitLocationChance >= 9000) && (hitLocationChance <= 10000)) { hitLocation = EquipType.Head; }// 10%
                        else { hitLocation = EquipType.Body; } // 50%

                        // endurance damage amount and hammer strip
                        int armourDamage = 1; bool stripAttempt = false;
                        if (Weapon != null) //TODO add stripAttempt to inventory
                        {
                            switch (Weapon.RelatedSkill)
                            {
                                case SkillType.Hammer: armourDamage = 20; stripAttempt = true; break;
                                case SkillType.Axe: armourDamage = 3; break;
                                default: armourDamage = 1; break;
                            }
                            armourDamage += Weapon.StripBonus;
                        }

                        // do damage absorb. return false if 0 damage remaining
                        if (!target.AbsorbMelee(ref damage, hitLocation, armourDamage, stripAttempt, inventory.PiercingBonus)) return false;

                        if (guild == character.Guild) damage /= 2; // same guild

                        //Apply Xelima after absorption
                        if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

                        // deal the damage and store any experience received
                        if (damage <= 1) damage = 1;

                        if (takeDamage)
                        {
                            //Add ComboCounter if doesn't exist
                            if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                            else
                            {
                                ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                                if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits
                                else { counter.HitCounter++; counter.DamageCounter += damage; }
                                counter.StartTime = DateTime.Now; //set new hit time
                            }

                            experience += character.TakeDamage(this, DamageType.Melee, damage, direction, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                            if (ShowDamageEnabled)
                            {
                                if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1} ({2}).", damage, character.Name, hitLocation.ToString())); }
                                else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2} ({3}).", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, character.Name, hitLocation.ToString())); }
                            }
                        }

                        if (destroyWeapon)
                        {
                            if (Weapon != null)
                            {
                                Weapon.Endurance = 0;
                                Notify(CommandMessageType.NotifyItemBroken, ((int)Weapon.EquipType), equipment[((int)Weapon.EquipType)]);
                                UnequipItem((int)EquipType.RightHand);
                            }
                        }

                        titles[TitleType.PlayerDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDamageDealt, titles[TitleType.PlayerDamageDealt]);
                        break;
                    }
                case OwnerType.Npc:
                    {
                        Npc npc = (Npc)target;
                        //TODO add hitlocation/armourDamage and StripAttempt to NPC's
                        // do damage absorb. return false if 0 damage remaining
                        if (!target.AbsorbMelee(ref damage, EquipType.None, 0, false, inventory.PiercingBonus)) return false;

                        if (Cache.World.IsCrusade)
                        {
                            if ((Side == OwnerSide.Neutral) || (Side == npc.Side && (npc.NpcType == NpcType.CrusadeEnergyShield || npc.NpcType == NpcType.CrusadeGrandMagicGenerator)))
                                return false; // cant hit ES or GMG if traveller or own side
                            // TODO crusade mode - hitting grand magic generator (type = 41) reduces mana stock. 500 hit points before mana stock down
                        }

                        //Apply Xelima after absorption
                        if (specialAbilityEnabled && inventory.XelimaWeapon) { if ((target.HP / 2) > damage) damage = (target.HP / 2); }

                        // deal the damage and store any experience received
                        if (isGod) damage = target.HP;
                        if (damage <= 1) damage = 1;
                        if (takeDamage)
                        {
                            //Add ComboCounter if doesn't exist
                            if (!comboDamageTargets.Keys.Contains(target.ObjectId)) { comboDamageTargets.Add(target.ObjectId, new ComboDamageCounter(0, DateTime.Now, damage)); }
                            else
                            {
                                ComboDamageCounter counter = comboDamageTargets[target.ObjectId];
                                if ((DateTime.Now - counter.StartTime).TotalMilliseconds > 1) { counter.HitCounter = 1; counter.DamageCounter = damage; } //Check counter for consecutive hits
                                else { counter.HitCounter++; counter.DamageCounter += damage; }
                                counter.StartTime = DateTime.Now; //set new hit time
                            }

                            experience += npc.TakeDamage(this, DamageType.Melee, damage, direction, true, comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter);

                            if (ShowDamageEnabled)
                            {
                                if (comboDamageTargets[target.ObjectId].HitCounter <= 1) { Cache.World.SendClientMessage(this, string.Format("You did {0} damage to {1}.", damage, npc.Name)); }
                                else { Cache.World.SendClientMessage(this, string.Format("You did {0}x Hits! {1} Total damage to {2}.", comboDamageTargets[target.ObjectId].HitCounter, comboDamageTargets[target.ObjectId].DamageCounter, npc.Name)); }
                            }
                        }

                        titles[TitleType.MonsterDamageDealt] += damage;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDamageDealt, titles[TitleType.MonsterDamageDealt]);
                        break;
                    }
                default: return false;
            }
            // apply poison
            if (inventory.PoisonousBonus > 0) { if (!target.EvadePoison()) { target.SetMagicEffect(new Magic(MagicType.Poison, 30, inventory.PoisonousBonus)); } }

            //Apply Ice
            if (specialAbilityEnabled && inventory.IceWeapon) { target.SetMagicEffect(new Magic(MagicType.Ice, 30)); }

            //Apply Medusa
            if (specialAbilityEnabled && inventory.MedusaWeapon) { target.SetMagicEffect(new Magic(MagicType.Paralyze, 10, 2)); }

            if (Weapon != null)
            {
                switch (Weapon.RelatedSkill)
                {
                    case SkillType.Axe:
                    case SkillType.Fencing:
                    case SkillType.Hammer:
                    case SkillType.ShortSword:
                    case SkillType.Staff:
                    case SkillType.LongSword:
                        skills[Weapon.RelatedSkill].Experience++;
                        break;
                }
            }
            else
            {
                skills[SkillType.Hand].Experience++;
            }

            AddExp(experience + (int)(experience * (double)((double)inventory.ExperienceBonus / (double)100)));

            if (inventory.VampiricBonus > 0) ReplenishHP((int)((((double)damage) / 100.0f) * (double)inventory.VampiricBonus), true);
            if (inventory.ZealousBonus > 0) ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ZealousBonus), true);
            if (inventory.ExhaustiveBonus > 0) target.DepleteSP(inventory.ExhaustiveBonus, true);

            if (damage > titles[TitleType.HighestMeleeDamage])
            {
                titles[TitleType.HighestMeleeDamage] = damage;
                Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestMeleeDamage, titles[TitleType.HighestMeleeDamage]);
            }
            return true;
        }

        /// <summary>
        /// Character performs a melee Attack. Calculate damage and resistance of target.
        /// </summary>
        /// <param name="sourceX">X coordinate of attack operation.</param>
        /// <param name="sourceY">Y coordinate of attack operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target location.</param>
        /// <param name="targetY">Y coordinate of the target location.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetType">Target object's type.</param>
        /// <returns>True or False to indicate if Attack has failed or not.</returns>
        public bool Attack(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, IOwner target, AttackType attackType, bool isDash)
        {
            bool isCritical = (!isDash && (attackType == AttackType.Critical) && criticals > 0);
            int distanceX = Math.Abs(sourceX - targetX);
            int distanceY = Math.Abs(sourceY - targetY);

            this.direction = direction; // should be set even if attack fails. to update clients

            if ((sourceX != this.mapX || sourceY != this.mapY) && !isDash && attackType == AttackType.Normal) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);
            if (!IsCombatMode) return false;
            if (target == null) return false;
            if (target.IsDead) return false;
            if (target.IsInvisible) return false;
            if (targetX != target.X || targetY != target.Y) return false; // target coordinates to not match the target object's location
            if (CurrentLocation.IsSafe || target.CurrentLocation.IsSafe) return false; // safe zones
            if (!CurrentMap.IsAttackEnabled) return false;
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start
            if ((CurrentMap.IsBuilding || CurrentMap.IsSafeMap) && target.OwnerType == Helbreath.OwnerType.Player) return false; //Can't attack players in safe zones

            OwnerType ownerType = target.OwnerType;
            if (ownerType == OwnerType.Player)
            {
                Character character = (Character)target;
                if (character.isGod || character.IsAdmin) return false;
                if (character.Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you
                if (HasParty & character.HasParty && party.ID.Equals(character.Party.ID)) return false;
                if (isSafeMode && character.Side == this.Side) return false; // safe mode
            }

            // if hungry or low sp, 10% chance of failed hit
            if (((hunger <= 10) || sp <= 0) && Dice.Roll(1, 10) == 5) return false;

            // Different weapons have their own damage calculations
            if (Weapon != null)
            {
                if (isDash) { if ((distanceX > 2 || distanceY > 2)) return false; }
                else if (Weapon.HasExtendedRange && !isCritical && (distanceX > Weapon.Range + 2 || distanceY > Weapon.Range + 2)) return false;
                else if (!Weapon.HasExtendedRange && !isCritical && (distanceX > Weapon.Range || distanceY > Weapon.Range)) return false;
                if (Weapon.HasExtendedRange && isCritical && (distanceX > Weapon.CriticalRange + 2 || distanceY > Weapon.CriticalRange + 2)) return false;
                else if (!Weapon.HasExtendedRange && isCritical && (distanceX > Weapon.CriticalRange || distanceY > Weapon.CriticalRange)) return false;

                switch (Weapon.RelatedSkill)
                {
                    case SkillType.BattleStaff: return BattleStaffAttack(target, isCritical, isDash);
                    case SkillType.Archery:
                        if (isCritical) // special critical type
                        {
                            List<IOwner> targets = Cache.World.GetNearbyTargets(this, 6, 2, target);
                            targets.Add(target); // current target is also hit
                            foreach (IOwner hit in targets)
                            {
                                RangedAttack(hit, true);
                                Cache.World.SendCommonEventToNearbyPlayers(this, CommandMessageType.ShootArrow, hit.X, hit.Y, 0, 0);
                            }

                            criticals--; // deplete criticals after damage calculations
                            return true;
                        }
                        else
                        {
                            Cache.World.SendCommonEventToNearbyPlayers(this, CommandMessageType.ShootArrow, targetX, targetY, 0, 0);
                            return RangedAttack(target, isCritical);
                        }
                    default: return MeleeAttack(target, isCritical, isDash);
                }
            }
            else // hand attack
            {
                if (distanceX > 1 || distanceY > 1) return false;
                return MeleeAttack(target, isCritical, isDash);
            }
        }

        /// <summary>
        /// Character performs a mining action.
        /// </summary>
        /// <param name="sourceX">X coordinate of mining operation.</param>
        /// <param name="sourceY">Y coordinate of mining operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target rock or gem.</param>
        /// <param name="targetY">Y coordinate of the target rock or gem.</param>
        /// <param name="target">Target rock or gem.</param>
        /// <returns>True or False to indicate a mineral was successfully mined.</returns>
        public bool Mine(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Mineral target)
        {
            if (isDead) return false;
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);
            if (Weapon == null || Weapon.RelatedSkill != SkillType.Mining) return false;

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (target == null) return false;
            if (target.RemainingMaterials <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false; // target coordinates to not match the target object's location

            int skillLevel = skills[SkillType.Mining].Level;
            skillLevel -= target.Difficulty; // reduces skill by difficulty
            if (skillLevel <= 0) skillLevel = 1;

            int result = Dice.Roll(1, 100);
            if (result < skillLevel)
            {
                Item mineral = target.Mine();
                if (mineral != null)
                {
                    // TODO - experience gain
                    if (CurrentLocation.SetItem(mineral)) ItemDropped(this, mineral);

                    skills[SkillType.Mining].Experience++;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Character performs a mining action.
        /// </summary>
        /// <param name="sourceX">X coordinate of mining operation.</param>
        /// <param name="sourceY">Y coordinate of mining operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target rock or gem.</param>
        /// <param name="targetY">Y coordinate of the target rock or gem.</param>
        /// <param name="target">Target rock or gem.</param>
        /// <returns>True or False to indicate a mineral was successfully mined.</returns>
        public bool Farm(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Npc target)
        {
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (Weapon != null && Weapon.RelatedSkill != SkillType.Farming) return false;
            if (level < 20) return false; // travs cant farm
            if (target == null) return false;
            if (target.BuildPoints <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false;

            int skillLevel = skills[SkillType.Farming].Level;
            if (skillLevel < 20) return false; // buy manual first for 20%

            int result = Dice.Roll(1, 100);
            switch (target.BuildPoints)
            {
                case 1:
                case 8:
                case 18:
                    if (target.BuildPoints == 18)
                        target.Appearance2 = target.BuildType << 8 | 2; // get bigger
                    else target.Appearance2 = target.BuildType << 8 | 3; // and bigger
                    if (skillLevel <= target.BuildLimit + 10)
                        skills[SkillType.Farming].Experience++;
                    if (Globals.FarmingDropTable[Math.Max(Math.Min((skillLevel - 20) / 10, 8), 0), Math.Max(Math.Min((target.BuildLimit - 20) / 10, 8), 0)] >= result)
                    {
                        Item vegetable = null; // this object is as clever as the original programmers
                        switch (target.BuildType)
                        {
                            case 1: vegetable = World.ItemConfiguration[479].Copy(); break; // watermelon
                            case 2: vegetable = World.ItemConfiguration[480].Copy(); break; // pumpkin
                            case 3: vegetable = World.ItemConfiguration[481].Copy(); break; // garlic
                            case 4: vegetable = World.ItemConfiguration[482].Copy(); break; // barley
                            case 5: vegetable = World.ItemConfiguration[483].Copy(); break; // carrot
                            case 6: vegetable = World.ItemConfiguration[484].Copy(); break; // radish
                            case 7: vegetable = World.ItemConfiguration[485].Copy(); break; // corn
                            case 8: vegetable = World.ItemConfiguration[486].Copy(); break; // chinese bellflower
                            case 9: vegetable = World.ItemConfiguration[487].Copy(); break; // melon
                            case 11: vegetable = World.ItemConfiguration[489].Copy(); break; // grapes
                            case 12: vegetable = World.ItemConfiguration[490].Copy(); break; // blue grapes
                            case 13: vegetable = World.ItemConfiguration[491].Copy(); break; // mushroom
                            case 14: vegetable = World.ItemConfiguration[422].Copy(); break; // ginseng
                        }

                        if (vegetable != null)
                        {
                            // TODO - experience gain
                            if (CurrentLocation.SetItem(vegetable)) ItemDropped(this, vegetable);
                        }
                    }
                    break;
                default: break;
            }

            if (Globals.FarmingSkillTable[Math.Max(Math.Min((skillLevel - 20) / 10, 8), 0), Math.Max(Math.Min((target.BuildLimit - 20) / 10, 8), 0)] >= result
                        || target.BuildPoints == 1 || target.BuildPoints == 8 || target.BuildPoints == 18)
            {
                if (Weapon != null) TakeItemDamage(1, Weapon, (int)EquipType.RightHand);

                target.BuildPoints--;
                if (target.BuildPoints < 0) target.BuildPoints = 0;
            }

            if (target.BuildPoints <= 1) target.Die(this, -1, (DamageType)0, 1, false); // this is bugged in old code. you wouldnt gain skill % because the Npc would no longer exist after item drop. so I have moved it to the bottom

            return true;
        }

        /// <summary>
        /// Character performs a building action on structures such as those in crusade.
        /// </summary>
        /// <param name="sourceX">X coordinate of character.</param>
        /// <param name="sourceY">Y coordinate of character.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <param name="targetX">X coordinate of the target structure.</param>
        /// <param name="targetY">Y coordinate of the target structure.</param>
        /// <param name="target">Target structure.</param>
        /// <returns>True or False to indicate a building was successfully built.</returns>
        public bool Build(int sourceX, int sourceY, MotionDirection direction, int targetX, int targetY, Npc target)
        {
            if (!IsCombatMode) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (IsInvisible) RemoveMagicEffect(MagicType.Invisibility);

            this.direction = direction; // should be set even if attack fails. to update nearby clients

            if (Weapon != null && Weapon.RelatedSkill != SkillType.Mining) return false;
            if (level < 20) return false; // travs cant build
            if (target == null) return false;
            if (target.BuildPoints <= 0) return false;
            if (targetX != target.X || targetY != target.Y) return false;
            if (crusadeDuty != CrusadeDuty.Constructor && adminLevel == 0) return false;

            switch (target.NpcType)
            {
                case NpcType.CrusadeArrowTower:
                case NpcType.CrusadeCannonTower:
                case NpcType.CrusadeDetector:
                case NpcType.CrusadeManaCollector:
                    switch (target.BuildPoints)
                    {
                        case 1:
                            target.Appearance2 = 0;
                            switch (target.NpcType)
                            {
                                case NpcType.CrusadeArrowTower: crusadeConstructionPoints += 700; crusadeWarContribution += 700; break;
                                case NpcType.CrusadeCannonTower: crusadeConstructionPoints += 700; crusadeWarContribution += 700; break;
                                case NpcType.CrusadeDetector: crusadeConstructionPoints += 500; crusadeWarContribution += 500; break;
                                case NpcType.CrusadeManaCollector: crusadeConstructionPoints += 500; crusadeWarContribution += 500; break;
                            }
                            Notify(CommandMessageType.NotifyCrusadeStatistics, crusadeConstructionPoints, crusadeWarContribution, 0);

                            break;
                        case 5: target.Appearance2 = 1; break;
                        case 10: target.Appearance2 = 2; break;
                    }
                    break;
            }

            target.BuildPoints--;
            if (target.BuildPoints < 0) target.BuildPoints = 0;

            return true;
        }

        /// <summary>
        /// Character performs a manual Fishing action when fishing a dynamic object after clicking "Try Now" in the client.
        /// </summary>
        public void Fish()
        {
            if (isDead) return;
            if (targetFish == null) return;

            int result = Dice.Roll(1, 100);
            if (targetFishChance >= result)
            {
                experienceStored += Dice.Roll(targetFish.Difficulty, 6);
                skills[SkillType.Fishing].Experience += targetFish.Difficulty;

                if (targetFish.FishItem != null)
                {
                    if (CurrentLocation.SetItem(targetFish.FishItem)) ItemDropped(this, targetFish.FishItem);
                }
                targetFish.Remove();
                Notify(CommandMessageType.NotifyFishingSuccess);
            }
            else Notify(CommandMessageType.NotifyFishingFailed);
            skillInUse = SkillType.None;
            targetFish = null;
        }

        /// <summary>
        /// Character performs a manufacturing action.
        /// </summary>
        /// <param name="itemId">Name of the item to manufacture.</param>
        /// <param name="itemIndex">List of item indexes used in the manufacturing process.</param>
        public void Manufacture(int itemId, int[] itemIndex)
        {
            if (isDead) return;

            if (!World.ItemConfiguration.ContainsKey(itemId) ||
                !World.ManufacturingConfiguration.ContainsKey(itemId) ||
                 World.ManufacturingConfiguration[itemId].SkillLimit > skills[SkillType.Manufacturing].Level)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }

            int totalIngrediants = World.ManufacturingConfiguration[itemId].Ingredients.Count;
            int foundIngrediants = 0, purityValue = 0;

            // check that all ingrediants are available
            foreach (KeyValuePair<int, int> ingrediant in World.ManufacturingConfiguration[itemId].Ingredients)
                if (World.ItemConfiguration.ContainsKey(ingrediant.Key))
                    for (int i = 0; i < 6; i++)
                        if (itemIndex[i] != -1 && inventory[itemIndex[i]] != null && inventory[itemIndex[i]].ItemId == World.ItemConfiguration[ingrediant.Key].ItemId)
                            if (inventory[itemIndex[i]].IsStackable)
                            {
                                if (inventory[itemIndex[i]].Count >= ingrediant.Value)
                                {
                                    foundIngrediants++;
                                    purityValue += inventory[itemIndex[i]].SpecialEffect2 - (inventory[itemIndex[i]].SpecialEffect2 - skills[SkillType.Manufacturing].Level) / 2;
                                }
                            }
                            else
                            {

                            }


            // not all ingrediants found
            if (foundIngrediants < totalIngrediants)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }

            // all ingrediants found perform manufacture of item based on skill %
            int result = Dice.Roll(1, 100);
            if (result > skills[SkillType.Manufacturing].Level)
            {
                Notify(CommandMessageType.NotifyManufactureFailed);
                return;
            }
            else
            {
                Item item = World.ItemConfiguration[itemId].Copy();
                // TODO
                //item.Attribute = (item.Attribute & 0xFFFFFFFE) | 0x00000001; // "custom item"

                if (item.Type == ItemType.Material)
                    item.SpecialEffect2 = (skills[SkillType.Manufacturing].Level / 2) + (Dice.Roll(1, (skills[SkillType.Manufacturing].Level / 2) + 1) - 1);
                else
                {
                    // TODO
                    //item.Attribute = (item.Attribute & 0x0000FFFF) | (World.ItemConfiguration[itemId].Attribute << 16);

                    int resultValue = purityValue - World.ManufacturingConfiguration[itemId].Difficulty;
                    resultValue = Math.Min(Math.Max(0, resultValue), 200); // must be 0 - 200

                    item.SpecialEffect2 = (int)(((double)resultValue / (double)(100 - World.ManufacturingConfiguration[itemId].Difficulty)) * 100.0f);

                    int itemEndurance = item.MaximumEndurance + (int)((double)((double)item.SpecialEffect2 / 100.0f) * (double)item.MaximumEndurance);
                    itemEndurance = Math.Max(1, itemEndurance); // must be > 0

                    if (itemEndurance <= item.MaximumEndurance * 2)
                    {
                        //item.MaximumEndurance = itemEndurance; // TODO - changed the way this works
                        item.SpecialEffect1 = itemEndurance;
                        item.Endurance = itemEndurance;
                    }
                    else item.SpecialEffect1 = item.MaximumEndurance;

                    item.Colour = (int)GameColor.Manufactured;
                    item.ColorType = GameColor.Manufactured;

                    AddInventoryItem(item);

                    Notify(CommandMessageType.NotifyManufactureSuccess, (item.SpecialEffect2 >= 0) ? item.SpecialEffect2 : item.SpecialEffect2 + 10000, (int)item.Type);

                    skills[SkillType.Manufacturing].Experience++;

                    // TODO experience
                }
            }

            // remove ingrediants from inventory and check for depleted items TODO cleanup
            foreach (KeyValuePair<int, int> ingrediant in World.ManufacturingConfiguration[itemId].Ingredients)
                if (World.ItemConfiguration.ContainsKey(ingrediant.Key))
                    for (int i = 0; i < 6; i++)
                        if (itemIndex[i] != -1 && inventory[itemIndex[i]] != null &&
                            inventory[itemIndex[i]].ItemId == World.ItemConfiguration[ingrediant.Key].ItemId)
                        {
                            inventory[itemIndex[i]].Count -= ingrediant.Value;
                            if (inventory[itemIndex[i]].Count <= 0) DepleteItem(itemIndex[i]);
                            else Notify(CommandMessageType.NotifyItemCount, itemIndex[i], inventory[itemIndex[i]].Count, 0);
                        }
        }

        /// <summary>
        /// Character performs an alchemy action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the alchemy process.</param>
        public void Alchemy(int[] itemIndex)
        {

        }

        /// <summary>
        /// Character performs a crafting action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the crafting process.</param>
        public void Craft(int[] itemIndex)
        {
            if (isDead) return;
        }

        /// <summary>
        /// Character performs an slate action.
        /// </summary>
        /// <param name="itemIndex">List of item indexes used in the slate process.</param>
        public void Slate(int[] itemIndex)
        {
            if (isDead) return;
        }

        public void TakeArmourDamage(int damage)
        {
            if (BodyArmour != null) TakeItemDamage(damage, BodyArmour, (int)EquipType.Body);
            if (Leggings != null) TakeItemDamage(damage, Leggings, (int)EquipType.Legs);
            if (Boots != null) TakeItemDamage(damage, Boots, (int)EquipType.Feet);
            if (Hauberk != null) TakeItemDamage(damage, Hauberk, (int)EquipType.Arms);
            if (Helmet != null) TakeItemDamage(damage, Helmet, (int)EquipType.Head);
        }

        private void TakeItemDamageByPercentage(int percentage, Item item, int itemIndex, bool isStripAttempt = false)
        {
            int reduction = (int)(((double)item.MaximumEndurance / 100) * (double)percentage);

            TakeItemDamage(reduction, item, itemIndex, isStripAttempt);
        }

        public void TakeItemDamage(int damage, Item item, int itemIndex, bool isStripAttempt = false)
        {
            if (item == null) return;
            if (item.IsBroken) return;
            if (damage > item.Endurance) damage = item.Endurance;
            item.Endurance -= damage;
            if (item.IsBroken)
            {
                //TODO add item destroyed?
                Notify(CommandMessageType.NotifyItemBroken, itemIndex, 0); //((int)item.EquipType), equipment[((int)item.EquipType)]);
                UnequipItem(itemIndex);
            }
            else
            {
                if (isStripAttempt && item.MaximumEndurance < Globals.MaximumEnduranceStripable)
                {
                    int chance = Dice.Roll(1, 100);
                    if (chance >= (int)(((double)item.Endurance / (double)item.MaximumEndurance) * 100.0f))
                    {
                        int stripChance = Dice.Roll(1, 100);
                        if (stripChance < Math.Min(damage, Globals.MaximumStripChance)) UnequipItem(itemIndex);
                    }
                }
                Notify(CommandMessageType.NotifyItemEndurance, itemIndex, item.Endurance);
            }
        }

        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection) { return TakeDamage(null, type, damage, flyDirection, true); }
        public int TakeDamage(DamageType type, int damage, MotionDirection flyDirection, bool notify) { return TakeDamage(null, type, damage, flyDirection, notify); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify = true) { return TakeDamage(attacker, type, damage, flyDirection, notify, 1, damage); }
        public int TakeDamage(IOwner attacker, DamageType type, int damage, MotionDirection flyDirection, bool notify, int hitCount, int totalDamage)
        {
            if (isDead) return 0;

            if (attacker != null && attacker.OwnerType == OwnerType.Player && !IsAlly(attacker)) //Only adds enemies
            {
                //Add the attacking player
                Character character = (Character)attacker;
                if (damagedByObjects.ContainsKey(character.ClientID)) { damagedByObjects[character.ClientID] = DateTime.Now; }
                else { damagedByObjects.Add(character.ClientID, DateTime.Now); }

                //Add all supporting players of the attacker
                foreach (KeyValuePair<int, DateTime> support in character.SupportedByObjects)
                {
                    if (damagedByObjects.ContainsKey(support.Key)) { damagedByObjects[support.Key] = DateTime.Now; }
                    else { damagedByObjects.Add(support.Key, DateTime.Now); }
                }
            }

            DepleteHP(damage);

            ChargeCritical();

            //ManaConverting
            if (inventory.ManaConvertingBonus > 0)
            {
                ReplenishMP((int)((((double)damage) / 100.0f) * (double)inventory.ManaConvertingBonus), true);
            }


            // remove paralyze for different damage types
            if (magicEffects.ContainsKey(MagicType.Paralyze))
                switch (type)
                {
                    case DamageType.Magic: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 2 || magicEffects[MagicType.Paralyze].Magic.Effect1 == 1) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Ranged:
                    case DamageType.Melee: if (magicEffects[MagicType.Paralyze].Magic.Effect1 == 1) RemoveMagicEffect(MagicType.Paralyze); break;
                    case DamageType.Environment: RemoveMagicEffect(MagicType.Paralyze); break; //TODO only specific environmental types break?
                }

            if (hp <= 0)
            {
                if (isGod || IsAdmin) hp = 1;
                else if (MaxLuck > 0 && Dice.Roll(1, 100) < MaxLuck)
                {
                    Cache.World.SendClientMessage(this, "You were lucky to survive a fatal blow!");
                    hp = 1;
                }
                else
                {
                    Die(attacker, totalDamage, type, hitCount, true);
                    return 0;
                }
            }

            Notify(CommandMessageType.NotifyHP, hp, mp);
            lastDamage = damage;
            lastDamageType = type;
            lastDamageTime = DateTime.Now;
            if (notify) DamageTaken(this, type, attacker, damage, flyDirection, hitCount, totalDamage);
            // TODO how do we calculate exp?

            return 0;
        }

        public bool Fly(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;

            if (!Enum.IsDefined(typeof(MotionDirection), direction)) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;

            int destinationX = mapX, destinationY = mapY;
            switch (direction)
            {
                case MotionDirection.North: destinationY = destinationY - 1; break;
                case MotionDirection.NorthEast: destinationX = destinationX + 1; destinationY = destinationY - 1; break;
                case MotionDirection.East: destinationX = destinationX + 1; break;
                case MotionDirection.SouthEast: destinationX = destinationX + 1; destinationY = destinationY + 1; break;
                case MotionDirection.South: destinationY = destinationY + 1; break;
                case MotionDirection.SouthWest: destinationX = destinationX - 1; destinationY = destinationY + 1; break;
                case MotionDirection.West: destinationX = destinationX - 1; break;
                case MotionDirection.NorthWest: destinationX = destinationX - 1; destinationY = destinationY - 1; break;
            }

            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                if (Cache.World.IsApocalypse)
                {
                    Portal dynamicPortal = null;
                    if (Cache.World.Apocalypse.HasPortal(new Location(map.Name, destinationX, destinationY), out dynamicPortal))
                    {
                        Teleport(dynamicPortal.PortalTargetLocation, MotionDirection.South);
                        return true;
                    }
                }

                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                prevMapX = mapX;
                prevMapY = mapY;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);

                UpdateParty();

                return true;
            }
            else return false;
        }

        /// <summary>
        /// Reduces the character's mana and notifys the client of changes.
        /// </summary>
        /// <param name="mana">Amount of mana to deplete.</param>
        public void DepleteMP(int points, bool notify)
        {
            int totalDepleted = points;
            int tempMp = mp;
            mp -= points;
            if (mp < 0)
            {
                totalDepleted = tempMp;
                mp = 0;
            }

            Notify(CommandMessageType.NotifyMP, mp);

            if (notify && totalDepleted != 0) VitalsChanged(this, -totalDepleted, VitalType.Mana);
            UpdateParty();
        }

        public void ReplenishMP(int points, bool notify)
        {
            int totalReplenished = points;
            int tempMp = mp;
            mp += points;
            if (mp > MaxMP)
            {
                totalReplenished = MaxMP - tempMp;
                mp = MaxMP;
            }

            Notify(CommandMessageType.NotifyMP, mp);
            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Mana);

            UpdateParty();
        }
        public void DepleteHP(int points)
        {
            hp -= points;
            if (hp < 0) hp = 0;
            Notify(CommandMessageType.NotifyHP, hp, mp);

            UpdateParty();
        }
        public void ReplenishHP(int points, bool notify)
        {
            int totalReplenished = points;
            int tempHp = hp;
            hp += points;
            if (hp > MaxHP)
            {
                totalReplenished = MaxHP - tempHp;
                hp = MaxHP;
            }

            Notify(CommandMessageType.NotifyHP, hp, mp);
            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Health);

            UpdateParty();
        }
        public void DepleteSP(int points, bool notify)
        {
            if (magicEffects.ContainsKey(MagicType.UnlimitedSP)) return;

            int totalDepleted = points;
            int tempSp = sp;
            sp -= points;
            if (sp < 0)
            {
                totalDepleted = tempSp;
                sp = 0;
            }

            Notify(CommandMessageType.NotifySP, sp);
            if (notify && totalDepleted != 0) VitalsChanged(this, -totalDepleted, VitalType.Stamina);

            UpdateParty();
        }

        public void ReplenishSP(int points, bool notify)
        {
            int totalReplenished = points;
            int tempSp = sp;
            sp += points;

            if (sp > MaxSP)
            {
                totalReplenished = MaxSP - tempSp;
                sp = MaxSP;
            }

            Notify(CommandMessageType.NotifySP, sp);
            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Stamina);

            UpdateParty();
        }

        public void ReplenishHunger(int points, bool notify)
        {
            int totalReplenished = points;
            int tempHunger = hunger;
            hunger += points;

            if (hunger > 100)
            {
                totalReplenished = 100 - tempHunger;
                hunger = 100;
            }

            hpStock += points;
            //if (hpStock > 500) hpStock = 500; Why limit this?

            Notify(CommandMessageType.NotifyHunger, hunger);
            if (notify && totalReplenished != 0) VitalsChanged(this, totalReplenished, VitalType.Hunger);
        }

        public void DepleteHunger(int points, bool notify) //TODO add this
        {
            int totalDepleted = points;
            int tempHunger = hunger;
            hunger -= points;

            if (hunger < 0)
            {
                totalDepleted = tempHunger;
                hunger = 0;
            }

            Notify(CommandMessageType.NotifyHunger, hunger);
            if (notify && totalDepleted != 0) VitalsChanged(this, -totalDepleted, VitalType.Hunger);
        }

        public void AddExp(int points, bool fromParty = false)
        {
            if (!fromParty)
            {
                // farm maps. over lvl 100 gets 10% exp. under lvl 100 gets 125% exp
                if (map.Name.Equals(Globals.ElvineFarmName) || map.Name.Equals(Globals.AresdenFarmName))
                    if (level >= 100) points /= 10; // TODO cleanup hardcoded level and exp modifier
                    else points = (int)((double)points * 1.25);
            }

            if (!fromParty && HasParty && points > 10)
            {
                int partyPoints = (int)(((double)points * 1.1) / party.MemberCount); //Add 10% exp for party
                for (int p = 0; p < Party.MemberCount; p++)
                    if (Cache.World.Players.ContainsKey(Party.Members[p]))
                        if (Cache.World.Players[Party.Members[p]].IsWithinRange(this, 20, 20)) // members must be close
                            Cache.World.Players[Party.Members[p]].AddExp(partyPoints, true);
                // TODO check same IP here to stop lame leveling
            }
            else
            {
                if ((status & 0x10000) != 0) points *= 3; // EXP slate
                experienceStored += points;
            }
        }

        public void Die(IOwner killer, int damage, DamageType type, int hitCount, bool dropLoot)
        {
            if (isDead) return; // avoid die-ception

            isDead = true;
            map[mapY][mapX].ClearOwner();
            map[mapY][mapX].SetDeadOwner(this);

            for (int i = 0; i < 50; i++)
                if (MagicEffects.ContainsKey((MagicType)i))
                    RemoveMagicEffect((MagicType)i);

            // reduce all equipment by 10%
            for (int i = 0; i < Globals.MaximumEquipment; i++)
                if (inventory[i] != null)
                    TakeItemDamageByPercentage(10, inventory[i], i);

            Notify(CommandMessageType.NotifyDead, killer.Name);

            if (StatusChanged != null) StatusChanged(this);

            Killed(this, killer, damage, type, hitCount);

            // titles
            switch (killer.OwnerType)
            {
                case OwnerType.Player:
                    ((Character)killer).Titles[TitleType.PlayersKilled]++;
                    ((Character)killer).Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayersKilled, ((Character)killer).Titles[TitleType.PlayersKilled]);

                    titles[TitleType.PlayerDeaths]++;
                    Notify(CommandMessageType.NotifyTitle, (int)TitleType.PlayerDeaths, titles[TitleType.PlayerDeaths]);
                    break;
                case OwnerType.Npc:
                    titles[TitleType.MonsterDeaths]++;
                    Notify(CommandMessageType.NotifyTitle, (int)TitleType.MonsterDeaths, titles[TitleType.MonsterDeaths]);
                    break;
            }

            if (dropLoot) DropLoot();
        }

        /// <summary>
        /// Character is restarted after death, resetting Hp/Mp/Sp etc.
        /// </summary>
        /// <param name="revivalZone">Specifies whether the character revives at their specified revival zone. False will revive them on the spot</param>
        public void Revive(bool revivalZone = true) //TODO check if all new additions have been added to this
        {
            if (!isDead) return;
            if (map[mapY][mapX].DeadOwner == this) map[mapY][mapX].ClearDeadOwner();
            Cache.World.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
            isDead = false;
            isCorpseExploited = false;
            hp = MaxHP;
            mp = MaxMP;
            sp = MaxSP;
            hunger = 100;
            if (IsCombatMode) ToggleCombatMode();

            //direction = MotionDirection.South;

            if (revivalZone)
                switch (town)
                {
                    case OwnerSide.Neutral: Teleport(Cache.World.Maps[Globals.TravellerRevivalZone], -1, -1, MotionDirection.South); break;
                    case OwnerSide.Aresden: Teleport(Cache.World.Maps[Globals.AresdenRevivalZone], -1, -1, MotionDirection.South); break;
                    case OwnerSide.Elvine: Teleport(Cache.World.Maps[Globals.ElvineRevivalZone], -1, -1, MotionDirection.South); break;
                }
            else Teleport(map, mapX, mapY, MotionDirection.South);
        }

        /// <summary>
        /// Character performs a Pick-Up to collect items at it's location.
        /// </summary>
        /// <param name="sourceX">X coordinate of pickup operation.</param>
        /// <param name="sourceY">Y coordinate of pickup operation.</param>
        /// <param name="direction">Direction in which the character is facing.</param>
        /// <returns>True or False to indicate if Pick Up has failed or not. Reasons for failure include the tile not having items or inventory is full.</returns>
        public bool PickUp(int sourceX, int sourceY, MotionDirection direction)
        {
            Item nextItem = null;
            if (isDead) return false;
            if (playType == PlayType.PvP)
            {
                Cache.World.SendClientMessage(this, "PvP characters cannot pick up items.");
                return false;
            }
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (map[mapY][mapX].Items.Count <= 0) return false;

            Item retrievedItem = map[mapY][mapX].Items.RemoveItem();

            if (Globals.EconomyType == EconomyType.Virtual && retrievedItem.IsGold)
            {
                AddGold(retrievedItem.Count);
                if (map[mapY][mapX].Items.Count <= 0)
                    nextItem = Item.Empty();
                else
                {
                    Item next = map[mapY][mapX].Items.CheckItem();
                    if (next == null)
                        nextItem = Item.Empty();
                    else nextItem = next;
                }
            }
            else
            {
                if (!AddInventoryItem(retrievedItem))
                {
                    map[mapY][mapX].Items.AddItem(retrievedItem); // stick it back on
                    return false;

                }
                else
                {

                    if (map[mapY][mapX].Items.Count <= 0)
                        nextItem = Item.Empty();
                    else
                    {
                        Item next = map[mapY][mapX].Items.CheckItem();
                        if (next == null)
                            nextItem = Item.Empty();
                        else nextItem = next;
                    }
                }
            }

            ItemPickedUp(this, nextItem);

            // identify item for title
            if (!retrievedItem.Identified)
            {
                switch (retrievedItem.Rarity)
                {
                    case ItemRarity.UltraRare:
                        titles[TitleType.RareItemsCollected]++;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.RareItemsCollected, titles[TitleType.RareItemsCollected]);
                        break;
                    default:
                        titles[TitleType.ItemsCollected]++;
                        Notify(CommandMessageType.NotifyTitle, (int)TitleType.ItemsCollected, titles[TitleType.ItemsCollected]);
                        break;
                }

                retrievedItem.Identified = true;
            }

            // highest valued item
            if (retrievedItem.GetGoldValue(true) > titles[TitleType.HighestValueItem])
            {
                titles[TitleType.HighestValueItem] = retrievedItem.GetGoldValue(true);
                Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, titles[TitleType.HighestValueItem]);
            }

            inventory.Update();
            return true;
        }

        /// <summary>
        /// Adds an item to the inventory.
        /// </summary>
        /// <param name="item">Item object to add.</param>
        /// <returns>True or False indicating that the item was successfully added or not.</returns>
        public bool AddInventoryItem(Item item) { int index; return AddInventoryItem(item, out index, true); }
        /// <summary>
        /// Adds an item to the inventory.
        /// </summary>
        /// <param name="item">Item object to add.</param>
        /// <param name="itemIndex">Outputs the index in the inventory that the new item is located.</param>
        /// <param name="notify">Specifies whether to notify the client or not.</param>
        /// <returns>True or False indicating that the item was successfully added or not.</returns>
        public bool AddInventoryItem(Item item, out int itemIndex, bool notify)
        {
            itemIndex = -1;
            if (item == null) return false;
            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            if (item.IsStackable && inventory.Contains(item))
            {
                for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) // start loop after equipment
                    if (inventory[i] != null && inventory[i].Equals(item))
                    {
                        inventory[i].Count += item.Count;
                        itemIndex = i;
                        break;
                    }
            }
            else
            {
                bool added = false;
                for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++) // start loop after equipment
                    if (inventory[i] == null)
                    {
                        inventory[i] = item;
                        itemIndex = i;
                        added = true;
                        break;
                    }
                if (!added) inventory.Add(item); // something wrong here?
            }

            if (itemIndex == -1) return false;
            inventory.Update();

            // we use inventory[itemIndex] here instead of item in case we combined with existing stack
            // we send item.Count so we get the obtained count, NOT the combined count
            if (notify) Notify(CommandMessageType.NotifyItemObtained, inventory[itemIndex].GetData(itemIndex), item.Count);
            return true;
        }

        public void AddGold(int amount)
        {
            switch (Globals.EconomyType)
            {
                case EconomyType.Classic:
                    if (amount > 0)
                    {
                        if (GoldItem != null)
                        {
                            GoldItem.Count += amount;
                            Notify(CommandMessageType.NotifyItemCount, GoldItemIndex, GoldItem.Count, 0);
                        }
                        else
                        {
                            Item goldItem = World.ItemConfiguration[86].Copy(amount); // TODO fix hard codin
                            AddInventoryItem(goldItem);
                        }
                    }
                    else if (amount < 0)
                    {
                        if (GoldItem != null)
                        {
                            GoldItem.Count -= Math.Abs(amount);
                            if (GoldItem.Count <= 0) DepleteItem(GoldItemIndex, true);
                            else Notify(CommandMessageType.NotifyItemCount, GoldItemIndex, GoldItem.Count, 0);
                        }
                    }
                    break;
                case EconomyType.Virtual:
                    gold += amount;
                    Notify(CommandMessageType.NotifyGold, gold);
                    break;
            }
        }

        public bool AddWarehouseItem(Item item) { int index; return AddWarehouseItem(item, 1, out index); }
        public bool AddWarehouseItem(Item item, int count) { int index; return AddWarehouseItem(item, count, out index); }
        public bool AddWarehouseItem(Item item, out int itemIndex) { return AddWarehouseItem(item, 1, out itemIndex); }
        public bool AddWarehouseItem(Item item, int count, out int itemIndex)
        {
            itemIndex = -1;
            if (item == null) return false;
            if (WarehouseCount >= Globals.MaximumWarehouseTabItems)
            {
                Notify(CommandMessageType.NotifyWarehouseFull);
                return false;
            }

            if (item.IsStackable && warehouse.Contains(item))
            {
                for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++)
                    if (warehouse[i] != null && warehouse[i].Equals(item)) // find existing
                    {
                        warehouse[i].Count += count;
                        itemIndex = i;
                        break;
                    }
            }
            else
            {
                bool added = false;
                for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++)
                    if (warehouse[i] == null)
                    {
                        warehouse[i] = item;
                        itemIndex = i;
                        added = true;
                        break;
                    }
                if (!added) warehouse.Add(item); // something wrong?
            }

            if (itemIndex == -1) return false;

            return true;
        }

        public bool Idle(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;

            this.direction = direction;

            map[mapY][mapX].ClearOwner();
            map[sourceY][sourceX].SetOwner(this);

            return true;
        }

        /// <summary>
        /// Moves this character on the Map in the direction specified.
        /// </summary>
        /// <param name="sourceX">Player X Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="sourceY">Player Y Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="direction">Direction in which the character is to move.</param>
        /// <returns>True or False to indicate success or failure of this action.</returns>
        public bool Run(int sourceX, int sourceY, MotionDirection direction)
        {
            if (sp <= 0) return false; // prevents client exploit?

            bool ret = Move(sourceX, sourceY, direction);

            if (ret) DepleteSP(1, false); // reduce stamina when running

            return ret;
        }

        /// <summary>
        /// Moves this character on the Map in the direction specified.
        /// </summary>
        /// <param name="sourceX">Player X Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="sourceY">Player Y Coordinate on the Map. Used to verify Client and Server coordinates are synchronized.</param>
        /// <param name="direction">Direction in which the character is to move.</param>
        /// <returns>True or False to indicate success or failure of this action.</returns>
        public bool Move(int sourceX, int sourceY, MotionDirection direction)
        {
            if (isDead) return false;
            if (!Enum.IsDefined(typeof(MotionDirection), direction)) return false;
            if (sourceX != this.mapX || sourceY != this.mapY) return false;
            if (magicEffects.ContainsKey(MagicType.Paralyze)) return false;

            int destinationX = sourceX, destinationY = sourceY;
            switch (direction)
            {
                case MotionDirection.North: destinationY = sourceY - 1; break;
                case MotionDirection.NorthEast: destinationX = sourceX + 1; destinationY = sourceY - 1; break;
                case MotionDirection.East: destinationX = sourceX + 1; break;
                case MotionDirection.SouthEast: destinationX = sourceX + 1; destinationY = sourceY + 1; break;
                case MotionDirection.South: destinationY = sourceY + 1; break;
                case MotionDirection.SouthWest: destinationX = sourceX - 1; destinationY = sourceY + 1; break;
                case MotionDirection.West: destinationX = sourceX - 1; break;
                case MotionDirection.NorthWest: destinationX = sourceX - 1; destinationY = sourceY - 1; break;
            }

            if (!map[destinationY][destinationX].IsMoveable) return false;
            if (destinationX < 0 || destinationX > map.Width || destinationY < 0 || destinationY > map.Height) return false; // map boundaries

            if (!map[destinationY][destinationX].IsBlocked)
            {
                if (Cache.World.IsApocalypse)
                {
                    Portal dynamicPortal = null;
                    if (Cache.World.Apocalypse.HasPortal(new Location(map.Name, destinationX, destinationY), out dynamicPortal))
                    {
                        Teleport(dynamicPortal.PortalTargetLocation, MotionDirection.South);
                        return true;
                    }
                }

                map[mapY][mapX].ClearOwner();
                this.direction = direction;
                prevMapX = mapX;
                prevMapY = mapY;
                mapX = destinationX;
                mapY = destinationY;
                map[destinationY][destinationX].SetOwner(this);


                //TODO -- Add game setting to turn this feature on/off 
                //TODO -- Check entire item stack for gold/stackable items 
                // Pick up gold automatically

                ItemStack stack = map[destinationY][destinationX].Items;
                for (int i = stack.Count - 1; i >= 0; i--) //start from top and work way down
                {
                    Item item = stack[i];
                    if (item.IsGold || (item.IsStackable && inventory.List.Contains(item)))
                    {
                        stack.CycleItem(i); //add item to top
                        PickUp(destinationX, destinationY, direction); //pickup item
                    }
                }

                //Item item = map[destinationY][destinationX].Items.CheckItem();
                //if (item != null)
                //{
                //    if (item.IsGold) { PickUp(destinationX, destinationY, direction); }

                //    //If player has item already and it is stackable, obtain it
                //    //TODO exclude equipment slots from this
                //    foreach (Item i in inventory.List)
                //    {
                //        if (i != null)
                //        {
                //            if (i.IsStackable && i.FriendlyName == item.FriendlyName) { PickUp(destinationX, destinationY, direction); }
                //        }
                //    }
                //}

                // spike field
                if (map[destinationY][destinationX].IsDynamicOccupied &&
                    map[destinationY][destinationX].DynamicObject.Type == DynamicObjectType.SpikeField)
                {
                    Spikes spikes = (Spikes)map[destinationY][destinationX].DynamicObject;
                    spikes.DoDamage(this);
                }

                UpdateParty();

                titles[TitleType.DistanceTravelled]++;

                return true;
            }
            else return false;
        }

        public void GetKillReward(Character target, bool killer)
        {
            if (killer) //Reward for person to dealt killing blow
            {
                if (target == this)
                {
                    if (target.IsCriminal) //Should we allow players to de crim themselves?
                    {
                        target.CriminalCount--;
                        target.Notify(CommandMessageType.NotifyCriminalCount, target.CriminalCount);
                    }
                    Cache.World.SendClientMessage(this, string.Format("You killed your self!"));
                    return; // no reward for killing self but can decrim?
                }

                // TODO - map no penalty
                if (target.Side == this.Side || target.Side == OwnerSide.Neutral)
                {
                    if (target.IsCriminal)
                    {
                        target.CriminalCount--;
                        target.Notify(CommandMessageType.NotifyCriminalCount, target.CriminalCount);
                        Cache.World.SendClientMessage(this, string.Format("You killed the criminal {0}!", target.name.ToString()));

                        if (!IsCriminal) //Criminal can't gain reward?
                        {
                            rewardGold += target.Level * 3;
                            Notify(CommandMessageType.NotifyCriminalCaptured, target.CriminalCount, target.Level, target.Name);
                        }
                    }
                    else
                    {
                        criminalCount++;
                        Notify(CommandMessageType.NotifyCriminalCount, criminalCount);
                        if (StatusChanged != null) StatusChanged(this);
                        reputation = Math.Min(Math.Max(reputation - 10, Globals.MinimumReputation), Globals.MaximumReputation);
                        Cache.World.SendClientMessage(this, string.Format("You are a criminal for killing {0}!", target.name.ToString())); //TODO reword this?
                        // TODO send to jail, lock map
                    }
                }
                else
                {
                    if (Cache.World.IsCrusade)
                    {
                        // TODO crusade reward - move to Npc.GetKillReward, give construction pt to summoner
                    }

                    enemyKills += Globals.EnemyKillModifier;
                    Notify(CommandMessageType.NotifyEnemyKills, enemyKills);

                    experienceStored += (Dice.Roll(3, (3 * target.Level)) + target.Level) / 3;

                    rewardGold += Dice.Roll(1, target.Level);
                    Cache.World.SendClientMessage(this, string.Format("You killed {0}!", target.name.ToString()));
                }
            }
            else //Reward for players who assisted the kill
            {
                enemyKills += Globals.EnemyKillModifier;
                Notify(CommandMessageType.NotifyEnemyKills, enemyKills);
                experienceStored += (Dice.Roll(3, (3 * target.Level)) + target.Level) / 3;
                rewardGold += Dice.Roll(1, target.Level);
                Cache.World.SendClientMessage(this, string.Format("You assisted in killing {0}!", target.name.ToString()));
            }

            Notify(CommandMessageType.NotifyEnemyKillReward, target);
        }

        public void GetHeldenianFlag()
        {
            if (!Cache.World.IsHeldenianSeige) return;
            if (enemyKills < 10) return;

            switch (town)
            {
                case OwnerSide.Aresden: AddInventoryItem(World.ItemConfiguration[163].Copy()); break; // TODO fix hard coding
                case OwnerSide.Elvine: AddInventoryItem(World.ItemConfiguration[164].Copy()); break; // 163= aresdenflag 164=elvineflag
                default: return;
            }

            enemyKills -= 10;
            Notify(CommandMessageType.NotifyEnemyKills, enemyKills);
        }

        public void SummonCrusadeUnit(int type, int count, int x, int y)
        {
            if (map.IsBuilding || CurrentMap.IsSafeMap || map.IsAttackEnabled == false) return;
            if (!Cache.World.IsCrusade) return;
            if (guild.CrusadeStructures.Count >= World.CrusadeConfiguration.StructuresPerGuild)
            {
                Notify(CommandMessageType.NotifyCrusadeStructureLimitReached);
                return;
            }

            // todo check sufficient construction points
            // todo check guild construct point within 10 cells

            Npc summon = null;
            NpcType npcType = NpcType.None;
            if (Enum.TryParse<NpcType>(type.ToString(), out npcType))
                switch (npcType)
                {
                    case NpcType.CrusadeDetector:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["DT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["DT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 50;
                        break;
                    case NpcType.CrusadeManaCollector:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["MS-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["MS-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 40;
                        break;
                    case NpcType.CrusadeArrowTower:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["AGT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["AGT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 80;
                        break;
                    case NpcType.CrusadeCannonTower:
                        switch (town)
                        {
                            case OwnerSide.Aresden: summon = World.NpcConfiguration["CGT-Aresden"].Copy(); break;
                            case OwnerSide.Elvine: summon = World.NpcConfiguration["CGT-Elvine"].Copy(); break;
                        }
                        summon.BuildPoints = 90;
                        break;
                }

            if (summon != null)
            {
                summon.Appearance2 = 3; // starts in a "packed" state
                summon.Side = town;
                guild.CrusadeStructures.Add(summon);
                Cache.World.CurrentEvent.NpcIDs.Add(Cache.World.InitNpc(summon, map, mapX, mapY, null));
            }
        }

        public void SwapItem(int itemIndex, int destinationItemIndex)
        {
            if (inventory[itemIndex] != null)
            {
                Item item = inventory[itemIndex];

                // dropping to an empty slot
                if (inventory[destinationItemIndex] == null)
                {
                    inventory[destinationItemIndex] = item;
                    inventory[itemIndex] = null;
                }
                // dropping on an occupied slot
                else
                {
                    Item destinationItem = inventory[destinationItemIndex];
                    inventory[itemIndex] = destinationItem;
                    inventory[destinationItemIndex] = item;
                }
            }
        }

        public void SwapWarehouseItem(int itemIndex, int destinationItemIndex)
        {
            if (warehouse[itemIndex] != null)
            {
                Item item = warehouse[itemIndex];

                // dropping to an empty slot
                if (warehouse[destinationItemIndex] == null)
                {
                    warehouse[destinationItemIndex] = item;
                    warehouse[itemIndex] = null;
                }
                // dropping on an occupied slot
                else
                {
                    Item destinationItem = warehouse[destinationItemIndex];
                    warehouse[itemIndex] = destinationItem;
                    warehouse[destinationItemIndex] = item;
                }
            }
        }

        /// <summary>
        /// Character performs a Drop Item operation.
        /// </summary>
        /// <param name="itemIndex">Index of the item being dropped.</param>
        /// <param name="itemId">Id of the item as per Items config file</param>
        /// <param name="amount">Count of the item being dropped. Only affects stackable items if anything above 1.</param>
        /// <returns>True or False to indicate item dropping succeeds or not.</returns>
        public bool DropItem(int itemIndex, int itemId, int amount)
        {
            if (isDead) return false;
            if (inventory[itemIndex] == null) return false;
            if (inventory[itemIndex].IsEquipped) UnequipItem(itemIndex);

            Item item = inventory[itemIndex];

            // if we are dropping less than the amount, split the stack and notify
            if (amount < item.Count)
            {
                Item splitItem = item.Copy(amount);
                if (map[mapY][mapX].SetItem(splitItem))
                {
                    splitItem.SetNumber = 0;
                    item.Count = item.Count - amount; // remove amount from in-bag item then notify
                    Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 0);
                    ItemDropped(this, splitItem); // notify the split amount is dropped
                    inventory.Update();
                    return true;
                }
                else return false;
            }
            else
            {
                if (map[mapY][mapX].SetItem(item))
                {
                    item.SetNumber = 0;
                    inventory[itemIndex] = null;
                    Notify(CommandMessageType.NotifyDropItemAndErase, itemIndex, item.Count);
                    ItemDropped(this, item);
                    inventory.Update();
                    return true;
                }
                else return false;
            }
        }

        public bool ItemToWarehouse(int itemIndex, int destinationIndex)
        {
            if (inventory[itemIndex] == null) return false;

            if (itemIndex < Globals.MaximumEquipment)
            {
                bool foundOriginalItem = false;
                for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
                {
                    if (inventory[i] != null)
                        if (inventory[itemIndex].GuidMatch(inventory[i])) { itemIndex = i; foundOriginalItem = true; break; }
                }

                if (!foundOriginalItem) return false;
            }

            Item item = inventory[itemIndex];
            if (item.IsEquipped) { UnequipItem(itemIndex); }
            if (item.SetNumber != 0) { item.SetNumber = 0; Notify(CommandMessageType.NotifyItemSet, itemIndex, 0); }

            // remove the item
            inventory[itemIndex] = null;


            // if item at destination, swap
            if (destinationIndex != -1)
            {
                if (warehouse[destinationIndex] != null)
                {
                    Item warehouseItem = warehouse[destinationIndex];
                    warehouse[destinationIndex] = null;

                    warehouse[destinationIndex] = item;
                    inventory[itemIndex] = warehouseItem;
                }
                else // otherwise just set the item
                {
                    warehouse[destinationIndex] = item;
                }
            }
            else AddWarehouseItem(item);

            Notify(CommandMessageType.NotifyItemToWarehouse, itemIndex, destinationIndex);
            inventory.Update();

            return true;
        }

        public bool ItemFromWarehouse(int itemIndex, int destinationIndex)
        {
            if (warehouse[itemIndex] == null) return false;
            if (inventory.InventoryCount == Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }
            // remove the item
            Item item = warehouse[itemIndex];
            warehouse[itemIndex] = null;


            // if item at destination, swap
            if (destinationIndex != -1)
            {
                if (destinationIndex < Globals.MaximumEquipment) //TODO 
                {
                    int index;
                    AddInventoryItem(item, out index, true);
                    EquipItem(index);
                    destinationIndex = -1;
                }
                else
                {
                    if (inventory[destinationIndex] != null)
                    {
                        Item inventoryItem = inventory[destinationIndex];
                        inventory[destinationIndex] = null;

                        inventory[destinationIndex] = item;
                        warehouse[itemIndex] = inventoryItem;

                        //TODO add equip if dragged into equip slots
                        if (destinationIndex < Globals.MaximumEquipment)
                            EquipItem(destinationIndex);
                    }
                    else // otherwise just set the item
                    {
                        inventory[destinationIndex] = item;
                    }
                }
            }
            else AddInventoryItem(item);

            Notify(CommandMessageType.NotifyItemFromWarehouse, itemIndex, destinationIndex);
            inventory.Update();

            return true;
        }

        /*public bool SetWarehouseItem(int itemIndex, int count)
        {
            int warehouseIndex = -1;
            if (inventory[itemIndex] == null) return false;
            if (WarehouseCount >= Globals.MaximumWarehouseTabItems)
            {
                Notify(CommandMessageType.NotifyWarehouseFull);
                return false;
            }

            Item item = inventory[itemIndex];

            UnequipItem(itemIndex);

            if (count >= item.Count)
            {
                AddWarehouseItem(item, count, out warehouseIndex);
                inventory[itemIndex] = null;
            }
            else
            {
                AddWarehouseItem(item, count, out warehouseIndex);
                inventory[itemIndex].Count -= count;
            }

            Notify(CommandMessageType.NotifyItemToWarehouse, item.GetInventoryToWarehouseData(warehouseIndex));
            Notify(CommandMessageType.NotifyGiveItemAndErase, itemIndex, count, Globals.WarehouseKeeperName);

            return true;
        }

        public bool RemoveWarehouseItem(int warehouseIndex, out int inventoryIndex)
        {
            inventoryIndex = -1;
            if (warehouse[warehouseIndex] == null) return false;
            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            AddInventoryItem(warehouse[warehouseIndex], out inventoryIndex, false);
            warehouse[warehouseIndex] = null;

            // quickly sort
            for (int i = 0; i <= Globals.MaximumWarehouseTabItems - 2; i++)
                if ((warehouse[i + 1] != null) && (warehouse[i] == null))
                {
                    warehouse[i] = warehouse[i + 1];
                    warehouse[i + 1] = null;
                }

            return true;
        }*/

        public void DropLoot() //TODO Add luck from inventory?
        {
            if (Cache.World.IsCrusade) return; // no drops in sade
            if (!Globals.PlayersDropItems) return;
            if (!map.LootEnabled) return;
            if ((Cache.World.IsHeldenianBattlefield || Cache.World.IsHeldenianSeige) && map.IsHeldenianMap) return; // no drops in heldenian on heldenian maps

            Item loot = null;

            // TODO zemstones
            // TODO criminals have more drops
            // TODO player drops

            if (loot != null) // drop it and notify
            {
                if (map[mapY][mapX].SetItem(loot)) ItemDropped(this, loot);
                inventory.Update();
            }
        }

        public bool IsWithinRange(IOwner other, int rangeX, int rangeY) { return IsWithinRange(other, rangeX, rangeY, 0); }
        public bool IsWithinRange(IOwner other, int rangeX, int rangeY, int modifier)
        {
            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinView(IOwner other)
        {
            int rangeX = (Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CellsWide] / 2) + Globals.OffScreenCells;
            int rangeY = (Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CellsHeigh] / 2) + Globals.OffScreenCells;

            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX) &&
                (mapX <= other.X + rangeX) &&
                (mapY >= other.Y - rangeY) &&
                (mapY <= other.Y + rangeY))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int rangeX, int rangeY, int modifier)
        {
            if ((CurrentMap == other.CurrentMap) &&
                (mapX >= other.X - rangeX - modifier) &&
                (mapX <= other.X + rangeX + modifier) &&
                (mapY >= other.Y - rangeY - modifier) &&
                (mapY <= other.Y + rangeY + modifier))
                return true;
            else return false;
        }

        public bool IsWithinRange(Location location, int range)
        {
            if ((mapX >= location.X - range) &&
                (mapX <= location.X + range) &&
                (mapY >= location.Y - range) &&
                (mapY <= location.Y + range))
                return true;
            else return false;
        }

        public bool IsValidTarget(IOwner owner)
        {
            if (owner.IsDead) return false;
            if (owner.IsRemoved) return false;
            if (owner.IsInvisible) return false;
            if (!IsWithinView(owner)) return false;

            return true;
        }

        public bool IsValidTarget(IOwner owner, int rangeX, int rangeY)
        {
            if (owner.IsDead) return false;
            if (owner.IsRemoved) return false;
            if (owner.IsInvisible) return false;
            if (!IsWithinRange(owner, rangeX, rangeY)) return false;

            return true;
        }

        public bool IsAlly(IOwner owner)
        {
            return (owner.Side == Side);
        }

        /// <summary>
        /// Removes this character from the map
        /// </summary>
        public bool Remove()
        {
            isRemoved = true;

            // clear party info
            LeaveParty(true);

            // clear special ability if enabled
            if (specialAbilityEnabled)
            {
                specialAbilityEnabled = false;
                specialAbilityTime = new TimeSpan(0, 0, Globals.SpecialAbilityTime);
            }

            if (isDead && map[mapY][mapX].DeadOwner == this) map[mapY][mapX].ClearDeadOwner();
            else if (map[mapY][mapX].Owner == this) map[mapY][mapX].ClearOwner();

            // remove character from map id cache
            if (map.Players.Contains(id)) map.Players.Remove(id);

            return true;
        }

        public void Disconnect(string reason)
        {
            if (!clientInfo.Closed) clientInfo.Close(reason);
        }

        /// <summary>
        /// Teleports the character from the current location to a destination passed in as parameters and sets the character's direction.
        /// </summary>
        /// <param name="destinationMap">Destination map to teleport the character to.</param>
        /// <param name="destinationX">X coordinate of the destination.</param>
        /// <param name="destinationY">Y coordinate of the destination.</param>
        /// <param name="direction">Direction the character should fast after teleporting.</param>
        /// <returns>Returns true or false to indicate teleporting has succeeded or not.</returns>
        public bool Teleport(Location location, MotionDirection direction) { return Teleport(Cache.World.Maps[location.MapName], location.X, location.Y, direction); }
        public bool Teleport(Map destinationMap, int destinationX, int destinationY) { return Teleport(destinationMap, destinationX, destinationY, this.direction); }
        public bool Teleport(Map destinationMap, int destinationX, int destinationY, MotionDirection direction) //TODO add randomize factor if location is blocked
        {
            if (isDead) return false;
            if (destinationX < 0 || destinationX > destinationMap.Width || destinationY < 0 || destinationY > destinationMap.Height)
                destinationX = destinationY = -1; // out of bounds, set to defaultLocation (-1,-1)

            if (destinationX == -1 || destinationY == -1)
            {
                if (destinationMap.DefaultLocations.Count > 0)
                {
                    Location location;
                    if (destinationMap.DefaultLocations.Count == 1) location = destinationMap.DefaultLocations[0];
                    else location = destinationMap.DefaultLocations[Math.Max(Dice.Roll(1, destinationMap.DefaultLocations.Count) - 1, 0)];

                    if (location.X < 0 || location.X > destinationMap.Width || location.Y < 0 || location.Y > destinationMap.Height)
                        return false; // TODO log message - default locations are fucked!

                    if (CurrentLocation.Owner == this) CurrentLocation.ClearOwner();
                    if (CurrentLocation.DeadOwner == this) CurrentLocation.ClearDeadOwner();
                    this.map.Players.Remove(id);
                    Cache.World.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
                    this.mapX = location.X;
                    this.mapY = location.Y;
                }
                else return false;
            }
            else
            {
                if (CurrentLocation.Owner == this) CurrentLocation.ClearOwner();
                if (CurrentLocation.DeadOwner == this) CurrentLocation.ClearDeadOwner();
                this.map.Players.Remove(id);
                Cache.World.SendLogEventToNearbyPlayers(this, CommandMessageType.Reject);
                this.mapX = destinationX;
                this.mapY = destinationY;
            }

            this.map = destinationMap;
            this.loadedMapName = this.map.Name;
            this.direction = direction;

            Cache.World.InitPlayerData(id);

            UpdateParty();
            Cache.World.TradeCancel(this.ClientID);

            return true;
        }

        public void UseSkill(SkillType type)
        {
            switch (type)
            {
                case SkillType.PretendCorpse: break; // TODO
            }
        }

        private bool ChangeAppearance(ItemEffectTypeChangeAttribute effect)
        {
            switch (effect)
            {
                case ItemEffectTypeChangeAttribute.HairColour:
                    hairColour++;
                    if (hairColour > 15) hairColour = 0;
                    break;
                case ItemEffectTypeChangeAttribute.HairStyle:
                    hairStyle++;
                    if (hairStyle > 7) hairStyle = 0;
                    break;
                case ItemEffectTypeChangeAttribute.Skin:
                    int skinNum = ((int)skin) + 1;
                    if (skinNum > 3) skinNum = 1;
                    skin = (SkinType)skinNum;

                    type = (gender == GenderType.Male ? 1 : 4) + (skinNum - 1);
                    break;
                case ItemEffectTypeChangeAttribute.Gender:
                    if (BodyArmour != null || Leggings != null || Hauberk != null || Helmet != null ||
                        Weapon != null || Shield != null || Cape != null)
                    {
                        Cache.World.SendClientMessage(this, "Cannot change gender with weapons or armour equipped.");
                        return false;
                    }

                    switch (gender)
                    {
                        case GenderType.Male: gender = GenderType.Female; break;
                        case GenderType.Female: gender = GenderType.Male; break;
                    }

                    type = (gender == GenderType.Male ? 1 : 4) + ((int)skin - 1);
                    break;
                case ItemEffectTypeChangeAttribute.Underwear:
                    underwearColour++;
                    if (underwearColour > 7) underwearColour = 0;
                    break;
            }

            if (StatusChanged != null) StatusChanged(this);

            return true;
        }

        /// <summary>
        /// Character uses a consumable item.
        /// </summary>
        /// <param name="itemIndex">Index of the item being used in the inventory.</param>
        /// <param name="destinationX">Map X Coordination.</param>
        /// <param name="destinationY">Map Y Coordination.</param>
        /// <param name="destinationItemIndex">Index of the target item.</param>
        public void UseItem(int itemIndex, int destinationX, int destinationY, int destinationItemIndex)
        {
            lock (statusLock)
            {
                if (isDead) return;
                if (inventory[itemIndex] == null) return;

                Item item = inventory[itemIndex];
                switch (item.Type)
                {
                    case ItemType.Eat:
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Food: ReplenishHunger(Dice.Roll(item.Effect1, item.Effect2, item.Effect3), true); break;
                            case ItemEffectType.HP:
                                if (magicEffects.ContainsKey(MagicType.TurnUndead))
                                    TakeDamage(DamageType.Spirit, Dice.Roll(item.Effect1, item.Effect2, item.Effect3), MotionDirection.None);
                                else ReplenishHP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3), true);
                                break;
                            case ItemEffectType.MP: ReplenishMP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3), true); break;
                            case ItemEffectType.SP: ReplenishSP(Dice.Roll(item.Effect1, item.Effect2, item.Effect3), true); break;
                            case ItemEffectType.Unfreeze: RemoveMagicEffect(MagicType.Ice); break;
                            case ItemEffectType.ChangeAttribute:
                                if (!ChangeAppearance((ItemEffectTypeChangeAttribute)item.Effect1)) return;
                                break;
                            case ItemEffectType.UnlimitedSP: SetMagicEffect(new Magic(MagicType.UnlimitedSP, item.Effect1)); break;
                        }
                        DepleteItem(itemIndex, true);
                        break;
                    case ItemType.Deplete:
                        bool confirmDeplete = false;
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Magic:
                                confirmDeplete = true;
                                Cache.World.SendMagicEventToNearbyPlayers(this, CommandMessageType.CastMagic, X, Y, item.Effect1 + 100);
                                Cache.World.CastMagic(this, mapX, mapY, World.MagicConfiguration[item.Effect1], true);
                                break;
                            case ItemEffectType.LearnMagic: confirmDeplete = LearnSpell(item.Effect1); break;
                            case ItemEffectType.LearnSkill: confirmDeplete = true; LearnSkill(item.Effect1, item.Effect2); break;
                            case ItemEffectType.Summon:
                                string npcName = "";
                                switch (item.Effect2) // TODO can this hardcoding be fixed?
                                {
                                    case 9: npcName = (town == OwnerSide.Aresden) ? "SOR-Aresden" : (town == OwnerSide.Elvine) ? "SOR-Elvine" : ""; break;
                                    case 10: npcName = (town == OwnerSide.Aresden) ? "ATK-Aresden" : (town == OwnerSide.Elvine) ? "ATK-Elvine" : ""; break;
                                    case 11: npcName = (town == OwnerSide.Aresden) ? "Elf-Aresden" : (town == OwnerSide.Elvine) ? "Elf-Elvine" : ""; break;
                                    case 12: npcName = (town == OwnerSide.Aresden) ? "DSK-Aresden" : (town == OwnerSide.Elvine) ? "DSK-Elvine" : ""; break;
                                    case 13: npcName = (town == OwnerSide.Aresden) ? "HBT-Aresden" : (town == OwnerSide.Elvine) ? "HBT-Elvine" : ""; break;
                                    case 14: npcName = (town == OwnerSide.Aresden) ? "Bar-Aresden" : (town == OwnerSide.Elvine) ? "Bar-Elvine" : ""; break;
                                    default: if (World.SummonConfiguration.ContainsKey(item.Effect1)) npcName = World.SummonConfiguration[item.Effect1]; break;
                                }
                                Cache.World.CreateNpc(npcName, map, mapX, mapY, this);
                                break;
                            default: break;
                        }
                        if (confirmDeplete) DepleteItem(itemIndex, true);
                        else Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 1);
                        // we use notify item count as a dummy because the client-side will release the item so it is no longer "disabled". we should creat its own command in new or updated clients. 1 = bIsItemResponse = true
                        break;
                    case ItemType.DepleteDestination:
                        Item destinationItem;
                        switch (item.EffectType)
                        {
                            case ItemEffectType.Flag: // has occupy status function and num flags unused
                                if (Cache.World.IsHeldenianSeige && map.Name.Equals(Globals.HeldenianCastleName) && guildRank == 0 &&
                                    destinationX == Cache.World.Heldenian.CastleWinningZone.X && destinationY == Cache.World.Heldenian.CastleWinningZone.Y &&
                                    Cache.World.Heldenian.CastleDefender != Side)
                                {
                                    Cache.World.EndWorldEvent(town);
                                    DepleteItem(itemIndex, true);
                                }
                                else Notify(CommandMessageType.NotifyHeldenianFlagFailed);
                                break;
                            case ItemEffectType.ConstructionKit: break; // TODO
                            case ItemEffectType.Dye:
                                if (destinationItemIndex == -1 || inventory[destinationItemIndex] == null) return;
                                destinationItem = inventory[destinationItemIndex];

                                switch (destinationItem.Category)
                                {
                                    case ItemCategory.Clothes:
                                    case ItemCategory.Capes:
                                    case ItemCategory.Robes:
                                    case ItemCategory.Armour:
                                        destinationItem.Colour = item.SpecialEffect2;
                                        destinationItem.ColorType = GameColor.Custom;
                                        Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, destinationItem.Colour, (int)destinationItem.ColorType);
                                        DepleteItem(itemIndex, true);
                                        break;
                                    case ItemCategory.Dyes:
                                        Item mixedDye = World.ItemConfiguration[583].Copy();
                                        mixedDye.SpecialEffect2 = ItemHelper.MixColours(item.SpecialEffect2, destinationItem.SpecialEffect2);

                                        DepleteItem(itemIndex, true);
                                        DepleteItem(destinationItemIndex, true);

                                        AddInventoryItem(mixedDye);
                                        break;
                                    default:
                                        Notify(CommandMessageType.NotifyItemColourChange, destinationItemIndex, -1);
                                        break;
                                }
                                break;
                            case ItemEffectType.FarmSeed:
                                // TODO map maximum crops

                                if (item.Effect2 > skills[SkillType.Farming].Level)
                                    Notify(CommandMessageType.NotifyFarmingSkillTooLow);
                                else if (map[destinationY][destinationX].IsBlocked || !map[destinationY][destinationX].IsFarm)
                                    Notify(CommandMessageType.NotifyFarmingInvalidLocation);
                                else
                                {
                                    Npc crop = World.NpcConfiguration["Crops"].Copy();
                                    crop.Side = town;
                                    crop.BuildType = item.Effect1;
                                    crop.BuildLimit = item.Effect2;
                                    crop.BuildPoints = 30; // TODO this previously used the "MinBravery" variable. we should add BuildPoints to the config
                                    crop.Appearance2 = item.Effect1 << 8 | 1;
                                    Cache.World.InitNpc(crop, map, mapX, mapY, this);

                                    DepleteItem(itemIndex, true);
                                }
                                break;
                            default: DepleteItem(itemIndex, true); break;
                        }
                        break;
                    case ItemType.Skill:
                        if (skillInUse == SkillType.None && item.Endurance > 0)
                        {
                            TakeItemDamage(1, item, itemIndex);

                            switch (item.RelatedSkill)
                            {
                                case SkillType.Fishing:
                                    skillInUse = SkillType.Fishing;
                                    skillTime = DateTime.Now; // triggers TimerProcess() then SkillProcess()
                                    skillTargetX = destinationX;
                                    skillTargetY = destinationY;
                                    break;
                            }
                        }
                        break;
                    default: DepleteItem(itemIndex, true); break;
                }
            }
        }

        public void LearnSkill(int skillIndex, int level)
        {
            if (skills[(SkillType)skillIndex] == null) return;

            // TODO - max of 700 levels? leveling down of skills?

            // only make changes and notify if the player is previously below requests level
            if (skills[(SkillType)skillIndex].Experience < Globals.SkillExperienceTable[level])
            {
                skills[(SkillType)skillIndex].Learn();
                Notify(CommandMessageType.NotifyStudySkillSuccess, skillIndex, level);
            }
        }

        public bool LearnSpell(string spellName, bool isPurchased) { foreach (Magic spell in World.MagicConfiguration.Values) if (spell.Name.Equals(spellName)) return LearnSpell(spell.Index, true); return false; }
        public bool LearnSpell(int spellIndex) { return LearnSpell(spellIndex, false); }
        public bool LearnSpell(int spellIndex, bool isPurchased)
        {
            Magic spell = null;
            if (World.MagicConfiguration.ContainsKey(spellIndex)) spell = World.MagicConfiguration[spellIndex];
            if (spell == null) return false;

            if (spellStatus[spellIndex] == true) { Cache.World.SendClientMessage(this, string.Format("{0} already learned.", spell.Name)); return false; }
            if (isPurchased && spell.GoldCost == -1) { Cache.World.SendClientMessage(this, string.Format("{0} can't be purchased.", spell.Name)); return false; } //TO - Cannot purchase unpurchasable spells
            if (intelligence + IntelligenceBonus < spell.RequiredIntelligence) //Can't learn spell if dont have required int
            {
                Notify(CommandMessageType.NotifyStudySpellFailed, spellIndex, spell.GoldCost, spell.RequiredIntelligence, spell.Name);
                return false;
            }

            if (isPurchased)
                switch (Globals.EconomyType)
                {
                    case EconomyType.Classic:
                        if (GoldItem == null || GoldItem.Count < spell.GoldCost)
                        {
                            Notify(CommandMessageType.NotifyStudySpellFailed, spellIndex, spell.GoldCost, spell.RequiredIntelligence, spell.Name);
                            return false;
                        }
                        else AddGold(-spell.GoldCost);
                        break;
                    case EconomyType.Virtual:
                        if (gold < spell.GoldCost)
                        {
                            Notify(CommandMessageType.NotifyStudySpellFailed, spellIndex, spell.GoldCost, spell.RequiredIntelligence, spell.Name);
                            return false;
                        }
                        else AddGold(-spell.GoldCost);
                        break;
                }

            spellStatus[spellIndex] = true;
            Notify(CommandMessageType.NotifyStudySpellSuccess, spellIndex, 0, spell.Name);

            return true;
        }

        public bool UnLearnSpell(int spellIndex)
        {
            if (spellIndex >= 0 && spellIndex < spellStatus.Length)
            {
                Magic spell = null;
                if (World.MagicConfiguration.ContainsKey(spellIndex)) spell = World.MagicConfiguration[spellIndex];
                if (spell == null) return false;
                if (spellStatus[spellIndex] == false) return false;

                //Check for spell book
                Item item = null;
                foreach (KeyValuePair<int, Item> itemInfo in World.ItemConfiguration)
                {
                    item = itemInfo.Value;
                    if (item != null && item.Type == ItemType.Deplete && item.EffectType == ItemEffectType.LearnMagic && item.Effect1 == spellIndex)
                    {
                        if (AddInventoryItem(item.Copy())) { break; }
                        else { return false; }
                    }
                }

                spellStatus[spellIndex] = false;
                Notify(CommandMessageType.NotifySpellUnlearned, spellIndex);
                return true;


            }
            return false;
        }

        public bool BuyItem(int merchantId, int itemId)
        {
            if (isDead) return false;
            if (playType == PlayType.PvP)
            {
                Cache.World.SendClientMessage(this, "PvP characters cannot buy items.");
                return false;
            }

            if (CurrentMap.BuildingType != BuildingType.Shop &&
                CurrentMap.BuildingType != BuildingType.Blacksmith && CurrentMap.BuildingType != BuildingType.ElvineCityhall) return false; // TODO might be other merchant type //TEST SERVER CHANGE -remove elvinecityhall

            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return false;
            }

            if (!World.MerchantConfiguration.ContainsKey(merchantId)) return false;
            if (!World.MerchantConfiguration[merchantId].ContainsKey(itemId)) return false;

            int count = 1; // TODO
            ItemPurchased(World.MerchantConfiguration[merchantId][itemId].DatabaseId, count); // update/remove from database

            Item item = World.MerchantConfiguration[merchantId][itemId].Item;
            int cost = item.GetGoldValue();

            // crusade winners get 10% discount
            if (World.WorldEventResults.ContainsKey(WorldEventType.Crusade) &&
                World.WorldEventResults[WorldEventType.Crusade].Winner == town)
                cost = ((int)((double)item.GetGoldValue() * 0.9) * count);
            else cost = item.GetGoldValue() * count;

            // heldenian losers get 100% penalty
            if (World.WorldEventResults.ContainsKey(WorldEventType.Heldenian) &&
                World.WorldEventResults[WorldEventType.Heldenian].Winner != town &&
                town != OwnerSide.Neutral)
                cost = (item.GetGoldValue() * 2) * count;

            // charisma discount (replaced with agility)
            //if ((charisma + charismaBonus) > 10)
            //cost -= (int)((double)cost * ((double)((charisma + charismaBonus) / 4) / 100.0f));

            switch (Globals.EconomyType)
            {
                case EconomyType.Virtual:
                    if (gold < cost) // you are poor
                        Notify(CommandMessageType.NotifyNotEnoughGold, -1); // -1 is the item index. since this is a new item, use -1
                    else
                    {
                        gold -= cost;
                        Notify(CommandMessageType.NotifyGold, gold);

                        if (item.IsStackable)
                        {
                            Item split = item.Copy(count); // split the merchant's stack to be given to the player
                            item.Count -= count; // set the merchant's stack count

                            int stackIndex; // if player already has a stack, we want to send the combined amount in the notify
                            AddInventoryItem(split, out stackIndex, true);

                            if (stackIndex != -1 && Inventory[stackIndex] != null)
                                Notify(CommandMessageType.NotifyItemPurchased, Inventory[stackIndex].GetPurchasedData((short)cost));

                            // remove from merchant item if the stack has none left
                            if (item.Count <= 0) World.MerchantConfiguration[merchantId].Remove(itemId);
                        }
                        else
                            for (int i = 0; i < count; i++)
                            {
                                AddInventoryItem(item);
                                Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                                World.MerchantConfiguration[merchantId].Remove(itemId);
                            }

                        // highest valued item
                        if (item.GetGoldValue(true) > titles[TitleType.HighestValueItem])
                        {
                            titles[TitleType.HighestValueItem] = item.GetGoldValue(true);
                            Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, titles[TitleType.HighestValueItem]);
                        }

                        return true;
                    }
                    break;
                case EconomyType.Classic:
                    if (GoldItem == null || GoldItem.Count < cost) // you are poor
                        Notify(CommandMessageType.NotifyNotEnoughGold, -1); // -1 is the item index. since this is a new item, use -1
                    else
                    {
                        AddGold(-cost);

                        if (item.IsStackable)
                        {
                            Item split = item.Copy(count);
                            AddInventoryItem(split);
                            item.Count -= count;

                            Notify(CommandMessageType.NotifyItemPurchased, split.GetPurchasedData((short)cost));

                            if (item.Count <= 0) World.MerchantConfiguration[merchantId].Remove(itemId);
                        }
                        else
                            for (int i = 0; i < count; i++)
                            {
                                AddInventoryItem(item);
                                Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));

                                World.MerchantConfiguration[merchantId].Remove(itemId);
                            }

                        // highest valued item
                        if (item.GetGoldValue(true) > titles[TitleType.HighestValueItem])
                        {
                            titles[TitleType.HighestValueItem] = item.GetGoldValue(true);
                            Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, titles[TitleType.HighestValueItem]);
                        }

                        return true;
                    }
                    break;
            }

            return false;
        }

        /*public void BuyItem(int itemId, int count)
        {
            if (isDead) return;

            if (CurrentMap.BuildingType != BuildingType.Shop &&
                CurrentMap.BuildingType != BuildingType.Blacksmith) return;

            if (InventoryCount >= Globals.MaximumInventoryItems)
            {
                Notify(CommandMessageType.NotifyInventoryFull);
                return;
            }

            Item item = null;

            item = World.ItemConfiguration[itemId].Copy();

            if (item == null) return;
            if (!item.IsForSale) return;

            // non stackable items take 1 slot each. make sure we dont go over max items
            if (!item.IsStackable && (InventoryCount + count > Globals.MaximumInventoryItems))
                count = (Globals.MaximumInventoryItems - InventoryCount);

            int cost = 0;

            // crusade winners get 10% discount
            if (World.WorldEventResults.ContainsKey(WorldEventType.Crusade) && 
                World.WorldEventResults[WorldEventType.Crusade].Winner == town)
                 cost = ((int)((double)item.Price * 0.9) * count);
            else cost = item.Price * count;

            // heldenian losers get 100% penalty
            if (World.WorldEventResults.ContainsKey(WorldEventType.Heldenian) &&
                World.WorldEventResults[WorldEventType.Heldenian].Winner != town &&
                town != OwnerSide.Neutral)
                cost = (item.Price * 2) * count;

            // charisma discount
            if ((charisma + charismaBonus) > 10)
                cost -= (int)((double)cost * ((double)((charisma + charismaBonus) / 4) / 100.0f));

            switch (Globals.EconomyType)
            {
                case EconomyType.Virtual:
                    if (gold < cost) // you are poor
                        Notify(CommandMessageType.NotifyNotEnoughGold, -1); // -1 is the item index. since this is a new item, use -1
                    else
                    {
                        gold -= cost;
                        Notify(CommandMessageType.NotifyGold, gold);

                        if (item.IsStackable)
                        {
                            item.Count *= count; // we multiple incase bought item already has count (arrows)
                            AddInventoryItem(item);
                            Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                        }
                        else
                            for (int i = 0; i < count; i++)
                            {
                                AddInventoryItem(item);
                                Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                            }
                    }
                    break;
                case EconomyType.Classic:
                    if (GoldItem == null || GoldItem.Count < cost) // you are poor
                        Notify(CommandMessageType.NotifyNotEnoughGold, -1); // -1 is the item index. since this is a new item, use -1
                    else
                    {
                        GoldItem.Count -= cost;
                        if (GoldItem.Count <= 0) DepleteItem(GoldItemIndex, true);
                        else Notify(CommandMessageType.NotifyItemCount, GoldItemIndex, GoldItem.Count, 0);

                        if (item.IsStackable)
                        {
                            item.Count *= count; // we multiple incase bought item already has count (arrows)
                            AddInventoryItem(item);
                            Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                        }
                        else
                            for (int i = 0; i < count; i++)
                            {
                                AddInventoryItem(item);
                                Notify(CommandMessageType.NotifyItemPurchased, item.GetPurchasedData((short)cost));
                            }
                    }
                    break;
            }
        }*/

        public bool SellItem(int merchantId, int itemIndex)
        {
            if (isDead) return false;
            if (playType == PlayType.PvP)
            {
                Cache.World.SendClientMessage(this, "PvP characters cannot sell items.");
                return false;
            }
            if (inventory[itemIndex] == null) return false;
            Item item = inventory[itemIndex];
            if (item.Count <= 0) return false;

            int count = item.Count; // TODO allow player to set the number of items to sell
            int sellPrice = (count * (item.GetGoldValue() / 2));
            Notify(CommandMessageType.NotifyItemSold, itemIndex);

            if (item.IsStackable)
            {
                MerchantItem merchantItem = null;
                // check if existing item in merchant list
                bool stack = false;
                foreach (MerchantItem itemCheck in World.MerchantConfiguration[merchantId].Values)
                    if (itemCheck.Item.ItemId == item.ItemId)
                    {
                        itemCheck.Item.Count += count;
                        merchantItem = itemCheck;
                        stack = true;
                        break;
                    }

                // if doesnt exist, add a new one
                if (merchantItem == null)
                {
                    merchantItem = new MerchantItem(merchantId, item, false);
                    World.MerchantConfiguration[merchantId].Add(merchantItem.Id, merchantItem);
                }

                // set the character's stack count, and remove if less than 1
                item.Count -= count;
                if (item.Count <= 0) DepleteItem(itemIndex);
                else Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 0);

                ItemSold(merchantItem, stack); // update database
            }
            else
            {
                DepleteItem(itemIndex);
                MerchantItem merchantItem = new MerchantItem(merchantId, item, false);
                World.MerchantConfiguration[merchantId].Add(merchantItem.Id, merchantItem);
                ItemSold(merchantItem, false); // update database
            }

            AddGold(sellPrice);

            return true;
        }

        /*public void SellItem(int itemIndex, int ownerType, int count, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (count <= 0 || item.Count < count) return;

            switch (ownerType) // bad way to do this... but must emulate for client. consider the way GiveItem works (checks destination and uses ownerID so no hard-coded values)
            {
                case 15: // ShopKeeper-W
                case 24: // Tom
                    int sellPrice = (Side == OwnerSide.Neutral) ? (item.Price / 4) * count : (item.Price / 2) * count;

                    // TODO stated items give higher price

                    switch (Globals.EconomyType)
                    {
                        case EconomyType.Classic:
                            if (GoldItem == null && InventoryCount + 1 < Globals.MaximumInventoryItems)
                                Notify(CommandMessageType.NotifySellItemFailed, itemIndex, 4, item.ItemId); // no space to take gold
                            else Notify(CommandMessageType.NotifySellItemQuote, itemIndex, item.Endurance, sellPrice, count, item.ItemId);
                            break;
                        case EconomyType.Virtual:
                            Notify(CommandMessageType.NotifySellItemQuote, itemIndex, item.Endurance, sellPrice, count, item.ItemId);
                            break;
                    }

                    
                    break;
            }
        }*/

        public bool RepairItem(int merchantId, int itemIndex)
        {
            if (isDead) return false;
            if (playType == PlayType.PvP)
            {
                Cache.World.SendClientMessage(this, "PvP characters cannot repair items.");
                return false;
            }
            if (inventory[itemIndex] == null) return false;
            Item item = inventory[itemIndex];
            if (item.Count <= 0) return false;

            int repairPrice = item.GetRepairCost();

            switch (Globals.EconomyType)
            {
                case EconomyType.Classic:
                    if (GoldItem == null || GoldItem.Count < repairPrice)
                    {
                        Notify(CommandMessageType.NotifyNotEnoughGold, itemIndex);
                        return false;
                    }
                    else
                    {
                        item.Endurance = item.MaximumEndurance;
                        Notify(CommandMessageType.NotifyItemRepaired, itemIndex, item.Endurance);

                        GoldItem.Count -= repairPrice;
                        Notify(CommandMessageType.NotifyItemCount, GoldItemIndex, GoldItem.Count, 0);
                    }
                    break;
                case EconomyType.Virtual:
                    if (gold < repairPrice)
                    {
                        Notify(CommandMessageType.NotifyNotEnoughGold, itemIndex);
                        return false;
                    }
                    else
                    {
                        item.Endurance = item.MaximumEndurance;
                        Notify(CommandMessageType.NotifyItemRepaired, itemIndex, item.Endurance);
                        gold -= repairPrice;
                        Notify(CommandMessageType.NotifyGold, gold);
                    }
                    break;
            }

            return true;
        }

        /*public void RepairItem(int itemIndex, int ownerType, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];

            switch (ownerType) // bad way to do this... but must emulate for client. consider the way GiveItem works (checks destination and uses ownerID so no hard-coded values)
            {
                case 15: // ShopKeeper-W
                case 24: // Tom
                    int repairPrice = (item.IsBroken) ? item.Price/2 : (int)((item.Price/2)-(int)((double)item.Price *(((double)item.Endurance/(double)item.MaximumEndurance)*0.5f)));

                    Notify(CommandMessageType.NotifyRepairItemQuote, itemIndex, item.Endurance, repairPrice, item.ItemId);
                    break;
            }
        }*/


        public void SellItemConfirm(int itemIndex, int count, int itemId)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (count <= 0 || item.Count < count) return;
            if (item.IsBroken) return;

            int sellPrice = (Side == OwnerSide.Neutral) ? (item.Price / 4) * count : (item.Price / 2) * count;

            // TODO stated items give higher price

            Notify(CommandMessageType.NotifyItemSold, itemIndex);

            if (item.IsStackable)
            {
                item.Count--;
                if (item.Count <= 0) DepleteItem(itemIndex);
                else Notify(CommandMessageType.NotifyItemCount, itemIndex, item.Count, 0);
            }
            else DepleteItem(itemIndex);

            AddGold(sellPrice);
        }

        public void RepairItemConfirm(int itemIndex, string itemName)
        {
            if (isDead) return;
            if (inventory[itemIndex] == null) return;
            Item item = inventory[itemIndex];
            if (item.IsBroken) return;

            int repairPrice = (item.IsBroken) ? item.Price / 2 : (int)((item.Price / 2) - (int)((double)item.Price * (((double)item.Endurance / (double)item.MaximumEndurance) * 0.5f)));


            switch (Globals.EconomyType)
            {
                case EconomyType.Classic:
                    if (GoldItem == null || GoldItem.Count < repairPrice)
                        Notify(CommandMessageType.NotifyNotEnoughGold, itemIndex);
                    else
                    {
                        item.Endurance = item.MaximumEndurance;
                        Notify(CommandMessageType.NotifyItemRepaired, itemIndex, item.Endurance);

                        GoldItem.Count -= repairPrice;
                        Notify(CommandMessageType.NotifyItemCount, GoldItemIndex, GoldItem.Count, 0);
                    }
                    break;
                case EconomyType.Virtual:
                    if (gold < repairPrice)
                        Notify(CommandMessageType.NotifyNotEnoughGold, itemIndex);
                    else
                    {
                        item.Endurance = item.MaximumEndurance;
                        Notify(CommandMessageType.NotifyItemRepaired, itemIndex, item.Endurance);
                        gold -= repairPrice;
                        Notify(CommandMessageType.NotifyGold, gold);
                    }
                    break;
            }
        }

        public bool BecomeCitizen()
        {
            if (level < 5 || town != OwnerSide.Neutral) return false;

            if (CurrentMap.BuildingType == BuildingType.AresdenCityhall)
                town = OwnerSide.Aresden;
            else if (CurrentMap.BuildingType == BuildingType.ElvineCityhall)
                town = OwnerSide.Elvine;

            return true;
        }

        public void BecomeCitizen(OwnerSide side)
        {
            this.town = side;

            byte[] command = new byte[10];
            command[0] = (byte)1;
            command[1] = (byte)(int)Town;
            Cache.World.Send(ClientID, CommandType.ResponseCitizenship, CommandMessageType.Confirm, command); // TODO dodgy but meh
        }

        public void ToggleCombatMode()
        {
            //int appearance = ((this.appearance2 & 0xF000) >> 12);

            //if (!IsCombatMode) this.appearance2 = (0xF000 | appearance2);
            //
            //else this.appearance2 = (0x0FFF & appearance2);
            isCombatMode = !isCombatMode;

            if (StatusChanged != null) StatusChanged(this);
        }

        public void ToggleSafeMode()
        {
            isSafeMode = !isSafeMode;
            Notify(CommandMessageType.NotifySafeMode, ((isSafeMode) ? 1 : 0));
        }

        public int GetRelationship(IOwner other)
        {
            switch (other.OwnerType)
            {
                case Helbreath.OwnerType.Player: return GetPlayerRelationship((Character)other);
                case Helbreath.OwnerType.Npc: return GetNpcRelationship((Npc)other);
                default: return 0;
            }
        }

        public int GetPlayerRelationship(Character other)
        {
            int status = 0;

            if (criminalCount > 0) status = 8;
            if (town != Helbreath.OwnerSide.Neutral) status = status | 4;
            if (town == Helbreath.OwnerSide.Aresden) status = status | 2;
            if (isCivilian) status = status | 1; // m_bIsHunter in old source

            return status;
        }

        public int GetNpcRelationship(Npc other)
        {
            int status = 0x0000;

            switch (other.Side)
            {
                case OwnerSide.Neutral:
                    break;
                case OwnerSide.Wild:
                    status |= 0x0001 << 3;
                    break;
                case OwnerSide.Aresden:
                    status |= 0x0001 << 2;
                    status |= 0x0001 << 1;
                    break;
                case OwnerSide.Elvine:
                    status |= 0x0001 << 2;
                    break;
            }

            return status;
        }

        /// <summary>
        /// Actions to be performed on a timer such as hunger, mp/hp regeneration etc.
        /// </summary>
        public void TimerProcess()
        {
            if (isDead) return;

            TimeSpan ts;


            // handle Hunger
            ts = DateTime.Now - hungerTime;
            if (ts.TotalSeconds >= Globals.HungerTime)
            {
                hunger--; //TODO use DepleteHunger() 
                if (hunger <= 0) hunger = 0;
                if (hunger < 30) Notify(CommandMessageType.NotifyHunger, hunger);
                hungerTime = DateTime.Now;
            }

            // if starving, cant regen
            if (hunger > 10)
            {
                // handle Hp Regen
                if (hp < MaxHP)
                {
                    ts = DateTime.Now - hpUpTime;
                    if (notifyTickHP == false && ts.TotalSeconds >= Globals.HPRegenTime - 3) { Notify(CommandMessageType.NotifyTickHP); notifyTickHP = true; }
                    if (ts.TotalSeconds >= Globals.HPRegenTime)
                    {
                        int regen = Dice.Roll(1, (vitality + VitalityBonus));
                        if (regen < (vitality + VitalityBonus) / 2) { regen = (vitality + VitalityBonus) / 2; }
                        regen += hpStock;
                        if (inventory.HPRecoveryBonus > 0) { regen += (int)((double)(((double)inventory.HPRecoveryBonus / 100.0f) * (double)regen)); }
                        if (inventory.Blood) { regen -= (regen / 5); } // Blood regen reduction
                        ReplenishHP(regen, true);
                        Notify(CommandMessageType.NotifyTickStopHP);
                        notifyTickHP = false;
                        hpUpTime = DateTime.Now;
                    }
                }

                // handle Mp Regen
                if (mp < MaxMP)
                {
                    ts = DateTime.Now - mpUpTime;
                    if (notifyTickMP == false && ts.TotalSeconds >= Globals.MPRegenTime - 3) { Notify(CommandMessageType.NotifyTickMP); notifyTickMP = true; }
                    if (ts.TotalSeconds >= Globals.MPRegenTime)
                    {
                        int regen = Dice.Roll(1, (magic + MagicBonus));

                        if (regen < (magic + MagicBonus) / 2) regen = (magic + MagicBonus) / 2;

                        if (inventory.MPRecoveryBonus > 0) regen += (int)(((double)inventory.MPRecoveryBonus / 100.0f) * (double)regen);

                        ReplenishMP(regen, false);
                        Notify(CommandMessageType.NotifyTickStopMP);
                        notifyTickMP = false;
                        mpUpTime = DateTime.Now;
                    }
                }

                // handle Sp Regen
                if (sp < MaxSP)
                {
                    ts = DateTime.Now - spUpTime;
                    if (notifyTickSP == false && ts.TotalSeconds >= Globals.SPRegenTime - 3) { Notify(CommandMessageType.NotifyTickSP); notifyTickSP = true; }
                    if (ts.TotalSeconds >= Globals.SPRegenTime)
                    {
                        int regen = Dice.Roll(1, (vitality + VitalityBonus) / 3, 15);

                        if (regen < (vitality + VitalityBonus) / 6) regen = (Vitality + VitalityBonus) / 6;

                        if (inventory.SPRecoveryBonus > 0) regen += (int)(((double)inventory.SPRecoveryBonus / 100.0f) * (double)regen);
                        ReplenishSP(regen, false);
                        Notify(CommandMessageType.NotifyTickStopSP);
                        notifyTickSP = false;
                        spUpTime = DateTime.Now;
                    }
                }
            }


            // handle poison time
            if (magicEffects.ContainsKey(MagicType.Poison))
            {
                ts = DateTime.Now - poisonTime;
                if (ts.TotalSeconds >= Globals.PoisonTime)
                {
                    int damage = magicEffects[MagicType.Poison].Magic.Effect2;
                    //int damage = Dice.Roll(1, magicEffects[MagicType.Poison].Magic.Effect2);
                    if (hp - damage < 1) damage = hp - 1;// cant go below 1 hp
                    DepleteHP(damage);
                    lastDamage = damage;
                    lastDamageTime = DateTime.Now;
                    DamageTaken(this, DamageType.Poison, null, damage, MotionDirection.None, 1, damage);
                    if (EvadePoison()) RemoveMagicEffect(MagicType.Poison);
                    poisonTime = DateTime.Now;
                }
            }

            // handle skill time
            if (skillInUse != SkillType.None)
            {
                ts = DateTime.Now - skillTime;
                if (ts.Seconds >= Globals.SkillTime)
                    SkillProcess(skillInUse);
            }

            // handle increase of stored experience and level up
            if (experienceStored > 0)
            {
                ts = DateTime.Now - experienceUpTime;
                if (notifyTickEXP == false && ts.TotalSeconds >= Globals.ExperienceRollTime - 3) { Notify(CommandMessageType.NotifyTickEXP); notifyTickEXP = true; }
                if (ts.TotalSeconds >= Globals.ExperienceRollTime)
                {
                    experience += (experienceStored * Globals.ExperienceMultiplier);

                    if (experience >= Globals.ExperienceTable[level + 1])
                    {
                        bool majesticUp = false;
                        bool levelUp = false;
                        int majesticCount = 0;
                        //int levelCount = 0;
                        // handle level up. use loop incase there is enough experience to level up more than once.
                        while (experience >= Globals.ExperienceTable[level + 1])
                            if (level + 1 > Globals.MaximumLevel)
                            {
                                majestics++;
                                majesticCount++;
                                if (!majesticUp) majesticUp = true;
                                experience -= Globals.ExperienceTable[Globals.MaximumLevel + 1] - Globals.ExperienceTable[Globals.MaximumLevel];
                            }
                            else if (town == OwnerSide.Neutral && level + 1 > Globals.TravellerLimit)
                            {
                                Notify(CommandMessageType.NotifyTravellerLimit);
                                experience = Globals.ExperienceTable[Globals.TravellerLimit] - 1;
                                break;
                            }
                            else
                            {
                                // give starter gold from lvl 2~20
                                if (level < 20 && Globals.EconomyType == EconomyType.Virtual)
                                {
                                    int addGold = (level * 80);
                                    AddGold(addGold);
                                }

                                if (!levelUp) levelUp = true;
                                level++;
                                //levelCount++;
                            }

                        if (majesticUp)
                        {
                            Notify(CommandMessageType.NotifyMajestics, majestics, majesticCount);
                            experience = Globals.ExperienceTable[Globals.MaximumLevel];
                        }

                        if (levelUp)
                        {
                            Notify(CommandMessageType.NotifyLevelUp);
                        }
                    }

                    Notify(CommandMessageType.NotifyTickStopEXP);
                    notifyTickEXP = false;

                    Notify(CommandMessageType.NotifyExperience);
                    experienceStored = 0;
                    experienceUpTime = DateTime.Now;
                }
            }

            // handle magic effect removal if time has expired
            if (magicEffects.Count > 0)
                for (int i = 0; i < 50; i++)
                    if (magicEffects.ContainsKey((MagicType)i))
                    {
                        MagicEffect effect = magicEffects[(MagicType)i];
                        if (effect.Magic.Type != MagicType.Poison) // poison have their own timer
                        {
                            ts = DateTime.Now - effect.StartTime;
                            if (ts.TotalSeconds >= effect.Magic.LastTime.TotalSeconds) // use total seconds as some spells last over 59 seconds
                                RemoveMagicEffect(effect.Magic.Type);
                        }
                    }

            // handle quest check for completion/notifications
            if (activeQuests.Count > 0)
                foreach (Quest quest in activeQuests)
                    switch (quest.Type)
                    {
                        case QuestType.Slayer: break;
                        default: break;
                    }

            //handle Combo Counters
            if (comboDamageTargets.Count > 0)
            {
                List<int> expiredCounters = new List<int>();
                foreach (KeyValuePair<int, ComboDamageCounter> HitObject in comboDamageTargets)
                {
                    ComboDamageCounter counter = HitObject.Value;
                    if ((DateTime.Now - counter.StartTime).TotalSeconds > 10) expiredCounters.Add(HitObject.Key);
                }

                if (expiredCounters.Count > 0)
                {
                    foreach (int counterID in expiredCounters)
                    {
                        comboDamageTargets.Remove(counterID);
                    }
                }

            }

            //Handle Damagedby Counters //TODO can we combine this and combo?
            if (damagedByObjects.Count > 0)
            {
                List<int> expiredCounters = new List<int>();
                foreach (KeyValuePair<int, DateTime> attacker in damagedByObjects)
                {
                    if ((DateTime.Now - attacker.Value).TotalSeconds > 10) expiredCounters.Add(attacker.Key);
                }

                if (expiredCounters.Count > 0)
                {
                    foreach (int counterID in expiredCounters)
                    {
                        damagedByObjects.Remove(counterID);
                    }
                }
            }

            //Handle Supported by Counters //TODO can we combine this and combo?
            if (supportedByObjects.Count > 0)
            {
                List<int> expiredCounters = new List<int>();
                foreach (KeyValuePair<int, DateTime> support in supportedByObjects)
                {
                    if ((DateTime.Now - support.Value).TotalSeconds > 10) expiredCounters.Add(support.Key);
                }

                if (expiredCounters.Count > 0)
                {
                    foreach (int counterID in expiredCounters)
                    {
                        supportedByObjects.Remove(counterID);
                    }
                }
            }

            ChargeCritical();

            ChargeSpecialAbility();
            if (muteTime.TotalSeconds > 0) muteTime = muteTime.Subtract(new TimeSpan(0, 0, 3));
            if (reputationTime.TotalSeconds > 0) reputationTime = reputationTime.Subtract(new TimeSpan(0, 0, 3));
        }

        /// <summary>
        /// Handles skills that need to process over time, such as fishing.
        /// </summary>
        /// <param name="skill"></param>
        private void SkillProcess(SkillType skill)
        {
            switch (skill)
            {
                case SkillType.Fishing:
                    int skillLevel = skills[SkillType.Fishing].Level;
                    int changeValue = Math.Max(skills[SkillType.Fishing].Level / 10, 1);

                    if (targetFish == null)
                        if (map[skillTargetY][skillTargetX].IsWater)
                        {
                            if (map[skillTargetY][skillTargetX].DynamicObject != null &&
                                map[skillTargetY][skillTargetX].DynamicObject.Type == DynamicObjectType.Fish) // rare fish/bubbles
                            {
                                Fish fish = (Fish)CurrentMap[skillTargetY][skillTargetX].DynamicObject;
                                targetFish = fish;
                                targetFishChance = 1;
                                Notify(CommandMessageType.NotifyFishingStarted, fish.FishItem.Price / 2, fish.FishItem.Sprite, fish.FishItem.SpriteFrame, fish.FishItem.ItemId);
                                Notify(CommandMessageType.NotifySkillEnd, 1);
                            }
                            else // standard fish - 100% success rate, low exp and skill exp
                            {
                                Item fish = World.ItemConfiguration[96].Copy(); // TODO fix hard coding
                                if (CurrentLocation.SetItem(fish)) ItemDropped(this, fish);
                                experienceStored += Dice.Roll(2, 5);
                                skills[SkillType.Fishing].Experience++;
                                Notify(CommandMessageType.NotifySkillEnd, 1);
                                skillInUse = SkillType.None;
                            }
                        }
                        else // no water, cant fish here
                        {
                            Notify(CommandMessageType.NotifySkillEnd, 0);
                            skillInUse = SkillType.None;
                        }

                    if (targetFish != null) // rare fish/bubbles at target
                    {
                        skillLevel = Math.Max(skillLevel - targetFish.Difficulty, 1);

                        int result = Dice.Roll(1, 100);
                        if (skillLevel > result)
                        {
                            targetFishChance = Math.Min(targetFishChance + changeValue, 99);
                            Notify(CommandMessageType.NotifyFishingChance, targetFishChance);
                        }
                        else
                        {
                            targetFishChance = Math.Max(targetFishChance - changeValue, 1);
                            Notify(CommandMessageType.NotifyFishingChance, targetFishChance);
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the incoming attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded or not.</returns>
        public bool EvadeMelee(int attackerHitChance)
        {
            int evadeChance = (dexterity + DexterityBonus) * 2;
            evadeChance += inventory.PhysicalResistance;
            if (magicEffects.ContainsKey(MagicType.Protect))
                switch (magicEffects[MagicType.Protect].Magic.Effect1)
                {
                    case 3: evadeChance += 40; break; // defence shield
                    case 4: evadeChance += 100; break; // greater defence shield
                }

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else return true;
        }

        public bool AbsorbMelee(ref int damage, EquipType hitLocation, int armourDamage, bool stripAttempt, int pierceValue)
        {
            if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false;
            if (IsAdmin || isGod) return false; //Can't be hurt if god/admin
            if (Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            int absorption = 0;

            //Piercing does double armor damage per hit
            if (pierceValue > 0) armourDamage *= 2;

            //base absorption from VIT ( 1% per 10 stat)
            absorption += VitalityAbsorption;

            // bonus PA such as rings and gems etc.
            if (inventory.PhysicalAbsorptionGeneral > 0)
            {
                absorption += inventory.PhysicalAbsorptionGeneral;
            }

            switch (hitLocation) // handle item location physical absoprtion
            {
                case EquipType.Body:
                    if (BodyArmour != null)
                    {
                        absorption += inventory.TotalPhysicalAbsorptionBody;
                        TakeItemDamage(armourDamage, BodyArmour, (int)EquipType.Body, stripAttempt);
                    }
                    break;
                case EquipType.Legs:
                    if (Leggings != null && Boots != null) // counts leggings and boots
                    {
                        absorption += inventory.TotalPhysicalAbsorptionLegs;
                        TakeItemDamage(armourDamage, Leggings, (int)EquipType.Legs, stripAttempt);
                        TakeItemDamage(armourDamage, Boots, (int)EquipType.Feet, stripAttempt);
                    }
                    else if (Leggings != null) // if no boots equipped then check only leggings
                    {
                        absorption += inventory.TotalPhysicalAbsorptionLegs;
                        TakeItemDamage(armourDamage, Leggings, (int)EquipType.Legs, stripAttempt);
                    }
                    else if (Boots != null) // if no leggings equipped then check only boots
                    {
                        absorption += inventory.TotalPhysicalAbsorptionLegs;
                        TakeItemDamage(armourDamage, Boots, (int)EquipType.Feet, stripAttempt);
                    }
                    break;
                case EquipType.Arms:
                    if (Hauberk != null)
                    {
                        absorption += inventory.TotalPhysicalAbsorptionArms;
                        TakeItemDamage(armourDamage, Hauberk, (int)EquipType.Arms, stripAttempt);
                    }
                    break;
                case EquipType.Head:
                    if (Helmet != null)
                    {
                        absorption += inventory.TotalPhysicalAbsorptionHead;
                        TakeItemDamage(armourDamage, Helmet, (int)EquipType.Head, stripAttempt);
                    }
                    break;
            }

            //Absorption can't be higher than Global max value;
            if (absorption > Globals.MaximumPhysicalAbsorption)
            {
                absorption = Globals.MaximumPhysicalAbsorption;
            }


            //Pierce through PA
            if (pierceValue > 0)
            {
                absorption -= pierceValue;
            }

            //Physical Absorption
            damage -= (int)(((double)damage / 100.0f) * (double)absorption);

            // additional absorption from shield (piercing not affecting) //TODO strip?
            if (Shield != null)
                if (Dice.Roll(1, 100) <= Skills[SkillType.Shield].Level)
                {
                    Skills[SkillType.Shield].Experience++;

                    if (inventory.TotalPhysicalAbsorptionShield >= Globals.MaximumPhysicalAbsorption)
                        damage = damage - (int)(((double)damage / 100.0f) * (double)Globals.MaximumPhysicalAbsorption);
                    else damage = damage - (int)(((double)damage / 100.0f) * (double)inventory.TotalPhysicalAbsorptionShield);

                    TakeItemDamage(1, Shield, (int)EquipType.LeftHand);
                }

            if (damage <= 0) return false;
            else return true;
        }

        public bool AbsorbMagic(MagicAttribute element, ref int damage, int pierceValue)
        {
            int absorption = 0;
            bool completeAbsorption = false;
            if (CurrentMap.IsBuilding || CurrentMap.IsSafeMap) return false;
            if (IsAdmin || isGod) return false; //Can't be hurt if god/admin
            if (Town == OwnerSide.Neutral || town == OwnerSide.Neutral) return false; // cant hurt travs & travs cant hurt you
            if (Cache.World.IsHeldenianBattlefield && map.IsHeldenianMap && Cache.World.Heldenian.IsStarted == false) return false; // safe mode for 5 minutes before start

            //Complete Absorption of elemental Damage
            if (inventory.CompleteMagicProtection && element != MagicAttribute.None) completeAbsorption = true;

            switch (element)
            {
                case MagicAttribute.Air: if (inventory.CompleteAirProtection) completeAbsorption = true; break;
                case MagicAttribute.Earth: if (inventory.CompleteEarthProtection) completeAbsorption = true; break;
                case MagicAttribute.Fire: if (inventory.CompleteFireProtection) completeAbsorption = true; break;
                case MagicAttribute.Water: if (inventory.CompleteWaterProtection) completeAbsorption = true; break;
                case MagicAttribute.Spirit: if (inventory.CompleteSpiritProtection) completeAbsorption = true; break;
                case MagicAttribute.Unholy: if (inventory.CompleteUnholyProtection) completeAbsorption = true; break;
                case MagicAttribute.Kinesis: if (inventory.CompleteKinesisProtection) completeAbsorption = true; break;
                case MagicAttribute.All:
                    if (inventory.CompleteEarthProtection || inventory.CompleteAirProtection || inventory.CompleteFireProtection || inventory.CompleteWaterProtection ||
inventory.CompleteUnholyProtection || inventory.CompleteSpiritProtection || inventory.CompleteKinesisProtection) completeAbsorption = true; break;
                default: break;
            }

            //Remove endurance from qualifying items - //TODO adjust to remove amount blocked?
            foreach (Item item in inventory.DepleteEnduranceMagicAbsorption)
            {
                TakeItemDamage(1, item, (int)item.EquipType);
            }

            //Return false if full absorption after item damage 
            if (completeAbsorption && pierceValue <= 0) { return false; }
            else if (completeAbsorption && pierceValue > 0) { absorption = Globals.MaximumMagicalAbsorption - pierceValue; } //TODO adjust value //TODO add amour damage/piercing armour damage to magic attacks
            else
            {
                if (element != MagicAttribute.None) //No MA reduction for Damage spells with no element
                {
                    //base absorption from VIT ( 1% per 10 stat)
                    absorption += VitalityAbsorption;

                    //absorption from equipment
                    absorption += inventory.MagicAbsorptionBonus;
                    //elemental absorption from equipment
                    switch (element)
                    {
                        case MagicAttribute.Air: absorption += inventory.AirAbsorptionBonus; break;
                        case MagicAttribute.Earth: absorption += inventory.EarthAbsorptionBonus; break;
                        case MagicAttribute.Fire: absorption += inventory.FireAbsorptionBonus; break;
                        case MagicAttribute.Water: absorption += inventory.WaterAbsorptionBonus; break;
                        case MagicAttribute.Spirit: absorption += inventory.SpiritAbsorptionBonus; break;
                        case MagicAttribute.Unholy: absorption += inventory.UnholyAbsorptionBonus; break;
                        case MagicAttribute.Kinesis: absorption += inventory.KinesisAbsorptionBonus; break;
                        case MagicAttribute.All:
                            absorption += inventory.AirAbsorptionBonus + inventory.EarthAbsorptionBonus + inventory.FireAbsorptionBonus + inventory.WaterAbsorptionBonus +
       inventory.SpiritAbsorptionBonus + inventory.UnholyAbsorptionBonus + inventory.KinesisAbsorptionBonus; break;
                    }
                }

                //Set Absorption can be higher than Global max value;
                if (absorption > Globals.MaximumMagicalAbsorption)
                {
                    absorption = Globals.MaximumMagicalAbsorption;
                }


                //Pierce through MA
                if (pierceValue > 0)
                {
                    absorption -= pierceValue;
                }

                // PFM
                if (MagicEffects.ContainsKey(MagicType.Protect) && MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                    damage /= 2;
            }

            // Magic absorption
            damage -= (int)(((double)damage / 100.0f) * (double)absorption);

            if (damage <= 0) return false;
            else return true;
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades a magic attack or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <param name="ignorePFM">Does this spell ignore PFM?</param>
        /// <returns>True or False to indicate that this Character has successfully evaded or not.</returns>
        public bool EvadeMagic(int attackerHitChance, bool ignorePFM)
        {
            if (attackerHitChance == -1) return true;
            if (inventory.CompleteMagicProtection) return true;
            int evadeChance = skills[SkillType.MagicResistance].Level;
            if (magic + MagicBonus > 50) evadeChance += (magic + MagicBonus) - 50;
            evadeChance += inventory.MagicResistanceBonus;

            if (magicEffects.ContainsKey(MagicType.Protect))
                if (magicEffects[MagicType.Protect].Magic.Effect1 == 5) return true; // AMP
                else if (MagicEffects[MagicType.Protect].Magic.Effect1 == 2 && !ignorePFM) return true; // PFM

            if (evadeChance < 1) evadeChance = 1;

            double temp1 = ((double)attackerHitChance / (double)evadeChance);
            double temp2 = ((double)(temp1 * 50.0f));
            int hitChance = (int)temp2;

            if (hitChance < Globals.MinimumHitChance) hitChance = Globals.MinimumHitChance;
            if (hitChance > Globals.MaximumHitChance) hitChance = Globals.MaximumHitChance;

            int result = Dice.Roll(1, 100);

            if (result <= hitChance) return false;
            else
            {
                skills[SkillType.MagicResistance].Experience++;
                return true;
            }
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the being frozen or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded being frozen or not.</returns>
        public bool EvadeIce()
        {
            if (inventory.CompleteWaterProtection || inventory.CompleteMagicProtection) return true;
            int evadeChance = inventory.WaterAbsorptionBonus * 2; // why x2 i dno.. maybe just set 50 and 100 in item configs? duh...
            if (evadeChance < 1) evadeChance = 1;
            if (evadeChance > Globals.MaximumFreezeProtection) evadeChance = Globals.MaximumFreezeProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        /// <summary>
        /// Rolls a dice to see if this Character successfully evades the being poisoned or not.
        /// </summary>
        /// <param name="attackerHitChance">Hit chance of the attacker.</param>
        /// <returns>True or False to indicate that this Character has successfully evaded being poisoned or not.</returns>
        public bool EvadePoison()
        {
            if (inventory.CompleteUnholyProtection || inventory.CompleteMagicProtection) return true;
            int evadeChance = skills[SkillType.PoisonResistance].Level + inventory.PoisonResistanceBonus;
            if (evadeChance < 10) evadeChance = 10;
            if (evadeChance > Globals.MaximumPoisonProtection) evadeChance = Globals.MaximumPoisonProtection;

            int result = Dice.Roll(1, 100);

            if (result <= evadeChance) return true;
            else return false;
        }

        public void DepleteItem(int itemIndex) { DepleteItem(itemIndex, false); }
        public void DepleteItem(int itemIndex, bool isUseItemResult)
        {
            if (inventory[itemIndex].IsEquipped) UnequipItem(itemIndex);

            Notify(CommandMessageType.NotifyItemDepleted, itemIndex, ((isUseItemResult) ? 1 : 0));

            inventory[itemIndex] = null;
            inventory.Update();
        }


        public void ChangeUpgradeItem(int itemIndex)
        {
            if (itemIndex >= 0 && itemIndex < Globals.MaximumTotalItems && inventory[itemIndex] != null)
            {
                if (itemIndex < Globals.MaximumEquipment)
                {
                    int originalItem = -1;
                    for (int i = Globals.MaximumEquipment; i < Globals.MaximumTotalItems; i++)
                    {
                        if (inventory[i] != null)
                        {
                            if (inventory[itemIndex].GuidMatch(inventory[i])) { originalItem = i; break; }
                        }
                    }
                    if (originalItem != -1 && inventory.ChangeUpgradeItem(originalItem)) { Notify(CommandMessageType.NotifyItemToUpgrade, originalItem); }
                }
                else if (inventory.ChangeUpgradeItem(itemIndex)) { Notify(CommandMessageType.NotifyItemToUpgrade, itemIndex); }
            }
        }

        public void UpgradeItem()
        {
            if (inventory.UpgradeItemCount != 1 || inventory.UpgradeItemIndex == -1) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.NoItem); return; }
            int itemIndex = inventory.UpgradeItemIndex;
            if (itemIndex < Globals.MaximumEquipment) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.EquippedItem); return; }
            Item item = inventory[itemIndex];
            if (item.UpgradeType == ItemUpgradeType.None || item.UpgradeType == ItemUpgradeType.Ingredient) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.NotUpgradable); return; }
            if (item.UpgradeType == ItemUpgradeType.Hero) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.NotImplemented); return; }

            int upgradeIndex = -1;
            Item upgrade = null;

            switch (item.UpgradeType)
            {
                case ItemUpgradeType.Merien:
                case ItemUpgradeType.Xelima:
                    {
                        if (inventory.UpgradeIngredientCount != 1 || inventory.UpgradeIngredientIndex == -1) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.NoIngredient); return; }
                        upgradeIndex = inventory.UpgradeIngredientIndex;
                        upgrade = inventory[upgradeIndex];
                        if (upgrade.UpgradeType != ItemUpgradeType.Ingredient) { Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.NotAnUpgradeIngredient); return; }
                        break;
                    }
                default: break;
            }


            if (item.Level >= Globals.MaximumItemLevel)
            {
                Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.MaxLevel);
                return;
            }

            if (item.IsBroken)
            {
                Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.ItemBroken);
                return;
            }

            switch (item.UpgradeType)
            {
                case ItemUpgradeType.Xelima:
                    if (upgrade.EffectType != ItemEffectType.Xelima)
                    {
                        Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.Incompatible);
                        return;
                    }

                    break;
                case ItemUpgradeType.Merien:
                    if (upgrade.EffectType != ItemEffectType.Merien)
                    {
                        Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.Incompatible);
                        return;
                    }
                    break;
                case ItemUpgradeType.Majestic:
                    if (majestics < item.MajesticUpgradeCost())
                    {
                        Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.ToFewMajestics);
                        return;
                    }
                    break;
                case ItemUpgradeType.Additive: break;
                default: return;
            }

            if (item.UpgradeType == ItemUpgradeType.Merien || item.UpgradeType == ItemUpgradeType.Xelima) DepleteItem(upgradeIndex);
            int chance = (int)item.UpgradeChance();
            if (Dice.Roll(1, 100) > chance)
            {
                // break the item
                TakeItemDamage(item.Endurance, item, itemIndex);
                inventory[itemIndex].IsUpgradeable = false; //notify for this? also figure out what this does
                Notify(CommandMessageType.NotifyItemUpgradeFailed, (int)UpgradeFailReason.ChanceFailed);
                return;
            }
            else
            {
                inventory[itemIndex].Level++;
                if (item.UpgradeType == ItemUpgradeType.Majestic) { majestics -= item.MajesticUpgradeCost(); Notify(CommandMessageType.NotifyMajestics, majestics); }
                Notify(CommandMessageType.NotifyItemUpgrade, itemIndex, item.Level);
                if (item.IsEquipped)
                {
                    switch (item.EquipType)
                    {
                        case EquipType.DualHand: inventory[(int)EquipType.RightHand] = inventory[itemIndex]; break;
                        case EquipType.FullBody: inventory[(int)EquipType.Body] = inventory[itemIndex]; break;
                        default: inventory[(int)item.EquipType] = inventory[itemIndex]; break;
                    }
                }
                inventory.Update();

                // highest valued item
                if (item.GetGoldValue(true) > titles[TitleType.HighestValueItem])
                {
                    titles[TitleType.HighestValueItem] = item.GetGoldValue(true);
                    Notify(CommandMessageType.NotifyTitle, (int)TitleType.HighestValueItem, titles[TitleType.HighestValueItem]);
                }
            }
            //inventory.ClearUpgradeItems();
        }

        /*public void UpgradeItem(int itemIndex)
        {
            if (inventory[itemIndex] == null) return;

            Item item = inventory[itemIndex];
            int requiredMajesticPoints = 0, maximumLevel;

            switch (item.EffectType)
            {
                case ItemEffectType.Defence:
                case ItemEffectType.DefenceAntiMine:
                case ItemEffectType.DefenceActivation:
                    maximumLevel = 7;
                    break;
                case ItemEffectType.Attack:
                case ItemEffectType.AttackBow:
                case ItemEffectType.AttackDefence:
                case ItemEffectType.AttackManaSave:
                case ItemEffectType.AttackActivation:
                    maximumLevel = 15;
                    break;
                case ItemEffectType.AddEffect: maximumLevel = 10; break;
                default: Notify(CommandMessageType.NotifyItemUpgradeFailed, 2); return;
            }

            if (item.Level >= maximumLevel) { Notify(CommandMessageType.NotifyItemUpgradeFailed, 1); return; }

            // TODO - determine this based on the upgradeability of an item (set in configs maybe?)
            ItemUpgradeType type = ItemUpgradeType.Majestic;

            switch (type)
            {
                case ItemUpgradeType.Majestic:
                    if (!item.IsAngel && !item.IsUpgradeable)
                        Notify(CommandMessageType.NotifyItemUpgradeFailed, 2);
                    else if (majestics > 0)
                    {
                        if (item.IsAngel)
                            switch (item.Level)
                            {
                                case 0: requiredMajesticPoints = 10; break;
                                case 1: requiredMajesticPoints = 11; break;
                                case 2: requiredMajesticPoints = 13; break;
                                case 3: requiredMajesticPoints = 16; break;
                                case 4: requiredMajesticPoints = 20; break;
                                case 5: requiredMajesticPoints = 25; break;
                                case 6: requiredMajesticPoints = 31; break;
                                case 7: requiredMajesticPoints = 38; break;
                                case 8: requiredMajesticPoints = 46; break;
                                case 9: requiredMajesticPoints = 55; break;
                            }
                        else if (item.IsUpgradeable)
                            switch (item.EffectType)
                            {
                                case ItemEffectType.Defence:
                                case ItemEffectType.DefenceAntiMine:
                                case ItemEffectType.DefenceActivation:
                                case ItemEffectType.Attack:
                                case ItemEffectType.AttackBow:
                                case ItemEffectType.AttackDefence:
                                case ItemEffectType.AttackManaSave:
                                case ItemEffectType.AttackActivation:
                                    requiredMajesticPoints = (item.Level * (item.Level + 6) / 8) + 2;
                                    break;
                            }

                        if (majestics - requiredMajesticPoints >= 0)
                        {
                            majestics -= requiredMajesticPoints;
                            Notify(CommandMessageType.NotifyMajestics, majestics);

                            int newItemLevel;
                            if (item.IsAngel) newItemLevel = item.Level + 1; // angels get +1 per upgrade
                            else newItemLevel = Math.Min(item.Level + 2, maximumLevel); // other items are +2 per upgrade until maximum level

                            // TODO - fix this hard coded bullshit - upgrade to new item. possible UpgradeTransformLevel="" and UpgradeTransformItem=""
                            /*switch (item.Id)
                            {
                                case "DarkKnightFlameberge":
                                    if (item.Level == 0)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["DarkKnightGiantSword"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkKnightGiantSword":
                                    if (item.Level >= 6)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["BlackKnightTemple"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkMageMagicStaff":
                                    if (item.Level >= 4)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["DarkMageMagicWand"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "DarkMageMagicWand":
                                    if (item.Level >= 6)
                                    {
                                        inventory[itemIndex] = World.ItemConfiguration["BlackMageTemple"].Copy();
                                        item = inventory[itemIndex]; // points to new item
                                        item.BoundID = this.databaseID;
                                        item.BoundName = this.name;
                                    }
                                    break;
                                case "BlackMageTemple":
                                case "BlackKnightTemple": 
                                    if (item.Level >= maximumLevel) item.Colour = 9; // gives it the glow
                                    break; 
                            }

                            // TODO
                            //item.Attribute = ((item.Attribute & 0x0FFFFFFF) | ((uint)(newItemLevel) << 28));
                            Notify(CommandMessageType.NotifyItemUpgrade, itemIndex);
                        }
                        else Notify(CommandMessageType.NotifyItemUpgradeFailed, 3);
                    }
                    else Notify(CommandMessageType.NotifyItemUpgradeFailed, 3); // 1 = item maxed, 2 = not possible, 3 = not enough majestic points
                    break;
                case ItemUpgradeType.Merien:
                case ItemUpgradeType.Xelima:
                    Notify(CommandMessageType.NotifyItemUpgradeFailed, 2);
                    // TODO stone upgrade
                    break;
                default: Notify(CommandMessageType.NotifyItemUpgradeFailed, 2); break;
            }
        }*/

        public byte[] GetStatsData()
        {
            byte[] data = new byte[183];

            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(mp), 0, data, 4, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(sp), 0, data, 8, 4);
            if (playType == PlayType.PvE)
                Buffer.BlockCopy(BitConverter.GetBytes(gold), 0, data, 12, 4);
            else if (playType == PlayType.PvP)
                Buffer.BlockCopy(BitConverter.GetBytes(gladiatorPoints), 0, data, 12, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(criticals), 0, data, 16, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(majestics), 0, data, 18, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(level), 0, data, 20, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(strength), 0, data, 24, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(intelligence), 0, data, 28, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(vitality), 0, data, 32, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(dexterity), 0, data, 36, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(magic), 0, data, 40, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(agility), 0, data, 44, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(AvailableLevelUpPoints), 0, data, 48, 4);
            data[52] = (byte)(int)SideStatus;
            data[53] = (byte)rebirthLevel;
            data[54] = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(experience), 0, data, 55, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(enemyKills), 0, data, 59, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(criminalCount), 0, data, 63, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(rewardGold), 0, data, 67, 4);
            Buffer.BlockCopy(Town.ToString().ToLower().GetBytes(10), 0, data, 71, 10);
            Buffer.BlockCopy(guild.Name.GetBytes(20), 0, data, 81, 20);
            Buffer.BlockCopy(BitConverter.GetBytes(guildRank), 0, data, 101, 4);
            data[105] = (byte)0; // fightzone number
            Buffer.BlockCopy(GetMinMeleeDamage(false, false).GetBytes(), 0, data, 106, 2);
            Buffer.BlockCopy(GetMaxMeleeDamage(false, false).GetBytes(), 0, data, 108, 2);
            Buffer.BlockCopy(GetMinMeleeDamage(true, false).GetBytes(), 0, data, 110, 2);
            Buffer.BlockCopy(GetMaxMeleeDamage(true, false).GetBytes(), 0, data, 112, 2);
            Buffer.BlockCopy(titles[TitleType.DistanceTravelled].GetBytes(), 0, data, 114, 4);
            Buffer.BlockCopy(titles[TitleType.ItemsCollected].GetBytes(), 0, data, 118, 4);
            Buffer.BlockCopy(titles[TitleType.RareItemsCollected].GetBytes(), 0, data, 122, 4);
            Buffer.BlockCopy(titles[TitleType.MonstersKilled].GetBytes(), 0, data, 126, 4);
            Buffer.BlockCopy(titles[TitleType.PlayersKilled].GetBytes(), 0, data, 130, 4);
            Buffer.BlockCopy(titles[TitleType.MonsterDeaths].GetBytes(), 0, data, 134, 4);
            Buffer.BlockCopy(titles[TitleType.PlayerDeaths].GetBytes(), 0, data, 138, 4);
            Buffer.BlockCopy(titles[TitleType.MonsterDamageDealt].GetBytes(), 0, data, 142, 4);
            Buffer.BlockCopy(titles[TitleType.PlayerDamageDealt].GetBytes(), 0, data, 146, 4);
            Buffer.BlockCopy(titles[TitleType.QuestsCompleted].GetBytes(), 0, data, 150, 4);
            Buffer.BlockCopy(titles[TitleType.HighestMeleeDamage].GetBytes(), 0, data, 154, 4);
            Buffer.BlockCopy(titles[TitleType.HighestMagicDamage].GetBytes(), 0, data, 158, 4);
            Buffer.BlockCopy(titles[TitleType.HighestValueItem].GetBytes(), 0, data, 162, 4);
            Buffer.BlockCopy(titles[TitleType.PublicEventsGold].GetBytes(), 0, data, 166, 4);
            Buffer.BlockCopy(titles[TitleType.PublicEventsSilver].GetBytes(), 0, data, 170, 4);
            Buffer.BlockCopy(titles[TitleType.PublicEventsBronze].GetBytes(), 0, data, 174, 4);

            return data;
        }

        public void GetMapStatusData(int mode, string mapName)
        {
            int size = 0, structureCount = 0;
            switch (mode)
            {
                case 1:
                    if (Guild != Guild.None) Notify(CommandMessageType.NotifyCrusadeTeleportLocation);
                    break;
                case 3:
                    byte[] buffer = new byte[1024];
                    Buffer.BlockCopy(mapName.GetBytes(10), 0, buffer, size, 10);
                    size += 10;
                    Buffer.BlockCopy(0.GetBytes(), 0, buffer, size, 2);
                    size += 2;
                    buffer[size] = 0;
                    size++;
                    foreach (KeyValuePair<OwnerSide, List<WorldEventStructure>> structureList in Cache.World.Crusade.Structures)
                        foreach (WorldEventStructure structure in structureList.Value)
                        {
                            buffer[size] = (byte)World.NpcConfiguration[structure.NpcName].Type;
                            size++;
                            Buffer.BlockCopy(structure.Location.X.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(structure.Location.Y.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            buffer[size] = (byte)((int)structureList.Key);
                            size++;
                            structureCount++;
                        }

                    foreach (Guild guild in Cache.World.Guilds.Values)
                        foreach (Npc structure in guild.CrusadeStructures)
                        {
                            buffer[size] = (byte)structure.Type;
                            size++;
                            Buffer.BlockCopy(structure.X.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            Buffer.BlockCopy(structure.Y.GetBytes(), 0, buffer, size, 2);
                            size += 2;
                            buffer[size] = (byte)((int)structure.Side);
                            size++;
                            structureCount++;
                        }

                    buffer[12] = (byte)structureCount;

                    byte[] data = new byte[size];
                    Buffer.BlockCopy(buffer, 0, data, 0, size);

                    Notify(CommandMessageType.NotifyMapStatusNext, data);
                    break;
            }
        }

        public byte[] GetItemsData()
        {
            int size = 0, inventoryCount = InventoryCount + EquipmentCount, warehouseCount = WarehouseCount;

            byte[] data = new byte[2 + (inventoryCount * 47) + (warehouseCount * 47) + Globals.MaximumSpells + Globals.MaximumSkills];

            data[size] = (byte)inventoryCount;
            size++;

            if (inventoryCount > 0)
                for (int i = 0; i < Globals.MaximumTotalItems; i++)
                {
                    Item item = inventory[i];

                    if (item != null)
                    {
                        byte[] itemData = item.GetData(i);
                        Buffer.BlockCopy(itemData, 0, data, size, itemData.Length);
                        size += itemData.Length;
                    }
                }

            data[size] = (byte)warehouseCount;
            size++;

            if (warehouseCount > 0)
                for (int i = 0; i < Globals.MaximumWarehouseTabItems; i++)
                {
                    Item item = warehouse[i];

                    if (item != null)
                    {
                        byte[] itemData = item.GetData(i);
                        Buffer.BlockCopy(itemData, 0, data, size, itemData.Length);
                        size += itemData.Length;
                    }
                }

            // MAGIC LEARNED
            for (int i = 0; i < Globals.MaximumSpells; i++)
                data[size + i] = (byte)Convert.ToInt32(spellStatus[i]);
            size += Globals.MaximumSpells;

            // SKILL LEVELS
            for (int i = 0; i < Globals.MaximumSkills; i++)
                if (Enum.IsDefined(typeof(SkillType), i) && skills.ContainsKey((SkillType)i)) // check this skill exists
                    data[size + i] = (byte)skills[(SkillType)i].Level;
                else data[size + i] = 0;
            size += Globals.MaximumSkills;

            return data;
        }

        public byte[] GetInitData()
        {
            //partialData = false;
            //byte[] mapData = map.GetMapData(this, mapX - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterX], mapY - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterY], out partialData);

            //byte[] data = new byte[mapData.Length + 90];
            byte[] data = new byte[90];

            Buffer.BlockCopy(BitConverter.GetBytes((short)id), 0, data, 0, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX), 0, data, 2, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY), 0, data, 4, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)type), 0, data, 6, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 8, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 10, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 12, 2);
            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 14, 2);
            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 16, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 20, 4);
            Buffer.BlockCopy(loadedMapName.GetBytes(10), 0, data, 24, 10);
            Buffer.BlockCopy(Cache.World.Maps[loadedMapName].FriendlyName.GetBytes(30), 0, data, 34, 30);
            Buffer.BlockCopy(map.MusicName.GetBytes(10), 0, data, 64, 10);
            data[74] = (byte)((int)map.TimeOfDay); //day/night
            data[75] = (byte)((int)map.Weather); //weather
            Buffer.BlockCopy(BitConverter.GetBytes(contribution), 0, data, 76, 4);
            data[80] = (byte)hunger;
            Buffer.BlockCopy(BitConverter.GetBytes(reputation), 0, data, 81, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 85, 4);
            data[89] = (byte)(int)playType;
            //Buffer.BlockCopy(mapData, 0, data, 90, mapData.Length);
            return data;
        }

        public byte[] GetInitMapData(int startX, int startY, out bool partialData, out int cellX, out int cellY)
        {
            byte[] mapData = map.GetMapData(this, mapX - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterX], mapY - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterY], startX, startY, out partialData, out cellX, out cellY);

            return mapData;
        }

        public byte[] GetMotionData(MotionType type, CommandMessageType messageType) { return GetMotionData(type, messageType, this.mapX, this.mapY, this.direction); }
        public byte[] GetMotionData(MotionType type, CommandMessageType messageType, int sourceX, int sourceY, MotionDirection direction)
        {
            byte[] data = new byte[1];
            byte[] mapData;
            switch (messageType)
            {
                case CommandMessageType.ObjectConfirmMotion: break;
                case CommandMessageType.ObjectConfirmAttack: break;
                case CommandMessageType.ObjectConfirmMove:
                    switch (type)
                    {
                        case MotionType.Move:
                        case MotionType.Run:
                            mapData = map.GetMapDataPan(this, sourceX - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterX], sourceY - Globals.Resolutions[(int)resolution, (int)ResolutionSetting.CenterY], direction);

                            data = new byte[mapData.Length + 12];

                            Buffer.BlockCopy(BitConverter.GetBytes((short)(sourceX - 10)), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)(sourceY - 7)), 0, data, 2, 2);
                            data[4] = (byte)((int)direction);
                            if (type == MotionType.Run && !magicEffects.ContainsKey(MagicType.UnlimitedSP))
                                data[5] = (byte)1; // stamina change
                            else data[5] = (byte)0;
                            data[6] = (byte)0; // m_iOccupyStatus not sure what to do with this yet
                            Buffer.BlockCopy(BitConverter.GetBytes(hp), 0, data, 7, 4);
                            Buffer.BlockCopy(mapData, 0, data, 11, mapData.Length);
                            break;
                        case MotionType.Idle: break;
                    }
                    break;
                case CommandMessageType.ObjectRejectMove:
                    switch (type)
                    {
                        case MotionType.Move:
                        case MotionType.Run:
                            data = new byte[35];

                            Buffer.BlockCopy(BitConverter.GetBytes((ushort)id), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX), 0, data, 2, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY), 0, data, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)this.type), 0, data, 6, 2);
                            data[8] = (byte)((int)direction);
                            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 19, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 21, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 23, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 25, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 27, 4);
                            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 31, 4);
                            break;
                        case MotionType.Idle:
                            data = new byte[36];

                            Buffer.BlockCopy(BitConverter.GetBytes((ushort)id), 0, data, 0, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapX), 0, data, 2, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)mapY), 0, data, 4, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)this.type), 0, data, 6, 2);
                            data[8] = (byte)((int)direction);
                            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance1), 0, data, 19, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance2), 0, data, 21, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance3), 0, data, 23, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes((short)appearance4), 0, data, 25, 2);
                            Buffer.BlockCopy(BitConverter.GetBytes(appearanceColour), 0, data, 27, 4);
                            Buffer.BlockCopy(BitConverter.GetBytes(status), 0, data, 31, 4);
                            data[35] = (isDead) ? (byte)0 : (byte)1; // dead or not?
                            break;
                    }
                    break;
            }

            return data;
        }

        public byte[] GetFullObjectData(Character requester)
        {
            byte[] data = new byte[46];

            Buffer.BlockCopy(id.GetBytes(), 0, data, 0, 2);
            Buffer.BlockCopy(mapX.GetBytes(), 0, data, 2, 2);
            Buffer.BlockCopy(mapY.GetBytes(), 0, data, 4, 2);
            Buffer.BlockCopy(type.GetBytes(), 0, data, 6, 2);
            data[8] = (byte)((int)direction);
            Buffer.BlockCopy(name.GetBytes(10), 0, data, 9, 10);
            /*Buffer.BlockCopy(appearance1.GetBytes(), 0, data, 19, 2);
            Buffer.BlockCopy(appearance2.GetBytes(), 0, data, 21, 2);
            Buffer.BlockCopy(appearance3.GetBytes(), 0, data, 23, 2);
            Buffer.BlockCopy(appearance4.GetBytes(), 0, data, 25, 2);
            Buffer.BlockCopy(appearanceColour.GetBytes(), 0, data, 27, 4);*/

            int status = ((0x0FFFFFFF & this.status) | (requester.GetPlayerRelationship(this) << 28));
            Buffer.BlockCopy(status.GetBytes(), 0, data, 31, 4);
            if (isDead)
                data[35] = 1;
            else data[35] = 0;
            data[36] = (byte)(int)town;
            data[37] = (byte)(int)SideStatus;

            // todo
            /*for (int i = 0; i < 7; i++)
            {
                data[36 + i] = (byte)(conditions.ContainsKey((ConditionGroup)i) ? conditions[(ConditionGroup)i].Magic.Effect1 : 0);
            }*/

            return data;
        }

        public void Notify(CommandMessageType type) { Notify(type, 0, 0, 0, ""); }
        public void Notify(CommandMessageType type, string stringValue) { Notify(type, 0, 0, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1) { Notify(type, value1, 0, 0, ""); }
        public void Notify(CommandMessageType type, int value1, string stringValue) { Notify(type, value1, 0, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2) { Notify(type, value1, value2, 0, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3) { Notify(type, value1, value2, value3, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, string stringValue) { Notify(type, value1, value2, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, string stringValue) { Notify(type, value1, value2, value3, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, int value4) { Notify(type, value1, value2, value3, value4, ""); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, int value4, string stringValue) { Notify(type, value1, value2, value3, value4, 0, stringValue); }
        public void Notify(CommandMessageType type, int value1, int value2, int value3, int value4, int value5, string stringValue = "")
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyCriminalCaptured:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 10, 10);
                    Buffer.BlockCopy(rewardGold.GetBytes(), 0, command, 20, 4);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 24, 4);
                    break;
                case CommandMessageType.NotifyParty:
                    command = new byte[150];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;

                    switch ((PartyAction)value1)
                    {
                        case PartyAction.Create: break;
                        case PartyAction.Reject:
                            command[7] = (byte)value2;
                            break;
                        case PartyAction.Join:
                            if (party != null)
                            {
                                command[7] = (byte)(int)party.Mode;
                                Buffer.BlockCopy(party.LeaderId.GetBytes(), 0, command, 8, 2);
                                command[10] = (byte)party.MemberCount;
                                for (int i = 0; i < party.MemberCount; i++)
                                    if (party.Members[i] != -1)
                                    {
                                        Buffer.BlockCopy(party.Members[i].GetBytes(), 0, command, (11 + (i * 35)), 2);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].Name.GetBytes(10), 0, command, (13 + (i * 35)), 10);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].X.GetBytes(), 0, command, (23 + (i * 35)), 2);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].Y.GetBytes(), 0, command, (25 + (i * 35)), 2);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].CurrentMap.Name.GetBytes(10), 0, command, (27 + (i * 35)), 10);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].HP.GetBytes(), 0, command, (37 + (i * 35)), 2);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].MP.GetBytes(), 0, command, (39 + (i * 35)), 2);
                                        //Buffer.BlockCopy(Cache.World.Players[party.Members[i]].SP.GetBytes(), 0, command, (41 + (i * 35)), 2);
                                        command[41 + (i + 35)] = (byte)(Cache.World.Players[party.Members[i]].MagicEffects.ContainsKey(MagicType.Poison) ? 1 : 0);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].MaxHP.GetBytes(), 0, command, (43 + (i * 35)), 2);
                                        Buffer.BlockCopy(Cache.World.Players[party.Members[i]].MaxMP.GetBytes(), 0, command, (45 + (i * 35)), 2);
                                    }
                            }
                            break;
                        case PartyAction.ChangeMode:
                            if (party != null)
                            {
                                command[7] = (byte)(int)party.Mode;
                            }
                            break;
                        case PartyAction.Update:
                            if (party != null)
                            {
                                command[7] = (byte)value2; // action
                                Buffer.BlockCopy(value3.GetBytes(), 0, command, 8, 2);
                                switch ((PartyAction)value2)
                                {
                                    case PartyAction.Join:
                                        Buffer.BlockCopy(Cache.World.Players[value3].name.GetBytes(10), 0, command, 10, 10);
                                        Buffer.BlockCopy(Cache.World.Players[value3].X.GetBytes(), 0, command, 20, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].Y.GetBytes(), 0, command, 22, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].CurrentMap.Name.GetBytes(10), 0, command, 24, 10);
                                        Buffer.BlockCopy(Cache.World.Players[value3].HP.GetBytes(), 0, command, 34, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MP.GetBytes(), 0, command, 36, 2);
                                        //Buffer.BlockCopy(Cache.World.Players[value3].SP.GetBytes(), 0, command, 38, 2);
                                        command[38] = (byte)(Cache.World.Players[value3].MagicEffects.ContainsKey(MagicType.Poison) ? 1 : 0);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MaxHP.GetBytes(), 0, command, 40, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MaxMP.GetBytes(), 0, command, 42, 2);
                                        break;
                                    case PartyAction.Leave: break;
                                    case PartyAction.Info:
                                        Buffer.BlockCopy(Cache.World.Players[value3].X.GetBytes(), 0, command, 10, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].Y.GetBytes(), 0, command, 12, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].CurrentMap.Name.GetBytes(10), 0, command, 14, 10);
                                        Buffer.BlockCopy(Cache.World.Players[value3].HP.GetBytes(), 0, command, 24, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MP.GetBytes(), 0, command, 26, 2);
                                        //Buffer.BlockCopy(Cache.World.Players[value3].SP.GetBytes(), 0, command, 28, 2);
                                        command[28] = (byte)(Cache.World.Players[value3].MagicEffects.ContainsKey(MagicType.Poison) ? 1 : 0);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MaxHP.GetBytes(), 0, command, 30, 2);
                                        Buffer.BlockCopy(Cache.World.Players[value3].MaxMP.GetBytes(), 0, command, 32, 2);
                                        break;
                                }
                            }
                            break;
                        case PartyAction.Request:
                            Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 8, 10);
                            break;
                    }
                    break;
                case CommandMessageType.NotifyItemBagPositions:
                    command = new byte[10 + (4 * Globals.MaximumTotalItems)];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    for (int i = 0; i < Globals.MaximumTotalItems; i++) //TODO Check this?
                        if (inventory[i] != null)
                        {
                            Buffer.BlockCopy(inventory[i].BagPosition.X.GetBytes(), 0, command, 6 + (i * 4), 2);
                            Buffer.BlockCopy(inventory[i].BagPosition.Y.GetBytes(), 0, command, 6 + (i * 4) + 2, 2);
                        }
                        else
                        {
                            Buffer.BlockCopy((40).GetBytes(), 0, command, 6 + (i * 4), 2);
                            Buffer.BlockCopy((30).GetBytes(), 0, command, 6 + (i * 4) + 2, 2);
                        }
                    break;
                case CommandMessageType.NotifyCrusadeTeleportLocation:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.X.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.Y.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(Guild.CrusadeTeleportLocation.MapName.GetBytes(10), 0, command, 10, 10);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.X.GetBytes(), 0, command, 20, 2);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.Y.GetBytes(), 0, command, 22, 2);
                    Buffer.BlockCopy(Guild.CrusadeBuildLocation.MapName.GetBytes(10), 0, command, 24, 10);
                    break;
                case CommandMessageType.NotifyDead:
                case CommandMessageType.NotifyPlayerNotOnline:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 6, 20);
                    break;
                case CommandMessageType.JoinGuildApprove:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 6, 20);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 26, 2);
                    break;
                case CommandMessageType.NotifyGuildAdmissionRequest:
                case CommandMessageType.NotifyGuildDismissalRequest:
                case CommandMessageType.NotifyWhisperOn:
                case CommandMessageType.NotifyWhisperOff:
                    command = new byte[25];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 6, 10);
                    break;
                case CommandMessageType.NotifyReputationSuccess:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 7, 10);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 17, 4);
                    break;
                case CommandMessageType.NotifyGuildDisbanded:
                    command = new byte[45];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(loadedGuildName.GetBytes(20), 0, command, 7, 20);
                    Buffer.BlockCopy(stringValue.GetBytes(10), 0, command, 27, 10);
                    break;
                case CommandMessageType.NotifyStudySpellSuccess:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    break;
                case CommandMessageType.NotifyStudySpellFailed:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    break;
                case CommandMessageType.NotifySpellUnlearned:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    break;
                case CommandMessageType.NotifyStudySkillSuccess:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    command[7] = (byte)value2;
                    break;
                case CommandMessageType.NotifySkill:
                case CommandMessageType.NotifySpecialAbilityStatus:
                case CommandMessageType.NotifyCrusadeStatistics:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 10, 2);
                    break;
                case CommandMessageType.NotifyDamageStatistics:
                case CommandMessageType.NotifyHeldenianStatistics:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 10, 2);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 12, 2);
                    break;
                case CommandMessageType.NotifyFishingStarted:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 12, 20);
                    break;
                case CommandMessageType.NotifyCriticals:
                case CommandMessageType.NotifyFishingChance:
                case CommandMessageType.NotifySkillEnd:
                case CommandMessageType.NotifyCrusadeStrikeIncoming:
                case CommandMessageType.NotifyHeldenianVictory:
                case CommandMessageType.NotifyReputationFailed:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(((short)value1).GetBytes(), 0, command, 6, 2);
                    break;
                case CommandMessageType.NotifyGold:
                case CommandMessageType.NotifyEnemyKills:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    break;
                case CommandMessageType.NotifyCriminalCount:
                    command = new byte[45];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(strength.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(vitality.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(dexterity.GetBytes(), 0, command, 18, 4);
                    Buffer.BlockCopy(intelligence.GetBytes(), 0, command, 22, 4);
                    Buffer.BlockCopy(magic.GetBytes(), 0, command, 26, 4);
                    Buffer.BlockCopy(agility.GetBytes(), 0, command, 30, 4);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 34, 4);
                    break;
                case CommandMessageType.NotifySpellsAndSkills:
                case CommandMessageType.NotifyStatChangeMajesticsSuccess:
                    command = new byte[180];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    for (int spell = 6; spell < 6 + Globals.MaximumSpells; spell++)
                        command[spell] = (byte)Convert.ToInt32(spellStatus[spell - 6]);
                    for (int skill = 6 + Globals.MaximumSpells; skill < 6 + Globals.MaximumSpells + Globals.MaximumSkills; skill++)
                        if (Enum.IsDefined(typeof(SkillType), skill - 6 - Globals.MaximumSpells) && skills.ContainsKey((SkillType)skill - 6 - Globals.MaximumSpells)) // check this skill exists
                            command[skill] = (byte)skills[(SkillType)skill - 6 - Globals.MaximumSpells].Level;
                        else command[skill] = 0;
                    break;
                case CommandMessageType.NotifyExperience:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(experience), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(reputation), 0, command, 10, 4);
                    break;
                case CommandMessageType.NotifyStatChangeLevelUpSuccess:
                case CommandMessageType.NotifyLevelUp:
                    command = new byte[40];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(level.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(AvailableLevelUpPoints.GetBytes(), 0, command, 10, 4);
                    /*Buffer.BlockCopy(BitConverter.GetBytes(strength), 0, command, 10, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(vitality), 0, command, 14, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(dexterity), 0, command, 18, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(intelligence), 0, command, 22, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(magic), 0, command, 26, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(charisma), 0, command, 30, 4);*/
                    break;
                case CommandMessageType.NotifyHP:
                case CommandMessageType.NotifyMP:
                case CommandMessageType.NotifySP:
                case CommandMessageType.NotifyHunger:
                case CommandMessageType.NotifyItemSold:
                case CommandMessageType.NotifyItemRepaired:
                case CommandMessageType.NotifyItemEndurance:
                case CommandMessageType.NotifyTitle:
                case CommandMessageType.NotifyItemUpgrade:
                case CommandMessageType.NotifyItemUpgradeFailed:
                case CommandMessageType.NotifyItemToWarehouse:
                case CommandMessageType.NotifyItemFromWarehouse:
                case CommandMessageType.NotifyItemSet:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 10, 4);
                    break;
                case CommandMessageType.NotifyItemSetUsed:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 10, 4);
                    break;
                case CommandMessageType.NotifyCrusade:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value1), 0, command, 6, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes((int)crusadeDuty), 0, command, 10, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 14, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes(value3), 0, command, 18, 4);
                    break;
                case CommandMessageType.NotifyTimeChange:
                case CommandMessageType.NotifyWeatherChange:
                case CommandMessageType.NotifySafeMode:
                case CommandMessageType.NotifyNotEnoughGold: // command[6] is item index
                case CommandMessageType.NotifyItemToUpgrade:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1;
                    break;
                case CommandMessageType.NotifyItemEquipped:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    break;
                case CommandMessageType.NotifyItemUnequipped:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 10, 2);
                    break;
                case CommandMessageType.NotifyDropItemAndErase:
                case CommandMessageType.NotifyItemDepleted:
                case CommandMessageType.NotifyItemBroken:
                case CommandMessageType.NotifyItemCount:
                case CommandMessageType.NotifyMagicEffectOn: //TODOPARA
                case CommandMessageType.NotifyMagicEffectOff:
                case CommandMessageType.NotifyManufactureSuccess:
                case CommandMessageType.NotifyItemColourChange:
                    command = new byte[20];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 8, 4);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value3), 0, command, 12, 2);
                    break;
                case CommandMessageType.NotifyGiveItemAndErase:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes(value2), 0, command, 8, 4);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 12, 20);
                    break;
                case CommandMessageType.NotifyGuildName:
                case CommandMessageType.NotifySellItemFailed:
                    command = new byte[35];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value2), 0, command, 8, 2);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 10, 20);
                    break;
                case CommandMessageType.NotifyAngelStats:
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 18, 4);
                    break;
                case CommandMessageType.NotifySellItemQuote:
                case CommandMessageType.NotifyRepairItemQuote:
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 18, 4);
                    Buffer.BlockCopy(stringValue.GetBytes(20), 0, command, 22, 20);
                    break;
                case CommandMessageType.NotifyPublicEventCreated:
                case CommandMessageType.NotifyPublicEventEnded:
                case CommandMessageType.NotifyPublicEventUpdate:
                case CommandMessageType.NotifyPublicEventStarted:
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(value1.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(value3.GetBytes(), 0, command, 14, 4);
                    Buffer.BlockCopy(value4.GetBytes(), 0, command, 18, 4);
                    Buffer.BlockCopy(value5.GetBytes(), 0, command, 22, 4);
                    int value6 = 0;
                    if (Int32.TryParse(stringValue, out value6))
                        Buffer.BlockCopy(value6.GetBytes(), 0, command, 26, 4);
                    break;
                case CommandMessageType.NotifyMajestics:
                    command = new byte[15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(BitConverter.GetBytes((short)value1), 0, command, 6, 2);
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 8, 4);
                    break;
                case CommandMessageType.NotifyFly:
                    command = new byte[16];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    command[6] = (byte)value1; //Direction
                    Buffer.BlockCopy(value2.GetBytes(), 0, command, 7, 4); //Damage
                    command[11] = (byte)value3; //DamageType
                    command[12] = (byte)value4; //Hitcount
                    break;
                case CommandMessageType.NotifyStatChangeMajesticsFailed:
                case CommandMessageType.NotifyStatChangeLevelUpFailed:
                case CommandMessageType.NotifyInventoryFull:
                case CommandMessageType.NotifyWarehouseFull:
                case CommandMessageType.NotifySpecialAbilityEnabled:
                case CommandMessageType.NotifyFarmingSkillTooLow:
                case CommandMessageType.NotifyFarmingInvalidLocation:
                case CommandMessageType.NotifyFishingSuccess:
                case CommandMessageType.NotifyFishingFailed:
                case CommandMessageType.NotifyCrusadeMeteorStrike:
                case CommandMessageType.NotifyCrusadeStructureLimitReached:
                case CommandMessageType.NotifyHeldenian:
                case CommandMessageType.NotifyHeldenianStarted:
                case CommandMessageType.NotifyHeldenianEnded:
                case CommandMessageType.NotifyHeldenianFlagFailed:
                case CommandMessageType.NotifyApocalypseStart:
                case CommandMessageType.NotifyApocalypseEnd:
                case CommandMessageType.NotifyApocalypseGateClosed:
                case CommandMessageType.NotifyManufactureFailed:
                case CommandMessageType.NotifyTravellerLimit:
                case CommandMessageType.NotifyTickHP:
                case CommandMessageType.NotifyTickMP:
                case CommandMessageType.NotifyTickSP:
                case CommandMessageType.NotifyTickEXP:
                case CommandMessageType.NotifyTickStopHP:
                case CommandMessageType.NotifyTickStopMP:
                case CommandMessageType.NotifyTickStopSP:
                case CommandMessageType.NotifyTickStopEXP:
                default:
                    command = new byte[10];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }

        public void Notify(CommandMessageType type, object dataObject)
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyEnemyKillReward:
                    Character victim = (Character)dataObject;
                    command = new byte[50];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(experience.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(enemyKills.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(victim.Name.GetBytes(10), 0, command, 14, 10);
                    Buffer.BlockCopy(victim.Guild.Name.GetBytes(20), 0, command, 24, 20);
                    Buffer.BlockCopy(victim.GuildRank.GetBytes(), 0, command, 44, 2);
                    Buffer.BlockCopy(crusadeWarContribution.GetBytes(), 0, command, 46, 2);
                    break;
                case CommandMessageType.NotifyApocalypseGateOpen:
                    Location location = (Location)dataObject;
                    command = new byte[30];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(location.X.GetBytes(), 0, command, 6, 4);
                    Buffer.BlockCopy(location.Y.GetBytes(), 0, command, 10, 4);
                    Buffer.BlockCopy(location.MapName.GetBytes(10), 0, command, 14, 10);
                    break;
                case CommandMessageType.NotifyCrusadeMeteorStrikeResult:
                    CrusadeStrikeResult result = (CrusadeStrikeResult)dataObject;
                    command = new byte[30 + (result.StrikePointsHP.Length * 2)];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(result.StructuresDestroyed.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(result.StructuresDamaged.GetBytes(), 0, command, 8, 2);
                    Buffer.BlockCopy(result.Casualties.GetBytes(), 0, command, 10, 2);
                    Buffer.BlockCopy(result.MapName.GetBytes(10), 0, command, 12, 10);
                    Buffer.BlockCopy(result.RemainingStructures.GetBytes(), 0, command, 22, 2);

                    if (result.StrikePointsHP.Length > 0)
                    {
                        Buffer.BlockCopy(result.StrikePointsHP.Length.GetBytes(), 0, command, 24, 2);
                        for (int i = 0; i < result.StrikePointsHP.Length; i++)
                            Buffer.BlockCopy(result.StrikePointsHP[i].GetBytes(), 0, command, 26 + (i * 2), 2);
                    }
                    else Buffer.BlockCopy(0.GetBytes(), 0, command, 24, 2);

                    break;
                default:
                    command = new byte[10];
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }

        public void Notify(CommandMessageType type, byte[] sendData, int count = 1)
        {
            byte[] command;
            switch (type)
            {
                case CommandMessageType.NotifyItemObtained:
                    command = new byte[sendData.Length + 15 + 2];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(count.GetBytes(), 0, command, 6, 2);
                    Buffer.BlockCopy(sendData, 0, command, 8, sendData.Length);
                    break;
                case CommandMessageType.NotifyMapStatusNext:
                case CommandMessageType.NotifyMapStatusLast:
                    command = new byte[sendData.Length + 15];
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(CommandType.Notify), 0, command, 0, 4);
                    Buffer.BlockCopy(Utility.GetCommandTypeBytes(type), 0, command, 4, 2);
                    Buffer.BlockCopy(sendData, 0, command, 6, sendData.Length);
                    break;
                default:
                    command = new byte[10];
                    break;
            }

            byte[] data = Utility.Encrypt(command, false);
            if (clientInfo != null) clientInfo.Send(data);
        }


        private void OnSkillLevelUp(Skill skill)
        {
            // handles stat limiters
            //switch (skill.Type)
            //{
            //    case SkillType.Archery:
            //    case SkillType.Axe:
            //    case SkillType.Fencing:
            //    case SkillType.Hammer:
            //    case SkillType.LongSword:
            //    case SkillType.Shield:
            //    case SkillType.ShortSword:
            //        if (dexterity * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //    case SkillType.Staff:
            //    case SkillType.Magic:
            //        if (magic * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //    case SkillType.Mining:
            //    case SkillType.Manufacturing:
            //    case SkillType.Hand:
            //        if (strength * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //    case SkillType.MagicResistance:
            //        if (level * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //    case SkillType.PoisonResistance:
            //        if (vitality * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //    case SkillType.Alchemy:
            //    case SkillType.PretendCorpse:
            //        if (intelligence * 2 < skill.Level)
            //        {
            //            skill.Experience = Globals.SkillExperienceTable[skill.Level] - 1;
            //            return;
            //        }
            //        break;
            //}

            Notify(CommandMessageType.NotifySkill, (int)skill.Type, skill.Level);
        }

        public int GetMinMeleeDamage(bool isCritical, bool isDash)
        {
            int damage = 0;

            if (Weapon != null)
                damage = (Weapon.DamageSmall.Die * 1) + Weapon.DamageSmall.Bonus; // * 1 is there for a bug?
            else damage = 1;

            if (Weapon != null && Weapon.RelatedSkill == SkillType.BattleStaff)
                damage += CalculateBattleStaffDamage(damage, isCritical, isDash); //TODO leaves out some bonuses
            else damage += CalculateMeleeDamage(damage, isCritical, isDash);

            return damage;
        }

        public int GetMaxMeleeDamage(bool isCritical, bool isDash)
        {
            int damage = 0;

            if (Weapon != null)
                damage = (Weapon.DamageSmall.Die * Weapon.DamageSmall.Faces) + Weapon.DamageSmall.Bonus;
            else damage = (strength + StrengthBonus) / 12;

            if (Weapon != null && Weapon.RelatedSkill == SkillType.BattleStaff)
                damage += CalculateBattleStaffDamage(damage, isCritical, isDash); //TODO leaves out some bonuses
            else damage += CalculateMeleeDamage(damage, isCritical, isDash);

            return damage;
        }

        private int CalculateMagicAttackDamage(int baseDamage, int targetReputation = 0, OwnerType ownerType = OwnerType.None, int currentHitId = 0)
        {
            if ((Weapon != null && Weapon.RelatedSkill != SkillType.BattleStaff && Weapon.RelatedSkill != SkillType.Staff)) return 0;

            int damage = baseDamage;
            damage = damage <= 0 ? 1 : damage;

            damage += (int)((float)damage * DamageMultiplier(false, DamageType.Magic, false)); //Can't Crit nor Dash with Magic Attack //TODO fix Critical attack
            damage += AdditionalDamage(false, DamageType.Magic, targetReputation, ownerType, currentHitId, true); //Can't Critical attack with Magic //TODO fix that

            return damage;
        }

        public int CalculateEnvironmentalDamage(int baseDamage, int targetReputation = 0, OwnerType ownerType = OwnerType.None, int currentHitId = 0)
        {
            int damage = baseDamage;
            damage = damage <= 0 ? 1 : damage;

            damage += (int)((float)damage * DamageMultiplier(false, DamageType.Magic, false)); //Can't Crit nor Dash with Magic Attack //TODO fix Critical attack //TODO fix to use enviroment?
            damage += AdditionalDamage(false, DamageType.Magic, targetReputation, ownerType, currentHitId, true); //Can't Critical attack with Magic //TODO fix that

            return damage;
        }

        private int CalculateBattleStaffDamage(int baseDamage, bool isCritical, bool isDash, int targetReputation = 0, OwnerType ownerType = OwnerType.None, int currentHitId = 0)
        {
            if (Weapon == null) return 0;
            if (Weapon.RelatedSkill != SkillType.BattleStaff) return 0;
            MagicAttribute attribute;
            if (!Enum.TryParse<MagicAttribute>(Weapon.Effect4.ToString(), out attribute)) return 0; // must have a magic attribute at effect4

            int damage = baseDamage;
            damage = damage <= 0 ? 1 : damage;

            damage += (int)((float)damage * DamageMultiplier(isCritical, DamageType.Magic, isDash));
            damage += AdditionalDamage(isCritical, DamageType.Magic, targetReputation, ownerType, currentHitId);

            return damage;
        }

        private int CalculateRangedDamage(int baseDamage, bool isCritical, int targetReputation = 0, OwnerType ownerType = OwnerType.None, int currentHitId = 0)
        {
            if (Weapon == null) return 0;
            if (Weapon.RelatedSkill != SkillType.Archery) return 0;
            if (Arrows == null) return 0;

            int damage = baseDamage;
            damage = damage <= 0 ? 1 : damage;

            damage += (int)((float)damage * DamageMultiplier(isCritical, DamageType.Ranged, false)); //Can't dash with Archery
            damage += AdditionalDamage(isCritical, DamageType.Ranged, targetReputation, ownerType, currentHitId);

            // bows do 3 hits with criticals. use this to distribute damage without bein overpowered
            if (isCritical) damage /= 3;

            return damage;
        }

        private int CalculateMeleeDamage(int baseDamage, bool isCritical, bool isDash, int targetReputation = 0, OwnerType ownerType = OwnerType.None, int currentHitId = 0)
        {
            int damage = baseDamage;
            if (Weapon != null)
            {
                if (Weapon.RelatedSkill == SkillType.Archery || Weapon.RelatedSkill == SkillType.BattleStaff) return 0;
            }
            damage = damage <= 0 ? 1 : damage;

            damage += (int)((float)damage * DamageMultiplier(isCritical, DamageType.Melee, isDash));
            damage += AdditionalDamage(isCritical, DamageType.Melee, targetReputation, ownerType, currentHitId);

            return damage;
        }

        private int CalculateMeleeHitProbability(bool isCritical, bool isDash, bool isBackHit)
        {
            int hitChance = 0;

            //Weapon Skill
            if (Weapon != null)
                hitChance = skills[Weapon.RelatedSkill].Level;
            else hitChance = skills[SkillType.Hand].Level;

            //HP from dex, Min 50, else +1 for each dex;
            if (dexterity + DexterityBonus > 50) hitChance += (dexterity + DexterityBonus);
            else { hitChance += 50; }

            // critical bonus
            if (isCritical) hitChance += 100;

            // dash bonus
            if (isDash) hitChance += 20;

            // Item Bonuses
            hitChance += inventory.HitChanceBonus;

            // weather hit chance penalty for bows
            if (Weapon != null && Weapon.RelatedSkill == SkillType.Archery)
            {
                switch (CurrentMap.Weather)
                {
                    case WeatherType.Rain: hitChance -= (hitChance / 20); break;
                    case WeatherType.RainMedium: hitChance -= (hitChance / 10); break;
                    case WeatherType.RainHeavy: hitChance -= (hitChance / 4); break;
                }
            }

            // old code uses target's DR/2 but we dont have scope in EvadeMelee
            // so, this must be the last hitChance modifier to emulate old code
            // alternatively, we could pass in a bool such as isBackHit
            if (isBackHit) hitChance *= 2;

            return hitChance;
        }

        private float DamageMultiplier(bool isCritical, DamageType type, bool isDash)
        {
            float damageMultiplier = 1.0f;
            switch (type)
            {
                case DamageType.Melee:
                    {
                        if (Weapon != null)
                            switch (Weapon.RelatedSkill)
                            {
                                case SkillType.Axe:
                                case SkillType.Fencing:
                                case SkillType.Hammer:
                                case SkillType.LongSword:
                                case SkillType.ShortSword:
                                case SkillType.Staff: damageMultiplier += ((float)(strength + StrengthBonus) / 500.0f); break;// 20% bonus from strength              
                            }
                        else
                        {
                            damageMultiplier += ((float)(strength + StrengthBonus) / 800.0f); break; // around 8% bonus from strength - unarmed //TODO change this?
                        }
                        break;
                    }
                case DamageType.Magic:
                    {
                        // magic bonus
                        damageMultiplier += ((float)(magic + MagicBonus) / 300.0f); //33% fBonus from Magic
                        break;
                    }
                case DamageType.Ranged:
                    {
                        damageMultiplier += ((float)(Agility + AgilityBonus) / 800.0f);  // 8% bonus from Agility
                        break;
                    }
                default: break;
            }

            //Critical
            if (isCritical)
            {
                damageMultiplier += ((float)level / 100.0f);  //1% Bonus Mulitplier for each level
                // weapon type critical bonuses
                if (Weapon != null)
                {
                    switch (Weapon.RelatedSkill)
                    {
                        case SkillType.BattleStaff: damageMultiplier += 0.1f; break;
                        case SkillType.Fencing: damageMultiplier += 0.1f; break;
                        case SkillType.Archery: damageMultiplier += 0.1f; break;
                        case SkillType.ShortSword: damageMultiplier += 1.0f; break;
                        case SkillType.LongSword: damageMultiplier += 0.1f; break;
                        case SkillType.Axe: damageMultiplier += 0.2f; break;
                        case SkillType.Hammer: damageMultiplier += 0.2f; break;
                        case SkillType.Staff: damageMultiplier += 0.2f; break;
                        default: break;
                    }
                }
            }

            //Dash
            if (isDash)
            {
                switch (Weapon.RelatedSkill)
                {
                    case SkillType.BattleStaff: damageMultiplier += 0.1f; break;
                    case SkillType.Fencing: damageMultiplier += 0.1f; break;
                    case SkillType.Archery: break; //Can't Dash with Archery
                    case SkillType.ShortSword: damageMultiplier += 1.0f; break;
                    case SkillType.LongSword: damageMultiplier += 0.1f; break;
                    case SkillType.Axe: damageMultiplier += 0.2f; break;
                    case SkillType.Hammer: damageMultiplier += 0.2f; break;
                    case SkillType.Staff: damageMultiplier += 0.2f; break;
                    default: break;
                }
            }

            if (inventory.Berserk) { damageMultiplier += 0.3f; }  //Zerkwand 30%  
            if (IsBerserked) { damageMultiplier += 1.0f; }// berserk spell damage bonus
            if (CurrentMap != null && CurrentMap.IsFightZone) { damageMultiplier += 0.3f; }// 30% fightzone damage bonus
            if (CurrentMap != null && CurrentMap.IsHuntZone) { damageMultiplier += 0.2f; }// 20% damage bonuses for hunt maps (2ndmiddle, middleland, icebound by default)
            if (Cache.World.IsCrusade && crusadeDuty == CrusadeDuty.Fighter) // crusade bonus (100% at level 1-80, 70% at level 80-100, 30% thereafter)
            {
                if (level <= 80) { damageMultiplier += 1.0f; }
                else if (level <= 100) { damageMultiplier += 0.7f; }
                else { damageMultiplier += 0.3f; };
            }

            // TODO - heldenian damage

            return damageMultiplier;
        }

        private int AdditionalDamage(bool isCritical, DamageType type, int targetReputation, OwnerType ownerType, int currentHitId, bool comboArea = false)
        {
            int additionalDamage = 0;
            switch (type)
            {
                case DamageType.Magic: if (inventory.MagicDamageBonus > 0) { additionalDamage += inventory.MagicDamageBonus; } break;
                case DamageType.Melee: if (inventory.PhysicalDamageBonus > 0) { additionalDamage += inventory.PhysicalDamageBonus; } break;
                case DamageType.Ranged: if (inventory.PhysicalDamageBonus > 0) { additionalDamage += inventory.PhysicalDamageBonus; } break; //TODO add arrow specific damage increase items
                default: break;
            }

            if (isCritical && inventory.CriticalDamageBonus > 0) { additionalDamage += inventory.CriticalDamageBonus; }


            if (inventory.DemonSlayer && ownerType == Helbreath.OwnerType.Npc && name.Equals("Demon")) { additionalDamage += 6; } //+ 6 Damage to demons

            if (inventory.RighteousBonus)
            {
                //Force Values from 0 to 2x Global Max Reputation
                int playerRep = Reputation + Globals.MaximumReputation;
                int targetRep = targetReputation + Globals.MaximumReputation;

                //See which value is larger
                bool playerRepGreater = (playerRep >= targetRep) ? true : false;

                //Find the difference of the two values
                int difference = (playerRepGreater) ? playerRep - targetRep : targetRep - playerRep;

                if (playerRepGreater) //Do extra damage if player has higher rep
                {
                    additionalDamage += Math.Min(10, difference / 10); //+1 Damage for every 10 points, max of 10 bonus damage
                }
                else //Do less damage if target has greater rep
                {
                    additionalDamage -= Math.Min(10, difference / 10); //-1 Damage for every 10 points, max of 10
                }
            }

            if (inventory.Kloness)
            {
                //Force Values from 0 to 2x Global Max Reputation
                int playerRep = Reputation + Globals.MaximumReputation;
                int targetRep = targetReputation + Globals.MaximumReputation;

                //See which value is larger
                bool playerRepGreater = (playerRep >= targetRep) ? true : false;

                //Find the difference of the two values
                int difference = (playerRepGreater) ? playerRep - targetRep : targetRep - playerRep;

                if (playerRepGreater) //Do extra damage if player has higher rep
                {
                    additionalDamage += difference / 200; //+1 Damage for every 200 points, max of 100 bonus damage
                }
                else //Do less damage if target has greater rep
                {
                    additionalDamage -= difference / 200; //-1 Damage for every 200 points, max of 100
                }
            }

            if (inventory.Light && CurrentMap.TimeOfDay == TimeOfDay.Day) { additionalDamage += 7; }
            if (inventory.Dark && (CurrentMap.TimeOfDay == TimeOfDay.Night || CurrentMap.TimeOfDay == TimeOfDay.NightChristmas)) { additionalDamage += 7; }

            //Combo Damage
            if (comboDamageTargets.Count > 0 && comboDamageTargets.Keys.Contains(currentHitId))
            {
                ComboDamageCounter counter = comboDamageTargets[currentHitId];
                if (counter.ComboDamage < 20)
                {
                    //Casting Magic - Multi-hit Spells add once for each successful hit
                    if (comboArea) { counter.ComboDamage += 1; }
                    //Melee/Archery/BattleStaff
                    else
                    {
                        if (Weapon != null)
                        {
                            switch (Weapon.RelatedSkill)
                            {
                                case SkillType.ShortSword:
                                case SkillType.Fencing:
                                case SkillType.Archery:
                                case SkillType.Staff:
                                    { counter.ComboDamage += 1; break; }
                                case SkillType.LongSword:
                                case SkillType.BattleStaff:
                                    { counter.ComboDamage += 2; break; }
                                case SkillType.Axe:
                                case SkillType.Hammer:
                                    { counter.ComboDamage += 3; break; }
                                default: break;
                            }
                        }
                        else
                        {
                            //Hand attack
                            counter.ComboDamage += 4;
                        }
                    }
                }

                //Max combo damage
                if (counter.ComboDamage > 20) { counter.ComboDamage = 20; }
                additionalDamage += (inventory.ComboDamageBonus > 0) ? counter.ComboDamage + inventory.ComboDamageBonus : counter.ComboDamage;
            }

            return additionalDamage;
        }

        public int Id { get; set; }
        public int ObjectId { get { return id; } }
        public String DatabaseID { get { return databaseID; } }
        public PlayType PlayType { get { return playType; } }
        public String Name { get { return name; } }
        public String Profile { get { return profile; } }
        public int Reputation { get { return reputation; } set { reputation = value; } }
        public int Strength { get { return strength; } }
        public int Dexterity { get { return dexterity; } }
        public int Vitality { get { return vitality; } }
        public int Magic { get { return magic; } }
        public int Intelligence { get { return intelligence; } }
        public int Agility { get { return agility; } }
        public string LoadedGuildName { get { return loadedGuildName; } }
        public Guild Guild { get { return guild; } set { guild = value; } }
        public int GuildRank { get { return guildRank; } set { guildRank = value; } }
        public bool HasGuild { get { return (!guild.Equals(Guild.None)); } }
        public Party Party { get { return party; } set { party = value; } }
        public bool HasParty { get { return (party != null); } }
        public int PartyRequestID { get { return partyRequestID; } set { partyRequestID = value; } }
        public List<int> TradeList;
        public int TradePartnerID { get { return tradePartner; } set { tradePartner = value; } }
        public bool TradeAccepted;
        public GenderType Gender { get { return gender; } }
        public SkinType Skin { get { return skin; } }
        public int HairColour { get { return hairColour; } }
        public int HairStyle { get { return hairStyle; } }
        public int UnderwearColour { get { return underwearColour; } }
        public int Appearance1 { get { return appearance1; } }
        public int Appearance2 { get { return appearance2; } }
        public int Appearance3 { get { return appearance3; } }
        public int Appearance4 { get { return appearance4; } }
        public int AppearanceColour { get { return appearanceColour; } }
        public int Level { get { return level; } }
        public int RebirthLevel { get { return rebirthLevel; } }
        public int GladiatorPoints { get { return gladiatorPoints; } }
        public long Experience { get { return experience; } }
        public long ExperienceStored { get { return experienceStored; } set { experienceStored = value; } }
        public DateTime LastLogin { get { return lastLogin; } }
        public OwnerSide Town { get { return town; } }
        public OwnerSide Side { get { return town; } }
        public OwnerSideStatus SideStatus { get { return (IsCriminal ? OwnerSideStatus.Criminal : OwnerSideStatus.Combatant); } }
        public int Type { get { return type; } }
        public OwnerType OwnerType { get { return OwnerType.Player; } }
        public OwnerSize Size { get { return OwnerSize.Small; } }
        public int Status { get { return status; } }
        public Dictionary<MagicType, MagicEffect> MagicEffects { get { return magicEffects; } }
        public Dictionary<ConditionGroup, MagicEffect> Conditions { get { return conditions; } }
        public Boolean IsBerserked { get { return magicEffects.ContainsKey(MagicType.Berserk); } }
        public Boolean IsInvisible { get { return magicEffects.ContainsKey(MagicType.Invisibility); } }
        public Boolean IsSafeMode { get { return isSafeMode; } }
        public Boolean IsCivilian { get { return isCivilian; } }
        public Boolean IsCombatMode { get { return isCombatMode; } }
        public Boolean IsDead { get { return isDead; } }
        public Boolean IsRemoved { get { return isRemoved; } }
        public bool IsCorpseExploited { get { return isCorpseExploited; } set { isCorpseExploited = value; } }
        public Boolean IsAdmin { get { return (adminLevel > 0); } }
        public int TotalLogins { get { return totalLogins; } set { totalLogins = value; } }
        public bool CanFly { get { return canFly; } set { canFly = value; } }
        public bool ShowDamageEnabled { get { return showDamageEnabled; } set { showDamageEnabled = value; } }
        public int LastDamage { get { return lastDamage; } set { lastDamage = value; } }

        public DamageType LastDamageType { get { return lastDamageType; } set { lastDamageType = value; } }
        public DateTime LastDamageTime { get { return lastDamageTime; } set { lastDamageTime = value; } }
        public bool NotifyTickHP { get { return notifyTickHP; } set { notifyTickHP = value; } }
        public bool NotifyTickMP { get { return notifyTickMP; } set { notifyTickMP = value; } }
        public bool NotifyTickSP { get { return notifyTickSP; } set { notifyTickSP = value; } }

        /// <summary>
        /// The ID associated with the player's TCP socket connection.
        /// </summary>
        public int ClientID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// ClientInfo object associated with the player's TCP socket connection.
        /// </summary>
        public ClientInfo ClientInfo
        {
            get { return clientInfo; }
            set { clientInfo = value; }
        }

        /// <summary>
        /// Gets the current direction in which this character is facing.
        /// </summary>
        public MotionDirection Direction
        {
            get { return direction; }
        }

        public MapTile CurrentLocation
        {
            get { return map[mapY][mapX]; }
        }

        public Resolution Resolution { set { resolution = value; } get { return resolution; } }
        public Map CurrentMap { set { map = value; } get { return map; } }

        public string LoadedMapName
        {
            get { return loadedMapName; }
        }

        /// <summary>
        /// Last X coordinate of this character's location.
        /// </summary>
        public int LastX
        {
            get { return prevMapX; }
        }

        /// <summary>
        /// Last Y coordinate of this character's location.
        /// </summary>
        public int LastY
        {
            get { return prevMapY; }
        }


        /// <summary>
        /// Current X coordinate of this character's location.
        /// </summary>
        public int X
        {
            get { return mapX; }
        }

        /// <summary>
        /// Current Y coordinate of this character's location.
        /// </summary>
        public int Y
        {
            get { return mapY; }
        }

        /// <summary>
        /// List of items in the character's warehouse.
        /// </summary>
        public List<Item> Warehouse
        {
            get { return warehouse; }
            set { warehouse = value; }
        }


        /// <summary>
        /// Gets the character's current hit points.
        /// </summary>
        public int HP
        {
            get { return hp; }
            set { hp = value; }
        }

        /// <summary>
        /// Calculates the maximum hit points this character can have.
        /// </summary>
        public int MaxHP
        {
            get
            {
                int value = ((vitality + VitalityBonus) * 3) + (level * 2) + ((strength + StrengthBonus) / 2);

                // hp increase bonus (v2)
                if (inventory.MaxHPBonus > 0)
                    value += (value / 100) * inventory.MaxHPBonus;

                // blood weapons reduce max hp by 20%
                if (inventory.Blood)
                    value -= (value / 5);

                return value;
            }
        }

        /// <summary>
        /// Gets the character's current magic points.
        /// </summary>
        public int MP
        {
            get { return mp; }
            set { mp = value; }
        }

        /// <summary>
        /// Calculates the maximum magic points this character can have.
        /// </summary>
        public int MaxMP
        {
            get
            {

                int value = ((magic + MagicBonus) * 2) + (level * 2) + ((intelligence + IntelligenceBonus) / 2);

                // mp increase bonus (v2)
                if (inventory.MaxMPBonus > 0)
                    value += (value / 100) * inventory.MaxMPBonus;

                return value;
            }
        }

        /// <summary>
        /// Gets the character's current stamina points.
        /// </summary>
        public int SP
        {
            get { return sp; }
            set { sp = value; }
        }

        /// <summary>
        /// Calculates the maximum stamina points this character can have.
        /// </summary>
        public int MaxSP
        {
            get
            {
                int value = ((strength + StrengthBonus) * 2) + (level * 2);

                // mp increase bonus (v2)
                if (inventory.MaxSPBonus > 0)
                    value += (int)(((double)value / (double)100) * (double)inventory.MaxSPBonus);

                return value;
            }
        }

        public int MaxLuck { get { return (luck + LuckBonus < Globals.MaximumLuck) ? luck + LuckBonus : Globals.MaximumLuck; } }

        public int Criticals { get { return criticals; } set { criticals = value; } }
        public int MaxCriticals { get { return (level / 10) + (inventory.MaxCritsBonus); } }
        public int Majestics { get { return majestics; } set { majestics = value; } }
        public List<int> Summons { get { return summons; } set { summons = value; } }
        public Dictionary<TitleType, int> Titles { get { return titles; } set { titles = value; } }
        public Dictionary<SkillType, Skill> Skills { get { return skills; } }
        public List<Quest> ActiveQuests { get { return activeQuests; } }
        public int CrusadeConstructionPoints { get { return crusadeConstructionPoints; } set { crusadeConstructionPoints = value; } }
        public int CrusadeWarContribution { get { return crusadeWarContribution; } set { crusadeWarContribution = value; } }
        public CrusadeDuty CrusadeDuty { get { return crusadeDuty; } set { crusadeDuty = value; } }

        /// <summary>
        /// Formats the spell status for saving in the database.
        /// </summary>
        public string SpellStatus
        {
            get
            {
                //Gives players all the magic spells -FOR TESTING ONLY
                //StringBuilder status = new StringBuilder();
                //for (int i = 0; i < Globals.MaximumSpells; i++)
                //{
                //    if (World.MagicConfiguration != null)
                //    {
                //        if (World.MagicConfiguration.ContainsKey(i))
                //        {
                //            status.Append("1");
                //        }
                //        else
                //        {
                //            status.Append("0");
                //        }
                //    }
                //}
                //return status.ToString();

                //Working code - DON"T DELETE
                StringBuilder status = new StringBuilder();
                for (int i = 0; i < Globals.MaximumSpells; i++) status.Append(Convert.ToInt32(spellStatus[i]));
                return status.ToString();
            }
        }

        /// <summary>
        /// Formats the skill status for saving in the database.
        /// </summary>
        public string SkillStatus
        {
            get
            {
                //gives players level 100 in all skills - FOR TESTING ONLY
                StringBuilder status = new StringBuilder();
                for (int i = 0; i < Globals.MaximumSkills; i++)
                    if (Enum.IsDefined(typeof(SkillType), i) && skills.ContainsKey((SkillType)i)) // check this skill exists
                        //status.Append(skills[(SkillType)i].Experience + " "); //DO NOT DELETE
                        status.Append("101 ");
                    else status.Append("0 ");

                return status.ToString();
            }
        }

        /// <summary>
        /// Gets the current hunger level of the character. Ranging from 0 to 100.
        /// </summary>
        public int Hunger { get { return hunger; } }

        /// <summary>
        /// Calculates the character's available level up points.
        /// </summary>
        public int AvailableLevelUpPoints { get { return (((level - 1) * 3) - ((strength + dexterity + intelligence + vitality + agility + magic) - 70)); } }

        /// <summary>
        /// Calculates the character's total allowed stat points.
        /// </summary>
        public int TotalStatPoints { get { return (((level - 1) * 3) + 70); } }
        public int WarehouseCount
        {
            get
            {
                int count = 0;
                foreach (Item item in warehouse)
                    if (item != null) count++;
                return count;
            }
        }

        public bool IsCriminal { get { return (criminalCount > 0); } }
        public int CriminalCount { set { criminalCount = value; } get { return criminalCount; } }
        public int EnemyKills { set { enemyKills = value; } get { return enemyKills; } }
        public TimeSpan SpecialAbilityTime { get { return specialAbilityTime; } }
        public bool SpecialAbilityEnabled { get { return specialAbilityEnabled; } }
        public TimeSpan ReputationTime { get { return reputationTime; } set { reputationTime = value; } }
        public TimeSpan MuteTime { get { return muteTime; } set { muteTime = value; } }
        public bool IsMuted { get { return muteTime.TotalSeconds > 0; } }
        public bool IsWhispering { get { return (whisperID != -1); } }
        public int WhisperIndex { get { return whisperID; } set { whisperID = value; } }
        public int VitalityAbsorption { get { return ((vitality + inventory.VitalityBonus) / 10); } }
        public int Gold { get { return gold; } } // EconomyType.Virtual

        /// <summary>
        /// Stun chance as a percentage
        /// </summary>
        public int StunChance
        {
            get
            {
                if (Weapon != null)
                    switch (Weapon.RelatedSkill)
                    {

                        case SkillType.Archery: return 65;
                        case SkillType.LongSword: return 90;
                        case SkillType.Fencing: return 71;
                        case SkillType.Axe: return 75;
                        case SkillType.Hammer:
                        case SkillType.Staff: return 80;
                        default: return 99;
                    }
                else return 99;
            }
        }
        public bool IsReady { get { return isReady; } set { isReady = value; } }

        //All Things Inventory related
        public Inventory Inventory { get { return inventory; } }

        /// <summary>
        /// Quick access to the Weapon currently equipped.
        /// </summary>
        public Item Weapon { get { return inventory.Weapon; } }

        /// <summary>
        /// Quick access to the Shield currently equipped.
        /// </summary>
        public Item Shield { get { return inventory.Shield; } }

        /// <summary>
        /// Quick access to the Necklace currently equipped.
        /// </summary>
        public Item Necklace { get { return inventory.Necklace; } }

        /// <summary>
        /// Quick access to the Helmet currently equipped.
        /// </summary>
        public Item Helmet { get { return inventory.Helmet; } }

        /// <summary>
        /// Quick access to the Body Armour currently equipped.
        /// </summary>
        public Item BodyArmour { get { return inventory.BodyArmour; } }

        /// <summary>
        /// Quick access to the Hauberk currently equipped.
        /// </summary>
        public Item Hauberk { get { return inventory.Hauberk; } }

        /// <summary>
        /// Quick access to the Leggings currently equipped.
        /// </summary>
        public Item Leggings { get { return inventory.Leggings; } }

        /// <summary>
        /// Quick access to the Boots currently equipped.
        /// </summary>
        public Item Boots { get { return inventory.Boots; } }

        /// <summary>
        /// Quick access to the Cape currently equipped.
        /// </summary>
        public Item Cape { get { return inventory.Cape; } }

        /// <summary>
        /// Quick access to the Costume currently equipped.
        /// </summary>
        public Item Costume { get { return inventory.Costume; } }

        /// <summary>
        /// Quick access to the Angel currently equipped
        /// </summary>
        public Item Angel { get { return inventory.Angel; } }

        /// <summary>
        /// Quick access to the RightRing currently equipped 
        /// </summary>
        public Item RightRing { get { return inventory.RightRing; } }

        /// <summary>
        /// Quick access to the LeftRing currently equipped (wasn't used)
        /// </summary>
        public Item LeftRing { get { return inventory.LeftRing; } }

        /// <summary>
        /// Quick access to Dual Handed Weapon
        /// </summary>
        public Item DualHandedWeapon { get { return inventory.DualHandedWeapon; } }

        //Item sets for extra Bonuses
        public List<ItemSet> SetBonuses { get { return inventory.SetBonuses; } }

        //TODO - check out what this does
        public int[] Equipment { get { return equipment; } }

        public List<ItemSpecialAbilityType> SpecialAbilitiesPassive { get { return inventory.SpecialAbilitiesPassive; } }
        public Dictionary<ItemSpecialAbilityType, int> SpecialAbilitiesActive { get { return inventory.SpecialAbilitiesActive; } }
        //public Item SpecialAbilityItem { get { return inventory[specialAbilityItemIndex]; } }
        public int InventoryCount { get { return inventory.InventoryCount; } }
        public int EquipmentCount { get { return inventory.EquipmentCount; } }
        public int GoldItemIndex { get { return inventory.GoldItemIndex; } }
        public Item GoldItem { get { return inventory.GoldItem; } }
        public bool HasArrows { get { return inventory.HasArrows; } }
        public Item Arrows { get { return inventory.Arrows; } }
        public int ArrowsIndex { get { return inventory.ArrowsIndex; } }
        public int StrengthBonus { get { return inventory.StrengthBonus; } }
        public int DexterityBonus { get { return inventory.DexterityBonus; } }
        public int VitalityBonus { get { return inventory.VitalityBonus; } }
        public int MagicBonus { get { return inventory.MagicBonus; } }
        public int IntelligenceBonus { get { return inventory.IntelligenceBonus; } }
        public int AgilityBonus { get { return inventory.AgilityBonus; } }
        public int LuckBonus { get { return inventory.LuckBonus; } }

        public Dictionary<int, DateTime> DamagedByObjects { get { return damagedByObjects; } set { damagedByObjects = value; } }
        public Dictionary<int, DateTime> SupportedByObjects { get { return supportedByObjects; } set { supportedByObjects = value; } }
        public Dictionary<int, ComboDamageCounter> ComboDamageTargets { get { return comboDamageTargets; } set { comboDamageTargets = value; } }
    }
}