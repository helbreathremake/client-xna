﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects
{
    public interface IDeadOwner
    {
        OwnerType OwnerType { get; }
        OwnerSide Side { get; }
        bool IsCorpseExploited { get; set; }
    }
}
