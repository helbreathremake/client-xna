﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    class Spikes : IDynamicObject
    {
        public event DynamicObjectHandler SpikeFieldRemoved;

        private IOwner owner;

        private int id;
        private int x, y;
        private Item item;
        private int difficulty;
        private DynamicObjectType type;
        private int spike;

        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private Dice dice;

        public Spikes(int id)
        {
            this.id = id;
        }

        public Spikes(int id, IOwner owner, Map map, int mapX, int mapY, int spike, TimeSpan lastTime, DynamicObjectHandler SpikeFieldRemoved, Dice dice)
        {
            this.id = id;
            this.owner = owner;
            this.map = map;
            this.mapX = mapX;
            this.mapY = mapY;
            this.spike = spike;
            this.lastTime = lastTime;
            this.SpikeFieldRemoved += SpikeFieldRemoved;
            this.dice = dice;
            creationTime = DateTime.Now;
            map[mapY][mapX].SetDynamicObject(this);
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.SpikeField; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public Dice Dice { get { return dice; } set { dice = value; } }
        public IOwner Owner { get { return owner; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);

            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (SpikeFieldRemoved != null) SpikeFieldRemoved(this);
        }


        public void DoDamage(IOwner target)
        {
            //if (!target.EvadeMagic(500, false)) // static. maybe make changable in configs?
            //Should evadeMagic happen before any damage?
            //TODo Add hit location legs? use pa to absorb damage/create bleeding effect?
            //Do Damage
            bool trySpike = false;
            int damage = Dice.Roll();

            //Owner of spikes is a player
            if (owner.OwnerType == OwnerType.Player)
            {
                Character spikeOwner = (Character)owner;
                trySpike = spikeOwner.EnvironmentalAttack(target, dice);
            }
            else //Owner of spikes is null or npc
            {
                target.TakeDamage(owner, DamageType.Environment, damage, MotionDirection.None, true);
                trySpike = true;
            }

            //TODO add effect ? spike


        }
    }
}
