﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    class PoisonCloud : IDynamicObject
    {
        public event DynamicObjectHandler PoisonCloudRemoved;

        private IOwner owner;

        private int id;
        private int mapX;
        private int mapY;
        private Map map;

        private DateTime creationTime;
        private TimeSpan lastTime;
        private int poisonDamage;
        private Dice dice;

        public PoisonCloud(int id)
        {
            this.id = id;
        }

        public PoisonCloud(int id, IOwner owner, Map map, int mapX, int mapY, int poisonDamage, TimeSpan lastTime, DynamicObjectHandler poisonCloudRemoved, Dice dice)
        {
            this.id = id;
            this.owner = owner;
            this.map = map;
            this.mapX = mapX;
            this.mapY = mapY;
            this.poisonDamage = poisonDamage;
            this.lastTime = lastTime;
            this.PoisonCloudRemoved += poisonCloudRemoved;
            this.dice = dice;
            creationTime = DateTime.Now;
            map[mapY][mapX].SetDynamicObject(this);
        }

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.PoisonCloudBegin; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public TimeSpan LastTime { get { return lastTime; } set { lastTime = value; } }
        public int PoisonDamage { get { return poisonDamage; } set { poisonDamage = value; } }
        public Dice Dice { get { return dice; } set { dice = value; } }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
            targetMap[targetY][targetX].SetDynamicObject(this);

            creationTime = DateTime.Now;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - creationTime;
            if (ts.TotalSeconds > lastTime.TotalSeconds) Remove();

            if (CurrentLocation.IsOccupied)
            {
                //Set up
                bool tryPoison = false;
                IOwner target = CurrentLocation.Owner;
                int damage = dice.Roll();

                // do some environmental damage
                //Owner is a player
                if (owner.OwnerType == OwnerType.Player)
                {
                    Character poisonCloudOwner = (Character)owner;
                    tryPoison = poisonCloudOwner.EnvironmentalAttack(target, dice);
                }
                else //Owner is null or npc
                {
                    target.TakeDamage(owner, DamageType.Environment, damage, MotionDirection.None, true);
                    tryPoison = true;
                }

                // then try to apply poison
                //if (!target.EvadeMagic(500, false)) // static. maybe make changable in configs?
                if (tryPoison && !target.EvadePoison())
                    target.SetMagicEffect(new Magic(MagicType.Poison, 30, poisonDamage));
            }
        }

        public void Remove()
        {
            map[mapY][mapX].ClearDynamicObject();
            if (PoisonCloudRemoved != null) PoisonCloudRemoved(this);
        }
    }
}
