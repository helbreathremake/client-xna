﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
    public class Portal : IDynamicObject
    {
        private int id;
        private Map map;
        private int mapX;
        private int mapY;
        private Location location;
        private Location targetLocation;
        private PortalType type;

        public Portal(Location location, Location targetLocation)
        {
            this.id = -1;
            this.location = location;
            this.targetLocation = targetLocation;
            // TODO - portals were never dynamic objects. in new client, they should be (use Portal(int) method)
            // this will mean players dont have to be notified of x,y and mapname, they would just see it when it appears
        }

        public Portal(int id)
        {
            this.id = id;
        }

        public void Init(Map targetMap, int targetX, int targetY)
        {
            this.map = targetMap;
            this.mapX = targetX;
            this.mapY = targetY;
        }

        public void TimerProcess()
        {
            return;
        }

        public void Remove()
        {
            throw new NotImplementedException();
        }

        public bool IsWithinRange(IOwner other, int range)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - range) &&
                (mapX <= other.X + range) &&
                (mapY >= other.Y - range) &&
                (mapY <= other.Y + range))
                return true;
            else return false;
        }

        public bool IsWithinRange(IDynamicObject other, int range)
        {
            if ((map == CurrentMap) &&
                (mapX >= other.X - range) &&
                (mapX <= other.X + range) &&
                (mapY >= other.Y - range) &&
                (mapY <= other.Y + range))
                return true;
            else return false;
        }

        #region IDynamicObject Members

        public int ID { get { return id; } }
        public DynamicObjectType Type { get { return DynamicObjectType.Portal; } }
        public Map CurrentMap { get { return map; } }
        public MapTile CurrentLocation { get { return map[mapY][mapX]; } }
        public int X { get { return mapX; } }
        public int Y { get { return mapY; } }
        public bool IsTraversable { get { return true; } }
        public PortalType PortalType { get { return type; } set { type = value; } }
        public Location PortalLocation { get { return location; } }
        public Location PortalTargetLocation { get { return targetLocation; } }

        #endregion
    }
}
