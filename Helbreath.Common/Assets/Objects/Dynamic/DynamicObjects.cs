﻿/* DynamicObjets.cs
 * Contains all objects that inherit IDynamicObject interface. These objects work different to IOwners
 * as they can be walkable or non-walkable, cannot move once placed, have timer processes that trigger
 * different effects and expires the object, and can be interactive with certain skills.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets.Objects.Dynamic
{
}
