﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Common.Assets
{
    public class ComboDamageCounter
    {
        private int comboDamage; // Bonus Combo Damage given
        private int damageCounter; //Counts total damage done over multiple hits
        private int hitCounter; //Counts total amount of hits taken
        private DateTime startTime;

        public ComboDamageCounter(int comboDamage, DateTime startTime, int damageCounter)
        {
            this.damageCounter = damageCounter;
            this.comboDamage = comboDamage;
            this.startTime = startTime;
            hitCounter = 1; //Created when first hit
            comboDamage = 0; //Zero bonus damage when created
        }

        public int ComboDamage
        {
            get { return comboDamage; }
            set { comboDamage = value; }
        }

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public int DamageCounter
        {
            get { return damageCounter; }
            set { damageCounter = value; }
        }

        public int HitCounter
        {
            get { return hitCounter; }
            set { hitCounter = value; }
        }
    }
}
