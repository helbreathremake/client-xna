﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;

namespace Helbreath.Common.Events
{
    public class Defence : IPublicEvent
    {
        public event PublicEventHandler EventEnded;

        private PublicEventState state;
        private Map map;
        private string name;
        private string description;
        private PublicEventDifficulty difficulty;
        private TimeSpan duration;
        private int wave;
        private PublicEventRating rating;
        private DateTime startTime;
        private DateTime endTime;

        private Location location;
        private Location targetLocation;
        private IOwner target;

        private List<int> npcIds;
        private List<int> participantIds;

        private int spawnRate; // number of seconds between spawns
        private int spawnCount; // number of mobs to spawn per iteration
        private DateTime lastSpawn; // last spawn time
        private int maximumEnemies; // maximum enemies allowed to spawn
        private int spawnedEnemies; // current number of enemies
        private Zone spawnZone; // zone in which the enemies can spawn

        public Defence(Map map)
        {
            this.map = map;

            name = "Defence";
            description = "Defend the Grand Master Generator";
            duration = new TimeSpan(0, 10, 0); // lasts 10 minutes

            npcIds = new List<int>();
            participantIds = new List<int>();

            SetDifficulty(PublicEventDifficulty.Random);
        }

        public void SetDifficulty(PublicEventDifficulty difficulty)
        {
            if (difficulty == PublicEventDifficulty.Random)
            {
                int roll = Dice.Roll(1, 5);
                this.difficulty = (PublicEventDifficulty)roll;
            }
            else this.difficulty = difficulty;

            switch (this.difficulty)
            {
                case PublicEventDifficulty.Easy:
                    maximumEnemies = 5;
                    spawnRate = 10;
                    spawnCount = 5;
                    break;
                case PublicEventDifficulty.Normal:
                    maximumEnemies = 8;
                    spawnRate = 6;
                    spawnCount = 4;
                    break;
                case PublicEventDifficulty.Hard:
                    maximumEnemies = 12;
                    spawnRate = 6;
                    spawnCount = 4;
                    break;
                case PublicEventDifficulty.Heroic:
                    maximumEnemies = 15;
                    spawnRate = 4;
                    spawnCount = 3;
                    break;
                case PublicEventDifficulty.Godly:
                    maximumEnemies = 15;
                    spawnRate = 4;
                    spawnCount = 3;
                    break;
            }
        }

        public void Create()
        {
            state = PublicEventState.Created;
            startTime = DateTime.Now.AddMinutes(3); // 3 minute pre-start

            // 1. find a location and zone the enemy spawn
            Zone zone = new Zone(map.Name, 15, 15, map.Height - 15, map.Width - 15);
            location = zone.GetRandomLocation();
            spawnZone = new Zone(map.Name, location.Y - 10, location.X - 10, location.Y + 10, location.X + 10);

            // 2. Create a GMG, put it on the map, set GMG as npcId[0]
            Npc gmg = World.NpcConfiguration["GMG-Aresden"].Copy();
            gmg.Side = OwnerSide.Neutral;
            gmg.MoveType = MovementType.None;
            gmg.Strength = 0;
            gmg.Vitality = 10000;
            gmg.HP = gmg.MaxHP; // makes maxhp of 30000
            gmg.IsPacifist = true;

            int gmgId = Cache.World.InitNpc(gmg, location);
            npcIds.Add(gmgId);
            target = gmg;

            // 3. notify all players on map of pre-start
            for (int p = 0; p < map.Players.Count; p++)
                if (Cache.World.Players.ContainsKey(map.Players[p]))
                    Notify(Cache.World.Players[map.Players[p]]);
        }

        public void Notify(Character owner)
        {
            switch (state)
            {
                case PublicEventState.Created:
                    owner.Notify(CommandMessageType.NotifyPublicEventCreated, (int)PublicEventType.Defence, location.X, location.Y, (int)difficulty, (int)(startTime - DateTime.Now).TotalSeconds);
                    break;
                case PublicEventState.Started:
                    owner.Notify(CommandMessageType.NotifyPublicEventStarted, (int)PublicEventType.Defence, location.X, location.Y, (int)difficulty, (int)(endTime - DateTime.Now).TotalSeconds, (participantIds.Contains(owner.ObjectId) ? "1" : "0"));
                    break;
                case PublicEventState.Ended:
                    owner.Notify(CommandMessageType.NotifyPublicEventEnded, (int)rating);
                    break;
            }
        }

        public void Start()
        {
            state = PublicEventState.Started;
            endTime = DateTime.Now.Add(duration); // dont use start time because it can be started early by participation

            // 1. notify all players on the map of start
            for (int p = 0; p < map.Players.Count; p++)
                if (Cache.World.Players.ContainsKey(map.Players[p]))
                    Notify(Cache.World.Players[map.Players[p]]);
        }

        public void TimerProcess()
        {
            if (state == PublicEventState.Ended) return;

            // check for new participants (players within range of the GMG)
            for (int p = 0; p < map.Players.Count; p++)
                if (Cache.World.Players.ContainsKey(map.Players[p]))
                    if (!participantIds.Contains(map.Players[p]) && Cache.World.Players[map.Players[p]].IsWithinRange(target, 5, 5))
                    {
                        participantIds.Add(map.Players[p]);
                        Notify(Cache.World.Players[map.Players[p]]);
                    }

            if (state == PublicEventState.Created)
            {
                // 1. Check if pre-start timer ended, or at least 1 participant, then start it
                if (DateTime.Now > startTime || participantIds.Count > 0)
                    Start();
            }
            else if (state == PublicEventState.Started)
            {
                // 1. Check if gmg dies, then End(failure)
                if (target == null || target.IsDead)
                    End(PublicEventResult.Failed);

                // 2. Check event timer ended
                if (DateTime.Now > endTime)
                    End(PublicEventResult.Succeeded);

                // 3. Randomly spawn attackers which attack the target
                if (spawnedEnemies < maximumEnemies && (DateTime.Now - lastSpawn).TotalSeconds >= spawnRate)
                {
                    lastSpawn = DateTime.Now;
                    for (int i = 0; i < spawnCount; i++)
                    {
                        Npc mob = World.NpcConfiguration["Faction-Npc"].Copy();

                        switch (difficulty)
                        {
                            case PublicEventDifficulty.Easy:
                                mob.Difficulty = AIDifficulty.Easy;
                                mob.Equipment = AIEquipment.Light;
                                break;
                            case PublicEventDifficulty.Normal:
                                mob.Difficulty = AIDifficulty.Normal;
                                mob.Equipment = AIEquipment.Medium;
                                break;
                            case PublicEventDifficulty.Hard:
                                mob.Difficulty = AIDifficulty.Hard;
                                mob.Equipment = AIEquipment.Heavy;
                                break;
                            case PublicEventDifficulty.Heroic:
                                mob.Difficulty = AIDifficulty.Heroic;
                                mob.Equipment = AIEquipment.Hero;
                                break;
                            case PublicEventDifficulty.Godly:
                                mob.Difficulty = AIDifficulty.Godly;
                                mob.Equipment = AIEquipment.Hero;
                                break;
                        }

                        if (mob.IsHuman) mob.GenerateHuman();
                        mob.Killed += RemoveMob;
                        mob.Objective = target;
                        int mobId = Cache.World.InitNpc(mob, spawnZone.GetRandomLocation());
                        npcIds.Add(mobId);
                        spawnedEnemies++;
                    }
                }
            }
        }

        private void RemoveMob(IOwner owner, IOwner killer, int damage, DamageType type, int hitCount)
        {
            // keep track of killed mobs
            if (state != PublicEventState.Ended)
            {
                npcIds.Remove(owner.ObjectId);
                spawnedEnemies--;
            }
        }

        public void End(PublicEventResult result)
        {
            state = PublicEventState.Ended;

            // Remove any Npcs
            for (int i = 0; i < npcIds.Count; i++)
                if (npcIds[i] != -1 && Cache.World.Npcs.ContainsKey(npcIds[i]))
                    Cache.World.Npcs[npcIds[i]].Die(null, 0, (DamageType)0, 1, false);
            npcIds.Clear();

            // check rating based on remaining GMG hp
            int remainingHpPercentage = (int)(((double)target.HP / (double)target.MaxHP) * 100.0f);
            if (remainingHpPercentage > 80) rating = PublicEventRating.Gold;
            else if (remainingHpPercentage > 40) rating = PublicEventRating.Silver;
            else rating = PublicEventRating.Bronze;

            if (result == PublicEventResult.Succeeded)
            {
                // give out rewards to participants
                for (int p = 0; p < participantIds.Count; p++)
                    if (Cache.World.Players.ContainsKey(participantIds[p]))
                    {
                        Character character = Cache.World.Players[participantIds[p]];
                        Item reward;
                        switch (rating)
                        {
                            case PublicEventRating.Bronze:
                                character.Titles[TitleType.PublicEventsBronze]++;
                                character.Notify(CommandMessageType.NotifyTitle, (int)TitleType.PublicEventsBronze, character.Titles[TitleType.PublicEventsBronze]);

                                character.AddGold(1000);
                                break;
                            case PublicEventRating.Silver:
                                character.Titles[TitleType.PublicEventsSilver]++;
                                character.Notify(CommandMessageType.NotifyTitle, (int)TitleType.PublicEventsSilver, character.Titles[TitleType.PublicEventsSilver]);

                                character.AddGold(2000);
                                break;
                            case PublicEventRating.Gold:
                                character.Titles[TitleType.PublicEventsGold]++;
                                character.Notify(CommandMessageType.NotifyTitle, (int)TitleType.PublicEventsGold, character.Titles[TitleType.PublicEventsGold]);

                                switch (Dice.Roll(1, 2))
                                {
                                    case 1: reward = World.ItemConfiguration[378].Copy(); break; //xelima stone
                                    default: reward = World.ItemConfiguration[379].Copy(); break; // merien stone
                                }

                                if (reward != null) character.AddInventoryItem(reward);

                                character.AddGold(3000);
                                break;
                        }
                    }
            }

            // notify all players on the map of ending
            for (int p = 0; p < map.Players.Count; p++)
                if (Cache.World.Players.ContainsKey(map.Players[p]))
                    Notify(Cache.World.Players[map.Players[p]]);

            EventEnded(this);
        }

        public bool IsStarted { get { return state == PublicEventState.Started; } }
        public PublicEventState State { get { return state; } }
        public Map CurrentMap { get { return map; } }
        public string Name { get { return name; } }
        public string Description { get { return description; } }
        public PublicEventDifficulty Difficulty { get { return difficulty; } set { difficulty = value; } }
        public DateTime StartTime { get { return startTime; } set { startTime = value; } }
        public TimeSpan Duration { get { return duration; } }
        public Location Location { get { return location; } set { location = value; } }
        public Location TargetLocation { get { return targetLocation; } set { targetLocation = value; } }
        public List<int> NpcIds { get { return npcIds; } set { npcIds = value; } }
        public List<int> ParticipantIds { get { return participantIds; } set { participantIds = value; } }
        public IOwner Target { get { return target; } set { target = value; } }
        public int Wave { get { return wave; } set { wave = value; } }
    }
}
