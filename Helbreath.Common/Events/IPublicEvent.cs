﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;

namespace Helbreath.Common.Events
{
    public interface IPublicEvent
    {
        void Create();
        void Start();
        void SetDifficulty(PublicEventDifficulty difficulty);
        void TimerProcess();
        void Notify(Character owner);
        void End(PublicEventResult result);

        event PublicEventHandler EventEnded;
        PublicEventState State { get; }
        bool IsStarted { get; }
        string Name { get; }
        string Description { get; }
        PublicEventDifficulty Difficulty { get; }
        DateTime StartTime { get; set; }
        TimeSpan Duration { get; }
        Location Location { get; set; }
        Location TargetLocation { get; set; }
        List<int> NpcIds { get; set; }
        List<int> ParticipantIds { get; set; }
        IOwner Target { get; set; }
        int Wave { get; set; }
    }
}
