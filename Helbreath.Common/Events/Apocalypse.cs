﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Events
{
    public class Apocalypse : IWorldEvent
    {
        private World world;
        private List<int> npcIDs;
        private Dictionary<string, Portal> portals;
        private DateTime apocalypseStartTime;

        private DateTime startTime;
        private WorldEventResult result;

        public Apocalypse()
        {
            npcIDs = new List<int>();

            portals = new Dictionary<string, Portal>();
        }

        public Apocalypse Copy()
        {
            Apocalypse apoc = new Apocalypse();
            apoc.Portals = portals;

            return apoc;
        }

        // TODO - new client should have dynamic portals as IDynamicObject which are stored on map tiles because this is inefficient
        public bool HasPortal(Location location, out Portal portal)
        {
            portal = null;

            if (portals.ContainsKey(location.MapName) &&
                    ((portals[location.MapName].PortalType == PortalType.ClearAllMobs && portals[location.MapName].CurrentMap.TotalMobs < 1)
                        || portals[location.MapName].PortalType == PortalType.Apocalypse))
            {
                portal = portals[location.MapName];
                return true;
            }

            return false;
        }

        public void TimerProcess()
        {
            TimeSpan ts = DateTime.Now - apocalypseStartTime;
            if (ts.Minutes >= 60)
            {
                world.EndWorldEvent(OwnerSide.None);
            }
        }

        public void Start()
        {
            startTime = DateTime.Now;

            Portal entrance = null;
            foreach (Portal portal in portals.Values)
                if (portal.PortalType == PortalType.Apocalypse)
                    entrance = portal;

            if (entrance == null) world.EndWorldEvent(OwnerSide.None);
            else
                foreach (Map map in world.Maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (world.Players.ContainsKey(map.Players[p]))
                        {
                            world.Players[map.Players[p]].Notify(CommandMessageType.NotifyApocalypseStart);
                            world.Players[map.Players[p]].Notify(CommandMessageType.NotifyApocalypseGateOpen, entrance.PortalLocation);
                        }

            apocalypseStartTime = DateTime.Now;
        }

        public void End(OwnerSide winner)
        {
            foreach (Map map in world.Maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (world.Players.ContainsKey(map.Players[p]))
                    {
                        world.Players[map.Players[p]].Notify(CommandMessageType.NotifyApocalypseGateClosed);
                        world.Players[map.Players[p]].Notify(CommandMessageType.NotifyApocalypseEnd);
                    }

            foreach (int npcID in npcIDs) world.RemoveNPC(npcID);

            result = new WorldEventResult(Type);
            result.StartDate = startTime;
            result.EndDate = DateTime.Now;
            result.Winner = winner;
        }

        public World CurrentWorld { get { return world; } set { world = value; } }
        public WorldEventType Type { get { return WorldEventType.Apocalypse; } }
        public List<int> NpcIDs { get { return npcIDs; } set { npcIDs = value; } }
        public Dictionary<string, Portal> Portals { get { return portals; } set { portals = value; } }
        public WorldEventResult Result { get { return result; } }
    }
}
