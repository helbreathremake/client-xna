﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Helbreath.Common.Assets;

namespace Helbreath.Common.Events
{
    public class WorldEventStructure
    {
        private string npcName;
        private Location location;

        public WorldEventStructure(string mapName, int x, int y, string npcName)
        {
            this.npcName = npcName;
            this.location = new Location(mapName, x, y);
        }

        public string NpcName { get { return npcName; } }
        public Location Location { get { return location; } }
    }
}
