﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Helbreath.Common;
using Helbreath.Common.Assets;
using Helbreath.Common.Assets.Objects;
using Helbreath.Common.Assets.Objects.Dynamic;

namespace Helbreath.Common.Events
{
    public enum CrusadeMode
    {
        End = 0,
        Start = 1
    }

    public enum CrusadeStrikeTarget
    {
        None,
        Structures,
        Players
    }

    public class CrusadeStrikeResult
    {
        public DateTime Time;
        public OwnerSide StrikeOwner;
        public int StructuresDamaged;
        public int StructuresDestroyed;
        public int Casualties;
        public int RemainingStructures;
        public string MapName;
        public int[] StrikePointsHP;
    }

    public class Crusade : IWorldEvent
    {
        private World world;
        private int manaPerStrike;
        private int structuresPerGuild;
        private Dictionary<OwnerSide, List<CrusadeStrikePoint>> strikePoints;
        private Dictionary<OwnerSide, List<WorldEventStructure>> structures;
        private int[] collectedMana;
        private List<int> npcIDs; // list of npcs associated with this event

        // striking
        private List<CrusadeStrikeResult> strikeResults;
        private DateTime strikeTimer; // used to delayed timer strike events
        private CrusadeStrikeTarget strikeTarget; // type of strike. hitting structures, players or none (disable timer events)
        private OwnerSide strikeTargetSide; // target town
        private int strikeCount; // number of strikes that have hit.

        private WorldEventResult result;
        private DateTime startTime;

        public Crusade()
        {
            strikePoints = new Dictionary<OwnerSide, List<CrusadeStrikePoint>>();
            strikePoints.Add(OwnerSide.Aresden, new List<CrusadeStrikePoint>());
            strikePoints.Add(OwnerSide.Elvine, new List<CrusadeStrikePoint>());

            structures = new Dictionary<OwnerSide, List<WorldEventStructure>>();
            structures.Add(OwnerSide.Aresden, new List<WorldEventStructure>());
            structures.Add(OwnerSide.Elvine, new List<WorldEventStructure>());
            structures.Add(OwnerSide.Neutral, new List<WorldEventStructure>());

            manaPerStrike = 80; // default, overridden by config
            collectedMana = new int[] { 0, 0, 0 };

            strikeTargetSide = OwnerSide.None;
            strikeTarget = CrusadeStrikeTarget.None;

            strikeResults = new List<CrusadeStrikeResult>();
            npcIDs = new List<int>();
        }

        public Crusade Copy()
        {
            Crusade crusade = new Crusade();
            crusade.StrikePoints = strikePoints;
            crusade.Structures = structures;
            crusade.ManaPerStrike = manaPerStrike;
            crusade.StructuresPerGuild = structuresPerGuild;

            return crusade;
        }

        /// <summary>
        /// Begins the Meteor Strike procedure.
        /// </summary>
        /// <param name="side">Target side.</param>
        /// <returns>True or False to indicate the meteor strike has started. If false, then a previous strike is already in progress.</returns>
        public bool PrepareMeteorStrike(OwnerSide side)
        {
            if (strikeTargetSide == OwnerSide.None) // only 1 strike can be initiated at a time.
            {
                strikeResults.Add(new CrusadeStrikeResult() { Time = DateTime.Now, StrikeOwner = side });
                strikeCount = 0;
                strikeTimer = DateTime.Now;
                strikeTargetSide = side;
                strikeTarget = CrusadeStrikeTarget.Structures;
                return true;
            }
            else return false;
        }

        private void MeteorStrikeDamageStructures()
        {
            string townName;
            switch (strikeTargetSide)
            {
                case OwnerSide.Elvine: townName = Globals.ElvineTownName; break;
                case OwnerSide.Aresden: townName = Globals.AresdenTownName; break;
                default: return;
            }

            strikeResults[strikeResults.Count - 1].MapName = townName;
            strikeResults[strikeResults.Count - 1].StrikePointsHP = new int[strikePoints[strikeTargetSide].Count];

            for (int p = 0; p < world.Maps[townName].Players.Count; p++)
                if (world.Players.ContainsKey(world.Maps[townName].Players[p]))
                    world.Players[world.Maps[townName].Players[p]].Notify(CommandMessageType.NotifyCrusadeMeteorStrike);

            int index = 0;
            foreach (CrusadeStrikePoint strikePoint in strikePoints[strikeTargetSide])
            {
                if (strikePoint.HP > 0)
                {
                    int remainingEnergyShield = 0;
                    for (int ix = strikePoint.Location.X - 10; ix < strikePoint.Location.X + 10; ix++)
                        for (int iy = strikePoint.Location.Y - 10; iy < strikePoint.Location.Y + 10; iy++)
                            if (world.Maps[townName][iy][ix].IsOccupied &&
                                world.Maps[townName][iy][ix].Owner.OwnerType == OwnerType.Npc &&
                                ((Npc)world.Maps[townName][iy][ix].Owner).NpcType == NpcType.CrusadeEnergyShield)
                                remainingEnergyShield++;

                    if (remainingEnergyShield < 2)
                    {
                        strikePoint.HP -= (2 - remainingEnergyShield); // so if 1 shield left then 1 damage, if 0 left then 2 damage
                        if (strikePoint.HP <= 0)
                        {
                            strikePoint.HP = 0;
                            strikeResults[strikeResults.Count - 1].StructuresDestroyed++;
                        }
                        else
                        {
                            strikeResults[strikeResults.Count - 1].StructuresDamaged++;
                            // TODO - fire effects
                        }
                    }
                }
                strikeResults[strikeResults.Count - 1].StrikePointsHP[index] = strikePoint.HP;
                index++;
            }

            strikeTimer = DateTime.Now;
            strikeTarget = CrusadeStrikeTarget.Players;
        }

        private void MeteorStrikeDamagePlayers()
        {
            strikeCount++;

            string townName;
            switch (strikeTargetSide)
            {
                case OwnerSide.Elvine: townName = Globals.ElvineTownName; break;
                case OwnerSide.Aresden: townName = Globals.AresdenTownName; break;
                default: return;
            }

            for (int p = 0; p < world.Maps[townName].Players.Count; p++)
                if (world.Players.ContainsKey(world.Maps[townName].Players[p]))
                {
                    Character character = world.Players[world.Maps[townName].Players[p]];
                    int damage = 0;

                    damage = Dice.Roll(1, character.Level) + character.Level;
                    if (damage > 255) damage = 255;
                    if (character.MagicEffects.ContainsKey(MagicType.Protect) &&
                        character.MagicEffects[MagicType.Protect].Magic.Effect1 == 2)
                        damage = damage / 2;

                    if (character.IsAdmin) damage = 0; // cant hurt me!

                    MotionDirection flyDirection = (MotionDirection)Dice.Roll(1, 8); // gets a random fly direction
                    character.TakeDamage(DamageType.Magic, damage, flyDirection, true);

                    if (character.IsDead) strikeResults[strikeResults.Count - 1].Casualties++;
                }

            strikeTimer = DateTime.Now;
            strikeTarget = CrusadeStrikeTarget.Players;
        }

        private void MeteorStrikeCalculateResult()
        {
            int activeStructures = 0;
            foreach (CrusadeStrikePoint strikePoint in strikePoints[strikeTargetSide])
                if (strikePoint.HP > 0) activeStructures++;

            if (activeStructures <= 0)
                switch (strikeTargetSide)
                {
                    case OwnerSide.Aresden: world.EndWorldEvent(OwnerSide.Elvine); break;
                    case OwnerSide.Elvine: world.EndWorldEvent(OwnerSide.Aresden); break;
                }
            else
            {
                foreach (Map map in world.Maps.Values)
                    for (int p = 0; p < map.Players.Count; p++)
                        if (world.Players.ContainsKey(map.Players[p]))
                            world.Players[map.Players[p]].Notify(CommandMessageType.NotifyCrusadeMeteorStrikeResult, strikeResults[strikeResults.Count - 1]);
            }

            strikeTarget = CrusadeStrikeTarget.None;
            strikeTargetSide = OwnerSide.None;
        }

        public void Start()
        {
            startTime = DateTime.Now;
            // TODO - create GUID to be stored in database CrusadeHistory table
            foreach (Map map in world.Maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (world.Players.ContainsKey(map.Players[p]))
                    {
                        Character character = world.Players[map.Players[p]];
                        character.CrusadeDuty = CrusadeDuty.None;
                        character.CrusadeConstructionPoints = 0;
                        character.CrusadeWarContribution = 0;
                        character.Notify(CommandMessageType.NotifyCrusade, (int)CrusadeMode.Start, 0, 0);
                    }

            foreach (WorldEventStructure structure in structures[OwnerSide.Aresden]) // build aresden structures
            {
                Npc aresStructure = World.NpcConfiguration[structure.NpcName].Copy();
                aresStructure.Side = OwnerSide.Aresden;
                aresStructure.IsPacifist = true;
                npcIDs.Add(world.InitNpc(aresStructure, structure.Location));
            }

            foreach (WorldEventStructure structure in structures[OwnerSide.Elvine]) // build elvine structures
            {
                Npc elvStructure = World.NpcConfiguration[structure.NpcName].Copy();
                elvStructure.Side = OwnerSide.Elvine;
                elvStructure.IsPacifist = true;
                npcIDs.Add(world.InitNpc(elvStructure, structure.Location));
            }

            foreach (WorldEventStructure structure in structures[OwnerSide.Neutral]) // build neutral structures
            {
                Npc neutralStructure = World.NpcConfiguration[structure.NpcName].Copy();
                neutralStructure.Side = OwnerSide.Neutral;
                neutralStructure.IsPacifist = true;
                npcIDs.Add(world.InitNpc(neutralStructure, structure.Location));
            }

            foreach (Guild guild in world.Guilds.Values) // reset crusade values for guilds
            {
                guild.CrusadeBuildLocation = new Location(Globals.MiddlelandName, -1, -1);
                guild.CrusadeTeleportLocation = new Location(Globals.MiddlelandName, -1, -1);
                guild.CrusadeStructures = new List<Npc>();
            }
        }

        public void End(OwnerSide winner)
        {
            // remove crusade npcs
            foreach (int npcID in npcIDs) world.RemoveNPC(npcID);

            // cleanup, notify and reward
            foreach (Map map in world.Maps.Values)
                for (int p = 0; p < map.Players.Count; p++)
                    if (world.Players.ContainsKey(map.Players[p]))
                    {
                        Character character = world.Players[map.Players[p]];
                        character.CrusadeDuty = CrusadeDuty.None;
                        character.Notify(CommandMessageType.NotifyCrusade, (int)CrusadeMode.End, 0, (int)winner);

                        if (character.CrusadeWarContribution > Globals.MaximumWarContribution) character.CrusadeWarContribution = Globals.MaximumWarContribution;

                        if (winner == OwnerSide.None)
                            character.ExperienceStored += character.CrusadeWarContribution / 6; // draw
                        else
                        {
                            if (character.Level <= 80) character.CrusadeWarContribution += character.Level * 200;
                            else if (character.Level <= 100) character.CrusadeWarContribution += character.Level * 100;
                            else character.CrusadeWarContribution += character.Level * 30;

                            if (character.Town == winner)
                                character.ExperienceStored += (int)((double)character.CrusadeWarContribution * 1.2); // win
                            else character.ExperienceStored += character.CrusadeWarContribution / 5; // lose
                        }
                    }

            result = new WorldEventResult(Type);
            result.StartDate = startTime;
            result.EndDate = DateTime.Now;
            result.Winner = winner;
        }

        public void TimerProcess()
        {
            TimeSpan ts;
            switch (strikeTarget)
            {
                case CrusadeStrikeTarget.Structures:
                    ts = DateTime.Now - strikeTimer;
                    if (ts.Seconds >= 5) MeteorStrikeDamageStructures();
                    break;
                case CrusadeStrikeTarget.Players:
                    ts = DateTime.Now - strikeTimer;
                    switch (strikeCount)
                    {
                        case 0: if (ts.Seconds >= 1) MeteorStrikeDamagePlayers(); break;
                        case 1: if (ts.Seconds >= 1) MeteorStrikeDamagePlayers(); break;
                        default: if (ts.Seconds >= 2) MeteorStrikeCalculateResult(); break;
                    }
                    break;
            }
        }

        public World CurrentWorld { get { return world; } set { world = value; } }
        public int ManaPerStrike { get { return manaPerStrike; } set { manaPerStrike = value; } }
        public int StructuresPerGuild { get { return structuresPerGuild; } set { structuresPerGuild = value; } }
        public WorldEventType Type { get { return WorldEventType.Crusade; } }
        public Dictionary<OwnerSide, List<CrusadeStrikePoint>> StrikePoints { get { return strikePoints; } set { strikePoints = value; } }
        public Dictionary<OwnerSide, List<WorldEventStructure>> Structures { get { return structures; } set { structures = value; } }
        public int[] CollectedMana { get { return collectedMana; } set { collectedMana = value; } }
        public List<CrusadeStrikeResult> StrikeResults { get { return strikeResults; } }
        public List<int> NpcIDs { get { return npcIDs; } set { npcIDs = value; } }
        public WorldEventResult Result { get { return result; } }
    }

    public class CrusadeStrikePoint
    {
        private List<Location> effectLocations;
        private Location location;
        private int hitPoints;
        private string relatedMap;

        public CrusadeStrikePoint(string mapName, int x, int y, string relatedMap, int hitPoints)
        {
            this.effectLocations = new List<Location>();
            this.location = new Location(mapName, x, y);
            this.relatedMap = relatedMap;
            this.hitPoints = hitPoints;
        }

        public Location Location { get { return location; } }
        public int HP { get { return hitPoints; } set { hitPoints = value; } }
        public string RelatedMap { get { return relatedMap; } }
        public List<Location> EffectLocations { get { return effectLocations; } set { effectLocations = value; } }
    }
}
