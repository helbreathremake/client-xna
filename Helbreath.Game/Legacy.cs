﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;


public struct LegacySpriteFrame
{
    public int Left;
    public int Top;
    public int Width;
    public int Height;
    public int PivotX;
    public int PivotY;

    public LegacySpriteFrame(byte[] frameInfo)
    {
        Left = BitConverter.ToInt16(frameInfo, 0);
        Top = BitConverter.ToInt16(frameInfo, 2);
        Width = BitConverter.ToInt16(frameInfo, 4);
        Height = BitConverter.ToInt16(frameInfo, 6);
        PivotX = BitConverter.ToInt16(frameInfo, 8);
        PivotY = BitConverter.ToInt16(frameInfo, 10);
    }
}


/// <summary>
/// Sprite class containing DIB bitmap information and frame information retrieved from .pak files.
/// A sprite is a single bitmap image stored at certain indexes in the .pak file. It also includes a list of rectangles
/// which determines each frame in the sprite. E.g a sprite such as armour has 8 frames (1 for each direction).
/// </summary>
public class LegacySprite
{
    private byte[] bitmapInfo;
    private int imageSize;
    private int imageWidth;
    private int imageHeight;
    private int imageColourBit;
    private Image image;
    private int index; // index of the sprite in the pak file

    private List<LegacySpriteFrame> frames;

    /// <summary>
    /// Reads data from a pak file and turns the raw bitmap of the sprite in to an Image object.
    /// </summary>
    /// <param name="fileData">Byte array of the pak file.</param>
    /// <param name="index">Sprite index in the pak file.</param>
    public LegacySprite(byte[] fileData, int index)
    {
        this.index = index;
        int spriteStart = BitConverter.ToInt32(fileData, 24 + (index * 8)); // index of the start of this sprite
        int frameCount = BitConverter.ToInt32(fileData, spriteStart + 100); // number of frames this sprite has
        int bitmapStart = spriteStart + (108 + (12 * frameCount));           // index of the start of the bitmap info (DIB format)

        frames = new List<LegacySpriteFrame>(frameCount);
        for (int i = 0; i < frameCount; i++)
        {
            byte[] frameInfo = new byte[12];
            Buffer.BlockCopy(fileData, spriteStart + 104 + (i * 12), frameInfo, 0, 12);
            frames.Add(new LegacySpriteFrame(frameInfo));
        }

        byte[] bitmapFileHeader = new byte[14]; // 14 bytes for Bitmap file header. we need size, rest is irrelevant for what we need
        Buffer.BlockCopy(fileData, bitmapStart, bitmapFileHeader, 0, 14);
        imageSize = (int)BitConverter.ToInt32(bitmapFileHeader, 2); //bitmap file format has a DWORD (4 bytes) at location 2 for storing bfSize
        uint imageSizeTest = BitConverter.ToUInt32(bitmapFileHeader, 2);

        bitmapInfo = new byte[imageSize];
        Buffer.BlockCopy(fileData, bitmapStart, bitmapInfo, 0, imageSize);

        // 40 bytes for Bitmap info header, we need width, height and colourbit (though hb uses 8bit colour (256 colours) exclusively i think):
        imageWidth = BitConverter.ToInt32(bitmapInfo, 14 + 4);
        imageHeight = BitConverter.ToInt32(bitmapInfo, 14 + 8);
        imageColourBit = BitConverter.ToInt32(bitmapInfo, 14 + 14);

        // http://en.wikipedia.org/wiki/BMP_file_format is useful to see the structure of the file header and info header

        // we need to create a copy of the temporary bitmap because it is stored in the stream.
        // and will be lost when the stream is closed... by design.
        try
        {
            MemoryStream stream = new MemoryStream(bitmapInfo);
            Bitmap temp = new Bitmap(stream);
            Bitmap bitmap = new Bitmap(temp);
            temp.Dispose();
            temp = null;
            stream.Close();
            stream = null;
            bitmapInfo = null;

            this.image = (Image)bitmap;
        }
        catch { }
    }

    public Image GetFrame(int frameIndex)
    {
        LegacySpriteFrame frame = frames[frameIndex];

        Rectangle frameBox = new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height);

        Bitmap frameImage = new Bitmap(frame.Width * 2, frame.Height * 2);
        Graphics g = Graphics.FromImage(frameImage);

        g.DrawImage(this.image, Math.Abs(frames[0].PivotX) + frame.PivotX, Math.Abs(frames[0].PivotY) + frame.PivotY, frameBox, GraphicsUnit.Pixel);

        return (Image)frameImage;
    }

    public void DrawFrame(int frameIndex, Graphics g, int x, int y)
    {
        LegacySpriteFrame frame = frames[frameIndex];
        Rectangle frameBox = new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height);
        g.DrawImage(this.image, x, y, frameBox, GraphicsUnit.Pixel);
    }

    public List<LegacySpriteFrame> Frames
    {
        get { return frames; }
    }

    public Image ImageWithFrames
    {
        get
        {
            Bitmap framedImage = new Bitmap(this.image);
            Graphics g = Graphics.FromImage(framedImage);

            int index = 0;
            foreach (LegacySpriteFrame frame in frames)
            {
                g.DrawRectangle(new Pen(Color.Red), new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height));
                g.DrawString(index.ToString(), new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), Brushes.Red, frame.Left + 3, frame.Top + 3);
                index++;
            }

            return (Image)framedImage;
        }
    }

    public Image Image
    {
        get { return image; }
    }

    public int Index
    {
        get { return index; }
    }

    public int Width { get { return imageWidth; } }
    public int Height { get { return imageHeight; } }
}

public class LegacyPakFile
{
    private int spriteCount;
    private string filename;
    private Dictionary<int, LegacySprite> sprites;

    /// <summary>
    /// Loads all sprites from a single .pak file.
    /// </summary>
    /// <param name="filename">Full path and file name of the .pak file.</param>
    public LegacyPakFile(string filename)
    {
        this.filename = filename;

        byte[] fileData = StreamFile(filename);

        spriteCount = BitConverter.ToInt32(fileData, 20); // .pak file format has a DWORD (4 bytes) to store spriteCount at position 20

        sprites = new Dictionary<int, LegacySprite>(spriteCount);
        if (spriteCount == 1)
            sprites.Add(0, new LegacySprite(fileData, 0));
        else
            for (int i = 0; i < spriteCount; i++)
            {
                sprites.Add(i, new LegacySprite(fileData, i));
            }
    }

    /// <summary>
    /// Reads a .pak file and copies it to a byte array buffer so that it can be worked on.
    /// </summary>
    /// <param name="filename">Full path and file name of the .pak file.</param>
    /// <returns>File data in byte array format.</returns>
    private byte[] StreamFile(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read); // open a stream for the file
        byte[] ImageData = new byte[fs.Length];   // create a byte buffer size of the open file
        fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));   // read stream data in to the byte buffer
        fs.Close();   // clean up

        return ImageData; //return the byte data buffer
    }

    public Dictionary<int, LegacySprite> Sprites
    {
        get { return sprites; }
    }

    public LegacySprite this[int index]
    {
        get { return sprites[index]; }
    }
}


//public struct BitmapFile
//{
//    public BitmapFileHeader bmpHeader;
//    public BitmapInfo bmpInfo;
//}

//public struct BitmapFileHeader
//{
//    public UInt16 type;
//    public UInt32 size;
//    public UInt16 reserved1;
//    public UInt16 reserved2;
//    public UInt32 offBits;
//}

//public struct BitmapInfo
//{
//    public BitmapInfoHeader bih;
//    public Int32[] bmiColours;
//}

//public struct BitmapInfoHeader
//{
//    public UInt32 biSize;
//    public Int32 biWidth;
//    public Int32 biHeight;
//    public UInt16 biPlanes;
//    public UInt16 biBitCount;
//    public UInt32 biCompression;
//    public UInt32 biSizeImage;
//    public Int32 biXPelsPerMeter;
//    public Int32 biYPelsPerMeter;
//    public UInt32 biClrUsed;
//    public UInt32 biClrImportant;
//}

//public class LegacyPakFileEncrypted
//{
//    public List<Sprite> sprites = new List<Sprite>();

//    private void DecryptDwords(byte[] bytes, int offset, int length, byte[] key)
//    {
//        if (length % 4 != 0)
//        {
//            throw new Exception("Buffer length must be multiple of 4.");
//        }

//        for (int i = offset; i < offset + length; i++)
//        {
//            bytes[i] ^= key[i % 4];
//            key[i % 4] ^= bytes[i];
//        }
//    }

//    private void DecryptBytes(byte[] bytes, int offset, int length, byte key)
//    {
//        for (int i = 0; i < length; i++)
//        {
//            bytes[offset + i] ^= key;
//            key ^= bytes[offset + i];
//        }
//    }

//    private void DecryptBuffer(byte[] bytes)
//    {
//        int cnt4 = bytes.Length / 4;
//        int cnt1 = bytes.Length % 4;
//        if (cnt4 > 0)
//        {
//            DecryptDwords(bytes, 0, cnt4 * 4, new byte[] { 0x4C, 0xB2, 0x3E, 0x1A });
//        }
//        if (cnt1 > 0)
//        {
//            DecryptBytes(bytes, cnt4 * 4, bytes.Length - cnt4 * 4, 0x9A);
//        }
//    }

//    private BinaryReader CreateDecryptedBlockReader(BinaryReader source, int count)
//    {
//        var bytes = source.ReadBytes(count);
//        DecryptBuffer(bytes);
//        return new BinaryReader(new MemoryStream(bytes));
//    }


//    private List<FrameInfo> ReadFrameInfo(BinaryReader br, int count)
//    {
//        List<FrameInfo> frameInfoList = new List<FrameInfo>();
//        for (int j = 0; j < count; j++)
//        {
//            FrameInfo fi = new FrameInfo();
//            fi.rect.X = br.ReadInt16();
//            fi.rect.Y = br.ReadInt16();
//            fi.rect.Width = br.ReadInt16();
//            fi.rect.Height = br.ReadInt16();
//            fi.fixX = br.ReadInt16();
//            fi.fixY = br.ReadInt16();
//            frameInfoList.Add(fi);
//        }
//        return frameInfoList;
//    }

//    private BinaryReader CreateDecryptedBitmapReader(BinaryReader source)
//    {
//        // Decrypt bmp header.
//        var hdrBytes = source.ReadBytes(14);
//        DecryptBuffer(hdrBytes);

//        var sig = Encoding.ASCII.GetString(hdrBytes, 0, 2);
//        if (sig != "BM")
//        {
//            throw new Exception("It is not bitmap.");
//        }

//        // Get bmp size.
//        var bmpSize = BitConverter.ToInt32(hdrBytes, 2);

//        // Decrypt bmp data.
//        var dataBytes = source.ReadBytes(bmpSize - 14);
//        DecryptBuffer(dataBytes);

//        // Create normal bitmap.
//        var ms = new MemoryStream();
//        ms.Write(hdrBytes, 0, hdrBytes.Length);
//        ms.Write(dataBytes, 0, dataBytes.Length);

//        Debug.Assert(ms.Length == bmpSize);

//        ms.Position = 0;

//        return new BinaryReader(ms);
//    }

//    private Bitmap ReadBitmap2(BinaryReader br)
//    {
//        BitmapFile BF = new BitmapFile();
//        BF.bmpHeader = ReadBitmapFileHeader(br);
//        br.BaseStream.Seek(-14, SeekOrigin.Current);
//        byte[] bitmapData = br.ReadBytes((int)BF.bmpHeader.size);

//        return new Bitmap(new MemoryStream(bitmapData));
//    }


//    private BitmapFileHeader ReadBitmapFileHeader(BinaryReader br)
//    {
//        BitmapFileHeader BFH = new BitmapFileHeader();

//        BFH.type = br.ReadUInt16();
//        BFH.size = br.ReadUInt32();
//        BFH.reserved1 = br.ReadUInt16();
//        BFH.reserved2 = br.ReadUInt16();
//        BFH.offBits = br.ReadUInt32();

//        return BFH;
//    }

//    /// <summary>
//    /// Load encrypted package.
//    /// </summary>
//    public bool LoadPak2(string path)
//    {
//        int XOR_DWORDS = 0x1A3EB24C;
//        int SpriteCountLimit = 1000;

//        try
//        {
//            using (var br = new BinaryReader(new FileStream(path, FileMode.Open, FileAccess.Read)))
//            {
//                br.BaseStream.Seek(20, SeekOrigin.Begin);
//                int spriteCount = br.ReadInt32() ^ XOR_DWORDS;

//                if (spriteCount < 1 || spriteCount > SpriteCountLimit)
//                {
//                    throw new Exception("Incorrect number of sprites");
//                }

//                for (int i = 0; i < spriteCount; i++)
//                {
//                    br.BaseStream.Seek(24 + i * 8, SeekOrigin.Begin);

//                    int start = br.ReadInt32() ^ XOR_DWORDS;

//                    br.BaseStream.Seek(start + 100, SeekOrigin.Begin);

//                    int totalFrames = br.ReadInt32() ^ XOR_DWORDS;
//                    int bitmapStart = start + 108 + (12 * totalFrames);

//                    Sprite spr = new Sprite();
//                    var brFrames = CreateDecryptedBlockReader(br, 12 * totalFrames);
//                    spr.frames = ReadFrameInfo(brFrames, totalFrames);

//                    br.BaseStream.Seek(bitmapStart, SeekOrigin.Begin);
//                    var brBitmap = CreateDecryptedBitmapReader(br);
//                    spr.image = ReadBitmap2(brBitmap);

//                    sprites.Add(spr);
//                }
//                return true;
//            }
//        }
//        catch
//        {
//            return sprites.Count != 0;
//        }
//    }
//}

//public class Sprite
//{
//    public List<FrameInfo> frames;
//    public Bitmap image;
//    public Sprite() { frames = new List<FrameInfo>(); }
//}

//public class FrameInfo
//{
//    public Rectangle rect;
//    public short fixX;
//    public short fixY;
//    public int Width { get { return rect.Width; } set { rect.Width = value; } }
//    public int Height { get { return rect.Height; } set { rect.Height = value; } }
//    public int X { get { return rect.X; } set { rect.X = value; } }
//    public int Y { get { return rect.Y; } set { rect.Y = value; } }
//    public void setX(short _x) { X = _x; }
//    public void setY(short _y) { Y = _y; }
//    public void setWidth(short _width) { Width = _width; }
//    public void setHeight(short _height) { Height = _height; }
//    public void setFixX(short _fixX) { fixX = _fixX; }
//    public void setFixY(short _fixY) { fixY = _fixY; }

//}
