﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

public struct LegacyNewSpriteFrame
{
    public int Left;
    public int Top;
    public int Width;
    public int Height;
    public int PivotX;
    public int PivotY;

    public LegacyNewSpriteFrame(byte[] frameInfo)
    {
        Left = BitConverter.ToInt16(frameInfo, 0);
        Top = BitConverter.ToInt16(frameInfo, 2);
        Width = BitConverter.ToInt16(frameInfo, 4);
        Height = BitConverter.ToInt16(frameInfo, 6);
        PivotX = BitConverter.ToInt16(frameInfo, 8);
        PivotY = BitConverter.ToInt16(frameInfo, 10);
    }
}


/// <summary>
/// Sprite class containing DIB bitmap information and frame information retrieved from .pak files.
/// A sprite is a single bitmap image stored at certain indexes in the .pak file. It also includes a list of rectangles
/// which determines each frame in the sprite. E.g a sprite such as armour has 8 frames (1 for each direction).
/// </summary>
public class LegacyNewSprite
{
    private byte[] bitmapInfo;
    private int imageSize;
    private int imageWidth;
    private int imageHeight;
    private int imageColourBit;
    private Image image;
    private int index; // index of the sprite in the pak file

    private List<LegacyNewSpriteFrame> frames;

    /// <summary>
    /// Reads data from a pak file and turns the raw bitmap of the sprite in to an Image object.
    /// </summary>
    /// <param name="fileData">Byte array of the pak file.</param>
    /// <param name="index">Sprite index in the pak file.</param>
    public LegacyNewSprite(byte[] fileData, int index)
    {
        this.index = index;
        int spriteStart = BitConverter.ToInt32(fileData, 24 + (index * 8)); // index of the start of this sprite
        int frameCount = BitConverter.ToInt32(fileData, spriteStart + 100); // number of frames this sprite has
        int bitmapStart = spriteStart + (108 + (12 * frameCount));           // index of the start of the bitmap info (DIB format)

        frames = new List<LegacyNewSpriteFrame>(frameCount);
        for (int i = 0; i < frameCount; i++)
        {
            byte[] frameInfo = new byte[12];
            Buffer.BlockCopy(fileData, spriteStart + 104 + (i * 12), frameInfo, 0, 12);
            frames.Add(new LegacyNewSpriteFrame(frameInfo));
        }

        byte[] bitmapFileHeader = new byte[14]; // 14 bytes for Bitmap file header. we need size, rest is irrelevant for what we need
        Buffer.BlockCopy(fileData, bitmapStart, bitmapFileHeader, 0, 14);
        imageSize = (int)BitConverter.ToInt32(bitmapFileHeader, 2); //bitmap file format has a DWORD (4 bytes) at location 2 for storing bfSize
        uint imageSizeTest = BitConverter.ToUInt32(bitmapFileHeader, 2);

        bitmapInfo = new byte[imageSize];
        Buffer.BlockCopy(fileData, bitmapStart, bitmapInfo, 0, imageSize);

        // 40 bytes for Bitmap info header, we need width, height and colourbit (though hb uses 8bit colour (256 colours) exclusively i think):
        imageWidth = BitConverter.ToInt32(bitmapInfo, 14 + 4);
        imageHeight = BitConverter.ToInt32(bitmapInfo, 14 + 8);
        imageColourBit = BitConverter.ToInt32(bitmapInfo, 14 + 14);

        // http://en.wikipedia.org/wiki/BMP_file_format is useful to see the structure of the file header and info header

        // we need to create a copy of the temporary bitmap because it is stored in the stream.
        // and will be lost when the stream is closed... by design.
        try
        {
            MemoryStream stream = new MemoryStream(bitmapInfo);
            Bitmap temp = new Bitmap(stream);
            Bitmap bitmap = new Bitmap(temp);
            temp.Dispose();
            temp = null;
            stream.Close();
            stream = null;
            bitmapInfo = null;

            this.image = (Image)bitmap;
        }
        catch { }
    }

    public Image GetFrame(int frameIndex)
    {
        LegacyNewSpriteFrame frame = frames[frameIndex];

        Rectangle frameBox = new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height);

        Bitmap frameImage = new Bitmap(frame.Width * 2, frame.Height * 2);
        Graphics g = Graphics.FromImage(frameImage);

        g.DrawImage(this.image, Math.Abs(frames[0].PivotX) + frame.PivotX, Math.Abs(frames[0].PivotY) + frame.PivotY, frameBox, GraphicsUnit.Pixel);

        return (Image)frameImage;
    }

    public void DrawFrame(int frameIndex, Graphics g, int x, int y)
    {
        LegacyNewSpriteFrame frame = frames[frameIndex];
        Rectangle frameBox = new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height);
        g.DrawImage(this.image, x, y, frameBox, GraphicsUnit.Pixel);
    }

    public List<LegacyNewSpriteFrame> Frames
    {
        get { return frames; }
    }

    public Image ImageWithFrames
    {
        get
        {
            Bitmap framedImage = new Bitmap(this.image);
            Graphics g = Graphics.FromImage(framedImage);

            int index = 0;
            foreach (LegacyNewSpriteFrame frame in frames)
            {
                g.DrawRectangle(new Pen(Color.Red), new Rectangle(frame.Left, frame.Top, frame.Width, frame.Height));
                g.DrawString(index.ToString(), new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), Brushes.Red, frame.Left + 3, frame.Top + 3);
                index++;
            }

            return (Image)framedImage;
        }
    }

    public Image Image
    {
        get { return image; }
    }

    public int Index
    {
        get { return index; }
    }

    public int Width { get { return imageWidth; } }
    public int Height { get { return imageHeight; } }
}

public class LegacyNewPakFile
{
    private int spriteCount;
    private string filename;
    private Dictionary<int, LegacyNewSprite> sprites;

    /// <summary>
    /// Loads all sprites from a single .pak file.
    /// </summary>
    /// <param name="filename">Full path and file name of the .pak file.</param>
    public LegacyNewPakFile(string filename)
    {
        this.filename = filename;

        byte[] fileData = StreamFile(filename);

        spriteCount = BitConverter.ToInt32(fileData, 20); // .pak file format has a DWORD (4 bytes) to store spriteCount at position 20

        sprites = new Dictionary<int, LegacyNewSprite>(spriteCount);
        if (spriteCount == 1)
            sprites.Add(0, new LegacyNewSprite(fileData, 0));
        else
            for (int i = 0; i < spriteCount; i++)
            {
                sprites.Add(i, new LegacyNewSprite(fileData, i));
            }
    }

    /// <summary>
    /// Reads a .pak file and copies it to a byte array buffer so that it can be worked on.
    /// </summary>
    /// <param name="filename">Full path and file name of the .pak file.</param>
    /// <returns>File data in byte array format.</returns>
    private byte[] StreamFile(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read); // open a stream for the file
        byte[] ImageData = new byte[fs.Length];   // create a byte buffer size of the open file
        fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));   // read stream data in to the byte buffer
        fs.Close();   // clean up

        return ImageData; //return the byte data buffer
    }

    public Dictionary<int, LegacyNewSprite> Sprites
    {
        get { return sprites; }
    }

    public LegacyNewSprite this[int index]
    {
        get { return sprites[index]; }
    }
}
