﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helbreath.Game
{
    public class GameSettings
    {
        public string CurrentBGM;
        public bool MusicOn;
        public bool SoundsOn;
        public float MusicVolume;
        public float SoundVolume;

        public int MaxChatHistory;
        public int MiniMapZoomLevel;
        public bool LockedDialogs;
        public bool SpellBookCasting;
        public bool HideDialogDuringCasting;
        public bool TransparentDialogs;
        public GraphicsDetail DetailMode;
        public Dictionary<GameColor, bool> ChatFilters;
        public Resolution Resolution;
        public DisplayMode DisplayMode;
        public int MouseDragTolerance;


        //Game Features
        public bool AutoPickUp;

        //Screen Effects
        public bool LowHealthIndicator;
        public float LowHealthFlashValue;

        public bool RegenRings;
        public bool HpRing;
        public bool MpRing;
        public bool SpRing;
        public bool ExpRing;

        //Dialogs 
        public bool InventoryV2;
        //public bool SpellBookV2;

        //Team Color
        public bool TeamColorOn;
        public bool PartyColorOn;
        public bool GuildColorOn;
        public bool TownColorOn;
        public int NoTeamColor;
        public int PartyTeamColor;
        public int GuildTeamColor;
        public int AresdenTeamColor;
        public int ElvineTeamColor;

        // logout
        public bool IsLoggingOut;
        public int LogOutCount;
        public DateTime LogOutTime;

        //restart
        public bool IsRestarting;
        public int RestartCount;
        public DateTime RestartTime;

        public static GameSettings Default()
        {
            GameSettings s = new GameSettings();
            s.Resolution = Resolution.Standard; //add to config
            s.DisplayMode = DisplayMode.Windowed; //add to config
            s.MusicOn = true;  //Add to config
            s.SoundsOn = true;//Add to config
            s.MusicVolume = 50;//Add to config
            s.SoundVolume = 50;//Add to config
            s.CurrentBGM = string.Empty;
            s.DetailMode = GraphicsDetail.High;//Add to config
            s.LockedDialogs = true;
            s.SpellBookCasting = true;
            s.HideDialogDuringCasting = false;
            s.TransparentDialogs = false;//Add to config
            s.MiniMapZoomLevel = 0;//Add to config
            s.MaxChatHistory = 5;//Add to config

            s.IsLoggingOut = s.IsRestarting = false;
            s.LogOutCount = 11;
            s.RestartCount = 6;
            s.LogOutTime = s.RestartTime = DateTime.Now;

            s.LowHealthIndicator = true;
            s.LowHealthFlashValue = 0.3F;

            s.RegenRings = false;
            s.HpRing = true;
            s.MpRing = true;
            s.SpRing = false;
            s.ExpRing = false;

            s.InventoryV2 = true;
            //s.SpellBookV2 = true;

            s.ChatFilters = new Dictionary<GameColor, bool>();
            s.ChatFilters.Add(GameColor.Local, true);//Add to config
            s.ChatFilters.Add(GameColor.Whisper, true);//Add to config
            s.ChatFilters.Add(GameColor.Global, true);//Add to config
            s.ChatFilters.Add(GameColor.Town, true);//Add to config
            s.ChatFilters.Add(GameColor.Party, true);//Add to config
            s.ChatFilters.Add(GameColor.Guild, true);//Add to config

            s.TeamColorOn = false;
            s.PartyColorOn = false;
            s.GuildColorOn = false;
            s.TownColorOn = true;

            s.PartyTeamColor = System.Drawing.Color.Magenta.ToArgb();//Add to config
            s.GuildTeamColor = System.Drawing.Color.Lime.ToArgb();//Add to config
            s.AresdenTeamColor = System.Drawing.Color.Red.ToArgb();//Add to config
            s.ElvineTeamColor = System.Drawing.Color.Blue.ToArgb();//Add to config
            s.NoTeamColor = System.Drawing.Color.White.ToArgb();

            s.AutoPickUp = true; //add to config

            s.MouseDragTolerance = 3; //add to config, hook up to game mouse some how

            return s;
        }
    }
}
