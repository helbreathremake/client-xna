using HelbreathWorld.Game.Assets.UI;
using log4net;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using System.Management;

namespace Game
{
#if WINDOWS || XBOX
    static class Program
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] arg)
        {
            logger.Info("Client started");
            bool updated = (arg.Length > 0 && arg[0] == "UpToDate") ? true : false;

            try
            {
                using (HelGame game = new HelGame(updated))
                {
                    game.Run();
                }
            }
            catch (Exception e)
            {
                logger.Error("Unhandled exception", e);
                WriteCrashLog(e);
                WinForms.ShowAlertMessage(string.Format("On noes! Looks like the client has crashed. Please consider sending the crash log to developers in Discord for further investigation. Crash log is stored: {0}\\Crash.log", Directory.GetCurrentDirectory()));
            }
        }

        private static void WriteCrashLog(Exception e)
        {
            try
            {
                File.Delete("./Crash.log");
                using (StreamWriter sw = File.AppendText("./Crash.log"))
                {
                    sw.WriteLine(string.Format("Windows version: {0}", Environment.OSVersion.VersionString));
                    WriteDisplayInformation(sw);
                    sw.WriteLine("Date: " + DateTime.Now);
                    sw.WriteLine("Error: " + e.Message);
                    sw.WriteLine(e.StackTrace);
                }
            } catch (Exception ex)
            {
                logger.Error("Failed to write crash log", ex);
            }
        }

        private static void WriteDisplayInformation(StreamWriter sw)
        {
            try
            {
                using (var searcher = new ManagementObjectSearcher("select * from Win32_VideoController"))
                {
                    foreach (ManagementObject obj in searcher.Get())
                    {
                        sw.WriteLine("-----------------------------------------------------------------");
                        sw.WriteLine("Name: " + obj["Name"]);
                        sw.WriteLine("AdapterRAM: " + obj["AdapterRAM"]);
                        sw.WriteLine("DriverVersion: " + obj["DriverVersion"]);
                        sw.WriteLine("VideoProcessor: " + obj["VideoProcessor"]);
                        sw.WriteLine("VideoArchitecture: " + obj["VideoArchitecture"]);
                        sw.WriteLine("VideoMemoryType: " + obj["VideoMemoryType"]);
                        sw.WriteLine("-----------------------------------------------------------------");
                    }
                }
            } catch (Exception e)
            {
                logger.Error("Failed to write display information into crash log", e);
            }
        }
    }
#endif
}

