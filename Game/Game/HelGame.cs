using Helbreath;
using Helbreath.Game;
using Helbreath.Game.Assets;
using Helbreath.Game.Assets.State;
using Helbreath.Game.Assets.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml;

namespace Game
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class HelGame : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private GameDisplay display;
        private Helbreath.DisplayMode originalDisplayMode;

        private bool updated;
        private string username;
        private string password;
        private string selectedCharacter;
        private List<Player> characters;
        private string loginAddress;
        private string gameAddress;
        private bool resolveDNS;
        private bool addressOverride;
        private int loginPort;
        private int gamePort;
        private bool showMainMenuLoginOnDisplayChange;

        //BACKGROUND
        private LinkedList<BackGroundImage> backgroundImages;
        private BackGroundImage[] backgroundcollection;
        private BackGroundImage background;
        private BackGroundImage backgroundblackbar;
        private BackGroundImage legs;
        private BackGroundImage weapon;
        private BackGroundImage tree;
        private BackGroundImage grass;
        private BackGroundImage grass2;
        private BackGroundImage grass3;
        private BackGroundImage smoke1;
        private BackGroundImage smoke2;
        private BackGroundImage smoke3;
        private BackGroundImage smoke4;
        private BackGroundImage smoke5;
        private BackGroundImage smoke6;
        private BackGroundImage smoke7;
        private BackGroundImage smoke8;
        private BackGroundImage logo;
        private BackGroundImage logoglow;
        private BackGroundImage warriorHead;
        private BackGroundImage warriorBody;

        private Texture2D backgroundTexture;
        private Texture2D backgroundblackbarTexture;
        private Texture2D legsTexture;
        private Texture2D weaponTexture;
        private Texture2D treeTexture;
        private Texture2D grassTexture;
        private Texture2D grass2Texture;
        private Texture2D grass3Texture;
        private Texture2D smoke1Texture;
        private Texture2D smoke2Texture;
        private Texture2D smoke3Texture;
        private Texture2D smoke4Texture;
        private Texture2D smoke5Texture;
        private Texture2D smoke6Texture;
        private Texture2D smoke7Texture;
        private Texture2D smoke8Texture;
        private Texture2D logoTexture;
        private Texture2D logoglowTexture;
        private Texture2D warriorHeadTexture;
        private Texture2D warriorBodyTexture;

        private Texture2D corner;
        private Texture2D textbox;
        private Texture2D dialogFader;
        private Texture2D dialogBorder;

        private bool fadeDirection;
        private bool fadeStage;
        private int fadeDelay = 10;
        private int backgroundDelay = 16;

        private int screenModeSwitchCounter = 0;

        //EFFECTS
        private Microsoft.Xna.Framework.Graphics.Effect berserkShader;
        private Microsoft.Xna.Framework.Graphics.Effect weaponShader;
        EffectParameter weaponParameters;

        //DEFAULT STATE
        private IDefaultState defaultState;

        private MainMenu mainMenuCache;
        //private RenderTarget2D mainRenderTarget;

        public HelGame(bool updated)
        {
            this.updated = true; //When auto updater is fixed, change this //TODO

            IsFixedTimeStep = false;
            TargetElapsedTime = TimeSpan.FromSeconds(1 / 60.0f); // Cap the menu frame to 60 FPS

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //LOAD GAME SETTINGS
            LoadGameSettings();

            // LOAD LOGIN CONFIG
            LoadLoginConfiguration();

            // LOAD INTERFACE
            display = new GameDisplay(Cache.GameSettings.Resolution, Window);
            display.Device = GraphicsDevice;
            display.DeviceManager = graphics;

            // LOAD MOUSE
            SpriteFile interfaceSprites = new SpriteFile("Sprites\\interface.spr");
            GameMouse mouse = new GameMouse(Texture2D.FromStream(GraphicsDevice, new MemoryStream(interfaceSprites[0].ImageData)), display);
            foreach (SpriteFrame frame in interfaceSprites[0].Frames)
                mouse.Frames.Add(new AnimationFrame(frame.Left, frame.Top, frame.Width, frame.Height, frame.PivotX, frame.PivotY));

            // LOAD KEYBOARD
            GameKeyboard keyboard = new GameKeyboard();
            keyboard.Dispatcher = new KeyboardDispatcher(this.Window);

            // LOAD FONTS
            //SpriteFile interface2Sprites = new SpriteFile("Sprites\\interface2.spr");
            //GameFont font = new GameFont(Texture2D.FromStream(GraphicsDevice, new MemoryStream(interface2Sprites[1].ImageData)));
            //font.InitCharacters(interface2Sprites[1].Frames);

            //gameInterface.GameFont = font;

            //FONTS
            Cache.Fonts = new Dictionary<FontType, SpriteFont>();
            Cache.Fonts.Add(FontType.GeneralSize10, Content.Load<SpriteFont>("Fonts\\General-Size-10"));
            Cache.Fonts.Add(FontType.GeneralSize10Bold, Content.Load<SpriteFont>("Fonts\\General-Size-10-Bold"));
            Cache.Fonts.Add(FontType.DamageSmallSize11, Content.Load<SpriteFont>("Fonts\\DamageSmall-Size-11"));
            Cache.Fonts.Add(FontType.DamageMediumSize13, Content.Load<SpriteFont>("Fonts\\DamageMedium-Size-13"));
            Cache.Fonts.Add(FontType.DamageLargeSize14Bold, Content.Load<SpriteFont>("Fonts\\DamageLarge-Size-14-Bold"));
            Cache.Fonts.Add(FontType.MagicMedieval14, Content.Load<SpriteFont>("Fonts\\MagicMedieval14"));
            Cache.Fonts.Add(FontType.MagicMedieval18, Content.Load<SpriteFont>("Fonts\\MagicMedieval18"));
            Cache.Fonts.Add(FontType.MagicMedieval24, Content.Load<SpriteFont>("Fonts\\MagicMedieval24"));
            Cache.Fonts.Add(FontType.MagicMedieval40, Content.Load<SpriteFont>("Fonts\\MagicMedieval40"));
            Cache.Fonts.Add(FontType.DisplayNameSize13Spacing1, Content.Load<SpriteFont>("Fonts\\DisplayName-Size-13-Spacing-1"));
            Cache.Fonts.Add(FontType.DialogSize8Bold, Content.Load<SpriteFont>("Fonts\\Dialog-Size-8-Bold"));
            Cache.Fonts.Add(FontType.DialogsSmallSize8, Content.Load<SpriteFont>("Fonts\\DialogSmall-Size-8"));
            Cache.Fonts.Add(FontType.DialogsSmallerSize7, Content.Load<SpriteFont>("Fonts\\DialogSmaller-Size-7"));
            Cache.Fonts.Add(FontType.Chat, Content.Load<SpriteFont>("Fonts\\Chat"));

            //GAMECOLORS
            Cache.Colors = ConfigurationHelper.LoadGameColors();


            display.Mouse = mouse;
            display.Keyboard = keyboard;

            Cache.BackgroundTexture = new Texture2D(GraphicsDevice, 1, 1);
            Cache.BackgroundTexture.SetData(new[] { Color.White });

            // SET UP GRAPHICS DEVICE
            graphics.PreferredBackBufferWidth = display.ResolutionWidth;
            graphics.PreferredBackBufferHeight = display.ResolutionHeight;
            graphics.SynchronizeWithVerticalRetrace = false;
            graphics.ApplyChanges();

            //LOAD EFFECTS
            berserkShader = Content.Load<Effect>("Berserk");
            weaponShader = Content.Load<Effect>("Weapon");
            weaponParameters = weaponShader.Parameters["weaponColorTint"];

            // LOAD SPRITE CACHE DICTIONARIES
            Cache.Tiles = new Dictionary<int, Tile>();
            Cache.SpriteQueue = new Queue<SpriteLoad>();
            Cache.HumanAnimations = new Dictionary<int, Animation>();
            Cache.MonsterAnimations = new Dictionary<int, Animation>();
            Cache.Equipment = new Dictionary<int, Animation>();
            Cache.Effects = new Dictionary<int, Animation>();
            Cache.Interface = new Dictionary<int, Animation>();
            Cache.MiniMaps = new Dictionary<string, Texture2D>();
            Cache.ZoomMaps = new Dictionary<string, Texture2D>();
            Cache.Music = new Dictionary<string, SoundEffect>();
            Cache.SoundEffects = new Dictionary<string, SoundEffect>();
            Cache.PixelShaders = new Dictionary<int, Effect>();
            Cache.PixelShaders.Add((int)PixelShader.Berserk, berserkShader);
            Cache.PixelShaders.Add((int)PixelShader.Weapon, weaponShader);
            Cache.ShaderParameters = new Dictionary<Effect, EffectParameter>();
            Cache.ShaderParameters.Add(weaponShader, weaponParameters);
            Cache.TransparencyFaders = new TransparencyFaders();

            originalDisplayMode = Cache.GameSettings.DisplayMode;
            Cache.GameSettings.DisplayMode = Helbreath.DisplayMode.Windowed;

            // SET UP FIRST SCREEN
            Loading loading = new Loading(updated);
            loading.Content = this.Content;
            loading.Display = display;
            loading.MainRenderTarget = LoadRenderTarget2D(loading.MainRenderTarget);

            //LOAD BACKGROUND
            LoadBackGroundTextures();
            LoadBackGroundImages(Cache.GameSettings.Resolution);
            loading.BackgroundImages = backgroundImages;
            loading.DialogBorder = DialogBorder;
            loading.DialogFader = DialogFader;
            loading.Textbox = Textbox;
            loading.Corner = Corner;
            loading.Init(Window);

            defaultState = (IDefaultState)loading;
        }

        private void CapFPS()
        {
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromSeconds(1 / 60.0f);
        }

        private void LoadLoginConfiguration()
        {
            try
            {
                string installPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

                if (!File.Exists(installPath + @"\login.xml")) return;

                XmlDocument document = new XmlDocument();
                document.Load(installPath + @"\login.xml");

                // server
                resolveDNS = false;
                XmlNode node = document.DocumentElement.SelectSingleNode("//Server");
                if (node.Attributes["ResolveDNS"] != null)
                    resolveDNS = Boolean.Parse(node.Attributes["ResolveDNS"].InnerText);

                if (node.Attributes["Override"] != null)
                    addressOverride = Boolean.Parse(node.Attributes["Override"].InnerText);

                if (resolveDNS)
                {

                    IPHostEntry hostEntry = new IPHostEntry();
                    hostEntry.AddressList = Dns.GetHostAddresses(node.InnerText);

                    if (hostEntry.AddressList.Length > 0)
                    {
                        loginAddress = gameAddress = hostEntry.AddressList[0].ToString();
                    }
                }
                else
                {
                    loginAddress = gameAddress = node.InnerText;
                }

                // port
                node = document.DocumentElement.SelectSingleNode("//Port");
                if (Int32.TryParse(node.InnerText, out loginPort)) gamePort = loginPort;
            }
            catch
            {
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        // This ensures that switching between windowed and windowed full screen screen bounds get adjusted properly
        private void ForceDoubleScreenRefresh(IDefaultState defaultState)
        {
            screenModeSwitchCounter++;
            if (screenModeSwitchCounter > 1)
            {
                screenModeSwitchCounter = 0;
                defaultState.DisplayModeOrResolutionChange = false;
                showMainMenuLoginOnDisplayChange = false;
            }
            else
            {
                defaultState.DisplayModeOrResolutionChange = true;
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (defaultState.State)
            {
                case DefaultState.Loading:
                    CapFPS(); // Cap the FPS while in menu
                    // CHECK IF LOADING IS COMPLETE, THEN GO TO MAIN MENU
                    if (defaultState.IsComplete)
                    {

                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        if (Cache.GameSettings.DisplayMode != originalDisplayMode)
                        {
                            showMainMenuLoginOnDisplayChange = true;
                            menu.DisplayModeOrResolutionChange = true;
                            Cache.GameSettings.DisplayMode = originalDisplayMode;
                        }
                        menu.Exit += OnExit;
                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);

                        Loading load = (Loading)defaultState;
                        defaultState = (IDefaultState)menu;
                        menu.Init(Window);


                        //Background
                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.BackgroundImages = BackgroundImages;

                        mainMenuCache = menu; // store for log out
                    }
                    break;
                case DefaultState.MainMenu:
                    CapFPS(); // Cap the FPS while in menu
                    if (defaultState.DisplayModeOrResolutionChange)
                    {
                        MainMenu oldMenuState = (MainMenu)defaultState;
                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        menu.Exit += OnExit;
                        display.ApplyDisplayModeAndResolution();

                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);
                        LoadBackGroundImages(Cache.GameSettings.Resolution);
                        menu.BackgroundImages = backgroundImages;
                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.Init(Window);

                        foreach (KeyValuePair<MenuDialogBoxType, IMenuDialogBox> box in menu.DialogBoxes)
                        {
                            if (box.Key != MenuDialogBoxType.Settings)
                            {
                                box.Value.Hide();
                            }
                        }
                        if (showMainMenuLoginOnDisplayChange)
                        {
                            menu.DialogBoxes[MenuDialogBoxType.Login].Show();
                        }
                        else
                        {
                            menu.DialogBoxes[MenuDialogBoxType.Settings].Show();
                        }

                        defaultState = (IDefaultState)menu;
                        ForceDoubleScreenRefresh(defaultState);
                    }

                    // CHECK IF LOGIN IS COMPLETE, THEN GO TO GAME
                    if (defaultState.IsComplete)
                    {
                        username = ((MainMenu)defaultState).Username;
                        password = ((MainMenu)defaultState).Password;
                        characters = ((MainMenu)defaultState).Characters;

                        CharacterSelect select = new CharacterSelect(loginAddress, loginPort, username, characters);
                        select.Display = display;
                        select.MainRenderTarget = LoadRenderTarget2D(select.MainRenderTarget);
                        MainMenu menu = (MainMenu)defaultState;
                        defaultState = (IDefaultState)select;
                        select.Init(Window);

                        select.DialogBorder = DialogBorder;
                        select.DialogFader = DialogFader;
                        select.Textbox = Textbox;
                        select.Corner = Corner;
                        select.BackgroundImages = BackgroundImages;
                    }
                    break;
                case DefaultState.CharacterSelect:
                    CapFPS(); // Cap the FPS while in menu
                    if (defaultState.Back)
                    {
                        MainMenu menu = new MainMenu(loginAddress, loginPort);
                        menu.Display = display;
                        menu.MainRenderTarget = LoadRenderTarget2D(menu.MainRenderTarget);
                        CharacterSelect select = (CharacterSelect)defaultState;
                        defaultState = (IDefaultState)menu;
                        menu.Init(Window);

                        menu.DialogBorder = DialogBorder;
                        menu.DialogFader = DialogFader;
                        menu.Textbox = Textbox;
                        menu.Corner = Corner;
                        menu.BackgroundImages = BackgroundImages;
                    }
                    else if (defaultState.IsComplete)
                    {
                        selectedCharacter = ((CharacterSelect)defaultState).SelectedCharacter;
                        gameAddress = (addressOverride ? loginAddress : ((CharacterSelect)defaultState).GameServerAddress);
                        gamePort = ((CharacterSelect)defaultState).GameServerPort;

                        MainGame game = new MainGame(gameAddress, gamePort);
                        game.DisplayModeOrResolutionChange = false;
                        game.Display = display;
                        game.MainRenderTarget = LoadRenderTarget2D(game.MainRenderTarget, RenderTargetUsage.PreserveContents);
                        game.RenderTargetObjects = LoadRenderTarget2D(game.RenderTargetObjects, RenderTargetUsage.DiscardContents);

                        game.Player = new Player(username, password, selectedCharacter);
                        defaultState = (IDefaultState)game;
                        game.Disconnect += new EventHandler(Disconnect);
                        game.Init(Window);
                    }
                    break;
                case DefaultState.Playing:
                    // Un-cap the FPS while playing
                    IsFixedTimeStep = false;
                    {
                        if (defaultState.DisplayModeOrResolutionChange)
                        {
                            MainGame oldGameState = (MainGame)defaultState;
                            switch (Cache.GameSettings.Resolution)
                            {
                                case Resolution.Classic:
                                    {
                                        oldGameState.Display.ApplyDisplayModeAndResolution();
                                        break;
                                    }
                                case Resolution.Standard:
                                    {
                                        oldGameState.Display.ApplyDisplayModeAndResolution();
                                        break;
                                    }
                                case Resolution.Large:
                                    {
                                        oldGameState.Display.ApplyDisplayModeAndResolution();
                                        break;
                                    }
                            }

                            LoadBackGroundImages(Cache.GameSettings.Resolution);
                            oldGameState.MainRenderTarget = LoadRenderTarget2D(oldGameState.MainRenderTarget, RenderTargetUsage.PreserveContents);
                            oldGameState.RenderTargetObjects = LoadRenderTarget2D(oldGameState.RenderTargetObjects, RenderTargetUsage.DiscardContents);
                            oldGameState.BackgroundImages = BackgroundImages;
                            oldGameState.UpdateResolution();
                            defaultState = (IDefaultState)oldGameState;
                            ForceDoubleScreenRefresh(defaultState);
                        }
                        break;
                    }
            }
            defaultState.Update(gameTime, this);

            base.Update(gameTime);
        }

        private void OnExit(object sender, EventArgs e)
        {
            Exit();
        }

        private void Disconnect(object sender, EventArgs e)
        {
            defaultState = (IDefaultState)mainMenuCache;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //Set the current render target to our mainRenderTarget
            graphics.GraphicsDevice.SetRenderTarget(defaultState.MainRenderTarget);
            //Clear the render target (make it all black)
            graphics.GraphicsDevice.Clear(Color.Black);
            //Start our spritebatch
            //Note: Add parameters to this as is necessary for your game
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);
            //...
            //Do all your drawing here
            base.Draw(gameTime);
            if (!defaultState.DisplayModeOrResolutionChange)
            {
                defaultState.Draw(spriteBatch, gameTime);
            }

            //End the spritebatch
            spriteBatch.End();
            //Set the render target to the back buffer again by passing null
            graphics.GraphicsDevice.SetRenderTarget(null);
            //Clear the back buffer
            graphics.GraphicsDevice.Clear(Color.Black);
            //Start the spritebatch we're going to use to draw what we've
            //   rendered (using a cool effect) to the screen
            //Note: the SpriteSortMode and BlendState may be different in
            //   your case.
            if (Cache.GameSettings.DisplayMode == Helbreath.DisplayMode.WindowedFullscreen)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, display.WindowedFullscreenScale);
            }
            else
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null);
            }
            //DrawItemPopup the render target to the screen with no tint
            spriteBatch.Draw(defaultState.MainRenderTarget, Vector2.Zero, Color.White);
            //if (defaultState.State == defaultState.Playing) { spriteBatch.DrawItemPopup(defaultState.MainRenderTarget, display.Device.Viewport.Bounds, display.Zoom, Color.White); }
            //else { spriteBatch.DrawItemPopup(defaultState.MainRenderTarget, Vector2.Zero, Color.White); }
            //End the spritebatch
            spriteBatch.End();

            //OLD CODE HERE
            //base.DrawItemPopup(gameTime);

            //GraphicsDevice.Clear(Color.Black);

            //spriteBatch.Begin();
            //if (!defaultState.ResolutionChange)
            //{
            //    defaultState.DrawItemPopup(spriteBatch, gameTime);
            //}
            //spriteBatch.End();
        }

        private void LoadBackGroundTextures()
        {
            BackgroundImages = new LinkedList<BackGroundImage>();
            SpriteFile backgroundSprites = new SpriteFile("Sprites\\loadingbg.spr");
            backgroundTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[0].ImageData));
            backgroundblackbarTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[1].ImageData));
            legsTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[2].ImageData));
            weaponTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[3].ImageData));
            warriorHeadTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[4].ImageData));
            warriorBodyTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[5].ImageData));
            treeTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[6].ImageData));
            grassTexture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            grass2Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            grass3Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[7].ImageData));
            smoke1Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke2Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke3Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke4Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke5Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke6Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[8].ImageData)).PreMultiply();
            smoke7Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();
            smoke8Texture = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[9].ImageData)).PreMultiply();

            dialogBorder = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[10].ImageData));
            corner = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[11].ImageData)).PreMultiply();
            dialogFader = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[12].ImageData)).PreMultiply();
            textbox = Texture2D.FromStream(GraphicsDevice, new MemoryStream(backgroundSprites[13].ImageData)).PreMultiply();
        }
        private void LoadBackGroundImages(Resolution resolution)
        {
            int height = display.ResolutionHeight;
            int width = display.ResolutionWidth;

            switch (resolution)
            {
                //4:3 aspect ratio (1440 x 1080)
                case Resolution.Classic: // 800x600
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.555555f, new Microsoft.Xna.Framework.Rectangle(240, 0, 1440, 1080));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 200 - 50, height - 200, 0.7f);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 200 + 40, height - 294 + 55, new Vector2(175, 290), 0.7f);
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 200 + 33, height - 264 - 48, 0.7f);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 200 + 10, height - 275, 0.7f);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -(1067 / 2), height - 765 - 50);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        break;
                    }
                case Resolution.Standard: // 1280x720
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.711111f, new Microsoft.Xna.Framework.Rectangle(65, 0, 1920, 1080));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 367 - 50 + 100, height - 255);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 367 + 60 + 100, height - 364 + 55, new Vector2(175, 290));
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 367 + 60 + 100, height - 364 - 53);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 367 + 25 + 100, height - 364);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -550, height - 810);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        break;
                    }
                case Resolution.Large: //1280x800
                    {
                        background = new BackGroundImage(BackgroundType.Stationary, 0, 0, 0.711111f, new Microsoft.Xna.Framework.Rectangle(65, 0, 1920, 1200));
                        backgroundblackbar = new BackGroundImage(BackgroundType.Stationary, height - 10);
                        legs = new BackGroundImage(BackgroundType.Legs, width - 367 - 50 + 100, height - 255);
                        weapon = new BackGroundImage(BackgroundType.Weapon, width - 367 + 60 + 100, height - 364 + 55, new Vector2(175, 290));
                        warriorHead = new BackGroundImage(BackgroundType.Head, width - 367 + 60 + 100, height - 364 - 53);
                        warriorBody = new BackGroundImage(BackgroundType.Body, width - 367 + 25 + 100, height - 364);
                        grass = new BackGroundImage(BackgroundType.Grass, 0, height - 84 - 5);
                        grass2 = new BackGroundImage(BackgroundType.Grass, 480, height - 84, 41);
                        grass3 = new BackGroundImage(BackgroundType.Grass, 800, height - 84 - 10, 41);
                        tree = new BackGroundImage(BackgroundType.Stationary, -550, height - 810);
                        smoke1 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke2 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke3 = new BackGroundImage(BackgroundType.Smoke, height - 1080 + 230);
                        smoke4 = new BackGroundImage(BackgroundType.Smoke, 50, height - 1080 + 230);
                        smoke5 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke6 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        smoke7 = new BackGroundImage(BackgroundType.Smokebehind, height - 1080 - 40);
                        smoke8 = new BackGroundImage(BackgroundType.Smokebehind, 50, height - 1080 - 40);
                        break;
                    }
            }

            background.Texture = backgroundTexture;
            backgroundblackbar.Texture = backgroundblackbarTexture;
            backgroundblackbar.UpdateSource();
            legs.Texture = legsTexture;
            legs.UpdateSource();
            weapon.Texture = weaponTexture;
            weapon.UpdateSource();
            warriorHead.Texture = warriorHeadTexture;
            warriorHead.UpdateSource();
            warriorBody.Texture = warriorBodyTexture;
            warriorBody.UpdateSource();
            tree.Texture = treeTexture;
            tree.UpdateSource();
            grass.Texture = grassTexture;
            grass.UpdateSource();
            grass2.Texture = grass2Texture;
            grass2.UpdateSource();
            grass3.Texture = grass3Texture;
            grass3.UpdateSource();
            smoke1.Texture = smoke1Texture;
            smoke1.UpdateSource();
            smoke2.Texture = smoke2Texture;
            smoke2.UpdateSource();
            smoke3.Texture = smoke3Texture;
            smoke3.UpdateSource();
            smoke4.Texture = smoke4Texture;
            smoke4.UpdateSource();
            smoke5.Texture = smoke5Texture;
            smoke5.UpdateSource();
            smoke6.Texture = smoke6Texture;
            smoke6.UpdateSource();
            smoke7.Texture = smoke7Texture;
            smoke7.UpdateSource();
            smoke8.Texture = smoke8Texture;
            smoke8.UpdateSource();

            backgroundImages.Clear();
            backgroundImages.AddFirst(background);
            backgroundImages.AddLast(smoke2);
            backgroundImages.AddLast(grass3);
            backgroundImages.AddLast(smoke1);
            backgroundImages.AddLast(smoke4);
            backgroundImages.AddLast(smoke6);
            backgroundImages.AddLast(smoke8);

            backgroundImages.AddLast(weapon);
            backgroundImages.AddLast(warriorBody);
            backgroundImages.AddLast(legs);
            backgroundImages.AddLast(warriorHead);

            backgroundImages.AddLast(grass);
            backgroundImages.AddLast(smoke3);
            backgroundImages.AddLast(smoke5);
            backgroundImages.AddLast(smoke7);
            backgroundImages.AddLast(grass2);
            backgroundImages.AddLast(backgroundblackbar);
        }

        private bool LoadGameSettings()
        {
            try
            {
                string configPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "") + @"\Configs\gamesettings.xml";

                Cache.GameSettings = ConfigurationHelper.LoadGameSettings(configPath);

                return true;
            }
            catch (Exception ex)
            {
                Cache.GameSettings = GameSettings.Default();
                return false;
            }
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            switch (defaultState.State)
            {
                case DefaultState.Loading:
                    break;
                case DefaultState.MainMenu:
                    break;
                case DefaultState.CharacterSelect:
                    break;
                case DefaultState.Playing:
                    return;
            }

            base.OnExiting(sender, args);
        }

        public RenderTarget2D LoadRenderTarget2D(RenderTarget2D renderTarget)
        {
            //Get the current presentation parameters
            PresentationParameters pp = display.Device.PresentationParameters;
            //Create our new render target
            renderTarget = new RenderTarget2D(display.Device,
                     pp.BackBufferWidth, //Same width as backbuffer
                     pp.BackBufferHeight, //Same height
                     false, //No mip-mapping
                     pp.BackBufferFormat, //Same colour format
                     pp.DepthStencilFormat, //Same depth stencil
                     pp.MultiSampleCount,
                     RenderTargetUsage.PreserveContents);
            return renderTarget;
        }

        public RenderTarget2D LoadRenderTarget2D(RenderTarget2D renderTarget, RenderTargetUsage renderUsage)
        {
            //Get the current presentation parameters
            PresentationParameters pp = display.Device.PresentationParameters;
            //Create our new render target
            renderTarget = new RenderTarget2D(display.Device,
                     pp.BackBufferWidth, //Same width as backbuffer
                     pp.BackBufferHeight, //Same height
                     false, //No mip-mapping
                     pp.BackBufferFormat, //Same colour format
                     pp.DepthStencilFormat, //Same depth stencil
                     pp.MultiSampleCount,
                     renderUsage);
            return renderTarget;
        }


        public Texture2D Corner { get { return corner; } set { corner = value; } }
        public Texture2D Textbox { get { return textbox; } set { textbox = value; } }
        public Texture2D DialogFader { get { return dialogFader; } set { dialogFader = value; } }
        public Texture2D DialogBorder { get { return dialogBorder; } set { dialogBorder = value; } }
        public LinkedList<BackGroundImage> BackgroundImages { get { return backgroundImages; } set { backgroundImages = value; } }
    }
}


//public void SetFrameRate(
//   GraphicsDeviceManager manager, int frames)
//{
//   double dt = (double)1000 / (double)frames;
//   manager.SynchronizeWithVerticalRetrace = false;
//   game.TargetElapsedTime = TimeSpan.FromMilliseconds(dt);
//   manager.ApplyChanges();
//}

//graphicsDeviceManager.SynchronizeWithVerticalRetrace = false;
//game.IsFixedTimeStep = false;
//graphicsDeviceManager.ApplyChanges();